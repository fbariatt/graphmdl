package graphMDL.simpleGraphs;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import graphMDL.StandardTable;
import graphMDL.UniversalInteger;
import graphMDL.Utils;
import org.apache.log4j.Logger;
import utils.GeneralUtils;
import utils.MapUtils;

import java.util.*;

/**
 * A standard table that contains in fact two independent ST: a vertex ST and an edge ST, to be used on simple graphs only. This class assumes that
 * there is at maximum one edge between two given vertices.
 * This is the encoding that has been used for the first version of GraphMDL (IDA2020 paper).
 */
public abstract class SimpleIndependentVertexAndEdgeST implements StandardTable
{
	/**
	 * The constant part of the description length for describing the structure of a vertex singleton pattern.
	 * The description length of the singleton's label is missing for a complete description length.
	 * Equivalent to: Vertex count + #labels on first vertex + #outEdges + no out edges description.
	 */
	private static final double SINGLETON_VERTEX_CONSTANT_DL = UniversalInteger.eliasDeltaWith0(1) + UniversalInteger.eliasDeltaWith0(1) + Utils.log2(1);
	/**
	 * The constant part of the description length for describing the structure of an edge singleton pattern.
	 * The description length of the singleton's label is missing for a complete description length.
	 * Equivalent to: Vertex count + #labels on v1 + #outEdges of v1 + target + #labels on v2 + #outEdges of v2 + no v2 out edges description.
	 */
	private static final double SINGLETON_EDGE_CONSTANT_DL = UniversalInteger.eliasDeltaWith0(2) + UniversalInteger.eliasDeltaWith0(0) + Utils.log2(2) + Utils.log2(2) + UniversalInteger.eliasDeltaWith0(0) + Utils.log2(2);
	private final Map<String, Double> vertexST;
	private final Map<String, Double> edgeST;

	public SimpleIndependentVertexAndEdgeST(Graph g)
	{
		this.vertexST = Collections.unmodifiableMap(computeVertexST(g));
		this.edgeST = Collections.unmodifiableMap(computeEdgeST(g));
		checkLabelSetsAreDisjoint();
	}

	public SimpleIndependentVertexAndEdgeST(Map<String, Double> vertexCodeLengths, Map<String, Double> edgeCodeLengths)
	{
		this.vertexST = Collections.unmodifiableMap(vertexCodeLengths);
		this.edgeST = Collections.unmodifiableMap(edgeCodeLengths);
		checkLabelSetsAreDisjoint();
	}

	/**
	 * Check that the two standard tables do not have any label in common.
	 * It is not strictly needed, but it could be an easy source of errors somewhere else in the code, and it is not difficult
	 * for the user to avoid this problem by just changing some labels.
	 */
	private void checkLabelSetsAreDisjoint()
	{
		// Checking that the label sets of the two standard tables are disjoint
		if (!GeneralUtils.isIntersectionEmpty(vertexST.keySet(), edgeST.keySet()))
		{
			Set<String> commonLabels = GeneralUtils.intersection(vertexST.keySet(), edgeST.keySet());
			Logger.getLogger(this.getClass()).error(String.format("Some labels appear in both vertex standard table and edge standard table: %s", commonLabels));
			throw new IllegalArgumentException("Some labels appear in both vertex standard table and edge standard table. Maybe try to capitalize one of the versions?");
		}
	}

	protected Map<String, Double> computeVertexST(Graph g)
	{
		Map<String, Double> st = new HashMap<>();

		// Computing label usages
		g.getVertices()
				.flatMap(vertex -> vertex.getLabels().keySet().stream())
				.forEach(label -> st.put(label, st.getOrDefault(label, 0.0) + 1));
		final int usageSum = st.values().stream().mapToInt(Double::intValue).sum();

		// Computing code lengths
		st.replaceAll((_label, count) -> -Utils.log2(count / usageSum));

		return st;
	}

	protected Map<String, Double> computeEdgeST(Graph g)
	{
		Map<String, Double> st = new HashMap<>();

		// Computing usages
		g.getEdges()
				.map(Edge::getLabel)
				.forEach(label -> st.put(label, st.getOrDefault(label, 0.0) + 1));
		final int usageSum = st.values().stream().mapToInt(Double::intValue).sum();

		// Computing code lengths
		st.replaceAll((_label, count) -> -Utils.log2(count / usageSum));

		return st;
	}

	@Override
	public double encode(Graph g) throws StandardTableEncodingException
	{
		final double logVertexCount = Utils.log2(g.getVertexCount());
		double edgeTargetsAndLabelsDL = 0.0;
		double allDLExceptEdgeTargetsAndLabelsDL = 0.0;

		// Number of vertices in the graph
		allDLExceptEdgeTargetsAndLabelsDL += UniversalInteger.eliasDeltaWith0(g.getVertexCount());

		Iterator<Vertex> vertices = g.getVertices().iterator();
		while (vertices.hasNext())
		{
			Vertex v = vertices.next();
			// Number of labels of this vertex
			allDLExceptEdgeTargetsAndLabelsDL += UniversalInteger.eliasDeltaWith0(v.getLabels().size());
			// Codes of the labels of this vertex
			allDLExceptEdgeTargetsAndLabelsDL += v.getLabels().keySet().stream()
					.mapToDouble(label -> MapUtils.mapGetOrThrowRuntime(vertexST, label, () -> new StandardTableEncodingException("Vertex Standard Table does not contain label " + label)))
					.sum();
			// Number of out-edges of this vertex. In a simple graph it can be between 0 and |V|-1.
			// We use an encoding that gives the same DL independently of the actual value.
			allDLExceptEdgeTargetsAndLabelsDL += logVertexCount;
			// For each edge we need to encode its target (uniform code based on |V|) and its label
			edgeTargetsAndLabelsDL += g.getOutEdges(v)
					.mapToDouble(edge -> logVertexCount + MapUtils.mapGetOrThrowRuntime(edgeST, edge.getLabel(), () -> new StandardTableEncodingException("Edge Standard Table does not contain label " + edge.getLabel())))
					.sum();
		}

		// If the graph is undirected, we just counted all edges (labels and targets) twice than what is actually needed
		if (!isDirected())
			edgeTargetsAndLabelsDL /= 2;

		return allDLExceptEdgeTargetsAndLabelsDL + edgeTargetsAndLabelsDL;
	}

	/**
	 * @return Whether this ST encodes directed or undirected graphs
	 */
	protected abstract boolean isDirected();

	@Override
	public double encodeSingleton(int arity, String singletonLabel) throws StandardTableEncodingException
	{
		switch (arity)
		{
			case 1:
				return encodeSingletonVertex(singletonLabel);
			case 2:
				return encodeSingletonEdge(singletonLabel);
			default:
				throw new StandardTableEncodingException(String.format("%s can only encode singleton vertices and edges, arity should be 1 or 2, got %d", getClass(), arity));
		}
	}

	@Override
	public double encodeSingletonVertex(String vertexLabel) throws StandardTableEncodingException
	{
		if (!vertexST.containsKey(vertexLabel))
			throw new StandardTableEncodingException("Vertex Standard Table does not contain label " + vertexLabel);

		return SINGLETON_VERTEX_CONSTANT_DL + vertexST.get(vertexLabel);
	}

	@Override
	public double encodeSingletonEdge(String edgeLabel) throws StandardTableEncodingException
	{
		if (!edgeST.containsKey(edgeLabel))
			throw new StandardTableEncodingException("Edge Standard Table does not contain label " + edgeLabel);

		return SINGLETON_EDGE_CONSTANT_DL + edgeST.get(edgeLabel);
	}

	@Override
	public Map<Integer, Map<String, Double>> getCodeLengths()
	{
		Map<Integer, Map<String, Double>> result = new HashMap<>();
		result.put(1, getVertexLabelsCodeLengths());
		result.put(2, getEdgeLabelsCodeLengths());
		return result;
	}

	@Override
	public int getLabelCount()
	{
		return this.vertexST.size() + this.edgeST.size();
	}

	@Override
	public Map<String, Double> getCodeLengths(int arity)
	{
		switch (arity)
		{
			case 1:
				return getVertexLabelsCodeLengths();
			case 2:
				return getEdgeLabelsCodeLengths();
			default:
				return Collections.emptyMap();
		}
	}

	@Override
	public Map<String, Double> getVertexLabelsCodeLengths()
	{
		return this.vertexST;
	}

	@Override
	public Map<String, Double> getEdgeLabelsCodeLengths()
	{
		return this.edgeST;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SimpleIndependentVertexAndEdgeST that = (SimpleIndependentVertexAndEdgeST) o;
		return vertexST.equals(that.vertexST) &&
				edgeST.equals(that.edgeST);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(vertexST, edgeST);
	}
}
