package graphMDL.simpleGraphs.undirected;

import graph.Graph;
import graphMDL.simpleGraphs.SimpleIndependentVertexAndEdgeST;

import java.util.Map;

/**
 * A {@link SimpleIndependentVertexAndEdgeST} for simple undirected graphs.
 */
public class SimpleUndirectedIndependentVertexAndEdgeST extends SimpleIndependentVertexAndEdgeST
{
	public SimpleUndirectedIndependentVertexAndEdgeST(Graph g)
	{
		super(g);
	}

	public SimpleUndirectedIndependentVertexAndEdgeST(Map<String, Double> vertexCodeLengths, Map<String, Double> edgeCodeLengths)
	{
		super(vertexCodeLengths, edgeCodeLengths);
	}

	@Override
	protected Map<String, Double> computeVertexST(Graph g)
	{
		// Nothing specific to undirected graphs w.r.t. the superclass version
		return super.computeVertexST(g);
	}

	@Override
	protected Map<String, Double> computeEdgeST(Graph g)
	{
		// The superclass version will count all edges two times, but it does not change their relative frequency
		return super.computeEdgeST(g);
	}

	@Override
	public double encode(Graph g) throws StandardTableEncodingException
	{
		// The superclass version takes into the account the possibility of dealing with an undirected graph
		return super.encode(g);
	}

	@Override
	protected boolean isDirected()
	{
		return false;
	}

	@Override
	public double encodeSingleton(int arity, String singletonLabel) throws StandardTableEncodingException
	{
		// Nothing specific to undirected graphs w.r.t. the superclass version
		return super.encodeSingleton(arity, singletonLabel);
	}

	@Override
	public double encodeSingletonVertex(String vertexLabel) throws StandardTableEncodingException
	{
		// Nothing specific to undirected graphs w.r.t. the superclass version
		return super.encodeSingletonVertex(vertexLabel);
	}

	@Override
	public double encodeSingletonEdge(String edgeLabel) throws StandardTableEncodingException
	{
		// The encoding DL is the same for undirected graphs as for directed, so the superclass version works as well
		return super.encodeSingletonEdge(edgeLabel);
	}
}
