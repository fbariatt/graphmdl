package graphMDL.simpleGraphs.undirected;

import graph.Graph;
import graphMDL.CodeTableRow;
import graphMDL.Krimp;
import graphMDL.StandardTable;

import java.util.Comparator;
import java.util.stream.Stream;

/**
 * Krimp algorithm for directed graphs interpreted as doubled up version of simple undirected graphs.
 * Replace some methods of the directed Krimp class to adapt them to simple undirected graph computation.
 */
public class SimpleUndirectedKrimp extends Krimp
{
	/**
	 * See {@link Krimp#Krimp}.
	 */
	public SimpleUndirectedKrimp(Graph data, Stream<Graph> candidates, Comparator<CodeTableRow> candidateComparator, Comparator<CodeTableRow> codeTableRowComparator, boolean useAutomorphisms)
	{
		super(data, candidates, candidateComparator, codeTableRowComparator, useAutomorphisms);
	}

	@Override
	protected StandardTable createStandardTableFromData()
	{
		return new SimpleUndirectedIndependentVertexAndEdgeST(this.data);
	}

	@Override
	protected boolean isValidCandidate(Graph pattern)
	{
		// A valid candidate is not a singleton and is not an empty graph (which shouldn't happen, but it doesn't cost much to verify)
		return !SimpleUndirectedUtils.isVertexSingleton(pattern) && !SimpleUndirectedUtils.isEdgeSingleton(pattern) && pattern.getVertexCount() != 0;
	}

	@Override
	protected int labelCount(Graph g)
	{
		return SimpleUndirectedUtils.graphLabelCount(g);
	}

	@Override
	protected void initCodeTable()
	{
		this.codeTable = new SimpleUndirectedCodeTable((SimpleUndirectedIndependentVertexAndEdgeST) this.st, codeTableComparator);
	}
}
