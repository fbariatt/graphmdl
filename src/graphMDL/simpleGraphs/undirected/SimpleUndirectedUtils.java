package graphMDL.simpleGraphs.undirected;

import graph.Graph;
import graph.GraphFactory;
import graph.Vertex;
import graphMDL.PatternInfo;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public final class SimpleUndirectedUtils
{
	/**
	 * @return The amount of labels that a graph has. The graph is interpreted as a doubled representation of a simple undirected graph
	 */
	public static int graphLabelCount(Graph g)
	{
		int vertexLabelCount = 0;
		int edgeLabelCount = 0;
		Iterator<Vertex> vertices = g.getVertices().iterator();
		while (vertices.hasNext())
		{
			Vertex vertex = vertices.next();
			vertexLabelCount += vertex.getLabels().size();
			edgeLabelCount += g.getOutEdgeCount(vertex); // Each edge has exactly one label
		}
		// Edges have been counted twice, so we half the value
		return vertexLabelCount + (edgeLabelCount / 2);
	}


	/**
	 * Create all possible simple undirected graphs composed of two vertices, each with one label, connected by an undirected edge with one label (edge is represented by a doubled up directed edge).
	 *
	 * @param vertexLabels All vertex labels that can be combined.
	 * @param edgeLabels   All edge labels that can be combined.
	 */
	public static Iterable<Graph> createTrivialGraphs(Iterable<String> vertexLabels, Iterable<String> edgeLabels)
	{
		List<Graph> result = new LinkedList<>();
		vertexLabels.forEach(vertexLabelA ->
				vertexLabels.forEach(vertexLabelB -> {
					if (vertexLabelA.compareTo(vertexLabelB) > 0) // Avoid creating duplicates
						return;
					edgeLabels.forEach(edgeLabel -> {
						Graph pattern = GraphFactory.createSimpleGraph();
						pattern.addVertex(vertexLabelA);
						pattern.addVertex(vertexLabelB);
						pattern.addEdge(0, 1, edgeLabel);
						pattern.addEdge(1, 0, edgeLabel);
						result.add(pattern);
					});
				}));
		return result;
	}

	/**
	 * Create a vertex-singleton graph for the given label.
	 *
	 * @see graphMDL.Utils#createVertexSingleton
	 */
	public static Graph createVertexSingleton(String label)
	{
		return graphMDL.Utils.createVertexSingleton(label);
	}

	/**
	 * Create a {@link PatternInfo} object describing a vertex-singleton graph of the given label.
	 *
	 * @see graphMDL.Utils#createVertexSingletonPatternInfo
	 */
	public static PatternInfo createVertexSingletonPatternInfo(String label)
	{
		return PatternInfo.createComputingSaturatedAutomorphisms(createVertexSingleton(label));
	}

	/**
	 * Create an edge-singleton graph for the given label
	 *
	 * @see graphMDL.Utils#createEdgeSingleton
	 */
	public static Graph createEdgeSingleton(String label)
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex();
		g.addVertex();
		g.addEdge(0, 1, label);
		g.addEdge(1, 0, label);
		return g;
	}

	/**
	 * Create a {@link PatternInfo} object describing an edge-singleton graph of the given label
	 * @see graphMDL.Utils#createEdgeSingletonPatternInfo
	 */
	public static PatternInfo createEdgeSingletonPatternInfo(String label)
	{
		return PatternInfo.createComputingSaturatedAutomorphisms(createEdgeSingleton(label));
	}

	/**
	 * @return Whether the given graph is a vertex-singleton graph
	 * See {@link graphMDL.Utils#isVertexSingleton}
	 */
	public static boolean isVertexSingleton(Graph graph)
	{
		return graphMDL.Utils.isVertexSingleton(graph);
	}

	/**
	 * @return Whether the given graph is an edge-singleton graph
	 * See {@link graphMDL.Utils#isEdgeSingleton}
	 */
	public static boolean isEdgeSingleton(Graph graph)
	{
		// An edge-singleton graph is composed of two unlabeled vertices, connected by a (doubled) edge
		Predicate<Graph> isSingleton = g ->
				g.getVertexCount() == 2
				&& g.getVertex(0).getLabels().size() == 0
				&& g.getVertex(1).getLabels().size() == 0
				&& g.getEdgeCount() == 2
				&& g.getEdges(0, 1).count() == 1
				&& g.getEdges(1,0).count() == 1
				&& g.getEdges(0,1).findFirst().get().getLabel().equals(g.getEdges(1,0).findFirst().get().getLabel());
		return isSingleton.test(graph);
	}
}
