package graphMDL.simpleGraphs.undirected;

import graphMDL.CodeTable;
import graphMDL.CodeTableRow;
import graphMDL.PatternInfo;
import graphMDL.simpleGraphs.SimpleCodeTable;

import java.util.Comparator;

/**
 * Code Table for directed graphs interpreted as doubled up version of simple undirected graphs.
 * Replace some methods of the directed CodeTable class to adapt them to simple undirected graph computation.
 */
public class SimpleUndirectedCodeTable extends SimpleCodeTable
{
	public SimpleUndirectedCodeTable(SimpleUndirectedIndependentVertexAndEdgeST st, Comparator<CodeTableRow> codeTableRowComparator)
	{
		super(st, codeTableRowComparator);
	}

	@Override
	protected CodeTable constructTableWithSameArgumentsAsThis()
	{
		return new SimpleUndirectedCodeTable((SimpleUndirectedIndependentVertexAndEdgeST) this.st, this.codeTableRowComparator);
	}

	@Override
	public boolean isDirected()
	{
		return false;
	}

	@Override
	protected PatternInfo createVertexSingletonPatternInfo(String vertexLabel)
	{
		return SimpleUndirectedUtils.createVertexSingletonPatternInfo(vertexLabel);
	}

	@Override
	protected PatternInfo createEdgeSingletonPatternInfo(String edgeLabel)
	{
		return SimpleUndirectedUtils.createEdgeSingletonPatternInfo(edgeLabel);
	}
}
