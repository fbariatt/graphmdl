package graphMDL.simpleGraphs;

import graphMDL.*;

import java.util.Comparator;

public abstract class SimpleCodeTable extends CodeTable
{
	public SimpleCodeTable(StandardTable standardTable, Comparator<CodeTableRow> codeTableRowComparator)
	{
		super(standardTable, codeTableRowComparator);
	}

	@Override
	protected double computeRowDescriptionLength(CodeTableRow row)
	{
		if (row.getUsage() == 0)
			return 0.0; // A row that is unused has a DL of 0, since it does not bring any information

		if (row.getPortCodeLengths().size() != row.getPortUsages().size())
			throw new NullPointerException("Row's codes should be set (by calling computeTableCodes) before trying to compute the row's description length");

		double descriptionLength = 0.0;
		// Pattern description
		descriptionLength += row.getPatternSTEncodingLength();
		// Pattern prefix code
		descriptionLength += row.getCodeLength();
		// Number of ports
		descriptionLength += Utils.log2(row.getPatternGraph().getVertexCount() + 1);
		// Which vertices are ports?
		descriptionLength += Utils.log2Binomial(row.getPatternGraph().getVertexCount(), row.getPortUsages().size());
		// Port codes
		for (double portCode : row.getPortCodeLengths().values())
		{
			descriptionLength += portCode;
		}
		return descriptionLength;
	}

	@Override
	protected double computeVertexSingletonPatternDescriptionLength(String label)
	{
		double descriptionLength = 0.0;

		// Pattern description
		descriptionLength += st.encodeSingletonVertex(label);
		// Pattern prefix code
		descriptionLength += singletonVertexLengths.get(label);
		// Port description
		descriptionLength += 1; // Equivalent of Utils.log2(2) + Utils.log2(Utils.binomial(1, 1)) + 0, i.e number of ports + which vertices are port + port codes

		return descriptionLength;
	}

	@Override
	protected double computeEdgeSingletonPatternDescriptionLength(String label)
	{
		double descriptionLength = 0.0;

		// Pattern description
		descriptionLength += st.encodeSingletonEdge(label);
		// Prefix code
		descriptionLength += singletonEdgeLengths.get(label);
		// Port description
		descriptionLength += Utils.log2(3) + 2; // Equivalent of Utils.log2(3) + Utils.log2(Utils.binomial(2, 2)) + 1 + 1, i.e number of ports + which vertices are port + port codes

		return descriptionLength;
	}

	/**
	 * Compute the rewritten graph description length.
	 * This way of computing the description length is not strictly specific to simple graphs, but we used it when we were still working on simple graphs only.
	 *
	 * @param _dataVertexCount Not needed for this subclass.
	 * @see CodeTable#computeEncodedGraphLength(int)
	 * @see graphMDL.multigraphs.MultiCodeTable
	 */
	@Override
	protected void computeEncodedGraphLength(int _dataVertexCount)
	{
		encodedGraphLength = 0.0;
		final double portCountLog = Utils.log2(rewrittenGraphPortCount);

		// Number of port vertices
		encodedGraphLength += UniversalInteger.eliasDeltaWith0(rewrittenGraphPortCount);

		// Non-singleton patterns
		for (CodeTableRow row : getRows())
		{
			if (row.getUsage() == 0)
				continue;

			final int patternUsage = row.getUsage();
			final double patternCode = row.getCodeLength();
			final int patternPortCount = row.getPortUsages().size();

			encodedGraphLength += patternCode * patternUsage; // Labels of embedding vertices
			encodedGraphLength += Utils.log2(patternPortCount + 1) * patternUsage; // Number of edges of each embedding vertex
			for (Integer port : row.getPortUsages().keySet())
			{
				final Integer portUsage = row.getPortUsages().get(port);
				final Double portCode = row.getPortCodeLengths().get(port);

				encodedGraphLength += portCode * portUsage; // Edge labels
			}
			encodedGraphLength += portCountLog * row.getPortUsageSum(); // Edge targets
		}

		// Singleton vertex patterns
		for (String vertexLabel : singletonVertexUsages.keySet())
		{
			final Integer singletonUsage = singletonVertexUsages.get(vertexLabel);
			final Double singletonCode = singletonVertexLengths.get(vertexLabel);

			encodedGraphLength += singletonCode * singletonUsage; // Labels of embedding vertices
			encodedGraphLength += singletonUsage; // == log(2) * singletonUsage. Number of edges of each embedding vertex
			// Edge labels: 0 bits
			encodedGraphLength += portCountLog * singletonUsage; // Edge targets
		}

		// Singleton edge patterns
		for (String edgeLabel : singletonEdgeUsages.keySet())
		{
			final Integer singletonUsage = singletonEdgeUsages.get(edgeLabel);
			final Double singletonCode = singletonEdgeLengths.get(edgeLabel);

			encodedGraphLength += singletonCode * singletonUsage; // Labels of embedding vertices
			encodedGraphLength += Utils.log2(3) * singletonUsage; // Number of edges of each embedding edge
			encodedGraphLength += 2 * singletonUsage; // Edge labels (each port has a code of 1 bit, every embedding has two ports)
			encodedGraphLength += portCountLog * 2 * singletonUsage; // Edge targets (every embedding has two ports)
		}
	}
}
