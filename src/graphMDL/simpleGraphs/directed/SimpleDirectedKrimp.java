package graphMDL.simpleGraphs.directed;

import graph.Graph;
import graphMDL.CodeTableRow;
import graphMDL.Krimp;
import graphMDL.StandardTable;
import graphMDL.Utils;

import java.util.Comparator;
import java.util.stream.Stream;

/**
 * Krimp algorithm for simple directed graphs.
 */
public class SimpleDirectedKrimp extends Krimp
{
	/**
	 * See {@link Krimp#Krimp}
	 */
	public SimpleDirectedKrimp(Graph data, Stream<Graph> candidates, Comparator<CodeTableRow> candidateComparator, Comparator<CodeTableRow> codeTableComparator, boolean useAutomorphisms)
	{
		super(data, candidates, candidateComparator, codeTableComparator, useAutomorphisms);
	}

	@Override
	protected StandardTable createStandardTableFromData()
	{
		return new SimpleDirectedIndependentVertexAndEdgeST(this.data);
	}

	@Override
	protected boolean isValidCandidate(Graph pattern)
	{
		// A valid candidate is not a singleton and is not an empty graph (which shouldn't happen, but it doesn't cost much to verify)
		return !Utils.isVertexSingleton(pattern) && !Utils.isEdgeSingleton(pattern) && pattern.getVertexCount() > 0;
	}

	@Override
	protected int labelCount(Graph g)
	{
		return Utils.graphLabelCount(g);
	}

	@Override
	protected void initCodeTable()
	{
		this.codeTable = new SimpleDirectedCodeTable((SimpleDirectedIndependentVertexAndEdgeST) this.st, codeTableComparator);
	}
}
