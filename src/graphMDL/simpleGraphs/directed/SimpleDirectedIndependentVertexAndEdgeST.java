package graphMDL.simpleGraphs.directed;

import graph.Graph;
import graphMDL.simpleGraphs.SimpleIndependentVertexAndEdgeST;

import java.util.Map;

/**
 * A {@link SimpleIndependentVertexAndEdgeST} for simple directed graphs.
 */
public class SimpleDirectedIndependentVertexAndEdgeST extends SimpleIndependentVertexAndEdgeST
{
	public SimpleDirectedIndependentVertexAndEdgeST(Graph g)
	{
		super(g);
	}

	public SimpleDirectedIndependentVertexAndEdgeST(Map<String, Double> vertexCodeLengths, Map<String, Double> edgeCodeLengths)
	{
		super(vertexCodeLengths, edgeCodeLengths);
	}

	@Override
	protected boolean isDirected()
	{
		return true;
	}
}
