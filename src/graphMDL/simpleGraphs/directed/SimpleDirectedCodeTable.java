package graphMDL.simpleGraphs.directed;

import graphMDL.CodeTable;
import graphMDL.CodeTableRow;
import graphMDL.PatternInfo;
import graphMDL.Utils;
import graphMDL.simpleGraphs.SimpleCodeTable;

import java.util.Comparator;

public class SimpleDirectedCodeTable extends SimpleCodeTable
{
	public SimpleDirectedCodeTable(SimpleDirectedIndependentVertexAndEdgeST st, Comparator<CodeTableRow> codeTableRowComparator)
	{
		super(st, codeTableRowComparator);
	}

	@Override
	protected CodeTable constructTableWithSameArgumentsAsThis()
	{
		return new SimpleDirectedCodeTable((SimpleDirectedIndependentVertexAndEdgeST) this.st, this.codeTableRowComparator);
	}

	@Override
	public boolean isDirected()
	{
		return true;
	}

	@Override
	protected PatternInfo createVertexSingletonPatternInfo(String vertexLabel)
	{
		return Utils.createVertexSingletonPatternInfo(vertexLabel);
	}

	@Override
	protected PatternInfo createEdgeSingletonPatternInfo(String edgeLabel)
	{
		return Utils.createEdgeSingletonPatternInfo(edgeLabel);
	}
}
