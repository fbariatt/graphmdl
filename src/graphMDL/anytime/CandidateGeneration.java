package graphMDL.anytime;

import graph.Graph;
import graph.automorphisms.certificates.GraphCertificate;
import graphMDL.rewrittenGraphs.EmbeddingVertex;
import graphMDL.rewrittenGraphs.PortEdge;
import graphMDL.rewrittenGraphs.PortVertex;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import org.apache.log4j.Logger;
import utils.GeneralUtils;
import utils.SingletonStopwatchCollection;
import utils.tuples.Tuple2;
import utils.tuples.Tuple3;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * This class allow generating a list of candidate patterns by looking at a rewritten graph and creating candidates from patterns that appear together (i.e. share ports).
 * This class returns an iterator that yields the candidates in order according to GraphMDL+ candidate ranking heuristic.
 */
public class CandidateGeneration
{
	private final Logger logger = Logger.getLogger(getClass());

	// Parameters
	private final RewrittenGraph rewrittenGraph;
	private final Map<GraphCertificate, MergeCandidate> alreadyTestedCandidates; // Candidates that have already been tested in a CT and as such should be returned last
	private final Supplier<Boolean> shouldStop;
	private final Predicate<EmbeddingVertex> embeddingConstraints;
	private final Function<Graph, Double> getPatternDL;
	// Used during candidate generation
	private Map<PortVertex, List<EmbeddingVertex>> embeddingsOfPorts; // Store the embeddings using each port vertex, while giving them an (arbitrary) order
	private CandidateIterator candidateIterator; // Result of the computation

	/**
	 * Create a new instance of the class and automatically start the computation of candidates from the rewritten graph.
	 *
	 * @param rewrittenGraph          The rewritten graph from which the candidates are generated.
	 * @param alreadyTestedCandidates Candidates that have already been tested in a CT.
	 *                                If some of them are generated, they will be sorted last, according to the GraphMDL+ candidate ranking.
	 * @param shouldStop              Function called during the generation to known whether it should stop (in that case, the result will be partial).
	 *                                Used to implement anytime behaviour.
	 * @param embeddingConstraints    Only embedding vertices of the rewritten graph that pass this check will be able to generate candidates.
	 * @param getPatternDL            Function for getting the description length of a pattern's structure. Used to sort candidates according to the GraphMDL+ candidate ranking.
	 */
	public CandidateGeneration(RewrittenGraph rewrittenGraph, Map<GraphCertificate, MergeCandidate> alreadyTestedCandidates, Supplier<Boolean> shouldStop, Predicate<EmbeddingVertex> embeddingConstraints, Function<Graph, Double> getPatternDL)
	{
		this.rewrittenGraph = rewrittenGraph;
		this.alreadyTestedCandidates = alreadyTestedCandidates;
		this.shouldStop = shouldStop;
		this.embeddingConstraints = embeddingConstraints;
		this.getPatternDL = getPatternDL;
		computeEmbeddingListForEachPort();
		generateCandidates();
	}

	/**
	 * Initialise embeddingsOfPorts by iterating through the rewritten graph and computing which embeddings appear around which port.
	 */
	private void computeEmbeddingListForEachPort()
	{
		SingletonStopwatchCollection.resume("anytimeGraphMDL.candidateGeneration.computeEmbeddingListForEachPort");
		embeddingsOfPorts = new HashMap<>();
		rewrittenGraph.getEmbeddingVertices()
				.filter(this.embeddingConstraints)  // Embedding vertices that do not pass constraints will not be considered for candidate generation
				.forEach(embeddingVertex ->
						rewrittenGraph.getPortsOf(embeddingVertex)
								.forEach(portVertex -> embeddingsOfPorts.computeIfAbsent(portVertex, _k -> new ArrayList<>()).add(embeddingVertex))
				);
		SingletonStopwatchCollection.stop("anytimeGraphMDL.candidateGeneration.computeEmbeddingListForEachPort");
	}

	/**
	 * Generate all candidates from the rewritten graph, by looking at which pairs of embeddings are connected via ports
	 */
	private void generateCandidates()
	{
		SingletonStopwatchCollection.resume("anytimeGraphMDL.candidateGeneration.generateCandidates");
		Map<Tuple3<GraphCertificate, GraphCertificate, Integer>, CandidateBin> groupedCandidates = new HashMap<>(); // The resulting list of candidates, grouped by P1, P2, |portTuples|
		// Iterate through all ports
		for (PortVertex portVertex : embeddingsOfPorts.keySet())
		{
			// Iterate through all pairs of embeddings around this port
			final List<EmbeddingVertex> embeddingsWithPort = embeddingsOfPorts.get(portVertex);
			for (int i = 0; i < embeddingsWithPort.size() - 1; ++i)
			{
				EmbeddingVertex embI = embeddingsWithPort.get(i);
				for (int j = i + 1; j < embeddingsWithPort.size(); ++j)
				{
					if (shouldStop.get()) // Should we stop the generation?
					{
						logger.info("Stop request received: stopping candidate generation");
						return;
					}
					EmbeddingVertex embJ = embeddingsWithPort.get(j);
					// Creating a candidate from these embedding vertices
					final boolean isExclusivePort = embeddingsWithPort.size() == 2; // Are these two embeddings the only embeddings connected to this port?
					MergeCandidate cand = processNeighbours(embI, embJ, isExclusivePort);
					groupedCandidates.computeIfAbsent(
									new Tuple3<>(cand.p1CanonicalName(), cand.p2CanonicalName(), cand.getPortTuples().size()),
									_k -> new CandidateBin(cand.getP1(), cand.getP2(), cand.getPortTuples().size())
							)
							.addCandidate(cand);
				}
			}
		}
		this.candidateIterator = new CandidateIterator(groupedCandidates.values());
		SingletonStopwatchCollection.stop("anytimeGraphMDL.candidateGeneration.generateCandidates");
	}

	/**
	 * Create a candidate from the given two embedding vertices.
	 * The embedding vertices must share at least one port.
	 * Expected to be called for each pair of embeddings around each port.
	 *
	 * @param incrementExclusivePortCount The embeddings were the only embeddings around the port that we were analysing,
	 *                                    so we should increment the counter for exclusive ports for the resulting candidate
	 */
	private MergeCandidate processNeighbours(EmbeddingVertex ev1, EmbeddingVertex ev2, boolean incrementExclusivePortCount)
	{
		SingletonStopwatchCollection.resume("anytimeGraphMDL.candidateGeneration.processNeighbours");
		// Creating candidate
		MergeCandidate cand = new MergeCandidate(
				ev1.getPatternCTInfo().getPatternInfo(),
				ev2.getPatternCTInfo().getPatternInfo(),
				commonPortsOfEmbeddingVertices(ev1, ev2)
		);
		// We want to make sure that candidates are always in the same form, independently of the choice of ev1 and ev2
		if (!cand.shouldBeReversed())
		{
			EmbeddingVertex tempEmbeddingVertex = ev1;
			ev1 = ev2;
			ev2 = tempEmbeddingVertex;
			cand = cand.reverse();
		}
		cand.addInstance(ev1, ev2);
		if (incrementExclusivePortCount)
			cand.incrementExclusivePortCount();
		SingletonStopwatchCollection.stop("anytimeGraphMDL.candidateGeneration.processNeighbours");
		return cand;
	}

	/**
	 * Find all ports of the rewritten graph that both the two given embedding vertices use.
	 * The result of this function corresponds to the "port tuples" of a candidate.
	 */
	private Set<Tuple2<Integer, Integer>> commonPortsOfEmbeddingVertices(EmbeddingVertex ev1, EmbeddingVertex ev2)
	{
		SingletonStopwatchCollection.resume("anytimeGraphMDL.candidateGeneration.createPortTuples");
		Map<PortVertex, PortEdge> portEdgesOf1ByPort = rewrittenGraph.getPortEdgesOf(ev1)
				.collect(Collectors.toMap(PortEdge::getPort, Function.identity()));
		Map<PortVertex, PortEdge> portEdgesOf2ByPort = rewrittenGraph.getPortEdgesOf(ev2)
				.collect(Collectors.toMap(PortEdge::getPort, Function.identity()));
		Set<Tuple2<Integer, Integer>> portTuples = GeneralUtils.intersection(portEdgesOf1ByPort.keySet(), portEdgesOf2ByPort.keySet()).stream()
				.map(portVertex -> new Tuple2<>(portEdgesOf1ByPort.get(portVertex), portEdgesOf2ByPort.get(portVertex)))
				.map(tuple -> tuple.map(PortEdge::getPatternPortNumber, PortEdge::getPatternPortNumber))
				.collect(Collectors.toSet());
		SingletonStopwatchCollection.stop("anytimeGraphMDL.candidateGeneration.createPortTuples");
		return portTuples;
	}

	/**
	 * Iterator corresponding to the result of the candidate generation.
	 * It returns candidates in the order of the GraphMDL+ candidate ranking.
	 * Implemented in a way that limits the number of automorphism computations that are done.
	 */
	private class CandidateIterator implements Iterator<MergeCandidate>
	{
		private final Comparator<MergeCandidate> candidateHeuristic = Comparator
				.comparing(MergeCandidate::getExclusivePortCount, Comparator.reverseOrder()) // Candidates that have more exclusive ports sort first
				.thenComparingDouble(MergeCandidate::getPatternSTEncodingDL); // "Simplest" (in term of DL) candidates sort first
		private List<CandidateBin> candidateBins; // Each bin contains one or more candidates, and overestimates their usage.
		private List<MergeCandidate> topRankingNewCandidates = Collections.emptyList();
		private Iterator<MergeCandidate> currentCandidatesIterator = Collections.emptyIterator();
		private List<MergeCandidate> alreadyTestedCandidates = new ArrayList<>(); // The candidates that have already been tested in a previous GraphMDL+ iteration will be considered last
		private boolean iteratingAlreadyTested = false; // Whether we tried all new candidates and we are currently iterating on the ones that have already been tested in a previous GraphMDL+ iteration.

		public CandidateIterator(Collection<CandidateBin> candidateBins)
		{
			this.candidateBins = new ArrayList<>(candidateBins);
		}

		@Override
		public boolean hasNext()
		{
			// Either we are iterating on some candidates, or we can generate some more from the bins, or we can start iterating on the already tested
			return this.currentCandidatesIterator.hasNext() || (!this.candidateBins.isEmpty()) || (!this.iteratingAlreadyTested && !this.alreadyTestedCandidates.isEmpty());
		}

		/**
		 * Yield the next candidate according to the GraphMDL+ candidate ranking.
		 * Note: a call to this method may take a long time, as it may need to sort candidates and compute candidate equivalences using automorphisms.
		 * @throws NoSuchElementException if called while {@link #hasNext()} returns false.
		 */
		@Override
		public MergeCandidate next()
		{
			if (!this.hasNext())
				throw new NoSuchElementException();

			// If the current iterator has still elements, return the next one
			if (this.currentCandidatesIterator.hasNext())
				return this.currentCandidatesIterator.next();

			// Starting here, the current iterator is empty

			// If we were iterating on the new candidates and we arrived at their end, we clear the list.
			if (!iteratingAlreadyTested)
				this.topRankingNewCandidates.clear();

			// If we have some unprocessed bins, we try to process them to generate new candidates
			if (!this.candidateBins.isEmpty())
			{
				this.currentCandidatesIterator = null;
				while (this.topRankingNewCandidates.isEmpty() && !this.candidateBins.isEmpty()) // It may takes multiple calls to generate candidates
					this.gatherTopRanking();
			}
			// The gather stopped, so either we gathered some new candidates, either we processed all bins finding only already tested candidates
			if (!this.topRankingNewCandidates.isEmpty()) // We gathered some new candidates
			{
				// Pattern description length is a metric used to sort candidates, so we compute it for the candidates that do not have it set
				this.topRankingNewCandidates.stream()
						.filter(candidate -> candidate.getPatternSTEncodingDL() == null)
						.forEach(candidate -> candidate.setPatternSTEncodingDL(getPatternDL.apply(candidate.getPatternInfo().getStructure())));
				// We sort the candidates
				this.topRankingNewCandidates.sort(this.candidateHeuristic);
				this.currentCandidatesIterator = this.topRankingNewCandidates.iterator();
				return this.currentCandidatesIterator.next();
			}
			else // We processed all bins finding only already tested candidates
			{
				this.iteratingAlreadyTested = true; // We start iterating on the already tested candidates
				// (No need to compute pattern description length here, as already tested candidates already have it set)
				this.alreadyTestedCandidates.sort(this.candidateHeuristic);
				this.currentCandidatesIterator = this.alreadyTestedCandidates.iterator();
				return this.currentCandidatesIterator.next();
			}
		}

		/**
		 * Find the candidate bins that rank the highest according to their (possibly overestimated) usage estimate.
		 * For each of those high-ranking bins:
		 * <ul>
		 *     <li>
		 *         If the bin contains a single candidate, then its usage estimate is correct and the candidate is a candidate with highest usage estimate:
		 *         <ul>
		 *             <li>If the candidate has not been tested before, it is added to {@link #topRankingNewCandidates}.</li>
		 *             <li>If the candidate has been tested before, then its high usage estimate does not matter and it is added to {@link #alreadyTestedCandidates}.</li>
		 *         </ul>
		 *     </li>
		 *     <li>
		 *         If the bin contains multiple candidates, then they are reduced to a set of equivalent candidates using automorphisms equivalence check. For each of these:
		 *         <ul>
		 *             <li>If the candidate's actual usage estimate is lower than what was estimated by the bin, then its usage estimate is not the highest anymore, and the candidate is added back to {@link #candidateBins} as a single bin (will be considered later).</li>
		 *             <li>If the candidate's usage estimate is still the same as before (i.e. the highest) and the candidate has not been tested before, it is added to {@link #topRankingNewCandidates}.</li>
		 *             <li>If the candidate's usage estimate is still the same (i.e. the highest) but the candidate has been tested before, its usage estimate does not matter and it is added to {@link #alreadyTestedCandidates}.</li>
		 *         </ul>
		 *     </li>
		 * </ul>
		 * This method modifies {@link #candidateBins}, {@link #topRankingNewCandidates} and {@link #alreadyTestedCandidates}.
		 * <strong>However, {@link #topRankingNewCandidates} is not guaranteed to contain something:</strong> if all the top-ranking bins overestimated the actual usage estimate of their candidates,
		 * then the list will stay empty. <strong>Multiple calls to this method may be needed to obtain top ranking candidates.</strong>
		 */
		public void gatherTopRanking()
		{
			SingletonStopwatchCollection.resume("CandidateGeneration.gatherTopRanking");
			this.topRankingNewCandidates = new ArrayList<>();
			// Gathering all merge candidates whose usage estimate is the top value in the list
			this.candidateBins.sort(Comparator.comparingInt(CandidateBin::getUsageEstimate)); // Highest usage estimate candidates will be last in the list
			int topUsageEstimate = this.candidateBins.get(this.candidateBins.size() - 1).getUsageEstimate();
			List<MergeCandidate> notTopRankingCandidates = new LinkedList<>();
			while (!this.candidateBins.isEmpty() && this.candidateBins.get(this.candidateBins.size() - 1).getUsageEstimate() == topUsageEstimate)
			{
				CandidateBin bin = this.candidateBins.remove(this.candidateBins.size() - 1);
				if (bin.isSingleCandidate()) // The bin contains only one candidate. This means that the usage estimate is correct and it is a top-ranking candidate.
				{
					MergeCandidate cand = bin.getCandidates().get(0);
					Optional<MergeCandidate> alreadyTested = getAlreadyTestedEquivalent(cand);
					if (alreadyTested.isPresent())
						this.alreadyTestedCandidates.add(alreadyTested.get());
					else
						this.topRankingNewCandidates.add(cand);
				}
				else // The bin contains more than one candidate
				{
					Collection<MergeCandidate> binCandidates = bin.splitIntoEquivalentsUsingAutomorphisms();
					for (MergeCandidate cand : binCandidates)
					{
						if (cand.getUsageEstimation() == topUsageEstimate) // The candidate's usage estimate is the highest
						{
							Optional<MergeCandidate> alreadyTested = getAlreadyTestedEquivalent(cand);
							if (alreadyTested.isPresent())
								this.alreadyTestedCandidates.add(alreadyTested.get());
							else
								this.topRankingNewCandidates.add(cand);
						}
						else // The bin's usage estimate was an exaggeration, the actual usage estimate for this candidate is lower.
						{
							notTopRankingCandidates.add(cand);
						}
					}
				}
			}
			// Adding the candidates that have been generated from high-estimate bins, but which have in fact a lower usage estimate back into the list (they may become top-ranking later)
			notTopRankingCandidates.stream()
					.map(CandidateBin::ofSingleCandidate)
					.forEach(this.candidateBins::add);
			SingletonStopwatchCollection.stop("CandidateGeneration.gatherTopRanking");
		}
	}

	/**
	 * See if in the list of candidates that have already been tested in previous iterations of GraphMDL+ there is a candidate that is equivalent to the given one
	 * (taking automorphisms into account).
	 * If one equivalent candidate is found, return it.
	 */
	private Optional<MergeCandidate> getAlreadyTestedEquivalent(MergeCandidate candidate)
	{
		SingletonStopwatchCollection.resume("CandidateGeneration.getAlreadyTestedEquivalent");
		if (candidate.hasAlreadyBeenTested())
		{
			SingletonStopwatchCollection.stop("CandidateGeneration.getAlreadyTestedEquivalent");
			return Optional.of(candidate);
		}
		else
		{
			SingletonStopwatchCollection.stop("CandidateGeneration.getAlreadyTestedEquivalent");
			return Optional.ofNullable(alreadyTestedCandidates.get(candidate.getPatternInfo().getAutomorphismInfo().getCanonicalName()));
		}
	}

	/**
	 * @return A {@link CandidateIterator} that yields the result of the generation. <strong>The same iterator is returned every time and once it is
	 * consumed, a new generation must be run.</strong>
	 */
	public CandidateIterator getCandidateIterator()
	{
		return candidateIterator;
	}
}
