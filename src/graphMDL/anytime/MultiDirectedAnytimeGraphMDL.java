package graphMDL.anytime;

import graph.Graph;
import graphMDL.*;
import graphMDL.multigraphs.MultiDirectedCodeTable;
import graphMDL.multigraphs.MultiDirectedStandardTable;

import java.util.Comparator;

/**
 * Implementation of {@link AnytimeGraphMDL} handling directed multigraphs.
 */
public class MultiDirectedAnytimeGraphMDL extends AnytimeGraphMDL
{

	public MultiDirectedAnytimeGraphMDL(Graph data, Comparator<CodeTableRow> codeTableOrder, PatternConstraints patternConstraints, CoverStrategy coverStrategy)
	{
		super(data, codeTableOrder, patternConstraints, coverStrategy);

		if (data.getEdges().anyMatch(e -> e.getSource() == e.getTarget()))
			throw new UnsupportedOperationException("The current implementation does not support edges that are self-loops (even though the MDL definitions allow for it)");
	}

	@Override
	protected StandardTable computeStandardTable()
	{
		return new MultiDirectedStandardTable(data);
	}

	@Override
	protected CodeTable createEmptyCodeTable()
	{
		return new MultiDirectedCodeTable(
				(MultiDirectedStandardTable) this.st,
				this.codeTableOrder
		);
	}
}
