package graphMDL.anytime;

import graph.automorphisms.certificates.GraphCertificate;
import graphMDL.PatternInfo;
import graphMDL.rewrittenGraphs.EmbeddingVertex;
import utils.SingletonStopwatchCollection;

import java.util.*;

/**
 * An object that can contain one or more candidates which are possibly equivalent.
 * Candidate equivalence using automorphisms is only computed on demand.
 * If this object contains more than one candidate, its "usage estimate" may be an exaggeration compared to the actual usage estimate of its candidates.
 */
public class CandidateBin
{
	private final GraphCertificate p1;
	private final boolean p1HasOnlyOneVertex;
	private final GraphCertificate p2;
	private final boolean p2HasOnlyOneVertex;
	private final int nbPortTuples;
	private List<MergeCandidate> candidates;
	private Set<EmbeddingVertex> instancesOfP1;
	private Set<EmbeddingVertex> instancesOfP2;

	public CandidateBin(PatternInfo p1, PatternInfo p2, int nbPortTuples)
	{
		this.p1 = p1.getAutomorphismInfo().getCanonicalName();
		this.p1HasOnlyOneVertex = p1.getStructure().getVertexCount() == 1;
		this.p2 = p2.getAutomorphismInfo().getCanonicalName();
		this.p2HasOnlyOneVertex = p2.getStructure().getVertexCount() == 1;
		this.nbPortTuples = nbPortTuples;
		this.candidates = new LinkedList<>();
		this.instancesOfP1 = new HashSet<>();
		this.instancesOfP2 = new HashSet<>();
	}

	/**
	 * Add a new candidate to the bin.
	 * The candidate must be compatible with the other candidates in the bin, but might not be necessarily equivalent.
	 * If another candidate that is exactly equal to the given one exist in the bin, the two are merged. However, automorphism equivalence is not computed.
	 */
	public void addCandidate(MergeCandidate candidate)
	{
		SingletonStopwatchCollection.resume("CandidateBin.addCandidate");
		if (candidate.getPortTuples().size() != this.nbPortTuples || !candidate.p1CanonicalName().equals(this.p1) || !candidate.p2CanonicalName().equals(this.p2))
			throw new IllegalArgumentException("Adding incompatible candidate to bin");

		Optional<MergeCandidate> existingCopy = candidates.stream()
				.filter(candidate::isExactlyEqual)
				.findAny();
		if (existingCopy.isPresent())
		{
			existingCopy.get().addInstances(candidate.getInstancesOfP1(), candidate.getInstancesOfP2());
			existingCopy.get().setExclusivePortCount(existingCopy.get().getExclusivePortCount() + candidate.getExclusivePortCount());
		}
		else
		{
			candidates.add(candidate);
		}
		if (!this.p1HasOnlyOneVertex)
			this.instancesOfP1.addAll(candidate.getInstancesOfP1());
		if (!this.p2HasOnlyOneVertex)
			this.instancesOfP2.addAll(candidate.getInstancesOfP2());
		SingletonStopwatchCollection.stop("CandidateBin.addCandidate");
	}

	/**
	 * Utility method to create a bin with a single candidate in one line.
	 */
	public static CandidateBin ofSingleCandidate(MergeCandidate cand)
	{
		CandidateBin result = new CandidateBin(cand.getP1(), cand.getP2(), cand.getPortTuples().size());
		result.addCandidate(cand);
		return result;
	}

	/**
	 * Return the usage estimate of the bin.
	 * <ul>
	 *     <li>If the bin contains a single candidate, the actual usage estimate of the candidate is returned.</li>
	 *     <li>If the bin contains more than one candidate (or 0, actually), returns an usage estimate that might be an exaggeration of the actual usage estimate of the individual candidates.</li>
	 * </ul>
	 */
	public int getUsageEstimate()
	{
		if (this.isSingleCandidate())
		{
			return this.candidates.get(0).getUsageEstimation();
		}
		else
		{
			if (this.p1HasOnlyOneVertex)
				return this.instancesOfP2.size();
			else if (this.p2HasOnlyOneVertex)
				return this.instancesOfP1.size();
			else
				return Math.min(this.instancesOfP1.size(), this.instancesOfP2.size());
		}
	}

	/**
	 * @return Whether this bin contains only one candidate.
	 * @implNote As expected, this method returns false if the bin contains no candidates.
	 */
	public boolean isSingleCandidate()
	{
		return this.candidates.size() == 1;
	}

	/**
	 * Compute candidate equivalence between all candidates in this bin, and returns all different candidates.
	 * This is done by comparing the canonical names of the resulting pattern of each candidate contained in the bin.
	 * This method may take some time.
	 */
	public Collection<MergeCandidate> splitIntoEquivalentsUsingAutomorphisms()
	{
		SingletonStopwatchCollection.resume("CandidateBin.splitIntoEquivalentsUsingAutomorphisms");
		Map<GraphCertificate, MergeCandidate> result = new HashMap<>();

		for (MergeCandidate candidate : this.candidates)
		{
			final GraphCertificate canonicalName = candidate.getPatternInfo().getAutomorphismInfo().getCanonicalName();
			result.merge(canonicalName, candidate, (a, b) -> {
				a.addInstances(b.getInstancesOfP1(), b.getInstancesOfP2());
				return a;
			});
		}
		SingletonStopwatchCollection.stop("CandidateBin.splitIntoEquivalentsUsingAutomorphisms");
		return result.values();
	}

	/**
	 * @return The candidates in this bin.
	 */
	public List<MergeCandidate> getCandidates()
	{
		return candidates;
	}
}
