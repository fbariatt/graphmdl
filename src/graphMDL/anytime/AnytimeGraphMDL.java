package graphMDL.anytime;

import graph.Graph;
import graph.Vertex;
import graph.automorphisms.AutomorphismSearch;
import graph.automorphisms.certificates.GraphCertificate;
import graphMDL.*;
import graphMDL.rewrittenGraphs.NonSingletonCTInfo;
import graphMDL.rewrittenGraphs.PatternCTInfo;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import graphMDL.rewrittenGraphs.SingletonCTInfo;
import org.apache.log4j.Logger;
import patternMatching.Embedding;
import patternMatching.MinimumImageBasedSupportFromEmbeddings;
import utils.BinQuickSort;
import utils.SingletonStopwatchCollection;
import utils.tuples.Tuple3;

import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AnytimeGraphMDL
{
	private final Logger logger = Logger.getLogger(getClass());

	// Parameters
	protected final Graph data;
	protected final Comparator<CodeTableRow> codeTableOrder;
	private final PatternConstraints patternConstraints;
	private final CoverStrategy coverStrategy;
	// Parameters computed from data
	private final Map<Vertex, Integer> dataVertexIndexMap;
	protected final StandardTable st;

	// Embeddings of singleton patterns
	// in case we need to compute pattern embeddings (because the cover strategy needs them)
	private Map<String, List<Embedding>> singletonVertexEmbeddings;
	private Map<String, List<Embedding>> singletonEdgeEmbeddings;

	// Attributes used during code table search
	protected CodeTable codeTable;
	private int testedCodeTablesCount;
	private RewrittenGraph rewrittenGraph;
	private double bestDL;
	private Map<GraphCertificate, MergeCandidate> testedCandidates; // Candidate that have already been tested, identified by the canonical certificate of the resulting pattern.
	private long durationLimit; // Maximum time (in seconds) that the computation can last

	/**
	 * @param data               The data graph from which patterns will be generated
	 * @param codeTableOrder     Comparator determining the order in which patterns are stored when added to the code table. See {@link graphMDL.CodeTableHeuristics}.
	 * @param patternConstraints Constraints about the generated patterns. The only patterns that will be generated are the ones that follow these constraints.
	 * @param coverStrategy      The {@link CoverStrategy} used to compute the cover of each code table during the search.
	 */
	public AnytimeGraphMDL(Graph data, Comparator<CodeTableRow> codeTableOrder, PatternConstraints patternConstraints, CoverStrategy coverStrategy)
	{
		this.data = data;
		this.codeTableOrder = codeTableOrder;
		this.patternConstraints = patternConstraints;
		this.coverStrategy = coverStrategy;

		this.dataVertexIndexMap = Collections.unmodifiableMap(data.getVertexIndexMap());
		this.st = computeStandardTable();
		if (coverStrategy.needsEmbeddingsSetOnCTRow())
		{
			this.singletonVertexEmbeddings = new HashMap<>();
			this.singletonEdgeEmbeddings = new HashMap<>();
		}
	}

	/**
	 * Compute the GraphMDL Standard Table from the data
	 */
	protected abstract StandardTable computeStandardTable();

	/**
	 * Apply the Anytime GraphMDL algorithm to search for the best code table.
	 *
	 * @param dlThreshold                    If a code table is found that yields a description length lower than this threshold, the search is stopped.
	 *                                       Set to value <= 0 to disable.
	 * @param durationLimit                  If the search takes more than this many seconds, it is stopped at the end of the next iteration.
	 *                                       Set to value <= 0 to disable.
	 * @param stripUnusedPatternsFromFinalCT Whether to remove unused patterns from the best code table found before returning it.
	 * @param usePruning                     Whether to prune the code table for now-useless patterns after a new pattern is found that reduces description length. See {@link CodeTablePruning}.
	 * @param descriptionLengthLog           (May be null) if not null, every time a new CT is found that improves description length,
	 *                                       the current DL and search time are logged to this object.
	 * @return The best code table found. Can also be retrieved afterwards with {@link #getCodeTable()}.
	 */
	public CodeTable computeBestCodeTable(double dlThreshold, long durationLimit, boolean stripUnusedPatternsFromFinalCT, boolean usePruning, PrintWriter descriptionLengthLog)
	{
		logger.debug("GraphMDL+ initialisation and CT0");
		this.durationLimit = durationLimit;
		codeTable = createEmptyCodeTable();
		testedCodeTablesCount = 0;
		testedCandidates = new HashMap<>();
		applyCodeTable(); // Apply the singleton-only code table to compute the initial description length and rewritten graph
		bestDL = codeTable.getModelDL() + codeTable.getEncodedGraphLength();

		logger.info("L(CT0) = " + codeTable.getModelDL());
		logger.info("L(D | CT0) = " + codeTable.getEncodedGraphLength());
		logger.info("L(D, CT0) = " + bestDL);

		SingletonStopwatchCollection.resetAndStart("anytimeGraphMDL.ctSearch");
		if (descriptionLengthLog != null)
			logCurrentDescriptionLengthAndElapsedTime(descriptionLengthLog);
		boolean searchFinished = false;
		logger.debug("GraphMDL+ search start");
		while (!searchFinished)
		{
			boolean betterCTFound = findABetterCT(usePruning); // Look for a better code table.

			if (betterCTFound && (descriptionLengthLog != null))
				logCurrentDescriptionLengthAndElapsedTime(descriptionLengthLog);
			searchFinished = !betterCTFound; // If better code table hasn't been found, then the search has finished.

			// In case we passed the DL limit, we stop the search anyway
			if (bestDL < dlThreshold)
			{
				logger.info("Found CT whose DL is under the threshold. Stopping search");
				searchFinished = true;
			}
			if (shouldStopComputation())
			{
				logger.info("Stop condition met. No better code table will be searched.");
				searchFinished = true;
			}

		}
		SingletonStopwatchCollection.stop("anytimeGraphMDL.ctSearch");

		// Applying the code table one last time, in case the last candidate was discarded
		applyCodeTable();

		if (stripUnusedPatternsFromFinalCT)
		{
			logger.info("Removing CT rows with unused patterns");
			codeTable.removedUnused();
			rewrittenGraph = null; // Rewritten graph will need to be recomputed, since pattern numbers have changed
		}
		logger.debug(String.format("GraphMDL+ search end, %d non-singleton patterns in CT", codeTable.getRows().size()));

		// Printing code table description lengths
		logger.info("L(CT) = " + codeTable.getModelDL());
		logger.info("L(D | CT) = " + codeTable.getEncodedGraphLength());
		logger.info("L(D, CT) = " + bestDL);

		return codeTable;
	}

	/**
	 * See {@link #computeBestCodeTable(double, long, boolean, boolean, PrintWriter)}
	 */
	public CodeTable computeBestCodeTable(PrintWriter descriptionLengthLog)
	{
		return computeBestCodeTable(0.0, -1, true, true, descriptionLengthLog);
	}

	/**
	 * See {@link #computeBestCodeTable(double, long, boolean, boolean, PrintWriter)}.
	 */
	public CodeTable computeBestCodeTableWithPruning(double dlThreshold, long durationLimit, boolean stripUnusedPatternsFromFinalCT, PrintWriter descriptionLengthLog)
	{
		return computeBestCodeTable(dlThreshold, durationLimit, stripUnusedPatternsFromFinalCT, true, descriptionLengthLog);
	}

	/**
	 * See {@link #computeBestCodeTable(double, long, boolean, boolean, PrintWriter)}.
	 */
	public CodeTable computeBestCodeTableWithoutPruning(double dlThreshold, long durationLimit, boolean stripUnusedPatternsFromFinalCT, PrintWriter descriptionLengthLog)
	{
		return computeBestCodeTable(dlThreshold, durationLimit, stripUnusedPatternsFromFinalCT, false, descriptionLengthLog);
	}

	/**
	 * Create a new empty code table
	 */
	protected abstract CodeTable createEmptyCodeTable();

	/**
	 * Apply the current code table to the data.
	 * Updates the codeTable attribute, the rewrittenGraph attribute and the testedCodeTablesCount attribute.
	 */
	private void applyCodeTable()
	{
		SingletonStopwatchCollection.resume("anytimeGraphMDL.applyCT");
		++testedCodeTablesCount;
		if (testedCodeTablesCount % 1000 == 0)
			logger.info("Processed " + testedCodeTablesCount + " candidate code tables");
		// Code table does not need rdf version of data since we compute pattern embeddings when creating candidates
		this.rewrittenGraph = codeTable.apply(data, coverStrategy, true).get();
		SingletonStopwatchCollection.stop("anytimeGraphMDL.applyCT");
	}

	/**
	 * Log the current best DL and the current elapsed search time to the given printer, separated by a comma.
	 */
	private void logCurrentDescriptionLengthAndElapsedTime(PrintWriter out)
	{
		out.println(SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.ctSearch").asMilliseconds() + "," + bestDL);
	}

	/**
	 * @return Whether the anytime search should stop.
	 * This can be because the duration limit has been exceeded, or because a call to {@link #askToStopSearch()} has been done.
	 */
	private boolean shouldStopComputation()
	{
		if (durationLimit > 0)
			return SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.ctSearch").asSeconds() > durationLimit;
		else
			return false;
	}

	/**
	 * Ask the anytime algorithm to stop the current search.
	 * The search will stop as soon as the stop condition is checked, therefore in the current implementation,
	 * it could take a relatively long time before the search is stopped.
	 */
	public void askToStopSearch()
	{
		this.durationLimit = 1;
	}

	/**
	 * Generate candidates from the current rewritten graph and add them to the current CT until a CT is found that yields a better description length.
	 *
	 * @param usePruning Whether to prune the code table for now-useless patterns after a new pattern is found that reduces description length. See {@link CodeTablePruning}.
	 * @return True if a better CT is found, False if all candidates have been tested and no better CT has been found.
	 */
	private boolean findABetterCT(boolean usePruning)
	{
		// Generating candidates by looking at which patterns are neighbours in the rewritten graph
		SingletonStopwatchCollection.resume("anytimeGraphMDL.candidateGeneration");
		logger.debug("Starting candidate generation");
		Iterator<MergeCandidate> candidateIterator = new CandidateGeneration(rewrittenGraph, testedCandidates, this::shouldStopComputation, this.patternConstraints.getCandidateGenerationEmbeddingConstraints(), st::encode).getCandidateIterator();
		logger.debug("Finished candidate generation, %d candidates found");
		SingletonStopwatchCollection.stop("anytimeGraphMDL.candidateGeneration");
		if (shouldStopComputation()) // Candidate generation has stopped because time ran out
		{
			logger.info("Stop condition met. Not testing the candidates generated in this iteration.");
			return false;
		}

		// Looking for a candidate that yields a better CT
		SingletonStopwatchCollection.resume("anytimeGraphMDL.candidateTesting");
		logger.debug("Testing candidates");
		while (true)
		{
			// Check if we can test another candidate or if we reached the time limit
			if (shouldStopComputation())
			{
				logger.info("Stop condition met. Stopping candidate testing.");
				return false;
			}

			if (candidateIterator.hasNext()) // We have a candidate to test
			{
				logger.debug("Testing a new candidate");
				SingletonStopwatchCollection.resume("anytimeGraphMDL.candidateIterator.next");
				MergeCandidate candidate = candidateIterator.next();
				SingletonStopwatchCollection.stop("anytimeGraphMDL.candidateIterator.next");
				if (!candidate.hasAlreadyBeenTested()) // If this is the first time we see the candidate, we add it to the list of already seen candidates
				{
					candidate.setHasAlreadyBeenTested(true);
					testedCandidates.put(candidate.getPatternInfo().getAutomorphismInfo().getCanonicalName(), candidate);
				}

				// Is the pattern generated by this candidate accepted by the pattern constraints?
				if (!this.patternConstraints.getPatternStructureConstraints().test(candidate.getPatternInfo().getStructure()))
				{
					logger.debug(String.format("Candidate %s is not accepted by pattern structure constraints", candidate.getPatternInfo().getAutomorphismInfo().getCanonicalName()));
					continue;
				}

				// Computing code table row for the candidate and verifying its constraints
				CodeTableRow ctRow = codeTableRowFromCandidate(candidate);
				if (!this.patternConstraints.getCodeTableRowConstraints().test(ctRow))
				{
					logger.debug(String.format("Candidate %s is not accepted by ct row constraints", ctRow.getAutomorphisms().getCanonicalName()));
					continue;
				}

				// Adding the pattern to the code table and computing cover
				codeTable.addRow(ctRow);
				logger.debug("Computing cover for new candidate");
				applyCodeTable();
				double newDL = codeTable.getModelDL() + codeTable.getEncodedGraphLength();
				if (newDL < bestDL) // If this code table improves the description length
				{
					logger.debug(String.format("New candidate improves DL: %f %s", newDL, ctRow.getAutomorphisms().getCanonicalName()));
					bestDL = newDL;
					if (usePruning)
					{
						CodeTablePruning.pruneCodeTable(codeTable, this::applyCodeTable, true);
						// The pruning function does not guarantee that the code table will be in a correct state after it finishes.
						// But we need it to be, since we use this ct and cover to compute new candidates during the next search round.
						// So we need compute the cover once more on the pruned code table
						applyCodeTable();
					}
					SingletonStopwatchCollection.stop("anytimeGraphMDL.candidateTesting");
					return true;
				}
				else // This code table does not improve the description length
				{
					logger.debug(String.format("New candidate does not improve DL: %f %s", newDL, ctRow.getAutomorphisms().getCanonicalName()));
					// Removing the row and moving on to the next candidate
					codeTable.removeRow(ctRow);
					// We make sure that the row does not have a reference to a list of embeddings to make sure that they are deleted,
					// since they can take up a lot of memory.
					ctRow.setEmbeddings(null);
					ctRow.getCoverEmbeddings().clear();
				}
			}
			else // Impossible to find another candidate: all candidates have already been tested
			{
				SingletonStopwatchCollection.stop("anytimeGraphMDL.candidateTesting");
				return false;
			}
		}
	}

	/**
	 * @return A code table row for the pattern described by the given merge candidate.
	 * The row's embeddings are only computed if the cover strategy needs them.
	 */
	private CodeTableRow codeTableRowFromCandidate(MergeCandidate candidate)
	{
		final Graph pattern = candidate.getPatternInfo().getStructure();
		SingletonStopwatchCollection.resume("anytimeGraphMDL.createCTRowFromCandidate");
		final PatternInfo patternInfo = candidate.getPatternInfo();
		logger.debug(String.format("Creating CT row for %s", patternInfo.getAutomorphismInfo().getCanonicalName()));


		List<Embedding> patternEmbeddings = null;
		if (coverStrategy.needsEmbeddingsSetOnCTRow())
		{
			List<Embedding> embeddingsOfP1 = getEmbeddingsOfPattern(candidate.getInstancesOfP1().iterator().next().getPatternCTInfo());
			List<Embedding> embeddingsOfP2 = candidate.isSymmetric() ? embeddingsOfP1 : getEmbeddingsOfPattern(candidate.getInstancesOfP2().iterator().next().getPatternCTInfo());
			patternEmbeddings = graphMDL.Utils.sortEmbeddings(PatternEmbeddingsFromMergeCandidate.joinEmbeddings(candidate, embeddingsOfP1, embeddingsOfP2, patternConstraints.getMaxEmbeddingsPerPattern()), pattern, data);
		}

		CodeTableRow row = new CodeTableRow(
				patternInfo,
				candidate.getPatternSTEncodingDL(),
				patternEmbeddings == null ? -1 : MinimumImageBasedSupportFromEmbeddings.imageBasedSupport(patternEmbeddings, pattern.getVertexCount()), // If we do not compute embeddings (because the cover strategy does not need them), we can not compute the support of the pattern
				graphLabelCount(pattern)
		);
		row.setEmbeddings(patternEmbeddings);
		SingletonStopwatchCollection.stop("anytimeGraphMDL.createCTRowFromCandidate");
		return row;
	}

	/**
	 * Verify if the given pattern is a vertex singleton. In that case, return its label.
	 *
	 * @return The label of the pattern if it is a vertex singleton, an empty optional otherwise
	 */
	protected Optional<String> isVertexSingleton(Graph pattern)
	{
		if (graphMDL.Utils.isVertexSingleton(pattern))
			return Optional.of(pattern.getVertex(0).getLabels().keySet().iterator().next());
		else
			return Optional.empty();
	}

	/**
	 * Verify if the given pattern is an edge singleton. In that case, return its label.
	 *
	 * @return The label of the pattern if it is an edge singleton, an empty optional otherwise
	 */
	protected Optional<String> isEdgeSingleton(Graph pattern)
	{
		if (graphMDL.Utils.isEdgeSingleton(pattern))
			return Optional.of(pattern.getEdges().iterator().next().getLabel());
		else
			return Optional.empty();
	}

	/**
	 * @return The embeddings of the given pattern. Either the pattern is a singleton, and the embeddings are computed,
	 * or it is not, and the embeddings are extracted from the CT.
	 */
	private List<Embedding> getEmbeddingsOfPattern(PatternCTInfo rewrittenGraphPatternInfo)
	{
		if (rewrittenGraphPatternInfo instanceof NonSingletonCTInfo) // The pattern is not a singleton
		{
			// This means that it has an associated ct row, with the embeddings
			return ((NonSingletonCTInfo) rewrittenGraphPatternInfo).getCtRow().getEmbeddings();
		}
		else if (rewrittenGraphPatternInfo instanceof SingletonCTInfo) // The pattern is a singleton
		{
			// This means that the pattern has no associated CT row object, therefore we find its embeddings in a different manner.
			final SingletonCTInfo info = (SingletonCTInfo) rewrittenGraphPatternInfo;
			if (info.isVertexSingleton())
				return this.singletonVertexEmbeddings.computeIfAbsent(info.getSingletonLabel(), this::computeEmbeddingsOfSingletonVertexPattern);
			else if (info.isEdgeSingleton())
				return this.singletonEdgeEmbeddings.computeIfAbsent(info.getSingletonLabel(), this::computeEmbeddingsOfSingletonEdgePattern);
			throw new RuntimeException("Unsupported singleton pattern of arity " + info.getArity());
		}
		// We should never get here because a pattern is either a singleton or a non singleton
		throw new RuntimeException("Trying to get embeddings of patterns that is neither a singleton nor a non-singleton: this should never happen");
	}

	/**
	 * Compute the embeddings of a vertex singleton pattern by scanning the data graph.
	 *
	 * @param vertexLabel The label of the singleton
	 */
	protected List<Embedding> computeEmbeddingsOfSingletonVertexPattern(String vertexLabel)
	{
		return data.getVerticesWithLabel(vertexLabel)
				.map(vertex -> {
					Embedding embedding = new Embedding(1);
					embedding.setMapping(null, vertex, 0, dataVertexIndexMap.get(vertex));
					return embedding;
				})
				.collect(Collectors.toList());
	}

	/**
	 * Compute the embeddings of an edge singleton pattern by scanning the data graph.
	 *
	 * @param edgeLabel The label of the singleton
	 */
	protected List<Embedding> computeEmbeddingsOfSingletonEdgePattern(String edgeLabel)
	{
		return data.getEdges()
				.filter(edge -> edge.getLabel().equals(edgeLabel))
				.map(edge -> {
					Embedding embedding = new Embedding(2);
					embedding.setMapping(null, edge.getSource(), 0, dataVertexIndexMap.get(edge.getSource()));
					embedding.setMapping(null, edge.getTarget(), 1, dataVertexIndexMap.get(edge.getTarget()));
					return embedding;
				})
				.collect(Collectors.toList());
	}

	/**
	 * @return The number of labels in the given graph (used in {@link #codeTableRowFromCandidate}).
	 */
	protected int graphLabelCount(Graph g)
	{
		return graphMDL.Utils.graphLabelCount(g);
	}

	/**
	 * @return The current code table. The best code table if called after {@link #computeBestCodeTable} has finished.
	 */
	public CodeTable getCodeTable()
	{
		return codeTable;
	}

	/**
	 * @return The rewritten graph generated by encoding the data with the code table given by {@link #getCodeTable()}.
	 */
	public RewrittenGraph getRewrittenGraph()
	{
		if (this.rewrittenGraph == null)
			this.rewrittenGraph = codeTable.apply(data, coverStrategy, true).get();
		return this.rewrittenGraph;
	}

	/**
	 * @return The number of code tables tested during the search for the best code table.
	 */
	public int getTestedCodeTablesCount()
	{
		return testedCodeTablesCount;
	}
}
