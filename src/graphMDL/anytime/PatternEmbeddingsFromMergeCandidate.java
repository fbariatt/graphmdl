package graphMDL.anytime;

import graph.Graph;
import patternMatching.Embedding;
import patternMatching.Mapping;
import utils.GeneralUtils;
import utils.SingletonStopwatchCollection;
import utils.tuples.Tuple2;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Compute embeddings of a pattern given by a {@link MergeCandidate}, by combining embeddings of the two patterns of the candidate
 */
public final class PatternEmbeddingsFromMergeCandidate
{
	private final MergeCandidate mergeCandidate;
	private final List<Embedding> embeddingsOfP1;
	private final List<Embedding> embeddingsOfP2;
	// List of ports for the two pattern, to make sure that iteration on ports is always performed in the same order
	private List<Integer> p1Ports;
	private List<Integer> p2Ports;
	// Embeddings of P2 grouped by which data vertices they map the port vertices
	private Map<DataVertexTuple, List<Embedding>> p2EmbeddingsByPortMapping;

	/**
	 * The constructor is private because the entrypoint of this class is supposed to be the static method {@link #joinEmbeddings(MergeCandidate, List, List, Optional)}.
	 * This class's instances are just created in that method to share attributes between related functions.
	 */
	private PatternEmbeddingsFromMergeCandidate(MergeCandidate mergeCandidate, List<Embedding> embeddingsOfP1, List<Embedding> embeddingsOfP2)
	{
		this.mergeCandidate = mergeCandidate;
		this.embeddingsOfP1 = embeddingsOfP1;
		this.embeddingsOfP2 = embeddingsOfP2;

		initializeAttributesCommonToAllGenerationMethods();
	}

	/**
	 * All generation method need some common preprocessing.
	 */
	private void initializeAttributesCommonToAllGenerationMethods()
	{
		// Creating list of ports for the two pattern, to make sure that iteration on ports is always performed in the same order
		p1Ports = new ArrayList<>(mergeCandidate.getPortTuples().size());
		p2Ports = new ArrayList<>(mergeCandidate.getPortTuples().size());
		for (Tuple2<Integer,Integer> portTuple : mergeCandidate.getPortTuples())
		{
			p1Ports.add(portTuple.first);
			p2Ports.add(portTuple.second);
		}
		p1Ports = Collections.unmodifiableList(p1Ports);
		p2Ports = Collections.unmodifiableList(p2Ports);

		// Grouping the embeddings of P2 based on which data vertices they map the port vertices
		p2EmbeddingsByPortMapping = new HashMap<>();
		for (Embedding embeddingOfP2 : embeddingsOfP2)
			p2EmbeddingsByPortMapping.computeIfAbsent(mappingOfPorts(p2Ports, embeddingOfP2), k -> new LinkedList<>()).add(embeddingOfP2);
		p2EmbeddingsByPortMapping = Collections.unmodifiableMap(p2EmbeddingsByPortMapping);
	}

	/**
	 * Create a {@link DataVertexTuple} for the ports of an embedding.
	 */
	private static DataVertexTuple mappingOfPorts(List<Integer> ports, Embedding embedding)
	{
		DataVertexTuple dataVertexTuple = new DataVertexTuple(ports.size());
		for (int i = 0; i < ports.size(); ++i)
		{
			dataVertexTuple.setDataVertex(i, embedding.getMapping(ports.get(i)).dataVertexIndex);
		}
		return dataVertexTuple;
	}

	/**
	 * Join the embeddings of the two patterns that compose a candidate to create embeddings of the candidate's pattern.
	 * Two embeddings are combined if the port nodes map to the same data vertex and the non-port nodes map to different data vertices.
	 *
	 * @param mergeCandidate The candidate for which the returned embeddings are computed.
	 * @param embeddingsOfP1 All the embeddings of the first pattern that compose the candidate.
	 * @param embeddingsOfP2 All the embeddings of the second pattern that compose the candidate.
	 * @param maxEmbeddings  The maximum number of embeddings that should be generated. If set, generation stops automatically when this amount of embeddings are reached.
	 *                       If set, generation is non-deterministic: embeddings are combined randomly and non-sequentially to increase expected diversity in the joined embeddings.
	 *                       Note that if not set, on some datasets the number of embeddings for certain pattern may explode (tens of millions have been observed on the Mondial dataset).
	 * @return The embeddings of the candidate's pattern. No particular order is ensured on the returned embeddings.
	 */
	public static List<Embedding> joinEmbeddings(MergeCandidate mergeCandidate, List<Embedding> embeddingsOfP1, List<Embedding> embeddingsOfP2, Optional<Integer> maxEmbeddings)
	{
		SingletonStopwatchCollection.resume("patternEmbeddingsFromMergeCandidate.joinEmbeddings");
		PatternEmbeddingsFromMergeCandidate obj = new PatternEmbeddingsFromMergeCandidate(mergeCandidate, embeddingsOfP1, embeddingsOfP2);
		List<Embedding> result;
		if (maxEmbeddings.isPresent())
			result = obj.combineEmbeddingsWithLimit(maxEmbeddings.get());
		else
			result = obj.combineEmbeddingsWithoutLimit();
		SingletonStopwatchCollection.stop("patternEmbeddingsFromMergeCandidate.joinEmbeddings");
		return result;
	}

	/**
	 * Join the embeddings of the two patterns that compose the candidate.
	 * This method does not pose any limit on the number of embeddings generated.
	 * <br/>
	 * This is the original method that was written for this class, when the patterns generated during the experiments did not have millions of embeddings.
	 */
	private LinkedList<Embedding> combineEmbeddingsWithoutLimit()
	{
		LinkedList<Embedding> result = new LinkedList<>();
		// Matching embeddings of P1 with embeddings of P2 that map the ports to the same data vertices
		for (Embedding embeddingOfP1 : embeddingsOfP1)
		{
			final List<Embedding> p2EmbeddingsThatMapPortsToSameDataVertices = p2EmbeddingsByPortMapping.getOrDefault(mappingOfPorts(p1Ports, embeddingOfP1), Collections.emptyList());
			for (Embedding embeddingOfP2 : p2EmbeddingsThatMapPortsToSameDataVertices)
			{
				// We combine the two embeddings only if the non-port vertices are mapped to different vertices in the data (to avoid the case where one pattern may be contained in another)
				if (nonPortsAreMappedToDifferentDataVertices(embeddingOfP1, embeddingOfP2))
				{
					result.add(mergeEmbeddings(embeddingOfP1, embeddingOfP2));
				}
			}
		}
		return result;
	}

	/**
	 * Join the embeddings of the two patterns that compose the candidate, but stop when a certain number of embeddings have been generated.
	 * This method does its best to generate embeddings in a way that is random and non-sequential, so that to ensure a certain variety in the generated embeddings.
	 * For example if combining (A,B,C) with (1,2,3), avoid generating only {(A,1), (A,2), (A,3)} and instead generate something like {(A,2), (B,1), (C,1)}.
	 */
	private LinkedList<Embedding> combineEmbeddingsWithLimit(int maxEmbeddings)
	{
		// We shuffle the list of P1 embeddings so that candidates with P1 do not always generated embeddings in the same order
		List<Embedding> embeddingsOfP1Copy = GeneralUtils.shuffledCopy(embeddingsOfP1);

		// For each embedding of P1, we make a list of the embeddings of p2 that can be combined with it
		List<List<Embedding>> embeddingsOfP2ToCombine = new ArrayList<>(embeddingsOfP1Copy.size());
		for (Embedding p1Embedding : embeddingsOfP1Copy)
		{
			final List<Embedding> correspondingEmbeddingsOfP2 = p2EmbeddingsByPortMapping.getOrDefault(mappingOfPorts(p1Ports, p1Embedding), Collections.emptyList());
			embeddingsOfP2ToCombine.add(GeneralUtils.shuffledCopy(correspondingEmbeddingsOfP2));
		}

		// We actually combine embeddings
		// In each iteration, we combine each embedding of P1 with the i-th embedding of P2 with which it can be combined
		LinkedList<Embedding> result = new LinkedList<>();
		int nbEmbeddingsFullyCombined = 0; // How many embeddings of P1 have been combined with all the embeddings of P2 with which they could be combined?
		for (int counter = 0; nbEmbeddingsFullyCombined < embeddingsOfP1Copy.size(); ++counter)
		{
			nbEmbeddingsFullyCombined = 0;
			// We iterate over the embedding of P1
			for (int p1EmbIndex = 0; p1EmbIndex < embeddingsOfP1Copy.size(); ++p1EmbIndex)
			{
				final Embedding p1Embedding = embeddingsOfP1Copy.get(p1EmbIndex);
				final List<Embedding> correspondingP2Embeddings = embeddingsOfP2ToCombine.get(p1EmbIndex);

				if (counter >= correspondingP2Embeddings.size()) // If this embedding of P1 has been combined with all P2 embeddings with which it could be combined
				{
					nbEmbeddingsFullyCombined++;
					continue; // We skip it
				}

				// We will try to combine this embedding of P1 with the i-th embedding of P2 with which it can be combined
				final Embedding p2Embedding = correspondingP2Embeddings.get(counter);

				// We combine the two embeddings if the non-port vertices are mapped to different vertices in the data
				// (this avoids the case where one pattern may be contained in another)
				if (nonPortsAreMappedToDifferentDataVertices(p1Embedding, p2Embedding))
					result.add(mergeEmbeddings(p1Embedding, p2Embedding));

				// We check if we reached the limit
				if (result.size() >= maxEmbeddings) // We have generated as many embeddings as we were told
					return result; // We stop here
			}
		}
		// We combined all embeddings of P1 with their corresponding embeddings of P2
		return result;
	}

	/**
	 * Verify that all non-port vertices are mapped to different data vertices by the two given embeddings.
	 */
	private boolean nonPortsAreMappedToDifferentDataVertices(Embedding embeddingOfP1, Embedding embeddingOfP2)
	{
		// Which data vertices are used by the embedding of P1?
		Set<Integer> dataVerticesUsedByP1 = Arrays.stream(embeddingOfP1.getMappingsByIndex())
				.map(mapping -> mapping.dataVertexIndex)
				.collect(Collectors.toSet());
		// Testing if any non-port vertex in the embedding of P2 uses any of P1 embedding's data vertices
		Mapping[] embeddingOfP2Mappings = embeddingOfP2.getMappingsByIndex();
		Set<Integer> p2Ports = mergeCandidate.getPortTuples().stream().map(tuple -> tuple.second).collect(Collectors.toSet());
		for (int v = 0; v < embeddingOfP2Mappings.length; ++v)
		{
			if (p2Ports.contains(v)) // If v is a port of P2
				continue; // We skip it. We only verify non-port vertices (port vertices should obviously be mapped to the same data vertex)

			if (dataVerticesUsedByP1.contains(embeddingOfP2Mappings[v].dataVertexIndex))
				return false;
		}
		return true;
	}

	/**
	 * Create a candidate's embedding by merging the two given embeddings.
	 *
	 * @implSpec This assumes that {@link MergeCandidate#getPatternInfo()} creates the pattern so that first all vertices of P1 are included,
	 * then all non-port vertices of P2 are included in following vertex numbers.
	 */
	private Embedding mergeEmbeddings(Embedding embeddingOfP1, Embedding embeddingOfP2)
	{
		final int sizeOfP1 = mergeCandidate.getP1().getStructure().getVertexCount();
		final int sizeOfP2 = mergeCandidate.getP2().getStructure().getVertexCount();
		final Graph mergedPattern = mergeCandidate.getPatternInfo().getStructure();
		final int sizeOfMergedPattern = mergedPattern.getVertexCount();
		assert sizeOfMergedPattern == sizeOfP1 + sizeOfP2 - mergeCandidate.getPortTuples().size(); // Just to verify that no hidden bugs appeared

		Embedding result = new Embedding(sizeOfMergedPattern);
		for (int v = 0; v < sizeOfP1; ++v)
		{
			result.setMapping(mergedPattern.getVertex(v), embeddingOfP1.getMapping(v).dataVertex, v, embeddingOfP1.getMapping(v).dataVertexIndex);
		}
		int mergedPatternVertex = sizeOfP1; // The vertex of the merged pattern to which the P2 vertices correspond
		Set<Integer> p2Ports = mergeCandidate.getPortTuples().stream().map(tuple -> tuple.second).collect(Collectors.toSet());
		for (int v = 0; v < sizeOfP2; ++v)
		{
			if (p2Ports.contains(v)) // If v is a port
				continue; // Its embedding has already been set while processing P1

			result.setMapping(mergedPattern.getVertex(mergedPatternVertex), embeddingOfP2.getMapping(v).dataVertex, mergedPatternVertex, embeddingOfP2.getMapping(v).dataVertexIndex);
			mergedPatternVertex++;
		}
		return result;
	}

	/**
	 * An ordered tuple of data vertex numbers.
	 * To be used as map key to group embeddings based on the data vertices they map the port vertices of the pattern(s)
	 */
	private static class DataVertexTuple
	{
		private int[] dataVertices;

		public DataVertexTuple(int size)
		{
			this.dataVertices = new int[size];
		}

		/**
		 * @param portIndex  Index of the port in the list of all ports
		 * @param dataVertex The data vertex to which the port is mapped
		 */
		public void setDataVertex(int portIndex, int dataVertex)
		{
			dataVertices[portIndex] = dataVertex;
		}

		@Override
		public boolean equals(Object o)
		{
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			DataVertexTuple that = (DataVertexTuple) o;
			return Arrays.equals(dataVertices, that.dataVertices);
		}

		@Override
		public int hashCode()
		{
			return Arrays.hashCode(dataVertices);
		}
	}
}
