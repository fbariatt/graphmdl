package graphMDL.anytime;

import graph.Graph;
import graphMDL.*;
import graphMDL.simpleGraphs.directed.SimpleDirectedCodeTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedIndependentVertexAndEdgeST;

import java.util.Comparator;

/**
 * Implementation of {@link AnytimeGraphMDL} handling simple directed graphs.
 */
public class SimpleDirectedAnytimeGraphMDL extends AnytimeGraphMDL
{
	public SimpleDirectedAnytimeGraphMDL(Graph data, Comparator<CodeTableRow> codeTableOrder, PatternConstraints patternConstraints, CoverStrategy coverStrategy)
	{
		super(data, codeTableOrder, patternConstraints, coverStrategy);

		if (!data.isEffectivelySimple())
			throw new IllegalArgumentException("Expected the data to be a simple graph but it is not.");
	}

	@Override
	protected StandardTable computeStandardTable()
	{
		return new SimpleDirectedIndependentVertexAndEdgeST(data);
	}

	@Override
	protected CodeTable createEmptyCodeTable()
	{
		return new SimpleDirectedCodeTable(
				(SimpleDirectedIndependentVertexAndEdgeST) this.st,
				this.codeTableOrder
		);
	}
}
