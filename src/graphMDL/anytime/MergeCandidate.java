package graphMDL.anytime;

import graph.Graph;
import graph.GraphFactory;
import graph.Vertex;
import graph.automorphisms.Automorphism;
import graph.automorphisms.AutomorphismInfo;
import graph.automorphisms.certificates.GraphCertificate;
import graphMDL.PatternInfo;
import graphMDL.rewrittenGraphs.EmbeddingVertex;
import utils.GeneralUtils;
import utils.SingletonStopwatchCollection;
import utils.tuples.Tuple2;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class represents embeddings that are neighbours in the rewritten graph, and therefore could be merged to create a new pattern.
 */
public class MergeCandidate
{
	private final PatternInfo P1;
	private final PatternInfo P2;
	private final Set<Tuple2<Integer, Integer>> portTuples;

	private Set<EmbeddingVertex> instancesOfP1;
	private Set<EmbeddingVertex> instancesOfP2;
	private int exclusivePortCount;
	private Boolean isSymmetric = null; // Lazy result of isSymmetric
	private Integer usageEstimation = null; // Lazy result of getUsageEstimation
	private PatternInfo patternInfo = null; // Lazy result of getPatternInfo
	// Attributes that can be stored in the candidate, but that this class won't modify
	private boolean hasAlreadyBeenTested = false;
	private Double patternSTEncodingDL = null;

	/**
	 * Create a new candidate having the two given patterns, with the given sets of port tuples.
	 * The candidate is not sorted: the given P1 will be stored as P1, independently on how the two canonical names compare.
	 */
	public MergeCandidate(PatternInfo p1, PatternInfo p2, Set<Tuple2<Integer, Integer>> portTuples)
	{
		this.P1 = p1;
		this.P2 = p2;
		if (portTuples.size() == 0)
			throw new IllegalArgumentException("A candidate must contain at least one port tuple");
		this.portTuples = portTuples;
		this.instancesOfP1 = new HashSet<>();
		this.instancesOfP2 = new HashSet<>();
		this.exclusivePortCount = 0;
	}

	/**
	 * @return A candidate that is the reverse of this one, e.g. if this candidate is /Pa,Pb,{(a,b),(c,d)}/ returns /Pb,Pa,{(b,a),(d,c)}/
	 */
	public MergeCandidate reverse()
	{
		return new MergeCandidate(P2, P1, portTuples.stream().map(Tuple2::reverse).collect(Collectors.toSet()));
	}

	/**
	 * Compute whether this candidate is in its canonical form or not.
	 * A candidate is in its canonical form iff:
	 * <ul>
	 *     <li>If the two patterns are different (i.e. a /P1,P2,{...}/ candidate), then P1 < P2</li>
	 *     <li>If the two patterns are the same (i.e. a /P1,P1,{...}/ candidate), then the set of port tuples is smaller than its inverse.</li>
	 * </ul>
	 * <p>
	 * If a candidate is not canonical, then its reverse will be.<br/>
	 * A symmetric candidate is always canonical.
	 */
	public boolean shouldBeReversed()
	{
		int patternsCompare = p1CanonicalName().compareTo(p2CanonicalName());
		if (patternsCompare == 0) // This is a <P1,P1,{...}> candidate
		{
			// Between this and its reverse, the canonical form is the one where port tuples compare lexicographically smaller
			return GeneralUtils.lexicographicCompare(portTuples.stream().sorted().iterator(), portTuples.stream().map(Tuple2::reverse).sorted().iterator()) <= 0;
		}
		else // This is a <P1, P2, {...}> candidate with P1 =/= P2.
			return patternsCompare < 0; // The canonical form is with P1 < P2
	}

	/**
	 * @return Pattern P1 of this candidate.
	 */
	public PatternInfo getP1()
	{
		return P1;
	}

	/**
	 * @return Pattern P2 of this candidate.
	 */
	public PatternInfo getP2()
	{
		return P2;
	}

	/**
	 * @return The port tuples of this candidate.
	 */
	public Set<Tuple2<Integer, Integer>> getPortTuples()
	{
		return portTuples;
	}

	/**
	 * @return The canonical name of pattern P1 of this candidate. Shortcut method for <pre>getP1().getAutomorphismInfo().getCanonicalName()</pre>
	 * @see AutomorphismInfo#getCanonicalName()
	 */
	public GraphCertificate p1CanonicalName()
	{
		return this.P1.getAutomorphismInfo().getCanonicalName();
	}

	/**
	 * @return The canonical name of pattern P2 of this candidate.  Shortcut method for <pre>getP2().getAutomorphismInfo().getCanonicalName()</pre>
	 * @see AutomorphismInfo#getCanonicalName()
	 */
	public GraphCertificate p2CanonicalName()
	{
		return this.P2.getAutomorphismInfo().getCanonicalName();
	}

	/**
	 * @return Whether this candidate is <em>exactly equal</em> to another one, meaning that P1 = P1', P2 = P2' and the sets of port tuples are the same.
	 * Automorphisms of the two patterns are not taken into account, so even if this method returns false the two candidates might actually be equivalent.
	 * This is meant as a quick equality test, since processing automorphisms can take a long time.
	 */
	public boolean isExactlyEqual(MergeCandidate other)
	{
		return this.p1CanonicalName().equals(other.p1CanonicalName())
				&& this.p2CanonicalName().equals(other.p2CanonicalName())
				&& this.portTuples.equals(other.portTuples);
	}

	/**
	 * @return Whether this candidate is symmetric, i.e. an instance of P1 may count as an instance of P2 as well.
	 * Note that this performs automorphism computations.
	 */
	public boolean isSymmetric()
	{
		if (isSymmetric == null)
			isSymmetric = p1CanonicalName().equals(p2CanonicalName()) && getPatternInfo().getAutomorphismInfo().getCanonicalName().equals(reverse().getPatternInfo().getAutomorphismInfo().getCanonicalName());
		return isSymmetric;
	}

	/**
	 * Add an instance of this candidate. An instance is composed of a EmbeddingVertex for each pattern.
	 *
	 * @see #addInstances(Collection, Collection)
	 */
	public void addInstance(EmbeddingVertex instanceOfP1, EmbeddingVertex instanceOfP2)
	{
		instancesOfP1.add(instanceOfP1);
		instancesOfP2.add(instanceOfP2);
		usageEstimation = null; // Invalidate lazy result
	}

	/**
	 * Add multiple instances of this candidate.
	 *
	 * @see #addInstance(EmbeddingVertex, EmbeddingVertex)
	 */
	public void addInstances(Collection<EmbeddingVertex> instancesOfP1, Collection<EmbeddingVertex> instancesOfP2)
	{
		this.instancesOfP1.addAll(instancesOfP1);
		this.instancesOfP2.addAll(instancesOfP2);
		usageEstimation = null; // Invalidate lazy result
	}

	public Set<EmbeddingVertex> getInstancesOfP1()
	{
		return Collections.unmodifiableSet(instancesOfP1);
	}

	public Set<EmbeddingVertex> getInstancesOfP2()
	{
		return Collections.unmodifiableSet(instancesOfP2);
	}

	public void incrementExclusivePortCount()
	{
		exclusivePortCount++;
	}

	public void setExclusivePortCount(int value)
	{
		exclusivePortCount = value;
	}

	public int getExclusivePortCount()
	{
		return exclusivePortCount;
	}


	/**
	 * Clear all instances-related information from this candidate, only leaving intact information related to the candidate's structure.
	 * Useful if a candidate is to be re-used.
	 */
	public void clearInstances()
	{
		this.instancesOfP1.clear();
		this.instancesOfP2.clear();
		this.usageEstimation = null;
		this.exclusivePortCount = 0;
	}

	/**
	 * @return The estimation of the usage of this candidate, i.e. how many instances of the resulting pattern can be used for covering the data graph
	 */
	public int getUsageEstimation()
	{
		if (usageEstimation == null)
		{
			SingletonStopwatchCollection.resume("mergeCandidate.usageEstimation");
			usageEstimation = computeUsageEstimation();
			SingletonStopwatchCollection.stop("mergeCandidate.usageEstimation");
		}
		return usageEstimation;
	}

	/**
	 * Actual computation of the usage estimation. See {@link #getUsageEstimation()}.
	 */
	private int computeUsageEstimation()
	{
		if (this.isSymmetric())
		{
			// The candidate is symmetric, instances of P1 count as instances of P2 and inversely
			// So we aggregate all instances in one of the two sets
			instancesOfP1.addAll(instancesOfP2);
			instancesOfP2.clear();
			return instancesOfP1.size() / 2; // At maximum half of these instances will be covered by the new pattern
		}
		else
		{
			// Patterns with only one vertex will not generate cover conflicts, so they are not limiting for the number of instances that a candidate could cover
			final boolean p1HasOnlyOneVertex = P1.getStructure().getVertexCount() == 1;
			final boolean p2HasOnlyOneVertex = P2.getStructure().getVertexCount() == 1;

			if (p1HasOnlyOneVertex && p2HasOnlyOneVertex) // Both patterns have only one vertex
				return instancesOfP1.size(); // We return the size of either sets
			if (p1HasOnlyOneVertex)
				return instancesOfP2.size();
			if (p2HasOnlyOneVertex)
				return instancesOfP1.size();
			return Math.min(instancesOfP1.size(), instancesOfP2.size());
		}
	}

	/**
	 * Return (or create) the pattern obtained by merging the patterns of this candidate on the ports of this candidate.
	 * The resulting pattern will first contain all vertices of P1, with the same numbers as in P1, then all non-port vertices of P2
	 * on the following numbers. Port vertices are merged, meaning that they will have the union of the two label sets and ede sets.
	 * The returned PatternInfo will contain only <em>base automorphisms</em> (see {@link PatternInfo#createComputingBaseAutomorphisms(Graph)}) in order to
	 * avoid runtime and memory explosions for complex patterns that are observed in experiments.
	 */
	public PatternInfo getPatternInfo()
	{
		if (patternInfo == null)
		{
			SingletonStopwatchCollection.resume("mergeCandidate.patternCreation");
			// We first add the whole P1 pattern
			Graph pattern = GraphFactory.copy(P1.getStructure());

			// Adding P2 vertices
			Map<Integer, Integer> p2PortsToP1Ports = portTuples.stream().collect(Collectors.toMap(tuple -> tuple.second, tuple -> tuple.first));
			Map<Vertex, Vertex> p2VerticesToPatternVertices = new HashMap<>(); // Map to associate P2 vertices to vertices in the new pattern
			for (int v = 0; v < P2.getStructure().getVertexCount(); ++v)
			{
				Vertex patternVertex = null;
				if (p2PortsToP1Ports.containsKey(v)) // If the vertex of P2 is a port
					patternVertex = pattern.getVertex(p2PortsToP1Ports.get(v)); // We use the vertex that was already created from P1
				else // The vertex of p2 is not a port
					patternVertex = pattern.addVertex(); // We create a new vertex
				P2.getStructure().getVertex(v).getLabels().keySet().forEach(patternVertex::addLabel); // We add all the vertex labels that this vertex has in P2
				p2VerticesToPatternVertices.put(P2.getStructure().getVertex(v), patternVertex);
			}

			// Adding P2 edges
			P2.getStructure().getEdges().forEach(edge ->
					pattern.addEdge(
							p2VerticesToPatternVertices.get(edge.getSource()),
							p2VerticesToPatternVertices.get(edge.getTarget()),
							edge.getLabel()
					)
			);

			// Computing automorphisms
			patternInfo = PatternInfo.createComputingBaseAutomorphisms(pattern);
			SingletonStopwatchCollection.stop("mergeCandidate.patternCreation");
		}
		return patternInfo;
	}

	/**
	 * Whether this candidate has already been tested at least once before.
	 * Must be set with {@link #setHasAlreadyBeenTested} (defaults to false).
	 * If candidates can be reused, this may be useful to mark a candidate so that to favour exploration of new candidates
	 * instead of always exploring the same ones over and over.
	 */
	public boolean hasAlreadyBeenTested()
	{
		return hasAlreadyBeenTested;
	}

	/**
	 * See {@link #hasAlreadyBeenTested()}.
	 */
	public void setHasAlreadyBeenTested(boolean hasAlreadyBeenTested)
	{
		this.hasAlreadyBeenTested = hasAlreadyBeenTested;
	}

	/**
	 * The description length of the pattern resulting from this candidate, when encoded with the Standard Table.
	 * This method does not actually perform the computation, the value must have been already set by {@link #setPatternSTEncodingDL},
	 * if no value has been set, null will be returned.
	 * This is because this class should be independent of the data graph or the way of computing ST.
	 * Also see {@link #getPatternInfo()}.
	 */
	public Double getPatternSTEncodingDL()
	{
		return patternSTEncodingDL;
	}

	/**
	 * Set the description length of the pattern resulting from this candidate, when encoded with the Standard Table.
	 * @see #getPatternSTEncodingDL()
	 * @see #getPatternInfo()
	 */
	public void setPatternSTEncodingDL(Double patternSTEncodingDL)
	{
		this.patternSTEncodingDL = patternSTEncodingDL;
	}
}
