package graphMDL.anytime;

import graph.Graph;
import graphMDL.*;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedCodeTable;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedIndependentVertexAndEdgeST;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedUtils;
import patternMatching.Embedding;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link AnytimeGraphMDL} handling simple undirected graphs.
 */
public class SimpleUndirectedAnytimeGraphMDL extends AnytimeGraphMDL
{
	public SimpleUndirectedAnytimeGraphMDL(Graph data, Comparator<CodeTableRow> codeTableOrder, PatternConstraints patternConstraints, CoverStrategy coverStrategy)
	{
		super(data, codeTableOrder, patternConstraints, coverStrategy);

		if (!data.isEffectivelySimple())
			throw new IllegalArgumentException("Expected the data to be a simple graph but it is not.");
	}

	@Override
	protected StandardTable computeStandardTable()
	{
		return new SimpleUndirectedIndependentVertexAndEdgeST(data);
	}

	@Override
	protected CodeTable createEmptyCodeTable()
	{
		return new SimpleUndirectedCodeTable(
				(SimpleUndirectedIndependentVertexAndEdgeST) this.st,
				this.codeTableOrder
		);
	}

	@Override
	protected Optional<String> isVertexSingleton(Graph pattern)
	{
		if (SimpleUndirectedUtils.isVertexSingleton(pattern))
			return Optional.of(pattern.getVertex(0).getLabels().keySet().iterator().next());
		else
			return Optional.empty();
	}

	@Override
	protected Optional<String> isEdgeSingleton(Graph pattern)
	{
		if (SimpleUndirectedUtils.isEdgeSingleton(pattern))
			return Optional.of(pattern.getEdges().iterator().next().getLabel());
		else
			return Optional.empty();
	}

	@Override
	protected List<Embedding> computeEmbeddingsOfSingletonVertexPattern(String vertexLabel)
	{
		// The directed version works as well for undirected graphs
		return super.computeEmbeddingsOfSingletonVertexPattern(vertexLabel);
	}

	@Override
	protected List<Embedding> computeEmbeddingsOfSingletonEdgePattern(String edgeLabel)
	{
		// The directed version works as well for undirected graphs: an undirected edge has two embeddings,
		// and the directed version will parse the edges in both direction
		return super.computeEmbeddingsOfSingletonEdgePattern(edgeLabel);
	}

	@Override
	protected int graphLabelCount(Graph g)
	{
		return SimpleUndirectedUtils.graphLabelCount(g);
	}
}
