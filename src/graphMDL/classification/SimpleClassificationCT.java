package graphMDL.classification;

import graph.Graph;
import graph.printers.GraphToRDF;
import graphMDL.CodeTableRow;
import graphMDL.ScanDataCoverStrategy;
import graphMDL.UniversalInteger;
import graphMDL.simpleGraphs.SimpleCodeTable;
import org.apache.jena.rdf.model.Model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * A class that computes the description length of a graph in a way that is suitable for classification tasks.
 * This class mimics the description length computation used by {@link graphMDL.simpleGraphs.SimpleCodeTable}.
 */
public class SimpleClassificationCT
{
	// The code table on which the classification is performed
	private final SimpleCodeTable baseCT;
	// Usage of the ports of the non-singleton patterns in the base ct.
	private final Map<CodeTableRow, Map<Integer, Integer>> nonSingletonPortUsagesOnOriginalData;
	// Code lengths used in the classification task
	private final Map<CodeTableRow, Double> nonSingletonsCodes;
	private final Map<CodeTableRow, Map<Integer, Double>> nonSingletonsPortCodes;
	private final Map<String, Double> vertexSingletonCodes;
	private final Map<String, Double> edgeSingletonCodes;

	/**
	 * Create a code table that is suitable to compute the L(D|M) for a classification task.
	 *
	 * @param baseCT               The code table on which this classification code table is based. The code table must have been previously applied on the data graph.
	 *                             <strong>The code table is not copied, and this object modifies it</strong>. You should not use that code table or its rows afterwards.
	 * @param vertexLabelsAlphabet Set of all vertex labels that the classification code table will be able to handle.
	 * @param edgeLabelsAlphabet   Set of all edge labels that the classification code table will be able to handle.
	 */
	public SimpleClassificationCT(SimpleCodeTable baseCT, Set<String> vertexLabelsAlphabet, Set<String> edgeLabelsAlphabet)
	{
		this.baseCT = baseCT;

		// Compute pattern codes used for classification: we add 1 to the usage of each pattern, so that all singletons in the alphabet have a usage of at least 1.
		// We then use these new usages to compute codes for each element.
		// In this way, we can be sure that all singletons in the alphabet will have a code, meaning that we can surely encode any graph defined on that alphabet.
		int newPatternUsageSum = baseCT.getRows().stream().mapToInt(row -> row.getUsage() + 1).sum()
				+ vertexLabelsAlphabet.stream().mapToInt(label -> baseCT.getSingletonVertexUsages().getOrDefault(label, 0) + 1).sum()
				+ edgeLabelsAlphabet.stream().mapToInt(label -> baseCT.getSingletonEdgeUsages().getOrDefault(label, 0) + 1).sum();
		nonSingletonsCodes = new HashMap<>();
		for (CodeTableRow row : baseCT.getRows())
		{
			final int newUsage = row.getUsage() + 1;
			nonSingletonsCodes.put(row, -graphMDL.Utils.log2((double) newUsage / newPatternUsageSum));
		}
		vertexSingletonCodes = new HashMap<>();
		for (String vertexLabel : vertexLabelsAlphabet)
		{
			final int newUsage = baseCT.getSingletonVertexUsages().getOrDefault(vertexLabel, 0) + 1;
			vertexSingletonCodes.put(vertexLabel, -graphMDL.Utils.log2((double) newUsage / newPatternUsageSum));
		}
		edgeSingletonCodes = new HashMap<>();
		for (String edgeLabel : edgeLabelsAlphabet)
		{
			final int newUsage = baseCT.getSingletonEdgeUsages().getOrDefault(edgeLabel, 0) + 1;
			edgeSingletonCodes.put(edgeLabel, -graphMDL.Utils.log2((double) newUsage / newPatternUsageSum));
		}

		// Similarly to pattern codes, we compute port codes used for classification: for each (non-singleton) pattern, we add 1 to the usage of each of its ports.
		// We then use the new usages to compute codes for all the ports of the pattern.
		// In this way, all ports of a pattern will have a code if they need to be used for covering the element to be classified.
		nonSingletonsPortCodes = new HashMap<>();
		nonSingletonPortUsagesOnOriginalData = new HashMap<>();
		for (CodeTableRow row : baseCT.getRows())
		{
			final Map<Integer, Integer> originalPortUsages = new HashMap<>(row.getPortUsages());

			nonSingletonPortUsagesOnOriginalData.put(row, originalPortUsages);

			int newUsageSum = IntStream.range(0, row.getPatternGraph().getVertexCount())
					.map(v -> originalPortUsages.getOrDefault(v, 0) + 1)
					.sum();

			Map<Integer, Double> newCodes = new HashMap<>();
			for (int v = 0; v < row.getPatternGraph().getVertexCount(); ++v)
			{
				final int newUsage = originalPortUsages.getOrDefault(v, 0) + 1;
				newCodes.put(v, -graphMDL.Utils.log2((double) newUsage / newUsageSum));
			}
			nonSingletonsPortCodes.put(row, newCodes);
		}

		// "Prime" code table rows with the usages of ports on the original data, so that automorphisms will favour those ports (which therefore have smaller codes)
		for (CodeTableRow row : baseCT.getRows())
		{
			row.setPrepareCTRowForCoverComputation(this::primeCodeTableRowPortUsages);
		}
	}

	/**
	 * Intended to be used as argument for {@link CodeTableRow#setPrepareCTRowForCoverComputation}.
	 * Applies the default reset function, but then sets port usages to be the port usages of the original data.
	 * In this way, we expect automorphisms to favour the ports that were most used to cover the original data, and therefore have the smallest codes.
	 */
	private void primeCodeTableRowPortUsages(CodeTableRow row)
	{
		CodeTableRow.resetAllCoverDependantAttributes(row);
		row.getPortUsages().putAll(nonSingletonPortUsagesOnOriginalData.get(row));
	}

	/**
	 * @return The description length of encoding the given graph with this code table.
	 * Note: the description length is intended for a classification task and may be different from the description length given by a {@link graphMDL.CodeTable}
	 */
	public double computeGraphDescriptionLength(Graph g)
	{
		baseCT.computeCoversAndPorts(g, new ScanDataCoverStrategy(0), g.getMarker() + 1, false);

		// Since we "primed" the port usages, we need to undo that to compute a correct DL
		for (CodeTableRow row : baseCT.getRows())
		{
			final Map<Integer, Integer> originalUsagesForRow = nonSingletonPortUsagesOnOriginalData.get(row);
			row.getPortUsages().replaceAll((port, usage) -> usage - originalUsagesForRow.getOrDefault(port, 0));
		}

		// Description length computation.
		// This mimics the description length formula in the SimpleCodeTable class, expects for the bits that change because
		// we add +1 usage to all elements.
		double descriptionLength = 0.0;

		double logOfNumberOfPortVerticesInRewrittenGraph = graphMDL.Utils.log2(baseCT.getRewrittenGraphPortCount());

		// Port count
		descriptionLength += UniversalInteger.eliasDeltaWith0(baseCT.getRewrittenGraphPortCount());

		// Non-singletons
		for (CodeTableRow row : baseCT.getRows())
		{
			if (row.getUsage() == 0)
				continue;

			final int patternUsage = row.getUsage();
			final Double patternCode = nonSingletonsCodes.get(row);
			final int patternMaximumPortCount = row.getPatternGraph().getVertexCount(); // Since we assigned a code to each port, the pattern has possibly as many ports as it has vertices

			descriptionLength += patternCode * patternUsage;
			descriptionLength += graphMDL.Utils.log2(patternMaximumPortCount + 1) * patternUsage;

			for (Integer port : row.getPortUsages().keySet())
			{
				final Integer portUsage = row.getPortUsages().get(port);
				final Double portCode = nonSingletonsPortCodes.get(row).get(port);

				if (portUsage == 0) // It may be 0 because of port usage "priming"
					continue;

				if (portCode == null) // If a code has not been assigned to this pattern (which should never happen)
				{
					throw new RuntimeException("ClassificationCT: impossible to encode the given graph with the code table: a port does not have an associated code");
				}

				descriptionLength += logOfNumberOfPortVerticesInRewrittenGraph * portUsage;
				descriptionLength += portCode * portUsage;
			}
		}

		// Vertex singletons
		for (String label : baseCT.getSingletonVertexUsages().keySet())
		{
			final Integer singletonUsage = baseCT.getSingletonVertexUsages().get(label);
			final Double singletonCode = vertexSingletonCodes.get(label);

			descriptionLength += singletonCode * singletonUsage;
			descriptionLength += singletonUsage; // == log(2) * singletonUsage. Number of edges of each embedding vertex
			descriptionLength += logOfNumberOfPortVerticesInRewrittenGraph * singletonUsage; // Edge targets
			// Edge labels amount to 0 bit each (always one port)
		}

		// Edge singletons
		for (String label : baseCT.getSingletonEdgeUsages().keySet())
		{
			final Integer singletonUsage = baseCT.getSingletonEdgeUsages().get(label);
			final Double singletonCode = edgeSingletonCodes.get(label);

			descriptionLength += singletonCode * singletonUsage; // Labels of embedding vertices
			descriptionLength += graphMDL.Utils.log2(3) * singletonUsage; // Number of edges of each embedding edge
			descriptionLength += logOfNumberOfPortVerticesInRewrittenGraph * 2 * singletonUsage; // Edge targets (every embedding has two ports)
			descriptionLength += 2 * singletonUsage; // Edge labels (each port has a code of 1 bit, every embedding has two ports)
		}

		return descriptionLength;
	}

}
