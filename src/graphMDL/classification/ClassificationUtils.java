package graphMDL.classification;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class ClassificationUtils
{
	/**
	 * Load an alphabet (i.e. a set of labels) from a file.
	 * Lines beginning with # in the file are ignored
	 *
	 * @param filename The path to the file from which to load the alphabet
	 * @throws FileNotFoundException if the file does not exist
	 */
	public static Set<String> loadAlphabet(String filename) throws FileNotFoundException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		return reader.lines()
				.filter(line -> line.length() > 0)
				.filter(line -> line.charAt(0) != '#')
				.collect(Collectors.toSet());
	}

	/**
	 * Given a list of class names, return the one associated with the given class.
	 * If the list of names is null, return the class number as string
	 */
	public static String getDisplayNameForClass(List<String> classNames, int classNumber)
	{
		return classNames == null ? Integer.toString(classNumber) : classNames.get(classNumber);
	}
}
