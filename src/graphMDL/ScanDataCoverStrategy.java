package graphMDL;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import org.apache.log4j.Logger;
import patternMatching.Embedding;
import patternMatching.Mapping;
import utils.FindFirstMax;
import utils.GeneralUtils;
import utils.SingletonStopwatchCollection;
import utils.Stopwatch;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * In order to compute the cover embeddings of a code table row, scan the data graph looking for uncovered parts of the graph that are possible embeddings of the pattern.
 * Allow the user to specify a maximum time for the cover, so that the search will stop after that much time has passed.
 * In our tests this was the best strategy when patterns had a large number of embeddings, as it does not need to compute and store *all*
 * possible embeddings.
 */
public class ScanDataCoverStrategy implements CoverStrategy
{
	// Strategy parameters
	private final int coverTimeoutMs;

	// Values used during cover computation, to avoid need to pass them around sub-functions
	private CodeTableRow row = null;
	private Map<Vertex, Integer> patternVertexIndexMap = null;
	private Graph data = null;
	private Map<Vertex, Integer> dataVertexIndexMap = null;
	private StandardTable standardTable = null;
	private Integer elementDescribedMarker = null;
	private Stopwatch coverDuration = null;

	/**
	 * @param coverTimeoutMs Stop the cover search after this much time (in milliseconds) has passed. Set to 0 or lower to disable.
	 */
	public ScanDataCoverStrategy(int coverTimeoutMs)
	{
		this.coverTimeoutMs = coverTimeoutMs;
	}

	@Override
	public boolean needsEmbeddingsSetOnCTRow()
	{
		return false;
	}

	/**
	 * See {@link CoverStrategy#computeCoverEmbeddings}.
	 * <br/>
	 * This method uses an arbitrary total ordering to break ties on equivalent edges, to make the computation deterministic.
	 */
	@Override
	public List<Embedding> computeCoverEmbeddings(CodeTableRow row, Graph data, StandardTable standardTable, int elementDescribedMarker)
	{
		// We throw an exception if embeddings are set on the code table row. This is not really a problem, but it will avoid useless computation if the embeddings are set
		// by error. In case this poses a problem to some usage cases, this check could be eliminated.
		if (row.getEmbeddings() != null)
			throw new RuntimeException("Embeddings are set on the code table row even though this strategy does not need them. This exception is thrown in order to avoid useless computation.");

		SingletonStopwatchCollection.resume("scanDataCoverStrategy.computeCoverEmbeddings");

		this.row = row;
		this.patternVertexIndexMap = row.getPatternGraph().getVertexIndexMap();
		this.data = data;
		this.dataVertexIndexMap = data.getVertexIndexMap();
		this.standardTable = standardTable;
		this.elementDescribedMarker = elementDescribedMarker;
		this.coverDuration = new Stopwatch();
		this.coverDuration.resetAndStart();

		List<Embedding> coverEmbeddings;
		if (row.getPatternGraph().getEdges().findAny().isPresent()) // Pattern has at least one edge
			coverEmbeddings = computeCoverEmbeddingsForPatternWithEdges();
		else
			coverEmbeddings = computeCoverEmbeddingsForPatternWithNoEdges();

		// Resetting values of attributes only used during the computation (in order to potentially free resources, and also to be sure that they are only set when needed)
		this.row = null;
		this.patternVertexIndexMap = null;
		this.data = null;
		this.dataVertexIndexMap = null;
		this.standardTable = null;
		this.elementDescribedMarker = null;
		this.coverDuration = null;

		SingletonStopwatchCollection.stop("scanDataCoverStrategy.computeCoverEmbeddings");
		return coverEmbeddings;
	}

	/**
	 * Compute the cover embeddings if the row's pattern has only one vertex (with possibly multiple labels) and no edges.
	 * <br/>
	 * The parts of the data corresponding to the returned embeddings are marked as described.
	 */
	private List<Embedding> computeCoverEmbeddingsForPatternWithNoEdges()
	{
		if (row.getPatternGraph().getVertexCount() != 1)
			throw new IllegalArgumentException(String.format("Pattern with no edges but more than one vertex (i.e. unconnected graph): got %d vertices", row.getPatternGraph().getVertexCount()));

		final Vertex patternVertex = row.getPatternGraph().getVertex(0);
		return data.getVertices() // No need to sort vertices to be deterministic, since one vertex occurrence can never overlap with another
				.filter(dataVertex -> dataVertex.getLabels().keySet().containsAll(patternVertex.getLabels().keySet())) // Finding data vertices that have all labels of the pattern vertex
				.filter(dataVertex -> patternVertex.getLabels().keySet().stream() // At least one label that is present in this pattern is not described (i.e. this occurrence would describe something)
						.anyMatch(label -> dataVertex.getLabels().get(label).getMarker() != elementDescribedMarker)
				)
				.map(dataVertex -> {
					Embedding embedding = new Embedding(1);
					embedding.setMapping(patternVertex, dataVertex, 0, dataIndex(dataVertex));
					return embedding;
				})
				.peek(embedding -> Utils.markEmbedding(row.getPatternGraph(), embedding, data, elementDescribedMarker))
				.collect(Collectors.toList());
	}

	/**
	 * Compute the cover embeddings if the row's pattern has at least one edge.
	 * This method assumes that the row's pattern is a <em>connected</em> graph.
	 * <br/>
	 * The parts of the data corresponding to the returned embeddings are marked as described.
	 */
	private List<Embedding> computeCoverEmbeddingsForPatternWithEdges()
	{
		List<Embedding> coverEmbeddings = new ArrayList<>();

		// We start by matching the pattern edge that has the least occurrences in the data (i.e. the longest code in ST)
		final Edge initialPatternEdge = FindFirstMax.find(
				row.getPatternGraph().getEdges(),
				Comparator.<Edge>comparingDouble(edge -> standardTable.getEdgeLabelsCodeLengths().get(edge.getLabel())) // Edges with the longest code will be chosen first
						.thenComparing(this.patternEdgeSorter().reversed()) // Ties are broken with a total ordering on pattern edges, just to make this algorithm deterministic (comparator reversed because we are looking for the max)
		);

		// Looking through all potential matches for this initial pattern edge
		Iterator<Edge> matchingDataEdges = data.getEdges()
				.filter(e -> e.getMarker() != elementDescribedMarker) // Only consider edges that have not been already covered. Note: more edges can be marked during the execution of this algorithm
				// Only consider edges with the correct label and whose source and target vertices have compatible labels
				.filter(e -> e.getLabel().equals(initialPatternEdge.getLabel()))
				.filter(e -> e.getSource().getLabels().keySet().containsAll(initialPatternEdge.getSource().getLabels().keySet())) // The data vertex must have at least all the labels of the pattern vertex
				.filter(e -> e.getTarget().getLabels().keySet().containsAll(initialPatternEdge.getTarget().getLabels().keySet())) // The data vertex must have at least all the labels of the pattern vertex
				// Impose a total order just to make this algorithm deterministic
				.sorted(this.dataEdgeSorter())
				.iterator();
		while (matchingDataEdges.hasNext())
		{
			if (this.coverTimeoutMs > 0 && this.coverDuration.getElapsedTime().asMilliseconds() > this.coverTimeoutMs)
			{
				Logger.getLogger(getClass()).debug(String.format("Reached timeout (%dms) while computing cover of %s", this.coverTimeoutMs, row.getAutomorphisms().getCanonicalName()));
				break;
			}

			Edge dataEdge = matchingDataEdges.next();

			// This edge may now be described by a previous embedding generated during the execution of this search, so we need to check this
			// condition again, even though the iterator only contained non-described edges initially
			if (dataEdge.getMarker() == elementDescribedMarker)
				continue;

			// Creating an embedding that we will fill up during this algorithm
			Embedding embedding = new Embedding(row.getPatternGraph().getVertexCount());
			embedding.setMapping(initialPatternEdge.getSource(), dataEdge.getSource(), patternIndex(initialPatternEdge.getSource()), dataIndex(dataEdge.getSource()));
			embedding.setMapping(initialPatternEdge.getTarget(), dataEdge.getTarget(), patternIndex(initialPatternEdge.getTarget()), dataIndex(dataEdge.getTarget()));

			TreeSet<Edge> toMatch = new TreeSet<>(
					Comparator.<Edge>comparingInt(patternEdge -> {
								// We make it so edges that have both ends defined in the embedding will appear first
								int degreesOfFreedom = 2;
								if (embedding.getMapping(patternIndex(patternEdge.getSource())) != null)
									degreesOfFreedom -= 1;
								if (embedding.getMapping(patternIndex(patternEdge.getTarget())) != null)
									degreesOfFreedom -= 1;
								return degreesOfFreedom;
							})
							// In case of equality, edges with rarer labels will appear first (rare label = higher code length in ST)
							.thenComparing(Comparator.<Edge>comparingDouble(patternEdge -> standardTable.getEdgeLabelsCodeLengths().get(patternEdge.getLabel())).reversed())
							// Total ordering to make this algorithm deterministic
							.thenComparing(this.patternEdgeSorter())
							// The total ordering above should make it so that two different edges never compare the same
							// however, just to be extra sure, we use identityHashCode (remember that if two elements compare the same they are the same in the eyes of
							// TreeSet, so one would be lost).
							.thenComparingInt(System::identityHashCode)
			);

			// Initially, we have to match all edges around the two ends of the initial edge
			GeneralUtils.streamConcat(
					row.getPatternGraph().getOutEdges(initialPatternEdge.getSource()),
					row.getPatternGraph().getInEdges(initialPatternEdge.getSource()),
					row.getPatternGraph().getOutEdges(initialPatternEdge.getTarget()),
					row.getPatternGraph().getInEdges(initialPatternEdge.getTarget())
			)
					.filter(e -> e != initialPatternEdge)
					.forEach(toMatch::add);

			Optional<Embedding> coverEmbedding = coverEmbeddingRecursiveSearch(embedding, toMatch);
			if (coverEmbedding.isPresent())
			{
				// We need to mark the data covered by the embedding as described, so that the searches for the following embeddings will skip it
				Utils.markEmbedding(row.getPatternGraph(), coverEmbedding.get(), data, elementDescribedMarker);
				coverEmbeddings.add(coverEmbedding.get());
			}
		}
		// We tried all possible data edges that could match the starting pattern edge
		return coverEmbeddings;
	}

	/**
	 * Recursive search method to find <em>one</em> cover embedding in {@link #computeCoverEmbeddingsForPatternWithEdges}.
	 *
	 * @param embedding The current embedding that is being constructed. Some mappings in the embedding may be set to null.
	 * @param toMatch     Pattern edges for which at least one end is in the current embedding, but whose presence has not been yet verified in the data.
	 * @return A complete embedding if one is found from these parameters, or nothing if the current embedding does not allow to find a complete match for the pattern in the data.
	 */
	private Optional<Embedding> coverEmbeddingRecursiveSearch(Embedding embedding, TreeSet<Edge> toMatch)
	{
		final Set<Integer> dataVerticesUsedInCurrentMapping = Arrays.stream(embedding.getMappingsByIndex())
				.filter(Objects::nonNull)
				.map(mapping -> mapping.dataVertexIndex)
				.collect(Collectors.toSet());

		// Let's see if we can map all the remaining pattern edges to the data
		while (!toMatch.isEmpty())
		{
			Edge patternEdge = toMatch.pollFirst();
			final int sourceIndex = patternIndex(patternEdge.getSource());
			final int targetIndex = patternIndex(patternEdge.getTarget());
			final Mapping sourceMapping = embedding.getMapping(sourceIndex);
			final Mapping targetMapping = embedding.getMapping(targetIndex);

			if (sourceMapping != null && targetMapping != null) // We already have a mapping for both ends of the edge
			{
				// It's just a matter of checking if the edge exists between the corresponding data vertices
				if (data.getEdge(sourceMapping.dataVertex, targetMapping.dataVertex, patternEdge.getLabel()) == null)
					return Optional.empty(); // The edge does not exist in the data, the current mapping is incorrect
			}
			else // We have the mapping for one of the ends but not the other
			{
				int nonMappedPatternIndex = sourceMapping == null ? sourceIndex : targetIndex;
				Function<Edge, Vertex> getNonMapped = sourceMapping == null ? Edge::getSource : Edge::getTarget;
				Stream<Edge> dataEdgesAroundMapped = sourceMapping == null ? data.getInEdges(targetMapping.dataVertex) : data.getOutEdges(sourceMapping.dataVertex);

				Iterator<Edge> matchingDataEdges = dataEdgesAroundMapped
						.filter(dataEdge -> dataEdge.getMarker() != elementDescribedMarker) // The edge must not be already used in the current cover
						.filter(dataEdge -> dataEdge.getLabel().equals(patternEdge.getLabel())) // The edge must have the same label as in the pattern (of course)
						.filter(dataEdge -> !dataVerticesUsedInCurrentMapping.contains(dataIndex(getNonMapped.apply(dataEdge)))) // The non-mapped vertex must not be already used for another pattern vertex in the current embedding (enforce isomorphic mapping)
						.filter(dataEdge -> getNonMapped.apply(dataEdge).getLabels().keySet().containsAll(getNonMapped.apply(patternEdge).getLabels().keySet())) // The data vertex must have all labels of the pattern vertex
						.sorted(this.dataEdgeSorter()) // Impose a total order just to make this algorithm deterministic
						.iterator();
				while (matchingDataEdges.hasNext())
				{
					if (this.coverTimeoutMs > 0 && this.coverDuration.getElapsedTime().asMilliseconds() > this.coverTimeoutMs)
						return Optional.empty(); // We are out of time

					Edge dataEdge = matchingDataEdges.next();
					// Trying to increment the current mapping
					embedding.setMapping(getNonMapped.apply(patternEdge), getNonMapped.apply(dataEdge), nonMappedPatternIndex, dataIndex(getNonMapped.apply(dataEdge)));
					// Edges to match in the recursive call: all remaining to match in this call, plus all edges around the newly-mapped pattern vertex
					TreeSet<Edge> newToMatch = new TreeSet<>(toMatch);
					GeneralUtils.streamConcat(row.getPatternGraph().getOutEdges(getNonMapped.apply(patternEdge)), row.getPatternGraph().getInEdges(getNonMapped.apply(patternEdge)))
							.filter(e -> e != patternEdge) // We avoid re-adding the edge we just mapped
							// Note that a total ordering of the edges is already imposed by the comparator of the parent TreeSet
							.forEach(newToMatch::add);
					// Recursive call to map the rest of the pattern from the current mapping
					Optional<Embedding> recResult = coverEmbeddingRecursiveSearch(embedding, newToMatch);
					if (recResult.isPresent()) // We found a complete match!
						return recResult; // We just need to return it up to the top
					// We did not find a complete match: evidently the newly-mapped vertex was not the correct one.
					// Let's remove it from the mapping and move to the next one
					embedding.getMappingsByIndex()[nonMappedPatternIndex] = null;
				}
				// We could not find a mapping for the other end of the edge giving a complete embedding. Evidently the current partial mapping is not correct
				return Optional.empty();
			}
		}
		// We managed to map all edges of the pattern
		return Optional.of(embedding);
	}

	/**
	 * @return a Comparator providing a total ordering to break ties on pattern edges.
	 */
	private Comparator<Edge> patternEdgeSorter()
	{
		return Comparator.<Edge>comparingInt(e -> patternIndex(e.getSource()))
				.thenComparingInt(e -> patternIndex(e.getTarget()))
				.thenComparing(Edge::getLabel);
	}

	/**
	 * @return a Comparator providing a total ordering to break ties on data graph edges.
	 */
	private Comparator<Edge> dataEdgeSorter()
	{
		return Comparator.<Edge>comparingInt(e -> dataIndex(e.getSource()))
				.thenComparingInt(e -> dataIndex(e.getTarget()))
				.thenComparing(Edge::getLabel);
	}

	/**
	 * @return The index of the given vertex in the row's pattern.
	 */
	private int patternIndex(Vertex v)
	{
		return patternVertexIndexMap.get(v);
	}

	/**
	 * @return The index of the given vertex in the data graph.
	 */
	private int dataIndex(Vertex v)
	{
		return dataVertexIndexMap.get(v);
	}
}
