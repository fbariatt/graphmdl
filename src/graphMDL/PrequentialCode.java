package graphMDL;

import utils.SingletonStopwatchCollection;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

/**
 * Methods for computing the description length of a sequence of elements using a prequential code, and for computing
 * gamma function values used in the prequential code DL computation.
 */
public final class PrequentialCode
{
	/**
	 * gamma(0.5) = sqrt(pi).
	 * See https://en.wikipedia.org/wiki/Particular_values_of_the_gamma_function
	 */
	private final static double gamma05 = Math.sqrt(Math.PI);
	/**
	 * log2(gamma(0.5))
	 */
	private final static double logGamma05 = Utils.log2(gamma05);

	/**
	 * Compute the description length of a sequence of elements encoded using a prequential code.
	 * This computation is based on the following formula:
	 * $DL = |P| \log\Gamma(\epsilon) - \sum_{p \in P} \left [ \log\Gamma(usage_{tot}(p)+\epsilon) \right ] + \log\Gamma(|S| + |P|\epsilon) - \log\Gamma(|P|\epsilon)$
	 * with $\log\Gamma(x) = log2(\Gamma(x))$, $P$ the set of all possible items in the sequence, $\epsilon$ an initialisation usage, and $|S|$ the size of the sequence.
	 *
	 * @param itemUsages For each possible item in the sequence, the number of times it appears in the sequence.
	 * @param epsilon    $\epsilon$ in the DL formula, initialisation usage for each possible item.
	 */
	public static double prequentialCodeDLGamma(Iterator<Integer> itemUsages, double epsilon)
	{
		if (epsilon == 0.5)
			return prequentialCodeDLGamma05(itemUsages);
		else if (epsilon == 1)
			return prequentialCodeDLGamma1(itemUsages);
		else
			throw new IllegalArgumentException(String.format("Prequential code computation using gamma formula only accepts epsilon of 0.5 or 1. Got %f", epsilon));
	}

	/**
	 * Implementation of {@link #prequentialCodeDLGamma(Iterator, double)} for epsilon = 0.5
	 */
	private static double prequentialCodeDLGamma05(Iterator<Integer> itemUsages)
	{
		SingletonStopwatchCollection.resume("prequentialCodeDL<gamma function,epsilon = 0.5>");
		double result = 0;
		int nbItems = 0; // Number of different items/patterns |P|
		int seqLength = 0; // Length of the sequence (tot number of elements) |S|
		while (itemUsages.hasNext())
		{
			int usage = itemUsages.next();
			nbItems++;
			seqLength += usage;
			result -= logGamma05(usage);
		}
		result += nbItems * logGamma05;
		final int nbItemsTimesEpsilon = nbItems / 2; // |P|e = |P| * (0.5)
		// |P|e for e = 0.5 will either be an int or an int + 0.5 depending on whether |P| is even
		Function<Integer, Double> logGamma = nbItems % 2 == 0 ? PrequentialCode::logGammaInt : PrequentialCode::logGamma05;
		result += logGamma.apply(seqLength + nbItemsTimesEpsilon);
		result -= logGamma.apply(nbItemsTimesEpsilon);
		SingletonStopwatchCollection.stop("prequentialCodeDL<gamma function,epsilon = 0.5>");
		return result;
	}

	/**
	 * Implementation of {@link #prequentialCodeDLGamma(Iterator, double)} for epsilon = 1
	 */
	private static double prequentialCodeDLGamma1(Iterator<Integer> itemUsages)
	{
		SingletonStopwatchCollection.resume("prequentialCodeDL<gamma function,epsilon = 1>");
		double result = 0;
		int nbItems = 0; // Number of different items/patterns |P|
		int seqLength = 0; // Length of the sequence (tot number of elements) |S|
		while (itemUsages.hasNext())
		{
			int usage = itemUsages.next();
			nbItems++;
			seqLength += usage;
			result -= logGammaInt(usage + 1);
		}
		//result += 0; // |P| * log(G(1)) = |P| * 0 = 0
		result += logGammaInt(seqLength + nbItems);
		result -= logGammaInt(nbItems);
		SingletonStopwatchCollection.stop("prequentialCodeDL<gamma function,epsilon = 1>");
		return result;
	}

	/**
	 * Compute the description length of a sequence of elements encoded using a prequential code.
	 * This computation is done iteratively by computing for each element in the sequence a prefix code based on the distribution of the
	 * previous elements.
	 * It can be shown that the result will be the same no matter what the order of elements in the sequence is.
	 * Therefore, this implementation treats each possible item one after the other.
	 *
	 * @param itemUsages For each possible item in the sequence, the number of times it appears in the sequence.
	 * @param epsilon    Initialisation usage for each possible item. Common values are 0.5 or 1.
	 */
	public static double prequentialCodeDLIterative(List<Integer> itemUsages, double epsilon)
	{
		SingletonStopwatchCollection.resume("prequentialCodeDL<iterative>");
		double currentItemUsageAccum;
		double currentUsageSumAccum = epsilon * itemUsages.size();
		double dl = 0.0;
		for (int itemUsage : itemUsages)
		{
			currentItemUsageAccum = epsilon; // The initial usage of an item is epsilon
			for (int i = 0; i < itemUsage; ++i)
			{
				dl += -Utils.log2(currentItemUsageAccum / currentUsageSumAccum); // Prefix code based on the previous occurrences
				currentItemUsageAccum++;
				currentUsageSumAccum++;
			}
		}
		SingletonStopwatchCollection.stop("prequentialCodeDL<iterative>");
		return dl;
	}

	/**
	 * Compute log2(n!) with n a positive or zero integer.
	 * By computing log2(n!) = log2(n) + log2((n-1)!) instead of log2(n!) = log2(n*(n-1)!) we avoid
	 * number overflow that otherwise happens quite quickly because of the quick increase of factorial values.
	 */
	private static double logFactorial(int n)
	{
		if (n < 0)
			throw new IllegalArgumentException(String.format("Can not compute factorial for n < 0, got %d", n));
		double result = 0.0;
		for (int i = 2; i <= n; ++i)
			result += Utils.log2(i);
		return result;
	}

	/**
	 * Compute log2(gamma(n)) with n an integer > 1.
	 * Since gamma(n) = (n-1)! this calls {@link #logFactorial}.
	 */
	public static double logGammaInt(int n)
	{
		return logFactorial(n - 1);
	}

	/**
	 * Compute log2(gamma(n+0.5)) with n a positive or zero integer.
	 * By computing log2(gamma(x)) = log2(x-1) + log2(gamma(x-1)) instead of log2(gamma(x)) = log2((n-1)*gamma(x-1)) we avoid
	 * number overflow that otherwise happens quite quickly because of the quick increase of gamma.
	 */
	public static double logGamma05(int n)
	{
		if (n < 0)
			throw new IllegalArgumentException(String.format("Can not compute gamma for value < 0.5 got (%d + 0.5)", n));
		double result = logGamma05;
		for (int i = 0; i < n; ++i)
			result += Utils.log2(i + 0.5);
		return result;
	}
}
