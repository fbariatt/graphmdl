package graphMDL.jsonSerializer.multigraphs;

import graph.Graph;
import graphMDL.*;
import graphMDL.jsonSerializer.GraphMDLDeserializer;
import graphMDL.jsonSerializer.PatternInfoDeserializer;
import graphMDL.multigraphs.MultiDirectedCodeTable;
import graphMDL.multigraphs.MultiDirectedStandardTable;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.Map;

/**
 * Extends {@link GraphMDLDeserializer} for directed multigraphs, serialized with {@link MultiDirectedGraphMDLSerializer}.
 */
public class MultiDirectedGraphMDLDeserializer extends GraphMDLDeserializer
{
	public MultiDirectedGraphMDLDeserializer(JSONObject serialization)
	{
		super(serialization);

		try
		{
			String kind = serialization.getString("kind");
			if (!kind.equals("multi_directed"))
				throw new IllegalArgumentException("Expected directed multigraph serialization ('kind' attribute), got " + kind);
		} catch (JSONException e) // The key does not exist
		{
			Logger.getLogger(getClass()).warn("Deserialization: no 'kind' attribute specified in serialization");
		}
	}

	@Override
	protected StandardTable createStandardTable(Map<String, Double> vertexCodeLengths, Map<String, Double> edgeCodeLengths)
	{
		return new MultiDirectedStandardTable(vertexCodeLengths, edgeCodeLengths);
	}

	@Override
	protected CodeTable createEmptyCodeTable(StandardTable standardTable, Comparator<CodeTableRow> codeTableRowComparator)
	{
		return new MultiDirectedCodeTable((MultiDirectedStandardTable) standardTable, codeTableRowComparator);
	}

	@Override
	protected boolean isVertexSingleton(Graph pattern)
	{
		return Utils.isVertexSingleton(pattern);
	}

	@Override
	protected boolean isEdgeSingleton(Graph pattern)
	{
		return Utils.isEdgeSingleton(pattern);
	}

	@Override
	protected int graphLabelCount(Graph g)
	{
		return Utils.graphLabelCount(g);
	}

	@Override
	protected PatternInfo deserializePatternInfo(JSONObject serialization)
	{
		return new PatternInfoDeserializer(serialization).createPatternInfo();
	}
}
