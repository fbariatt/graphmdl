package graphMDL.jsonSerializer.multigraphs;

import graph.Graph;
import graphMDL.CodeTable;
import graphMDL.PatternInfo;
import graphMDL.Utils;
import graphMDL.jsonSerializer.GraphMDLSerializer;
import graphMDL.jsonSerializer.PatternInfoSerializer;
import graphMDL.multigraphs.MultiCodeTable;
import graphMDL.multigraphs.MultiDirectedCodeTable;
import org.json.JSONObject;

/**
 * Extends {@link GraphMDLSerializer} for the case where data and patterns are directed multigraphs.
 */
public class MultiDirectedGraphMDLSerializer extends GraphMDLSerializer
{
	public MultiDirectedGraphMDLSerializer(MultiDirectedCodeTable ct, Graph data)
	{
		super(ct, data);
	}

	@Override
	protected Graph createVertexSingletonGraph(String label)
	{
		return Utils.createVertexSingleton(label);
	}

	@Override
	protected Graph createEdgeSingletonGraph(String label)
	{
		return Utils.createEdgeSingleton(label);
	}

	@Override
	protected JSONObject serializePatternInfo(PatternInfo patternInfo)
	{
		return new PatternInfoSerializer(patternInfo).getJSONObject();
	}

	@Override
	protected void customizeSerialization()
	{
		this.serialization.put("kind", "multi_directed");
	}
}
