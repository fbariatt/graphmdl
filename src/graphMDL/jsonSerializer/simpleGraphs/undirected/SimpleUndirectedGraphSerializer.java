package graphMDL.jsonSerializer.simpleGraphs.undirected;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import graphMDL.jsonSerializer.GraphSerializer;
import org.json.JSONArray;

import java.util.Comparator;
import java.util.Map;

/**
 * Extends {@link GraphSerializer} for serialization of simple undirected graphs, which are represented with doubled edges.
 */
public class SimpleUndirectedGraphSerializer extends GraphSerializer
{
	/**
	 * Serialize the given graph.
	 * The graph should be a graph in which each edge is doubled.
	 */
	public SimpleUndirectedGraphSerializer(Graph g)
	{
		super(g);
	}

	@Override
	protected JSONArray serializeEdges()
	{
		JSONArray edges = new JSONArray();
		Map<Vertex, Integer> vertexIndexMap = g.getVertexIndexMap();
		g.getEdges()
				.filter(edge -> vertexIndexMap.get(edge.getSource()) < vertexIndexMap.get(edge.getTarget())) // Only serialize the edge in one direction
				.sorted(Comparator.<Edge>comparingInt(edge -> vertexIndexMap.get(edge.getSource())).thenComparingInt(edge -> vertexIndexMap.get(edge.getTarget())).thenComparing(Edge::getLabel))
				.map(edge -> serializeEdge(edge, vertexIndexMap))
				.forEach(edges::put);
		return edges;
	}
}
