package graphMDL.jsonSerializer.simpleGraphs.undirected;

import graph.Graph;
import graphMDL.CodeTable;
import graphMDL.CodeTableRow;
import graphMDL.PatternInfo;
import graphMDL.StandardTable;
import graphMDL.jsonSerializer.GraphMDLDeserializer;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedCodeTable;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedIndependentVertexAndEdgeST;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.Map;

/**
 * Extends {@link GraphMDLDeserializer} for simple undirected graphs, serialized with {@link SimpleUndirectedGraphMDLSerializer}.
 */
public class SimpleUndirectedGraphMDLDeserializer extends GraphMDLDeserializer
{
	public SimpleUndirectedGraphMDLDeserializer(JSONObject serialization)
	{
		super(serialization);

		try
		{
			String kind = serialization.getString("kind");
			if (!kind.equals("simple_undirected"))
				throw new IllegalArgumentException("Expected simple undirected serialization ('kind' attribute), got " + kind);
		} catch (JSONException e) // The key does not exist
		{
			System.err.println("[SimpleUndirectedGraphMDLDeserializer] Warning: no 'kind' attribute specified in serialization");
		}
	}

	@Override
	protected StandardTable createStandardTable(Map<String, Double> vertexCodeLengths, Map<String, Double> edgeCodeLengths)
	{
		return new SimpleUndirectedIndependentVertexAndEdgeST(vertexCodeLengths, edgeCodeLengths);
	}

	@Override
	protected CodeTable createEmptyCodeTable(StandardTable standardTable, Comparator<CodeTableRow> codeTableRowComparator)
	{
		return new SimpleUndirectedCodeTable((SimpleUndirectedIndependentVertexAndEdgeST) standardTable, codeTableRowComparator);
	}

	@Override
	protected boolean isVertexSingleton(Graph pattern)
	{
		return SimpleUndirectedUtils.isVertexSingleton(pattern);
	}

	@Override
	protected boolean isEdgeSingleton(Graph pattern)
	{
		return SimpleUndirectedUtils.isEdgeSingleton(pattern);
	}

	@Override
	protected int graphLabelCount(Graph g)
	{
		return SimpleUndirectedUtils.graphLabelCount(g);
	}

	@Override
	protected PatternInfo deserializePatternInfo(JSONObject serialization)
	{
		return new SimpleUndirectedPatternInfoDeserializer(serialization).createPatternInfo();
	}
}
