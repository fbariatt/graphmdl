package graphMDL.jsonSerializer.simpleGraphs.undirected;

import graph.Graph;
import graphMDL.jsonSerializer.PatternInfoDeserializer;
import org.json.JSONObject;

/**
 * Extends {@link PatternInfoDeserializer}, for simple undirected graph patterns that have been serialized with {@link SimpleUndirectedPatternInfoSerializer}.
 */
public class SimpleUndirectedPatternInfoDeserializer extends PatternInfoDeserializer
{
	public SimpleUndirectedPatternInfoDeserializer(JSONObject serialization)
	{
		super(serialization);
	}

	@Override
	protected Graph loadStructure()
	{
		return new SimpleUndirectedGraphDeserializer(serialization).createGraph();
	}
}
