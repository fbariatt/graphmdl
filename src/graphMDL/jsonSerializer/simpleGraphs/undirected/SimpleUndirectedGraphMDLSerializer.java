package graphMDL.jsonSerializer.simpleGraphs.undirected;

import graph.Graph;
import graph.Vertex;
import graphMDL.PatternInfo;
import graphMDL.jsonSerializer.GraphMDLSerializer;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedCodeTable;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

/**
 * Extends {@link GraphMDLSerializer} for the case where data and patterns are simple undirected graphs, which are represented with doubled edges.
 */
public class SimpleUndirectedGraphMDLSerializer extends GraphMDLSerializer
{
	public SimpleUndirectedGraphMDLSerializer(SimpleUndirectedCodeTable ct, Graph data)
	{
		super(ct, data);
	}

	@Override
	protected Graph createVertexSingletonGraph(String label)
	{
		return SimpleUndirectedUtils.createVertexSingleton(label);
	}

	@Override
	protected Graph createEdgeSingletonGraph(String label)
	{
		return SimpleUndirectedUtils.createEdgeSingleton(label);
	}

	@Override
	protected JSONArray serializeSingletonEdgeEmbeddings(String label, Map<Vertex, Integer> vertexIndexMap)
	{
		// No need for a special implementation for undirected graphs:
		// vertices are doubled, which means that the parent method will give every undirected edge two embeddings,
		// but this is fine, since undirected edges do have two embeddings (one for each direction).
		return super.serializeSingletonEdgeEmbeddings(label, vertexIndexMap);
	}

	@Override
	protected JSONArray serializeSingletonEdgeEmbeddingsUsedInCover(String label, Map<Vertex, Integer> vertexIndexMap)
	{
		// No need for a special implementation for undirected graphs:
		// during the CT cover computation, one of the two doubled edges is already marked as described,
		// so the parent method will skip it
		return super.serializeSingletonEdgeEmbeddingsUsedInCover(label, vertexIndexMap);
	}

	@Override
	protected JSONObject serializePatternInfo(PatternInfo patternInfo)
	{
		return new SimpleUndirectedPatternInfoSerializer(patternInfo).getJSONObject();
	}

	@Override
	protected void customizeSerialization()
	{
		this.serialization.put("kind", "simple_undirected");
	}
}
