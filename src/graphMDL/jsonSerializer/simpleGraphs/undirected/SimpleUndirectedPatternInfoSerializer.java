package graphMDL.jsonSerializer.simpleGraphs.undirected;

import graphMDL.PatternInfo;
import graphMDL.jsonSerializer.PatternInfoSerializer;
import org.json.JSONObject;

/**
 * Extends {@link PatternInfoSerializer} for PatternInfo objects containing a simple undirected graph.
 */
public class SimpleUndirectedPatternInfoSerializer extends PatternInfoSerializer
{
	public SimpleUndirectedPatternInfoSerializer(PatternInfo patternInfo)
	{
		super(patternInfo);
	}

	@Override
	protected JSONObject serializeStructure()
	{
		return new SimpleUndirectedGraphSerializer(this.patternInfo.getStructure()).getJSONObject();
	}
}
