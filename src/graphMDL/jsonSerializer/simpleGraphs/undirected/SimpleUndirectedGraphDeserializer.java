package graphMDL.jsonSerializer.simpleGraphs.undirected;

import graph.Graph;
import graph.GraphFactory;
import graphMDL.jsonSerializer.GraphDeserializer;
import graphMDL.jsonSerializer.SerializationUtils;
import org.json.JSONObject;

/**
 * Extends {@link GraphDeserializer}, for simple undirected graphs that have been serialized with {@link SimpleUndirectedGraphSerializer}.
 */
public class SimpleUndirectedGraphDeserializer extends GraphDeserializer
{
	public SimpleUndirectedGraphDeserializer(JSONObject serialization)
	{
		super(serialization);
	}

	@Override
	public Graph createEmptyGraph()
	{
		return GraphFactory.createSimpleGraph();
	}

	@Override
	protected void loadEdges(Graph graph)
	{
		SerializationUtils.<JSONObject>castJSONArrayElements(serialization.getJSONArray("edges"))
				.forEach(info -> {
					graph.addEdge(info.getInt("source"), info.getInt("target"), info.getString("label"));
					graph.addEdge(info.getInt("target"), info.getInt("source"), info.getString("label"));
				});
	}
}
