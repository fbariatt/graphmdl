package graphMDL.jsonSerializer.simpleGraphs.directed;

import graph.Graph;
import graphMDL.*;
import graphMDL.jsonSerializer.GraphMDLDeserializer;
import graphMDL.simpleGraphs.directed.SimpleDirectedCodeTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedIndependentVertexAndEdgeST;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.Map;

/**
 * Extends {@link GraphMDLDeserializer} for simple directed graphs, serialized with {@link SimpleDirectedGraphMDLSerializer}.
 */
public class SimpleDirectedGraphMDLDeserializer extends GraphMDLDeserializer
{
	public SimpleDirectedGraphMDLDeserializer(JSONObject serialization)
	{
		super(serialization);

		try
		{
			String kind = serialization.getString("kind");
			if (!kind.equals("simple_directed"))
				throw new IllegalArgumentException("Expected simple directed serialization ('kind' attribute), got " + kind);
		} catch (JSONException e) // The key does not exist
		{
			System.err.println("[SimpleDirectedGraphMDLDeserializer] Warning: no 'kind' attribute specified in serialization");
		}
	}

	@Override
	protected StandardTable createStandardTable(Map<String, Double> vertexCodeLengths, Map<String, Double> edgeCodeLengths)
	{
		return new SimpleDirectedIndependentVertexAndEdgeST(vertexCodeLengths, edgeCodeLengths);
	}

	@Override
	protected CodeTable createEmptyCodeTable(StandardTable standardTable, Comparator<CodeTableRow> codeTableRowComparator)
	{
		return new SimpleDirectedCodeTable((SimpleDirectedIndependentVertexAndEdgeST) standardTable, codeTableRowComparator);
	}

	@Override
	protected boolean isVertexSingleton(Graph pattern)
	{
		return Utils.isVertexSingleton(pattern);
	}

	@Override
	protected boolean isEdgeSingleton(Graph pattern)
	{
		return Utils.isEdgeSingleton(pattern);
	}

	@Override
	protected int graphLabelCount(Graph g)
	{
		return Utils.graphLabelCount(g);
	}

	@Override
	protected PatternInfo deserializePatternInfo(JSONObject serialization)
	{
		return new SimpleDirectedPatternInfoDeserializer(serialization).createPatternInfo();
	}
}
