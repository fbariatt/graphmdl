package graphMDL.jsonSerializer.simpleGraphs.directed;

import graph.Graph;
import graphMDL.PatternInfo;
import graphMDL.Utils;
import graphMDL.jsonSerializer.GraphMDLSerializer;
import graphMDL.jsonSerializer.PatternInfoSerializer;
import graphMDL.simpleGraphs.directed.SimpleDirectedCodeTable;
import org.json.JSONObject;

/**
 * Extends {@link GraphMDLSerializer} for the case where data and patterns are simple directed graphs.
 */
public class SimpleDirectedGraphMDLSerializer extends GraphMDLSerializer
{
	public SimpleDirectedGraphMDLSerializer(SimpleDirectedCodeTable ct, Graph data)
	{
		super(ct, data);
	}

	@Override
	protected Graph createVertexSingletonGraph(String label)
	{
		return Utils.createVertexSingleton(label);
	}

	@Override
	protected Graph createEdgeSingletonGraph(String label)
	{
		return Utils.createEdgeSingleton(label);
	}

	@Override
	protected JSONObject serializePatternInfo(PatternInfo patternInfo)
	{
		return new PatternInfoSerializer(patternInfo).getJSONObject();
	}

	@Override
	protected void customizeSerialization()
	{
		this.serialization.put("kind", "simple_directed");
	}
}
