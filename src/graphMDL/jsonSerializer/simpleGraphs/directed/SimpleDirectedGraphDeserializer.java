package graphMDL.jsonSerializer.simpleGraphs.directed;

import graph.Graph;
import graph.GraphFactory;
import graphMDL.jsonSerializer.GraphDeserializer;
import org.json.JSONObject;

/**
 * Extends {@link GraphDeserializer}, for simple directed graphs.
 */
public class SimpleDirectedGraphDeserializer extends GraphDeserializer
{
	public SimpleDirectedGraphDeserializer(JSONObject serialization)
	{
		super(serialization);
	}

	@Override
	protected Graph createEmptyGraph()
	{
		// We create a simple graph, which makes sure that the graph will always be a simple graph.
		return GraphFactory.createSimpleGraph();
	}
}
