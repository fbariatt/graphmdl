package graphMDL.jsonSerializer.simpleGraphs.directed;

import graph.Graph;
import graphMDL.jsonSerializer.PatternInfoDeserializer;
import org.json.JSONObject;

/**
 * Extends {@link PatternInfoDeserializer}, for simple directed graph patterns that have been serialized with {@link graphMDL.jsonSerializer.PatternInfoSerializer}.
 */
public class SimpleDirectedPatternInfoDeserializer extends PatternInfoDeserializer
{
	public SimpleDirectedPatternInfoDeserializer(JSONObject serialization)
	{
		super(serialization);
	}

	@Override
	protected Graph loadStructure()
	{
		return new SimpleDirectedGraphDeserializer(serialization).createGraph();
	}
}
