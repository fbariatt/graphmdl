package graphMDL.jsonSerializer;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.Map;

/**
 * This class serializes a {@link Graph} object.
 * Instances of this class are intended to be used as ephemeral objects, each one serializing a single graph.
 */
public class GraphSerializer
{
	private JSONObject serialization;
	protected final Graph g;

	/**
	 * Serialize the given graph.
	 */
	public GraphSerializer(Graph g)
	{
		this.g = g;

		this.serialize();
	}

	/**
	 * Perform the serialization and update the attribute containing the result.
	 */
	private void serialize()
	{
		this.serialization = new JSONObject();

		serialization.put("vertices", serializeVertices());
		serialization.put("edges", serializeEdges());
	}

	/**
	 * @return An array whose elements are the serializations of the vertices of the graph, in order.
	 */
	private JSONArray serializeVertices()
	{
		JSONArray vertices = new JSONArray();
		for (int v = 0; v < g.getVertexCount(); ++v)
			vertices.put(serializeVertex(v));
		return vertices;
	}

	/**
	 * Serialize a single vertex of the graph.
	 *
	 * @return An array containing the labels of the vertex.
	 */
	private JSONArray serializeVertex(int v)
	{
		JSONArray labels = new JSONArray();
		for (String label : g.getVertex(v).getLabels().keySet())
			labels.put(label);
		return labels;
	}

	/**
	 * @return An array whose elements are the serializations of the edges of the graph, sorted based on the index of their vertices in the graph, then their labels.
	 */
	protected JSONArray serializeEdges()
	{
		JSONArray edges = new JSONArray();
		Map<Vertex, Integer> vertexIndexMap = g.getVertexIndexMap();
		g.getEdges()
				.sorted(Comparator.<Edge>comparingInt(edge -> vertexIndexMap.get(edge.getSource())).thenComparingInt(edge -> vertexIndexMap.get(edge.getTarget())).thenComparing(Edge::getLabel))
				.map(edge -> serializeEdge(edge, vertexIndexMap))
				.forEach(edges::put);
		return edges;
	}

	/**
	 * Serialize a single edge of the graph.
	 *
	 * @param edge           The edge to serialize
	 * @param vertexIndexMap See {@link Graph#getVertexIndexMap()}. Used to retrieve indexes of the vertices connected to the edge.
	 */
	protected JSONObject serializeEdge(Edge edge, Map<Vertex, Integer> vertexIndexMap)
	{
		JSONObject result = new JSONObject();
		result.put("source", vertexIndexMap.get(edge.getSource()));
		result.put("target", vertexIndexMap.get(edge.getTarget()));
		result.put("label", edge.getLabel());
		return result;
	}

	/**
	 * @return The serialization of the graph object, as json.
	 */
	public JSONObject getJSONObject()
	{
		return serialization;
	}
}
