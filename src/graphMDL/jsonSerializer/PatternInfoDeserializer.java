package graphMDL.jsonSerializer;

import graph.Graph;
import graph.automorphisms.Automorphism;
import graph.automorphisms.AutomorphismInfo;
import graph.automorphisms.certificates.GraphCertificate;
import graphMDL.PatternInfo;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Deserialize {@link PatternInfo} objects, which have been serialized with {@link PatternInfoSerializer}.
 * Each instance of this class deserializes a single json representation, but multiple instances of the described objects can be created from it.
 */
public class PatternInfoDeserializer
{
	protected final JSONObject serialization;

	public PatternInfoDeserializer(JSONObject serialization)
	{
		this.serialization = serialization;
	}

	public PatternInfo createPatternInfo()
	{
		Graph structure = loadStructure();

		GraphCertificate graphCertificate = new GraphCertificateDeserializer(serialization.getJSONObject("canonical_certificate")).createGraphCertificate();

		LinkedHashSet<Automorphism> automorphisms = new LinkedHashSet<>();
		Iterator<JSONArray> automorphismMappings = SerializationUtils.<JSONArray>castJSONArrayElements(serialization.getJSONArray("automorphisms")).iterator();
		while (automorphismMappings.hasNext())
		{
			List<Integer> mapping = SerializationUtils.<Integer>castJSONArrayElements(automorphismMappings.next()).collect(Collectors.toList());
			assert structure.getVertexCount() == mapping.size(); // The automorphism should have as many elements as there are vertices in the graph
			int[] arrayMapping = new int[mapping.size()];
			for (int i = 0; i < mapping.size(); ++i)
				arrayMapping[i] = mapping.get(i);

			automorphisms.add(new Automorphism(arrayMapping));
		}

		boolean automorphismsSaturated =  serialization.optBoolean("automorphisms_saturated", true); // Before this key was added, all automorphisms were saturated

		return new PatternInfo(structure, new AutomorphismInfo(graphCertificate, automorphisms, automorphismsSaturated));
	}

	/**
	 * @return A {@link Graph} object, with vertices and edges as described in the serialization.
	 */
	protected Graph loadStructure()
	{
		return new GraphDeserializer(this.serialization).createGraph();
	}
}
