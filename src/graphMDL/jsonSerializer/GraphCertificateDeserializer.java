package graphMDL.jsonSerializer;

import graph.automorphisms.certificates.CertificateElement;
import graph.automorphisms.certificates.EdgeCertificateElement;
import graph.automorphisms.certificates.GraphCertificate;
import graph.automorphisms.certificates.VertexLabelCertificateElement;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This class creates a {@link GraphCertificate} object from a json representation created with {@link GraphCertificateSerializer}.
 * Each instance of this class deserializes a single json representation, but multiple instances of the described object can be created from it.
 */
public class GraphCertificateDeserializer
{
	private final JSONObject serialization;

	public GraphCertificateDeserializer(JSONObject serialization)
	{
		this.serialization = serialization;
	}

	/**
	 * @return a {@link GraphCertificate} instance created from the given serialization.
	 */
	public GraphCertificate createGraphCertificate()
	{
		List<CertificateElement> certificateElements = new LinkedList<>();

		Iterator<JSONObject> elementsInfo = SerializationUtils.<JSONObject>castJSONArrayElements(serialization.getJSONArray("elements")).iterator();
		while (elementsInfo.hasNext())
		{
			JSONObject elementInfo = elementsInfo.next();
			final String elementType = elementInfo.getString("type");
			if (elementType.equals(GraphCertificateSerializer.VertexLabelElementType))
				certificateElements.add(deserializeVertexLabelElement(elementInfo));
			else if (elementType.equals(GraphCertificateSerializer.EdgeElementType))
				certificateElements.add(deserializeEdgeElement(elementInfo));
			else
				throw new IllegalArgumentException("Impossible to deserialize graph certificate element: unknown type " + elementType);
		}
		Collections.sort(certificateElements); // Not necessary because the certificate must have been sorted when saved, but done in case the certificate has been edited after saving

		return new GraphCertificate(certificateElements, serialization.getInt("vertex_count"));
	}

	/**
	 * Deserializes a single {@link VertexLabelCertificateElement} from the given description.
	 */
	private VertexLabelCertificateElement deserializeVertexLabelElement(JSONObject elementInfo)
	{
		return new VertexLabelCertificateElement(elementInfo.getString("label"), elementInfo.getInt("vertex"));
	}

	/**
	 * Deserializes a single {@link EdgeCertificateElement} from the given description.
	 */
	private EdgeCertificateElement deserializeEdgeElement(JSONObject elementInfo)
	{
		return new EdgeCertificateElement(elementInfo.getString("label"), elementInfo.getInt("source"), elementInfo.getInt("target"));
	}
}
