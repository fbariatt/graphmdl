package graphMDL.jsonSerializer;

import graph.Graph;
import graphMDL.CodeTable;
import graphMDL.CodeTableRow;
import graphMDL.PatternInfo;
import graphMDL.StandardTable;
import graphMDL.rewrittenGraphs.EmbeddingVertex;
import graphMDL.rewrittenGraphs.PatternCTInfo;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import graphMDL.rewrittenGraphs.SingletonCTInfo;
import org.json.JSONArray;
import org.json.JSONObject;
import patternMatching.Embedding;
import patternMatching.MinimumImageBasedSupportFromEmbeddings;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Deserialize GraphMDL-related objects, which have been serialized with {@link GraphMDLSerializer}.
 * Each instance of this class deserializes a single json representation, but multiple instances of the described objects can be created from it.
 */
public abstract class GraphMDLDeserializer
{
	protected final JSONObject serialization;

	public GraphMDLDeserializer(JSONObject serialization)
	{
		this.serialization = serialization;
	}

	/**
	 * Create a rewritten graph created from the cover information given by the serialization, with the additional
	 * constraint that the only pattern embeddings that are included are the ones that satisfy a given predicate.
	 *
	 * @param embeddingFilter Predicate that pattern embeddings used in cover must satisfy to be included in the rewritten graph.
	 */
	public RewrittenGraph createRewrittenGraph(Predicate<JSONObject> embeddingFilter)
	{
		RewrittenGraph rewrittenGraph = new RewrittenGraph();

		List<JSONObject> patternsUsedInCover = SerializationUtils.<JSONObject>castJSONArrayElements(serialization.getJSONArray("patterns"))
				.filter(pattern -> !pattern.getJSONArray("used_embeddings").isEmpty())
				.collect(Collectors.toList());
		for (int i = 0; i < patternsUsedInCover.size(); ++i)
		{
			JSONObject pattern = patternsUsedInCover.get(i);
			final PatternInfo patternInfo = deserializePatternInfo(pattern.getJSONObject("structure"));
			final boolean isSingleton = pattern.getBoolean("singleton");

			Iterator<JSONObject> embeddingsUsedInCover = SerializationUtils.<JSONObject>castJSONArrayElements(pattern.getJSONArray("used_embeddings")).iterator();
			while (embeddingsUsedInCover.hasNext())
			{
				JSONObject embeddingInfo = embeddingsUsedInCover.next();
				if (embeddingFilter.test(embeddingInfo))
				{
					EmbeddingVertex embeddingVertex;
					if (isSingleton)
					{
						Graph structure = patternInfo.getStructure();
						int arity = structure.getVertexCount();
						String label = arity == 1 ? structure.getVertex(0).getLabels().keySet().iterator().next() : structure.getEdges().iterator().next().getLabel();
						embeddingVertex = rewrittenGraph.addEmbeddingVertex(new SingletonCTInfo(arity, label, patternInfo));
					}
					else
					{
						embeddingVertex = rewrittenGraph.addEmbeddingVertex(PatternCTInfo.nonSingleton(i + 1, null));
					}
					JSONObject portInfo = embeddingInfo.getJSONObject("ports");
					Iterator<String> patternPorts = portInfo.keys();
					while (patternPorts.hasNext())
					{
						String patternPort = patternPorts.next();
						rewrittenGraph.addPort(embeddingVertex, Integer.parseInt(patternPort), portInfo.getInt(patternPort));
					}
				}
			}
		}
		return rewrittenGraph;
	}

	/**
	 * Create a rewritten graph created from the cover information given by the serialization, with the additional
	 * constraint that the only pattern embeddings included are the ones that use data vertices between two given bounds.
	 * Note that the lower bound is included but not the higher.
	 *
	 * @param dataVertexMin Only embeddings using data vertices with an index equal or higher than this value will be retained.
	 * @param dataVertexMax Only embeddings using data vertices with an index lower than this value will be retained.
	 */
	public RewrittenGraph createRewrittenGraph(int dataVertexMin, int dataVertexMax)
	{
		return createRewrittenGraph(embeddingInfo ->
				SerializationUtils.<Integer>castJSONArrayElements(embeddingInfo.getJSONArray("mapping"))
						.allMatch(i -> i >= dataVertexMin && i < dataVertexMax));
	}

	/**
	 * Create a rewritten graph created from the cover information given by the serialization.
	 * Also see {@link #createRewrittenGraph(Predicate)}.
	 */
	public RewrittenGraph createRewrittenGraph()
	{
		return createRewrittenGraph(o -> true);
	}

	/**
	 * Load a code table from the serialization.
	 * This method loads all attributes of the code table, so that the loaded table is in the same state as when it was serialized (provided the same row comparator is used).
	 * <strong>Important:</strong> this method should only be used to load a CT in order for it to be applied back to the same data
	 * from which it was originally computed (since it loads standard tables, embeddings and support). In order to load CT patterns to be used on a different
	 * data graph, use a {@link PatternInfoDeserializer} on each (non-singleton) pattern's structure and create the {@link CodeTableRow} by hand.
	 *
	 * @param codeTableRowComparator Heuristic for ordering rows of the code table. Note that if this is different
	 *                               from the heuristic used when the code table was serialized, the result may differ from the original.
	 * @param loadEmbeddings         Whether code table rows should contain the pattern's embeddings (loaded from the serialization).
	 *                               If true, then the original data graph is needed to properly create the embeddings.
	 * @param data                   The data graph to which this code table applies. Can be null if loadEmbeddings is false.
	 *                               If set, it must be the same data graph that was used to serialize the code table.
	 * @implNote This method uses reflection to set private attributes on the code table object. This is slower than using setters, but allows to fully load the code table
	 * without a need for it to expose private attributes using setters. Also, if the code table attributes change, this method won't work anymore. It is suggested to
	 * test this method every time any of the code table classes is modified.
	 */
	public CodeTable createCodeTable(Comparator<CodeTableRow> codeTableRowComparator, boolean loadEmbeddings, Graph data)
	{
		if (loadEmbeddings && data == null)
			throw new IllegalArgumentException("Data graph must be specified when embeddings are to be loaded, but null was given.");

		// Loading standard table
		final JSONObject vertexSTSerialization = serialization.getJSONObject("vertex_standard_table");
		final JSONObject edgeSTSerialization = serialization.getJSONObject("edge_standard_table");
		Map<String, Double> vertexST = new HashMap<>();
		vertexSTSerialization.keySet().forEach(label -> vertexST.put(label, vertexSTSerialization.getDouble(label)));
		Map<String, Double> edgeST = new HashMap<>();
		edgeSTSerialization.keySet().forEach(label -> edgeST.put(label, edgeSTSerialization.getDouble(label)));
		// Create the code table
		CodeTable ct = createEmptyCodeTable(createStandardTable(vertexST, edgeST), codeTableRowComparator);

		// Since we use reflection to set some attributes, the following try block is used to catch all possible reflection exceptions
		// and re-throw them as RuntimeException
		try
		{
			// Code table description lengths
			final JSONObject description_lengths = serialization.getJSONObject("description_lengths");
			SerializationUtils.getFieldIgnoringAccess(CodeTable.class, "modelDL").setDouble(ct, description_lengths.getDouble("model"));
			SerializationUtils.getFieldIgnoringAccess(CodeTable.class, "encodedGraphLength").setDouble(ct, description_lengths.getDouble("data"));
			int rewrittenGraphPortCount = SerializationUtils.<JSONObject>castJSONArrayElements(serialization.getJSONArray("patterns"))
					.flatMap(pattern -> SerializationUtils.<JSONObject>castJSONArrayElements(pattern.getJSONArray("used_embeddings"))) // For each pattern we select its embeddings that are used in the cover
					.map(embeddingInfo -> embeddingInfo.getJSONObject("ports")) // Each embedding contains a port information object
					.flatMap(portInfoOfEmbedding -> portInfoOfEmbedding.keySet().stream().map(portInfoOfEmbedding::getInt)) // The values in the port information object represent data vertices that are used as ports
					// We count how many different data vertices are used as ports across all embeddings of all patterns
					.collect(Collectors.toSet())
					.size();
			SerializationUtils.getFieldIgnoringAccess(CodeTable.class, "rewrittenGraphPortCount").setInt(ct, rewrittenGraphPortCount);

			// Non-singletons patterns
			Iterator<JSONObject> patterns = SerializationUtils.<JSONObject>castJSONArrayElements(serialization.getJSONArray("patterns"))
					.filter(pattern -> !pattern.getBoolean("singleton"))
					.iterator();
			while (patterns.hasNext())
			{
				final JSONObject pattern = patterns.next();
				final PatternInfo patternInfo = deserializePatternInfo(pattern.getJSONObject("structure"));

				CodeTableRow row = new CodeTableRow(
						patternInfo,
						pattern.getDouble("st_length"),
						pattern.getInt("image_based_support"),
						graphLabelCount(patternInfo.getStructure())
				);
				if (loadEmbeddings)
					row.setEmbeddings(deserializeEmbeddings(pattern.getJSONArray("embeddings"), patternInfo.getStructure(), data));
				SerializationUtils.getFieldIgnoringAccess(CodeTableRow.class, "usage").setInt(row, pattern.getInt("usage"));
				SerializationUtils.getFieldIgnoringAccess(CodeTableRow.class, "codeLength").set(row, pattern.getDouble("code_length"));

				// Port information for this pattern
				final JSONObject patternPorts = pattern.getJSONObject("ports");
				Map<Integer, Integer> portUsages = new HashMap<>();
				Map<Integer, Double> portCodeLengths = new HashMap<>();
				int portUsageSum = 0;
				for (String patternVertexAsString : patternPorts.keySet()) // Pattern port numbers are used as key in the port information object
				{
					final int patternVertex = Integer.parseInt(patternVertexAsString);
					final JSONObject portInfo = patternPorts.getJSONObject(patternVertexAsString);
					portUsageSum += portInfo.getInt("usage");
					portUsages.put(patternVertex, portInfo.getInt("usage"));
					portCodeLengths.put(patternVertex, portInfo.getDouble("code_length"));
				}
				SerializationUtils.getFieldIgnoringAccess(CodeTableRow.class, "portUsages").set(row, portUsages);
				SerializationUtils.getFieldIgnoringAccess(CodeTableRow.class, "portCodeLengths").set(row, portCodeLengths);
				SerializationUtils.getFieldIgnoringAccess(CodeTableRow.class, "portUsageSum").setInt(row, portUsageSum);
				ct.addRow(row);
			}

			// Singletons
			Iterator<JSONObject> singletons = SerializationUtils.<JSONObject>castJSONArrayElements(serialization.getJSONArray("patterns"))
					.filter(pattern -> pattern.getBoolean("singleton"))
					.iterator();
			Map<String, Integer> singletonVertexUsages = new HashMap<>();
			Map<String, Double> singletonVertexLengths = new HashMap<>();
			Map<String, Integer> singletonEdgeUsages = new HashMap<>();
			Map<String, Double> singletonEdgeLengths = new HashMap<>();
			while (singletons.hasNext())
			{
				final JSONObject singletonPattern = singletons.next();
				final Graph patternStructure = deserializePatternInfo(singletonPattern.getJSONObject("structure")).getStructure();
				if (isVertexSingleton(patternStructure))
				{
					final String singletonLabel = patternStructure.getVertex(0).getLabels().keySet().stream().findFirst().get();
					singletonVertexUsages.put(singletonLabel, singletonPattern.getInt("usage"));
					singletonVertexLengths.put(singletonLabel, singletonPattern.getDouble("code_length"));
				}
				else if (isEdgeSingleton(patternStructure))
				{
					final String singletonLabel = patternStructure.getEdges().findAny().get().getLabel();
					singletonEdgeUsages.put(singletonLabel, singletonPattern.getInt("usage"));
					singletonEdgeLengths.put(singletonLabel, singletonPattern.getDouble("code_length"));
				}
				else
				{
					throw new RuntimeException(String.format("Impossible to load pattern marked as singleton because it does not follow a singleton structure:\n%s", singletonPattern.getJSONObject("structure").toString(2)));
				}
			}
			SerializationUtils.getFieldIgnoringAccess(CodeTable.class, "singletonVertexUsages").set(ct, singletonVertexUsages);
			SerializationUtils.getFieldIgnoringAccess(CodeTable.class, "singletonVertexLengths").set(ct, singletonVertexLengths);
			SerializationUtils.getFieldIgnoringAccess(CodeTable.class, "singletonEdgeUsages").set(ct, singletonEdgeUsages);
			SerializationUtils.getFieldIgnoringAccess(CodeTable.class, "singletonEdgeLengths").set(ct, singletonEdgeLengths);
		} catch (IllegalAccessException | NoSuchFieldException e)
		{
			throw new RuntimeException("Impossible to set CT attributes using reflection", e);
		}

		return ct;
	}

	/**
	 * See {@link #createCodeTable(Comparator, boolean, Graph)}.
	 */
	public CodeTable createCodeTableWithoutEmbeddings(Comparator<CodeTableRow> codeTableRowComparator)
	{
		return createCodeTable(codeTableRowComparator, false, null);
	}

	/**
	 * See {@link #createCodeTable(Comparator, boolean, Graph)}.
	 */
	public CodeTable createCodeTableWithEmbeddings(Comparator<CodeTableRow> codeTableRowComparator, Graph data)
	{
		return createCodeTable(codeTableRowComparator, true, data);
	}

	/**
	 * @return A {@link StandardTable} object of the correct class for this deserializer, based on the given code lengths.
	 */
	protected abstract StandardTable createStandardTable(Map<String, Double> vertexCodeLengths, Map<String, Double> edgeCodeLengths);


	protected abstract CodeTable createEmptyCodeTable(StandardTable standardTable, Comparator<CodeTableRow> codeTableRowComparator);

	/**
	 * @return Whether the given graph pattern is a vertex singleton pattern.
	 */
	protected abstract boolean isVertexSingleton(Graph pattern);

	/**
	 * @return Whether the given graph pattern is an edge singleton pattern.
	 */
	protected abstract boolean isEdgeSingleton(Graph pattern);

	/**
	 * Load a list of embeddings from their serialization.
	 *
	 * @param embeddingsInfo The serialization of the embedding. Must be an array of arrays.
	 * @param pattern        The pattern of these embeddings. Used because a mapping requires a vertex object for the pattern.
	 * @param data           The data of these embeddings. Used because a mapping requires a vertex object for the data.
	 */
	public List<Embedding> deserializeEmbeddings(JSONArray embeddingsInfo, Graph pattern, Graph data)
	{
		if (embeddingsInfo.length() == 0) // If the serialization has no embeddings set, we treat the information as missing
			return null;

		List<Embedding> embeddings = new LinkedList<>();
		SerializationUtils.<JSONArray>castJSONArrayElements(embeddingsInfo)
				.forEachOrdered(embeddingInfo -> embeddings.add(deserializeEmbedding(embeddingInfo, pattern, data)));
		return embeddings;
	}

	/**
	 * Load a single pattern embedding from its serialization.
	 *
	 * @param embeddingInfo The serialization of the embedding
	 * @param pattern       The pattern of the embedding. Used because a mapping requires a vertex object for the pattern.
	 * @param data          The data of the embedding. Used because a mapping requires a vertex object for the data.
	 */
	private Embedding deserializeEmbedding(JSONArray embeddingInfo, Graph pattern, Graph data)
	{
		if (embeddingInfo.length() != pattern.getVertexCount())
			throw new IllegalArgumentException("Error: trying to deserialize embedding of size " + embeddingInfo.length() + " for a pattern of size " + pattern.getVertexCount());
		Embedding embedding = new Embedding(embeddingInfo.length());
		for (int v = 0; v < embeddingInfo.length(); ++v)
		{
			final int dataVertexIndex = embeddingInfo.getInt(v);
			embedding.setMapping(pattern.getVertex(v), data.getVertex(dataVertexIndex), v, dataVertexIndex);
		}
		return embedding;
	}

	/**
	 * @return The number of labels (vertex labels + edge labels) in the given graph.
	 */
	protected abstract int graphLabelCount(Graph g);

	/**
	 * Deserialize a {@link PatternInfo} object from the given serialization.
	 */
	protected abstract PatternInfo deserializePatternInfo(JSONObject serialization);


}
