package graphMDL.jsonSerializer;

import graph.Graph;
import graph.GraphFactory;
import graph.Vertex;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * This class creates a {@link Graph} object from a json representation created with {@link GraphSerializer}.
 * Each instance of this class deserializes a single json representation, but multiple instances of the described object can be created from it.
 */
public class GraphDeserializer
{
	protected final JSONObject serialization;

	public GraphDeserializer(JSONObject serialization)
	{
		this.serialization = serialization;
	}

	/**
	 * @return A graph created from the given description.
	 */
	public Graph createGraph()
	{
		Graph graph = createEmptyGraph();
		loadVertices(graph);
		loadEdges(graph);
		return graph;
	}

	/**
	 * Create a new {@link Graph} instance, used as basis for deserializing.
	 */
	protected Graph createEmptyGraph()
	{
		return GraphFactory.createDefaultGraph();
	}

	/**
	 * Deserialize all vertices from the description and add them to the given graph.
	 */
	protected void loadVertices(Graph graph)
	{
		Iterator<JSONArray> labelsOfAllVertices = SerializationUtils.<JSONArray>castJSONArrayElements(serialization.getJSONArray("vertices")).iterator();
		while (labelsOfAllVertices.hasNext())
		{
			JSONArray labelsOfVertex = labelsOfAllVertices.next();
			Vertex vertex = graph.addVertex();
			SerializationUtils.<String>castJSONArrayElements(labelsOfVertex)
					.forEach(vertex::addLabel);
		}
	}

	/**
	 * Deserialize all edges from the description and add them to the given graph.
	 */
	protected void loadEdges(Graph graph)
	{
		SerializationUtils.<JSONObject>castJSONArrayElements(serialization.getJSONArray("edges"))
				.forEach(info -> graph.addEdge(info.getInt("source"), info.getInt("target"), info.getString("label")));
	}
}
