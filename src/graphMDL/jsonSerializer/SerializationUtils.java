package graphMDL.jsonSerializer;

import org.json.JSONArray;

import java.lang.reflect.Field;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Utility functions useful for json (de)serialization.
 */
public final class SerializationUtils
{
	private SerializationUtils()
	{}

	/**
	 * {@link JSONArray} objects return lists/iterators of {@link Object}.
	 * This method casts all elements of the array to a given type T.
	 * Warning: only use on json arrays that are known to only contain objects of the given type.
	 */
	@SuppressWarnings("unchecked")
	public static <T> Stream<T> castJSONArrayElements(JSONArray array)
	{
		return StreamSupport.stream(array.spliterator(), false)
				.map(object -> (T) object);
	}

	/**
	 * Get a {@link Field} object from a given class that can be used to get/set attributes of an object of that class through
	 * reflection ignoring accessibility (i.e. public/private) rules.
	 *
	 * @param clazz     The class that declares the field.
	 * @param fieldName The name of the field.
	 * @throws NoSuchFieldException If the class does not declare the requested field
	 */
	public static <T> Field getFieldIgnoringAccess(Class<T> clazz, String fieldName) throws NoSuchFieldException
	{
		Field result = clazz.getDeclaredField(fieldName);
		result.setAccessible(true);
		return result;
	}
}
