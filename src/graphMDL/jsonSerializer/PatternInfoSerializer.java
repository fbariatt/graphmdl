package graphMDL.jsonSerializer;

import graph.automorphisms.Automorphism;
import graph.automorphisms.AutomorphismInfo;
import graphMDL.PatternInfo;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This class serializes a {@link PatternInfo} object.
 * Instances of this class are intended to be used as ephemeral objects, each one serializing a single object.
 */
public class PatternInfoSerializer
{
	private JSONObject serialization;
	protected final PatternInfo patternInfo;

	/**
	 * Serialize the given object.
	 */
	public PatternInfoSerializer(PatternInfo patternInfo)
	{
		this.patternInfo = patternInfo;

		this.serialize();
	}

	/**
	 * Perform the serialization and update the attribute containing the result.
	 */
	private void serialize()
	{
		this.serialization = new JSONObject();

		JSONObject structureSerialization = serializeStructure();
		serialization.put("vertices", structureSerialization.getJSONArray("vertices"));
		serialization.put("edges", structureSerialization.getJSONArray("edges"));

		AutomorphismInfo automorphismInfo = patternInfo.getAutomorphismInfo();
		serialization.put("canonical_certificate", new GraphCertificateSerializer(automorphismInfo.getCanonicalName()).getJSONObject());
		JSONArray automorphisms = new JSONArray();
		for (Automorphism automorphism : automorphismInfo.getAutomorphisms())
			automorphisms.put(serializeAutomorphism(automorphism));
		serialization.put("automorphisms", automorphisms);
		serialization.put("automorphisms_saturated", automorphismInfo.isSaturated());
	}

	/**
	 * Serialize the graph structure contained in the PatternInfo.
	 */
	protected JSONObject serializeStructure()
	{
		return new GraphSerializer(this.patternInfo.getStructure()).getJSONObject();
	}

	/**
	 * Serialize an automorphism.
	 *
	 * @return An array containing the new numbering given by the automorphism.
	 * The first element is the new number of the first vertex of the pattern, and so on.
	 */
	private JSONArray serializeAutomorphism(Automorphism automorphism)
	{
		JSONArray result = new JSONArray();
		for (int v = 0; v < automorphism.getVertexCount(); ++v)
			result.put(automorphism.getMapping(v));
		return result;
	}

	/**
	 * @return The serialization, as json.
	 */
	public JSONObject getJSONObject()
	{
		return serialization;
	}
}
