package graphMDL.jsonSerializer;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import graph.VertexLabel;
import graphMDL.CodeTable;
import graphMDL.CodeTableRow;
import graphMDL.PatternInfo;
import org.json.JSONArray;
import org.json.JSONObject;
import patternMatching.Embedding;
import patternMatching.Mapping;
import utils.GeneralUtils;
import utils.SingletonStopwatchCollection;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This class serializes a code table and how it covers a data graph.
 * Instances of this class are intended to be used as ephemeral objects, each one serializing a single CT.
 */
public abstract class GraphMDLSerializer
{
	protected final CodeTable ct;
	private final Graph data;
	protected JSONObject serialization;

	/**
	 * Serialize the given code table.
	 * The code table must have been used to cover some data, i.e. the {@link CodeTable#apply} method must have been called.
	 * The data graph markers must have not been modified since the cover has been computed.
	 */
	public GraphMDLSerializer(CodeTable ct, Graph data)
	{
		this.ct = ct;
		this.data = data;

		this.serialize();
	}

	/**
	 * Perform the serialization and update the attribute containing the result.
	 */
	private void serialize()
	{
		SingletonStopwatchCollection.resume("GraphMDLSerializer.serializations");
		serialization = new JSONObject();

		JSONObject descriptionLengths = new JSONObject();
		descriptionLengths.put("model", ct.getModelDL());
		descriptionLengths.put("data", ct.getEncodedGraphLength());
		descriptionLengths.put("total", ct.getModelDL() + ct.getEncodedGraphLength());
		serialization.put("description_lengths", descriptionLengths);

		// Note: for now we do not consider the possibility of serializing hypergraphs
		// Hypergraph support would need to modify a lot of the codebase anyway
		serialization.put("vertex_standard_table", new JSONObject(ct.getStandardTable().getVertexLabelsCodeLengths()));
		serialization.put("edge_standard_table", new JSONObject(ct.getStandardTable().getEdgeLabelsCodeLengths()));

		JSONArray patterns = new JSONArray();
		for (CodeTableRow row : ct.getRows())
			patterns.put(serializeCTRow(row));
		for (String label : ct.getSingletonVertexUsages().keySet())
			patterns.put(serializeVertexSingleton(label));
		for (String label : ct.getSingletonEdgeUsages().keySet())
			patterns.put(serializeEdgeSingleton(label));
		serialization.put("patterns", patterns);

		this.customizeSerialization();
		SingletonStopwatchCollection.stop("GraphMDLSerializer.serializations");
	}

	/**
	 * Serialize the give code table row.
	 */
	private JSONObject serializeCTRow(CodeTableRow row)
	{
		JSONObject result = new JSONObject();

		result.put("structure", serializePatternInfo(row.getPatternInfo()));

		result.put("singleton", false); // A CodeTableRow object is not a singleton by definition
		result.put("st_length", row.getPatternSTEncodingLength());
		result.put("image_based_support", row.getImageBasedSupport());
		result.put("usage", row.getUsage());
		result.put("code_length", row.getCodeLength());

		JSONArray embeddings = new JSONArray();
		for (Embedding embedding : GeneralUtils.<List<Embedding>>defaultIfNull(row.getEmbeddings(), Collections.emptyList()))
			embeddings.put(serializeEmbedding(embedding));
		result.put("embeddings", embeddings);

		JSONArray embeddingsUsedInCover = new JSONArray();
		for (Embedding embedding : GeneralUtils.<List<Embedding>>defaultIfNull(row.getCoverEmbeddings(), Collections.emptyList()))
		{
			JSONObject embeddingInfo = new JSONObject();
			embeddingInfo.put("mapping", serializeEmbedding(embedding));

			JSONObject portInfo = new JSONObject();
			for (int v = 0; v < row.getPatternGraph().getVertexCount(); ++v)
			{
				if (embedding.getMapping(v).dataVertex.getIsPortMarker() == data.getMarker()) // If the vertex is marked as port
					portInfo.put(Integer.toString(v), embedding.getMapping(v).dataVertexIndex);
			}
			embeddingInfo.put("ports", portInfo);
			embeddingsUsedInCover.put(embeddingInfo);

		}
		result.put("used_embeddings", embeddingsUsedInCover);

		JSONObject ports = new JSONObject();
		for (Integer port : row.getPortUsages().keySet())
			ports.put(Integer.toString(port), serializePort(row, port));
		result.put("ports", ports);

		return result;
	}

	/**
	 * Serialize a vertex singleton pattern for the given label
	 */
	private JSONObject serializeVertexSingleton(String label)
	{
		JSONObject result = new JSONObject();

		result.put("structure", serializePatternInfo(PatternInfo.createComputingSaturatedAutomorphisms(createVertexSingletonGraph(label))));

		result.put("singleton", true);
		result.put("st_length", ct.getStandardTable().encodeSingletonVertex(label));
		result.put("usage", ct.getSingletonVertexUsages().get(label));
		result.put("code_length", ct.getSingletonVertexLengths().get(label));

		JSONArray embeddings = new JSONArray();
		JSONArray embeddingsUsedInCover = new JSONArray();
		for (int v = 0; v < data.getVertexCount(); ++v)
		{
			Map<String, VertexLabel> vertexLabels = data.getVertex(v).getLabels();
			if (vertexLabels.containsKey(label)) // The vertex has this label, therefore it is a possible embedding of the singleton
			{
				Embedding embedding = new Embedding(1);
				embedding.setMapping(null, null, 0, v);
				embeddings.put(serializeEmbedding(embedding)); // We add the embedding to the possible embeddings

				if (vertexLabels.get(label).getMarker() != data.getMarker()) // The label has not been marked, therefore it has not been covered by a CT row, therefore a singleton is used
				{
					JSONObject embeddingUsedInCover = new JSONObject();
					embeddingUsedInCover.put("mapping", serializeEmbedding(embedding));
					JSONObject ports = new JSONObject();
					ports.put("0", v);
					embeddingUsedInCover.put("ports", ports);
					embeddingsUsedInCover.put(embeddingUsedInCover);
				}
			}
		}
		result.put("embeddings", embeddings);
		result.put("used_embeddings", embeddingsUsedInCover);

		JSONObject ports = new JSONObject();
		// Singleton vertices only have one port
		JSONObject port0 = new JSONObject();
		port0.put("usage", ct.getSingletonVertexUsages().get(label));
		port0.put("code_length", 0.0); // Since there is only one port, it has a code length of 0
		ports.put("0", port0);
		result.put("ports", ports);

		return result;
	}

	/**
	 * Create a {@link Graph} object that represents a vertex singleton having the given label.
	 */
	protected abstract Graph createVertexSingletonGraph(String label);

	/**
	 * Serialize an edge singleton pattern with the given label.
	 */
	private JSONObject serializeEdgeSingleton(String label)
	{
		JSONObject result = new JSONObject();

		result.put("structure", serializePatternInfo(PatternInfo.createComputingSaturatedAutomorphisms(createEdgeSingletonGraph(label))));

		result.put("singleton", true);
		result.put("st_length", ct.getStandardTable().encodeSingletonEdge(label));
		result.put("usage", ct.getSingletonEdgeUsages().get(label));
		result.put("code_length", ct.getSingletonEdgeLengths().get(label));

		Map<Vertex, Integer> vertexIndexMap = data.getVertexIndexMap();
		result.put("embeddings", serializeSingletonEdgeEmbeddings(label, vertexIndexMap));
		result.put("used_embeddings", serializeSingletonEdgeEmbeddingsUsedInCover(label, vertexIndexMap));

		JSONObject ports = new JSONObject();
		// For singleton edges, we consider that the two ends are always both ports
		JSONObject port0 = new JSONObject();
		JSONObject port1 = new JSONObject();
		// Therefore, the two ends have the same usage (the singleton usage)
		port0.put("usage", ct.getSingletonEdgeUsages().get(label));
		port1.put("usage", ct.getSingletonEdgeUsages().get(label));
		// Since we assume that they are used the same number of times, they both have a code length of 1
		port0.put("code_length", 1.0);
		port1.put("code_length", 1.0);
		ports.put("0", port0);
		ports.put("1", port1);
		result.put("ports", ports);

		return result;
	}

	/**
	 * Create a {@link Graph} object that represents an edge singleton having the given label.
	 */
	protected abstract Graph createEdgeSingletonGraph(String label);

	/**
	 * Serialize the embeddings of the singleton edge pattern having the given label.
	 */
	protected JSONArray serializeSingletonEdgeEmbeddings(String label, Map<Vertex, Integer> vertexIndexMap)
	{
		JSONArray embeddings = new JSONArray();

		Iterator<Edge> edges = data.getEdges()
				.filter(edge -> edge.getLabel().equals(label))
				.iterator();
		while (edges.hasNext())
		{
			Edge edge = edges.next();
			Embedding embedding = new Embedding(2);
			embedding.setMapping(null, null, 0, vertexIndexMap.get(edge.getSource()));
			embedding.setMapping(null, null, 1, vertexIndexMap.get(edge.getTarget()));
			embeddings.put(serializeEmbedding(embedding));
		}

		return embeddings;
	}

	/**
	 * Serialize the embeddings of the singleton edge pattern having the given label that are used in the cover.
	 */
	protected JSONArray serializeSingletonEdgeEmbeddingsUsedInCover(String label, Map<Vertex, Integer> vertexIndexMap)
	{
		JSONArray embeddingsUsedInCover = new JSONArray();

		Iterator<Edge> edges = data.getEdges()
				.filter(edge -> edge.getLabel().equals(label))
				.filter(edge -> edge.getMarker() != data.getMarker()) // Edges that have not been described by a pattern are described as embeddings of the singleton
				.iterator();
		while (edges.hasNext())
		{
			Edge edge = edges.next();
			JSONObject embeddingInfo = new JSONObject();

			Embedding embedding = new Embedding(2);
			embedding.setMapping(null, null, 0, vertexIndexMap.get(edge.getSource()));
			embedding.setMapping(null, null, 1, vertexIndexMap.get(edge.getTarget()));
			embeddingInfo.put("mapping", serializeEmbedding(embedding));

			JSONObject ports = new JSONObject();
			ports.put("0", vertexIndexMap.get(edge.getSource()));
			ports.put("1", vertexIndexMap.get(edge.getTarget()));
			embeddingInfo.put("ports", ports);

			embeddingsUsedInCover.put(embeddingInfo);
		}

		return embeddingsUsedInCover;
	}

	/**
	 * Serialize a graph object.
	 */
	protected abstract JSONObject serializePatternInfo(PatternInfo patternInfo);

	/**
	 * Serialize a given embedding of a pattern in the data.
	 *
	 * @return An array whose elements correspond to the indexes of the data vertices to which the pattern's vertices are mapped, in order.
	 */
	private JSONArray serializeEmbedding(Embedding embedding)
	{
		JSONArray dataVerticesIndexes = new JSONArray();
		for (Mapping mapping : embedding.getMappingsByIndex())
			dataVerticesIndexes.put(mapping.dataVertexIndex);
		return dataVerticesIndexes;
	}

	/**
	 * Serialize the information of a given port vertex of a given pattern.
	 */
	private JSONObject serializePort(CodeTableRow pattern, int port)
	{
		JSONObject result = new JSONObject();
		result.put("usage", pattern.getPortUsages().get(port));
		// If this port is the only port of the pattern, its code length comes out as -0 (negative zero).
		// By using Max, we convert it to positive zero, which is nicer to read.
		// Other than that, code lengths are never negative, so negative zero is the only case where the value is modified.
		double codeLength = Math.max(pattern.getPortCodeLengths().get(port), 0);
		result.put("code_length", codeLength);
		return result;
	}

	/**
	 * Called when the serialization is complete. Provides a way for subclasses to perform modifications on the whole serialization.
	 */
	protected void customizeSerialization()
	{}

	/**
	 * @return The serialization, as json.
	 */
	public JSONObject getJSONObject()
	{
		return serialization;
	}
}
