package graphMDL.jsonSerializer;

import graph.automorphisms.certificates.CertificateElement;
import graph.automorphisms.certificates.EdgeCertificateElement;
import graph.automorphisms.certificates.GraphCertificate;
import graph.automorphisms.certificates.VertexLabelCertificateElement;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This class serializes a {@link GraphCertificate} object.
 * Beware that if the graph as doubled edges (as it is the case in undirected graphs), the certificate serialization will have doubled edges as well.
 * Instances of this class are intended to be used as ephemeral objects, each one serializing a single object.
 */
public class GraphCertificateSerializer
{
	public static final String VertexLabelElementType = "vertex_label";
	public static final String EdgeElementType = "edge";

	private JSONObject serialization;
	protected final GraphCertificate certificate;

	/**
	 * Serialize the given graph certificate.
	 */
	public GraphCertificateSerializer(GraphCertificate certificate)
	{
		this.certificate = certificate;

		serialize();
	}

	/**
	 * Perform the serialization and update the attribute containing the result.
	 */
	private void serialize()
	{
		serialization = new JSONObject();

		serialization.put("vertex_count", certificate.getVertexCount());

		JSONArray certificateElements = new JSONArray();
		for (CertificateElement certificateElement : certificate.getCertificateElements())
		{
			if (certificateElement.getClass().equals(VertexLabelCertificateElement.class))
				certificateElements.put(serializeVertexLabelElement((VertexLabelCertificateElement) certificateElement));
			else if (certificateElement.getClass().equals(EdgeCertificateElement.class))
				certificateElements.put(serializeEdgeElement((EdgeCertificateElement) certificateElement));
			else
				throw new IllegalArgumentException("[GraphCertificateSerializer] Serialization of class " + certificateElement.getClass() + " not implemented");
		}
		serialization.put("elements", certificateElements);
	}

	/**
	 * Serialize a {@link VertexLabelCertificateElement}.
	 */
	protected JSONObject serializeVertexLabelElement(VertexLabelCertificateElement element)
	{
		JSONObject result = new JSONObject();
		result.put("type", VertexLabelElementType);
		result.put("vertex", element.getVertex());
		result.put("label", element.getLabel());
		return result;
	}

	/**
	 * Serialize a {@link EdgeCertificateElement}.
	 */
	protected JSONObject serializeEdgeElement(EdgeCertificateElement element)
	{
		JSONObject result = new JSONObject();
		result.put("type", EdgeElementType);
		result.put("source", element.getSource());
		result.put("target", element.getTarget());
		result.put("label", element.getEdgeLabel());
		return result;
	}

	/**
	 * @return The serialization, as json
	 */
	public JSONObject getJSONObject()
	{
		return serialization;
	}
}
