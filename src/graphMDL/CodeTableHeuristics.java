package graphMDL;

import java.util.Comparator;

/**
 * This class contains Comparator<CodeTableRow> that can be used as ordering functions for Code Tables.
 * The comparators in this class assume that the Code Table is ordered by ascending value of comparator, so in order for x to be placed higher in the Code Table than y, compare(x, y) must be < 0.
 *
 * @see Comparator
 */
public final class CodeTableHeuristics
{
	private CodeTableHeuristics() {}

	/**
	 * The heuristics in this subclass give a total order such that different patterns will always rank different.
	 */
	public static final class TotalOrder
	{

		/**
		 * Patterns are ordered by their canonical name, with the lowest at the top of the table.
		 */
		public static final Comparator<CodeTableRow> lexicographicAsc =
				Comparator.comparing(pattern -> pattern.getAutomorphisms().getCanonicalName());

		/**
		 * @return A new comparator that perform the same as the given comparator, except that equalities are broken
		 * using {@link #lexicographicAsc}.
		 */
		public static Comparator<CodeTableRow> arbitrarilyBreakEqualities(Comparator<CodeTableRow> comparator)
		{
			return comparator.thenComparing(lexicographicAsc);
		}
	}

	/**
	 * The heuristics in this subclass do not guarantee that two distinct patterns will necessarily have a different sorting value.
	 */
	public static final class PartialOrder
	{

		/**
		 * Patterns are ordered by their vertex count with the biggest at the top of the Code Table.
		 */
		public static final Comparator<CodeTableRow> vertexCountDesc =
				Comparator.<CodeTableRow>comparingInt(pattern -> pattern.getPatternGraph().getVertexCount()).reversed();

		/**
		 * Patterns are ordered by their support, with the most covering one at the top of the Code Table
		 */
		public static final Comparator<CodeTableRow> imageBasedSupportDesc =
				Comparator.comparingInt(CodeTableRow::getImageBasedSupport).reversed();

		/**
		 * Patterns are ordered by the number of labels they describe. The pattern with the most labels will be at the top of the Code Table.
		 */
		public static final Comparator<CodeTableRow> labelCountDesc =
				Comparator.comparingInt(CodeTableRow::getPatternLabelCount).reversed();

		/**
		 * Patterns are first ordered by descending number of labels, then by descending support.
		 */
		public static final Comparator<CodeTableRow> labelCountDescImageBasedSupportDesc =
				PartialOrder.labelCountDesc.thenComparing(PartialOrder.imageBasedSupportDesc);

		/**
		 * Patterns are first ordered by descending support, then by descending amount of labels.
		 */
		public static final Comparator<CodeTableRow> imageBasedSupportDescLabelCountDesc =
				PartialOrder.imageBasedSupportDesc.thenComparing(PartialOrder.labelCountDesc);

		/**
		 * Always yield 0 (i.e. elements have the same order).
		 * Note that with this heuristic if you delete a row from the code table and then re-add it, it will *not* give the same code table as before.
		 * This heuristic is intended to be used primarily in tests, as a quick way to create a code table in any order.
		 */
		public static final Comparator<CodeTableRow> noOrder = (_row1, _row2) -> 0;
	}
}
