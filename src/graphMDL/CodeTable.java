package graphMDL;

import graph.Edge;
import graph.Graph;
import graph.VertexLabel;
import graphMDL.rewrittenGraphs.EmbeddingVertex;
import graphMDL.rewrittenGraphs.PatternCTInfo;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import utils.GeneralUtils;
import utils.SingletonStopwatchCollection;
import utils.SortedList;

import java.util.*;
import java.util.function.Predicate;

public abstract class CodeTable
{
	// Parameters set at creation time
	protected final StandardTable st;
	protected final Comparator<CodeTableRow> codeTableRowComparator;

	// PatternInfo objects for singletons. Used in particular when creating the rewritten graph
	// Stored so that they do not need to be recomputed every time they are needed
	private final Map<String, PatternInfo> vertexSingletonsPatternInfo;
	private final Map<String, PatternInfo> edgeSingletonsPatternInfo;

	// Code Table rows, can be modified
	protected final SortedList<CodeTableRow> rows;

	// Singleton patterns usages and codes, dependant on cover
	protected final Map<String, Integer> singletonVertexUsages;
	protected final Map<String, Double> singletonVertexLengths;
	protected final Map<String, Integer> singletonEdgeUsages;
	protected final Map<String, Double> singletonEdgeLengths;

	// Description-length related attributes that depends on cover
	protected int usageSum; // Sum of usages of all patterns
	protected int rewrittenGraphPortCount; // Number of port nodes in encoded graph
	protected double modelDL; // Description length of the code table
	protected double encodedGraphLength; // Description length of the data graph encoded with this table

	// Rewritten graph, depends on cover and is optional (created only if asked for when apply function is called)
	protected RewrittenGraph rewrittenGraph;

	/**
	 * @param standardTable          Standard Table, used to compute pattern's description length
	 * @param codeTableRowComparator Function for ordering rows in the Code Table. Rows are ordered by ascending value of comparator, so in order for x to be placed higher than y, compare(x, y) must be < 0.
	 */
	public CodeTable(final StandardTable standardTable, final Comparator<CodeTableRow> codeTableRowComparator)
	{
		this.st = standardTable;

		this.codeTableRowComparator = codeTableRowComparator;
		this.rows = new SortedList<>(this.codeTableRowComparator);
		this.singletonVertexUsages = new HashMap<>();
		this.singletonVertexLengths = new HashMap<>();
		this.singletonEdgeUsages = new HashMap<>();
		this.singletonEdgeLengths = new HashMap<>();
		this.vertexSingletonsPatternInfo = new HashMap<>();
		this.edgeSingletonsPatternInfo = new HashMap<>();
	}

	/**
	 * Create a code table whose row set is a deep copy of the current one's (i.e. rows are different objects).
	 * Only rows for which keepRow predicate is true are included in the copy.
	 * Standard tables are shared between the two code tables.
	 * Usage and description length information is not copied, therefore the new code table need to be applied to the data graph to compute them.
	 */
	public CodeTable deepCopyWithRowFilter(Predicate<CodeTableRow> keepRow)
	{
		CodeTable result = this.constructTableWithSameArgumentsAsThis();
		this.rows.stream()
				.filter(keepRow)
				.map(row -> new CodeTableRow(row.getPatternInfo(), row.getPatternSTEncodingLength(), row.getImageBasedSupport(), row.getPatternLabelCount()))
				.forEach(result::addRow);

		return result;
	}

	/**
	 * Equivalent to creating a code table of the same class, passing the same parameters to the constructor as they have been passed to this object.
	 * Used in copy methods.
	 */
	protected abstract CodeTable constructTableWithSameArgumentsAsThis();


	/**
	 * Create a deep copy of the current code table (see deepCopyWithRowFilter) with all rows of the current code table.
	 */
	public CodeTable deepCopy()
	{
		return deepCopyWithRowFilter(r -> true);

	}

	/**
	 * Create a deep copy of the current code table (see deepCopyWithRowFilter) with only rows whose usage not null.
	 */
	public CodeTable deepCopyDiscardUnused()
	{
		return deepCopyWithRowFilter(row -> row.getUsage() > 0);
	}

	/**
	 * Add a row to this code table
	 */
	public void addRow(CodeTableRow row)
	{
		this.rows.add(row);
	}

	/**
	 * Remove a given row from this Code Table
	 */
	public void removeRow(CodeTableRow row)
	{
		this.rows.remove(row);
	}

	/**
	 * Remove all rows matching filter
	 */
	public void removeIf(Predicate<CodeTableRow> filter)
	{
		this.rows.removeIf(filter);
	}

	/**
	 * Remove all rows whose usage is 0
	 */
	public void removedUnused()
	{
		this.removeIf(row -> row.getUsage() == 0);
	}

	/**
	 * Use the code table to cover a given data graph, computing usages and codes for all patterns, and description lengths for both the data and the code table
	 *
	 * @param data                 The data, in graph form.
	 * @param coverStrategy        The {@link CoverStrategy} used to compute the cover of the table's rows.
	 * @param createRewrittenGraph If true, compute and return a rewritten graph that represents how this code table covers the data.
	 */
	public Optional<RewrittenGraph> apply(Graph data, CoverStrategy coverStrategy, boolean createRewrittenGraph)
	{
		final int elementDescribedMarker = data.getMarker() + 1;
		data.setMarker(elementDescribedMarker);
		computeCoversAndPorts(data, coverStrategy, elementDescribedMarker, createRewrittenGraph); // The rewritten graph is initialised in this function
		computeCodesAndDescriptionLengths(data.getVertexCount());

		return Optional.ofNullable(rewrittenGraph); // We return it if it has been created
	}

	public Optional<RewrittenGraph> apply(Graph data, boolean createRewrittenGraph)
	{
		return this.apply(data, new ScanDataCoverStrategy(0), createRewrittenGraph);
	}

	/**
	 * Compute the cover of a given data graph, computing usages for all patterns.
	 * This is called during {@link #apply}.
	 *
	 * @param data                   Same as for {@link #apply}.
	 * @param coverStrategy          Same as for {@link #apply}.
	 * @param elementDescribedMarker Value to use for markers on vertex and edge labels to tell whether they are described by a pattern or not.
	 *                               If this is called multiple times on the same graph, this value must be changed each time.
	 * @param createRewrittenGraph   If true, compute the rewritten graph that represents how this code table covers the data.
	 */
	public void computeCoversAndPorts(Graph data, CoverStrategy coverStrategy, int elementDescribedMarker, boolean createRewrittenGraph)
	{
		data.setMarker(elementDescribedMarker); // We set the value in case this function was called directly and not from apply
		if (createRewrittenGraph)
			rewrittenGraph = new RewrittenGraph();
		else
			rewrittenGraph = null; // We destroy the previous one if there was one
		usageSum = 0;
		SingletonStopwatchCollection.resume("codeTable.nonSingletonPatternsCover");
		computeNonSingletonPatternsCover(data, coverStrategy, elementDescribedMarker);
		SingletonStopwatchCollection.stop("codeTable.nonSingletonPatternsCover");
		SingletonStopwatchCollection.resume("codeTable.SingletonsCoverAndPorts");
		computeSingletonsCoverAndPortNodes(data, elementDescribedMarker);
		SingletonStopwatchCollection.stop("codeTable.SingletonsCoverAndPorts");
	}

	/**
	 * Compute cover of non-singleton patterns, update usages accordingly.
	 * Normally called during {@link #computeCoversAndPorts}
	 *
	 * @param data                   Sames as for {@link #apply}.
	 * @param coverStrategy          Same as for {@link #apply}.
	 * @param elementDescribedMarker Same as for {@link #computeCoversAndPorts}.
	 */
	private void computeNonSingletonPatternsCover(Graph data, CoverStrategy coverStrategy, int elementDescribedMarker)
	{
		int patternNumber = 1;
		for (CodeTableRow row : rows)
		{
			row.computeCover(data, st, coverStrategy, elementDescribedMarker, rewrittenGraph, patternNumber);
			usageSum += row.getUsage();
			patternNumber++;
		}
	}

	/**
	 * Compute cover of vertex-singleton and edge-singleton patterns. Update usages accordingly.
	 * Also compute how many port nodes will be in the encoded graph.
	 * {@link #computeNonSingletonPatternsCover} must have been applied previously with same elementDescribedMarker.
	 * Normally called during {@link #apply}
	 *
	 * @param data                   The data graph
	 * @param elementDescribedMarker Value to use for markers on vertex and edge labels to tell whether they are described by a pattern or not. See {@link #computeCoversAndPorts} function.
	 */
	private void computeSingletonsCoverAndPortNodes(Graph data, int elementDescribedMarker)
	{
		rewrittenGraphPortCount = 0;
		singletonVertexUsages.clear();
		singletonVertexLengths.clear();
		singletonEdgeUsages.clear();
		singletonEdgeLengths.clear();

		for (int v = 0; v < data.getVertexCount(); ++v)
		{
			boolean isPort = false;

			// Looking for non-described labels
			for (VertexLabel label : data.getVertex(v).getLabels().values())
			{
				if (label.getMarker() != elementDescribedMarker) // If label is not described
				{
					isPort = true;
					singletonVertexUsages.merge(label.getLabelString(), 1, Integer::sum);
					++usageSum;

					if (rewrittenGraph != null)
					{
						PatternInfo patternInfo = vertexSingletonsPatternInfo.computeIfAbsent(label.getLabelString(), this::createVertexSingletonPatternInfo);
						EmbeddingVertex embeddingVertex = rewrittenGraph.addEmbeddingVertex(PatternCTInfo.vertexSingleton(label.getLabelString(), patternInfo));
						rewrittenGraph.addPort(embeddingVertex, 0, v);
					}
				}
			}
			// Looking for non-described edges
			Iterator<Edge> edges = data.getOutEdges(v).iterator();
			while (edges.hasNext())
			{
				Edge edge = edges.next();
				if (edge.getMarker() != elementDescribedMarker) // If edge is not described
				{
					isPort = true;
					singletonEdgeUsages.merge(edge.getLabel(), 1, Integer::sum);
					++usageSum;

					// Undirected graphs need a special treatment here, since when an out-edge is not described there must be a corresponding in-edge that is not
					// described as well, but that should not be counted in order to avoid counting edges twice.
					// It is a bit ugly to put this treatment here instead of putting it in the child class(es). However, I think that since this is the only place
					// where being directed or not matters, it is actually more clear to do it like this instead of creating yet another method that must be overridden by
					// subclasses.
					if (!this.isDirected())
					{
						// We mark the inverse edge as described as well, since we don't want to count edges between the same vertices twice
						data.getEdge(edge.getTarget(), edge.getSource(), edge.getLabel()).setMarker(elementDescribedMarker);
					}

					if (rewrittenGraph != null)
					{
						PatternInfo patternInfo = edgeSingletonsPatternInfo.computeIfAbsent(edge.getLabel(), this::createEdgeSingletonPatternInfo);
						EmbeddingVertex embeddingVertex = rewrittenGraph.addEmbeddingVertex(PatternCTInfo.edgeSingleton(edge.getLabel(), patternInfo));
						rewrittenGraph.addPort(embeddingVertex, 0, v);
						rewrittenGraph.addPort(embeddingVertex, 1, data.getVertexIndex(edge.getTarget()));
					}
				}
			}

			// Is the vertex marked as port
			if (data.getVertex(v).getIsPortMarker() == elementDescribedMarker)
				isPort = true;

			// If any of the in-edges is not described, it will be described by a singleton, that will have this vertex as port.
			// The actual processing of the non-described edge will happen on the vertex for which the edge is an out-edge, but we need to mark
			// this vertex as port here.
			// (!isPort allows to short-circuit the evaluation if we already know the vertex is a port)
			if (!isPort && data.getInEdges(v).anyMatch(edge -> edge.getMarker() != elementDescribedMarker))
				isPort = true;

			// If the vertex is a port: act accordingly
			if (isPort)
			{
				++rewrittenGraphPortCount;
				data.getVertex(v).setIsPortMarker(elementDescribedMarker);
			}
		}
	}

	/**
	 * @return A {@link PatternInfo} object for a singleton vertex pattern with the given label
	 */
	protected abstract PatternInfo createVertexSingletonPatternInfo(String vertexLabel);

	/**
	 * @return A {@link PatternInfo} object for a singleton edge pattern with the given label
	 */
	protected abstract PatternInfo createEdgeSingletonPatternInfo(String edgeLabel);

	/**
	 * Compute codes associated to each pattern and port, and the description lengths of both the CT and the encoded graph.
	 * This must be called after {@link #computeCoversAndPorts} has been called.
	 * It is normally automatically called during {@link #apply}
	 */
	private void computeCodesAndDescriptionLengths(int dataVertexCount)
	{
		computeTableCodes();
		computeTableDescriptionLength();
		computeEncodedGraphLength(dataVertexCount);
	}

	/**
	 * Compute the prefix codes of all code table elements.
	 * This must be called after {@link #computeCoversAndPorts} has been called.
	 * It is normally called during {@link #computeCodesAndDescriptionLengths}.
	 */
	protected void computeTableCodes()
	{
		for (CodeTableRow row : rows)
			row.computeCodeLengths(usageSum);
		for (String vertexLabel : singletonVertexUsages.keySet())
			singletonVertexLengths.put(vertexLabel, -Utils.log2((double) singletonVertexUsages.get(vertexLabel) / usageSum));
		for (String edgeLabel : singletonEdgeUsages.keySet())
			singletonEdgeLengths.put(edgeLabel, -Utils.log2((double) singletonEdgeUsages.get(edgeLabel) / usageSum));
	}

	/**
	 * Compute the description length of the Code Table itself.
	 * This must be called after {@link #computeTableCodes} and its dependencies have been called.
	 * Normally called during {@link #computeCodesAndDescriptionLengths}.
	 */
	private void computeTableDescriptionLength()
	{
		modelDL = 0.0;
		for (CodeTableRow row: rows)
			modelDL += computeRowDescriptionLength(row);
		for (String label : singletonVertexUsages.keySet())
			modelDL += computeVertexSingletonPatternDescriptionLength(label);
		for (String label: singletonEdgeUsages.keySet())
			modelDL += computeEdgeSingletonPatternDescriptionLength(label);
	}

	/**
	 * Compute and return the description length of a single row.
	 * <br/>
	 * This method should not modify any attribute.
	 */
	protected abstract double computeRowDescriptionLength(CodeTableRow row);

	/**
	 * Compute and return the description length of a row that would represent the vertex singleton pattern of the given label.
	 * <br/>
	 * This method should not modify any attribute.
	 */
	protected abstract double computeVertexSingletonPatternDescriptionLength(String label);

	/**
	 * Compute and return the description length of a row that would represent the vertex singleton pattern of the given label.
	 * <br/>
	 * This method should not modify any attribute.
	 */
	protected abstract double computeEdgeSingletonPatternDescriptionLength(String label);

	/**
	 * Compute description length of the data graph encoded with this Code Table.
	 * This must be called after {@link #computeTableCodes} and its dependencies have been called.
	 * Normally called during {@link #computeCodesAndDescriptionLengths}.
	 *
	 * @param dataVertexCount The number of vertices in the data (needed in {@link graphMDL.multigraphs.MultiCodeTable}).
	 */
	protected abstract void computeEncodedGraphLength(int dataVertexCount);

	/**
	 * @return The Standard Table used by this code table.
	 */
	public StandardTable getStandardTable()
	{
		return this.st;
	}

	public List<CodeTableRow> getRows()
	{
		return rows;
	}

	public Map<String, Integer> getSingletonVertexUsages()
	{
		return singletonVertexUsages;
	}

	public Map<String, Double> getSingletonVertexLengths()
	{
		return singletonVertexLengths;
	}

	public Map<String, Integer> getSingletonEdgeUsages()
	{
		return singletonEdgeUsages;
	}

	public Map<String, Double> getSingletonEdgeLengths()
	{
		return singletonEdgeLengths;
	}

	public int getRewrittenGraphPortCount()
	{
		return rewrittenGraphPortCount;
	}

	public double getModelDL()
	{
		return modelDL;
	}

	public double getEncodedGraphLength()
	{
		return encodedGraphLength;
	}

	/**
	 * @return The rewritten graph obtained when applying this code table to data using {@link #apply},
	 * if the createRewrittenGraph parameter was set.
	 */
	public Optional<RewrittenGraph> getRewrittenGraph()
	{
		return Optional.ofNullable(rewrittenGraph);
	}

	/**
	 * @return Whether this code table handles directed or undirected graphs
	 */
	public abstract boolean isDirected();
}
