package graphMDL;

import graph.Graph;
import graph.automorphisms.Automorphism;
import graph.automorphisms.AutomorphismInfo;
import graphMDL.rewrittenGraphs.EmbeddingVertex;
import graphMDL.rewrittenGraphs.PatternCTInfo;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import org.apache.log4j.Logger;
import patternMatching.Embedding;
import utils.FindFirstMax;
import utils.SingletonStopwatchCollection;

import java.util.*;
import java.util.function.Consumer;

/**
 * A row of a Code Table: a graph pattern, which can have ports, a usage and a code length.
 */
public class CodeTableRow
{
	// Invariable attributes
	private final PatternInfo patternInfo;
	private final double patternSTEncodingLength;
	// Used in heuristics only
	private final int imageBasedSupport;
	private final int patternLabelCount; // Number of labels of this pattern (number of vertex labels + number of edge labels)
	// Embeddings of this pattern. This class does not use them but can be useful to store them alongside this object.
	private List<Embedding> embeddings = null;

	// Function called before computing the cover in computeCover. Its basic implementation should reset all values that depend on cover
	private Consumer<CodeTableRow> prepareCTRowForCoverComputation;
	// Values depending on cover
	private int usage = 0;
	private List<Embedding> coverEmbeddings = null;
	private double codeLength = 0.0;
	private Map<Integer, Integer> portUsages;
	private Map<Integer, Double> portCodeLengths;
	private int portUsageSum = 0;
	// Stored in this object but not managed by this class
	/**
	 * The usage of this row in the last CT that passed the acceptance test.
	 * This class does not modify this value.
	 */
	public double lastAcceptedUsage = 0;


	/**
	 * @param patternInfo                     The pattern's graph structure and automorphisms
	 * @param patternSTEncodingLength         Length of the pattern when encoded with standard tables.
	 * @param imageBasedSupport               (used in heuristics only) image-based support of the pattern in the data.
	 * @param patternLabelCount               (used in heuristics only) number of labels of this pattern (number of vertex labels + number of edge labels)
	 * @param prepareCTRowForCoverComputation function called before computing the cover in {@link #computeCover} : it should reset all cover-dependant attributes so that a new cover can be applied correctly.
	 *                                        This parameter exists because sometimes it may be needed to override it with a  custom function (e.g. for {@link graphMDL.classification.SimpleClassificationCT})
	 */
	public CodeTableRow(PatternInfo patternInfo, double patternSTEncodingLength, int imageBasedSupport, int patternLabelCount, Consumer<CodeTableRow> prepareCTRowForCoverComputation)
	{
		this.patternInfo = patternInfo;
		this.patternSTEncodingLength = patternSTEncodingLength;
		this.prepareCTRowForCoverComputation = prepareCTRowForCoverComputation;
		this.imageBasedSupport = imageBasedSupport;
		this.patternLabelCount = patternLabelCount;

		this.portUsages = new HashMap<>();
		this.portCodeLengths = new HashMap<>();
	}

	/**
	 * See {@link #CodeTableRow(PatternInfo, double, int, int, Consumer)}
	 */
	public CodeTableRow(PatternInfo patternInfo, double patternSTEncodingLength, int imageBasedSupport, int patternLabelCount)
	{
		this(patternInfo, patternSTEncodingLength, imageBasedSupport, patternLabelCount, CodeTableRow::resetAllCoverDependantAttributes);
	}

	/**
	 * Compute the cover of this pattern on the data graph.
	 *
	 * @param data                   The data, in graph form
	 * @param standardTable          The standard table associated to the data.
	 * @param coverStrategy          The {@link CoverStrategy} used to compute the embeddings that form this row's cover.
	 * @param elementDescribedMarker Value to use for markers on vertex and edge labels to tell whether they are described by a pattern or not.
	 * @param rewrittenGraph         (optional, many be null) If not null, store in it the result of covering the data with this pattern.
	 * @param patternNumber          If rewrittenGraph is not null, the index of this row in the code table, to be used when creating embedding vertices in the rewritten graph.
	 */
	public void computeCover(Graph data, StandardTable standardTable, CoverStrategy coverStrategy, int elementDescribedMarker, RewrittenGraph rewrittenGraph, int patternNumber)
	{
		Logger.getLogger(getClass()).debug(String.format("Computing cover for ct row %s", patternInfo.getAutomorphismInfo().getCanonicalName()));
		prepareCTRowForCoverComputation.accept(this);

		SingletonStopwatchCollection.resume("codeTableRow.computeCoverEmbeddings");
		coverEmbeddings = coverStrategy.computeCoverEmbeddings(this, data, standardTable, elementDescribedMarker);
		SingletonStopwatchCollection.stop("codeTableRow.computeCoverEmbeddings");

		for (Embedding embedding : coverEmbeddings)
		{
			// The cover embeddings search should have already marked the embeddings as described.
			// Thus, this line is no longer needed.
			//Utils.markEmbedding(patternInfo.getStructure(), embedding, data, elementDescribedMarker);

			++usage;

			EmbeddingVertex rewrittenGraphEmbedding = null;
			if (rewrittenGraph != null)
				rewrittenGraphEmbedding = rewrittenGraph.addEmbeddingVertex(PatternCTInfo.nonSingleton(patternNumber, this));

			// Which pattern vertices are ports in this embedding?
			SingletonStopwatchCollection.resume("codeTableRow.vertexRequiresPort");
			List<Integer> portVertices = new LinkedList<>();
			for (int v = 0; v < getPatternGraph().getVertexCount(); ++v)
			{
				if (Utils.vertexRequiresPort(getPatternGraph(), v, embedding, data, elementDescribedMarker))
				{
					portVertices.add(v);
					embedding.getMapping(v).dataVertex.setIsPortMarker(elementDescribedMarker); // We mark the corresponding data vertex as being a port
				}
			}
			SingletonStopwatchCollection.stop("codeTableRow.vertexRequiresPort");

			SingletonStopwatchCollection.resume("codeTableRow.automorphismPortNumberSearch");
			// Automorphisms: could the ports be called something else? We choose the renumbering that maximises port usage
			Automorphism bestAutomorphism = FindFirstMax.find(
					patternInfo.getAutomorphismInfo().getAutomorphisms().stream(), // Between all automorphism
					// We find the one that renumbers the ports to ports that are very used
					automorphism -> portVertices.stream()
							.map(automorphism::getMapping) // To do so, for each port we compute what the automorphism maps it to
							.mapToInt(mappedPort -> portUsages.getOrDefault(mappedPort, 0)) // What is the current usage of this new port?
							.sum() // We want the automorphism for which the sum of all these usages is maximised
			);
			SingletonStopwatchCollection.stop("codeTableRow.automorphismPortNumberSearch");
			// Applying the automorphism and increasing the port usages accordingly
			for (Integer portVertex : portVertices)
			{
				final int portNumberInBestAutomorphism = bestAutomorphism.getMapping(portVertex);
				portUsages.put(portNumberInBestAutomorphism, portUsages.getOrDefault(portNumberInBestAutomorphism, 0) + 1);
				++portUsageSum;
				if (rewrittenGraph != null)
					rewrittenGraph.addPort(rewrittenGraphEmbedding, portNumberInBestAutomorphism, embedding.getMapping(portVertex).dataVertexIndex); // Note: to get the data vertex we must use the port number as it was found in the embedding
			}
		}
		Logger.getLogger(getClass()).debug("Finished computing cover for ct row");
	}

	/**
	 * Default implementation of {@link #prepareCTRowForCoverComputation} constructor parameter.
	 * Resets all cover-dependant values.
	 */
	public static void resetAllCoverDependantAttributes(CodeTableRow row)
	{
		row.usage = 0;
		row.coverEmbeddings = null;
		row.codeLength = 0.0;
		row.portUsages.clear();
		row.portCodeLengths.clear();
		row.portUsageSum = 0;
	}

	/**
	 * Compute and update the codes of this row and its ports.
	 *
	 * @param rowsUsageSum The sum of all usages of the code table. Needed to compute the row's code length.
	 */
	public void computeCodeLengths(int rowsUsageSum)
	{
		if (usage == 0)
			codeLength = 0.0; // If we use the formula it will be Infinity, which will pose a problem for JSON serialization (and is not much more correct).
		else
			codeLength = -Utils.log2((double) usage / rowsUsageSum);
		for (int port : portUsages.keySet())
		{
			double code = -Utils.log2((double) portUsages.get(port) / portUsageSum);
			portCodeLengths.put(port, Math.max(code, 0.0)); // If it is the only port, the formula will return -0.0, which is different to 0.0
		}
	}

	public PatternInfo getPatternInfo()
	{
		return patternInfo;
	}

	public Graph getPatternGraph()
	{
		return patternInfo.getStructure();
	}

	public AutomorphismInfo getAutomorphisms()
	{
		return patternInfo.getAutomorphismInfo();
	}

	public double getPatternSTEncodingLength()
	{
		return patternSTEncodingLength;
	}

	public int getPatternLabelCount()
	{
		return patternLabelCount;
	}

	public int getImageBasedSupport()
	{
		return imageBasedSupport;
	}

	/**
	 * @return The embeddings of this pattern. This class does not modify this value.
	 */
	public List<Embedding> getEmbeddings()
	{
		return embeddings;
	}

	/**
	 * Allow to set the embeddings of this pattern. Some cover strategies need them.
	 * This class does not modify this value.
	 */
	public void setEmbeddings(List<Embedding> embeddings)
	{
		this.embeddings = embeddings;
	}

	/**
	 * @return The embeddings used in the current cover.
	 */
	public List<Embedding> getCoverEmbeddings()
	{
		return coverEmbeddings;
	}

	public Consumer<CodeTableRow> getPrepareCTRowForCoverComputation()
	{
		return prepareCTRowForCoverComputation;
	}

	public void setPrepareCTRowForCoverComputation(Consumer<CodeTableRow> prepareCTRowForCoverComputation)
	{
		this.prepareCTRowForCoverComputation = prepareCTRowForCoverComputation;
	}

	public int getUsage()
	{
		return usage;
	}

	public double getCodeLength()
	{
		return codeLength;
	}

	public void setCodeLength(double codeLength)
	{
		this.codeLength = codeLength;
	}

	public Map<Integer, Integer> getPortUsages()
	{
		// Modifiable because we need to modify it in classification CT
		return portUsages;
	}

	public Map<Integer, Double> getPortCodeLengths()
	{
		// Write access needed in MultiCodeTable
		return portCodeLengths;
	}

	public int getPortUsageSum()
	{
		return portUsageSum;
	}
}
