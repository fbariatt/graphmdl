package graphMDL;

import graph.Graph;

import java.util.Map;

/**
 * A GraphMDL Standard Table, used to compute the description length needed to encode the graph structure of a pattern
 * based on the labels present in the data graph.
 */
public interface StandardTable
{
	/**
	 * @return The description length needed to encode the given graph pattern with this standard table.
	 * @throws StandardTableEncodingException If this ST can not encode the pattern, e.g. some labels are not in the ST
	 */
	double encode(Graph g) throws StandardTableEncodingException;

	/**
	 * @return The description length needed to encode a singleton pattern for the given arity and label with this standard table.
	 * @throws StandardTableEncodingException If this ST can not encode the pattern, e.g. the label is not in the ST
	 */
	double encodeSingleton(int arity, String singletonLabel) throws StandardTableEncodingException;

	/**
	 * @return The description length needed to encode a vertex singleton pattern with the given label with this standard table.
	 * @see #encodeSingleton
	 */
	default double encodeSingletonVertex(String vertexLabel) throws StandardTableEncodingException
	{
		return encodeSingleton(1, vertexLabel);
	}

	/**
	 * @return The description length needed to encode an edge singleton pattern with the given label with this standard table.
	 * @see #encodeSingleton
	 */
	default double encodeSingletonEdge(String edgeLabel) throws StandardTableEncodingException
	{
		return encodeSingleton(2, edgeLabel);
	}

	/**
	 * @return The code lengths for each label of each arity in this ST.
	 */
	Map<Integer, Map<String, Double>> getCodeLengths();

	/**
	 * @return The number of labels in this standard table, of all arities.
	 */
	default int getLabelCount()
	{
		return getCodeLengths().values().stream().mapToInt(Map::size).sum();
	}

	/**
	 * @return The code lengths for each label of a given arity. An empty map if there are no labels for that arity.
	 */
	Map<String, Double> getCodeLengths(int arity);

	/**
	 * @return The code lengths for each vertex label.
	 * @see #getCodeLengths(int)
	 */
	default Map<String, Double> getVertexLabelsCodeLengths()
	{
		return getCodeLengths(1);
	}

	/**
	 * @return The code lengths for each edge label.
	 * @see #getCodeLengths(int)
	 */
	default Map<String, Double> getEdgeLabelsCodeLengths()
	{
		return getCodeLengths(2);
	}

	/**
	 * This exception is raised whenever the program tries to encode a graph with one of the standard tables, but the standard table does not contain some of the graph's labels.
	 */
	class StandardTableEncodingException extends RuntimeException
	{
		public StandardTableEncodingException()
		{
			super();
		}

		public StandardTableEncodingException(String s)
		{
			super(s);
		}

		public StandardTableEncodingException(Throwable throwable)
		{
			super(throwable);
		}

		public StandardTableEncodingException(String s, Throwable throwable)
		{
			super(s, throwable);
		}
	}
}
