package graphMDL;

/**
 * Implementations of universal integer encoding functions to be used in MDL description length computations.
 * <br/>
 * See P. Elias, "Universal codeword sets and representations of the integers", IEEE Transactions on Information Theory, vol. 21, no. 2, pp. 194–203, Mar. 1975, doi: 10.1109/TIT.1975.1055349.
 */
public final class UniversalInteger
{
	private UniversalInteger() {}

	private static IllegalArgumentException createMustBeStrictlyPositiveException(int receivedValue)
	{
		return new IllegalArgumentException("Expected strictly positive number, instead got " + receivedValue);
	}

	private static IllegalArgumentException createMustBe0OrGreaterException(int receivedValue)
	{
		return new IllegalArgumentException("Expected positive number or 0, instead got " + receivedValue);
	}

	/**
	 * @return floor(log2(x)) for an x>0
	 */
	private static int floorLog(int x)
	{
		// According to javadoc, 31 - Integer.numberOfLeadingZeros(x) is equivalent to floor(log2(x)) for all positive int values of x
		if (x < 1)
			throw createMustBeStrictlyPositiveException(x);
		return 31 - Integer.numberOfLeadingZeros(x);
	}

	/**
	 * @return The number of bits needed to encode the strictly positive integer x in binary
	 */
	public static int numberOfBinaryBits(int x)
	{
		if (x < 1)
			throw createMustBeStrictlyPositiveException(x);
		return floorLog(x) + 1;
	}

	/**
	 * @return The number of bits necessary to encode x with Elias gamma encoding.
	 */
	public static int eliasGamma(int x)
	{
		if (x < 1)
			throw createMustBeStrictlyPositiveException(x);
		return 2 * floorLog(x) + 1;
	}

	/**
	 * @return The number of bits necessary to encode x with an Elias gamma encoding allowing for 0.
	 */
	public static int eliasGammaWith0(int x)
	{
		if (x < 0)
			throw createMustBe0OrGreaterException(x);
		return eliasGamma(x + 1);
	}

	/**
	 * @return The number of bits necessary to encode x with Elias delta encoding.
	 */
	public static int eliasDelta(int x)
	{
		if (x < 1)
			throw createMustBeStrictlyPositiveException(x);
		int floorLog = floorLog(x);
		return floorLog + eliasGamma(floorLog + 1);
	}

	/**
	 * @return The number of bits necessary to encode x with an Elias delta encoding that allows for 0.
	 */
	public static int eliasDeltaWith0(int x)
	{
		if (x < 0)
			throw createMustBe0OrGreaterException(x);
		return eliasDelta(x + 1);
	}

	/**
	 * @return The number of bits necessary to encode x with Elias omega encoding.
	 */
	public static int eliasOmega(int x)
	{
		if (x < 1)
			throw createMustBeStrictlyPositiveException(x);

		int length = 1;
		int nbits = numberOfBinaryBits(x);
		while (nbits >= 2)
		{
			length += nbits;
			nbits = numberOfBinaryBits(nbits - 1);
		}
		return length;
	}

	/**
	 * @return The number of bits necessary to encode x with an Elias omega encoding that allows for 0.
	 */
	public static int eliasOmegaWith0(int x)
	{
		if (x < 0)
			throw createMustBe0OrGreaterException(x);
		return eliasOmega(x + 1);
	}

}
