package graphMDL.multigraphs;

import graphMDL.CodeTable;
import graphMDL.CodeTableRow;

import java.util.Comparator;

public class MultiDirectedCodeTable extends MultiCodeTable
{
	public MultiDirectedCodeTable(MultiDirectedStandardTable standardTable, Comparator<CodeTableRow> codeTableRowComparator)
	{
		super(standardTable, codeTableRowComparator);
	}

	@Override
	protected CodeTable constructTableWithSameArgumentsAsThis()
	{
		return new MultiDirectedCodeTable((MultiDirectedStandardTable) st, codeTableRowComparator);
	}

	@Override
	public boolean isDirected()
	{
		return true;
	}
}
