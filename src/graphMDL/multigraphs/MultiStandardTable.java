package graphMDL.multigraphs;

import graph.Edge;
import graph.Graph;
import graphMDL.StandardTable;
import graphMDL.UniversalInteger;
import graphMDL.Utils;
import org.apache.log4j.Logger;
import utils.GeneralUtils;
import utils.MapUtils;

import java.util.*;
import java.util.stream.IntStream;

/**
 * A standard table where all labels are in the same code-space, independently of their arity.
 * It is actually equivalent to having a first prefix code to indicate the arity (based on the distribution of arities frequency) and then a second
 * prefix code to indicate the label between all labels of the same arity (based on the distribution of frequency of all labels with that arity).
 * This standard table is suitable to encode multigraphs.
 */
public abstract class MultiStandardTable implements StandardTable
{
	/**
	 * The constant part of the description length for describing the structure of a vertex singleton pattern.
	 * The description length of the singleton's label and log(|ST|) are missing for a complete description length.
	 * Equivalent to: Vertex count + #occ of label + occurrences of label.
	 */
	private static final double SINGLETON_VERTEX_CONSTANT_DL = UniversalInteger.eliasDeltaWith0(1) + UniversalInteger.eliasDeltaWith0(1) + Utils.log2Binomial(1, 1);
	/**
	 * The constant part of the description length for describing the structure of an edge singleton pattern.
	 * The description length of the singleton's label and log(|ST|) are missing for a complete description length.
	 * Equivalent to: Vertex count + #occ of label + occurrences of label.
	 */
	private static final double SINGLETON_EDGE_CONSTANT_DL = UniversalInteger.eliasDeltaWith0(2) + UniversalInteger.eliasDeltaWith0(1) + Utils.log2Binomial(4, 1);

	private final Map<Integer, Map<String, Double>> st;
	private final double logOfSTSize;

	public MultiStandardTable(Graph g)
	{
		Map<String, Integer> vertexUsages = new HashMap<>();
		Map<String, Integer> edgeUsages = new HashMap<>();

		// Vertex labels usages
		g.getVertices()
				.flatMap(vertex -> vertex.getLabels().keySet().stream())
				.forEach(label -> vertexUsages.merge(label, 1, Integer::sum));
		// Edge labels usages
		g.getEdges()
				.map(Edge::getLabel)
				.forEach(label -> edgeUsages.merge(label, 1, Integer::sum));
		if (!isDirected()) // In an undirected graph each edge would have been counted twice
			edgeUsages.replaceAll((_label, usage) -> usage / 2);
		// Checking that label sets are disjoint
		checkLabelSetsAreDisjoint(vertexUsages.keySet(), edgeUsages.keySet());
		// Total usage
		final int usageSum = IntStream.concat(
				vertexUsages.values().stream().mapToInt(value -> value),
				edgeUsages.values().stream().mapToInt(value -> value)
		).sum();

		// Vertex labels code lengths
		Map<String, Double> vertexCodeLengths = new HashMap<>();
		vertexUsages.forEach((label, usage) -> vertexCodeLengths.put(label, -Utils.log2((double) usage / usageSum)));
		// Edge labels code lengths
		Map<String, Double> edgeCodeLengths = new HashMap<>();
		edgeUsages.forEach((label, usage) -> edgeCodeLengths.put(label, -Utils.log2((double) usage / usageSum)));

		this.st = Map.of(
				1, Collections.unmodifiableMap(vertexCodeLengths),
				2, Collections.unmodifiableMap(edgeCodeLengths)
		);
		this.logOfSTSize = Utils.log2(vertexCodeLengths.size() + edgeCodeLengths.size());
	}

	public MultiStandardTable(Map<String, Double> vertexCodeLengths, Map<String, Double> edgeCodeLengths)
	{
		checkLabelSetsAreDisjoint(vertexCodeLengths.keySet(), edgeCodeLengths.keySet());
		this.st = Map.of(
				1, Collections.unmodifiableMap(vertexCodeLengths),
				2, Collections.unmodifiableMap(edgeCodeLengths)
		);
		this.logOfSTSize = Utils.log2(vertexCodeLengths.size() + edgeCodeLengths.size());
	}

	/**
	 * Check that the vertex labels and edge labels are disjoint.
	 * It is not strictly needed, but it could be an easy source of errors somewhere else in the code, and it is not difficult
	 * for the user to avoid this problem by just changing some labels.
	 */
	private void checkLabelSetsAreDisjoint(Set<String> vertexLabels, Set<String> edgeLabels)
	{
		// Checking that the label sets of the two standard tables are disjoint
		if (!GeneralUtils.isIntersectionEmpty(vertexLabels, edgeLabels))
		{
			Set<String> commonLabels = GeneralUtils.intersection(vertexLabels, edgeLabels);
			Logger.getLogger(this.getClass()).error(String.format("Some labels appear both on vertices and on edges: %s", commonLabels));
			throw new IllegalArgumentException("Some labels appear both on vertices and on edges. Maybe try to capitalize one of the versions?");
		}
	}

	@Override
	public double encode(Graph g) throws StandardTableEncodingException
	{
		double descriptionLength = 0.0;
		final int vertexCount = g.getVertexCount();

		// Number of vertices
		descriptionLength += UniversalInteger.eliasDeltaWith0(vertexCount);
		// Number of different labels
		descriptionLength += logOfSTSize;

		// Vertex labels
		Map<String, Integer> vertexLabelsOccurrenceCounts = new HashMap<>();
		g.getVertices() // Counting occurrences
				.flatMap(vertex -> vertex.getLabels().keySet().stream())
				.forEach(label -> vertexLabelsOccurrenceCounts.merge(label, 1, Integer::sum));
		for (Map.Entry<String, Integer> labelCount : vertexLabelsOccurrenceCounts.entrySet()) // Computing DL for each label
		{
			// Label code
			descriptionLength += MapUtils.mapGetOrThrowRuntime(st.get(1), labelCount.getKey(), () -> new StandardTableEncodingException("Standard Table does not contain vertex label " + labelCount.getKey()));
			// Number of occurrences
			descriptionLength += UniversalInteger.eliasDeltaWith0(labelCount.getValue());
			// Which are the occurrences?
			descriptionLength += Utils.log2Binomial(vertexCount, labelCount.getValue());
		}

		// Edge labels
		final int maxOccurrencesOfAnEdge = isDirected() ? vertexCount * vertexCount : vertexCount * (vertexCount + 1) / 2; // Max number of occurrences that any edge label can have in this graph
		Map<String, Integer> edgeLabelsOccurrenceCounts = new HashMap<>();
		g.getEdges()
				.map(Edge::getLabel)
				.forEach(label -> edgeLabelsOccurrenceCounts.merge(label, 1, Integer::sum));
		for (Map.Entry<String, Integer> labelCount : edgeLabelsOccurrenceCounts.entrySet()) // Computing DL for each label
		{
			// Label code
			descriptionLength += MapUtils.mapGetOrThrowRuntime(st.get(2), labelCount.getKey(), () -> new StandardTableEncodingException("Standard Table does not contain edge label " + labelCount.getKey()));
			// Number of occurrences
			// If the graph is undirected, each occurrence has been counted twice
			descriptionLength += UniversalInteger.eliasDeltaWith0(isDirected() ? labelCount.getValue() : labelCount.getValue() / 2);
			// Which are the occurrences?
			descriptionLength += Utils.log2Binomial(maxOccurrencesOfAnEdge, labelCount.getValue());
		}
		return descriptionLength;
	}

	@Override
	public double encodeSingleton(int arity, String singletonLabel) throws StandardTableEncodingException
	{
		switch (arity)
		{
			case 1:
				return encodeSingletonVertex(singletonLabel);
			case 2:
				return encodeSingletonEdge(singletonLabel);
			default:
				throw new StandardTableEncodingException(String.format("%s can only encode singleton vertices and edges (for now), arity should be 1 or 2, got %d", getClass(), arity));
		}
	}

	@Override
	public double encodeSingletonVertex(String vertexLabel) throws StandardTableEncodingException
	{
		Double labelCode = getVertexLabelsCodeLengths().get(vertexLabel);
		if (labelCode == null)
			throw new StandardTableEncodingException("Standard Table does not contain vertex label " + vertexLabel);

		return SINGLETON_VERTEX_CONSTANT_DL + labelCode + logOfSTSize;
	}

	@Override
	public double encodeSingletonEdge(String edgeLabel) throws StandardTableEncodingException
	{
		Double labelCode = getEdgeLabelsCodeLengths().get(edgeLabel);
		if (labelCode == null)
			throw new StandardTableEncodingException("Standard Table does not contain edge label " + edgeLabel);

		return SINGLETON_EDGE_CONSTANT_DL + labelCode + logOfSTSize;
	}

	/**
	 * @return Whether this ST encodes directed or undirected graphs
	 */
	protected abstract boolean isDirected();

	@Override
	public Map<Integer, Map<String, Double>> getCodeLengths()
	{
		return this.st;
	}

	@Override
	public Map<String, Double> getCodeLengths(int arity)
	{
		return this.st.getOrDefault(arity, Collections.emptyMap());
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		MultiStandardTable that = (MultiStandardTable) o;
		return st.equals(that.st);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(st);
	}
}
