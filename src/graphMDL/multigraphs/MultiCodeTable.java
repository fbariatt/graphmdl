package graphMDL.multigraphs;

import graph.Graph;
import graphMDL.*;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import utils.SingletonStopwatchCollection;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * {@link CodeTable} for multigraphs. Introduced for the KG-MDL approach.
 * This code table uses different description length formulas than the one for non-multigraphs.
 * In particular, this code table uses prequential codes and does not use prefix codes.
 */
public abstract class MultiCodeTable extends CodeTable
{
	public MultiCodeTable(MultiStandardTable standardTable, Comparator<CodeTableRow> codeTableRowComparator)
	{
		super(standardTable, codeTableRowComparator);
	}

	@Override
	protected PatternInfo createVertexSingletonPatternInfo(String vertexLabel)
	{
		return Utils.createVertexSingletonPatternInfo(vertexLabel);
	}

	@Override
	protected PatternInfo createEdgeSingletonPatternInfo(String edgeLabel)
	{
		return Utils.createEdgeSingletonPatternInfo(edgeLabel);
	}

	@Override
	public Optional<RewrittenGraph> apply(Graph data, CoverStrategy coverStrategy, boolean _createRewrittenGraph)
	{
		// The rewritten graph information is needed to compute D|M, so we force to create one every time
		return super.apply(data, coverStrategy, true);
	}

	/**
	 * Prefix codes are not needed for the KG-MDL multigraph encoding.
	 * This method sets them to 0 instead of computing them based on usages.
	 */
	@Override
	protected void computeTableCodes()
	{
		rows.forEach(
				row -> {
					row.setCodeLength(0.0);
					row.getPortUsages().forEach((k, _v) -> row.getPortCodeLengths().put(k, 0.0));
				}
		);
		singletonVertexUsages.forEach((k, _v) -> singletonVertexLengths.put(k, 0.0));
		singletonEdgeUsages.forEach((k, _v) -> singletonEdgeLengths.put(k, 0.0));
	}

	@Override
	protected double computeRowDescriptionLength(CodeTableRow row)
	{
		if (row.getUsage() == 0)
			return 0.0; // Unused row are not taken into account in DL computations

		double descriptionLength = 0.0;
		// Pattern structure
		descriptionLength += st.encode(row.getPatternGraph());
		// Port count
		descriptionLength += Utils.log2(row.getPatternGraph().getVertexCount() + 1);
		// Which vertices are ports
		descriptionLength += Utils.log2Binomial(row.getPatternGraph().getVertexCount(), row.getPortUsages().size());

		return descriptionLength;
	}

	@Override
	protected double computeVertexSingletonPatternDescriptionLength(String label)
	{
		double descriptionLength = 0.0;
		// Pattern structure
		descriptionLength += st.encodeSingletonVertex(label);
		// Port count
		descriptionLength += 1; // Equivalent of log(vertex count + 1), i.e. log(2)
		// Which vertices are ports
		// log(binomial(1,1)) is 0. We assume that the vertex will always be used as port at least once.

		return descriptionLength;
	}

	@Override
	protected double computeEdgeSingletonPatternDescriptionLength(String label)
	{
		double descriptionLength = 0.0;
		// Pattern structure
		descriptionLength += st.encodeSingletonEdge(label);
		// Port count
		descriptionLength += Utils.log2(3); // Equivalent of log(vertex count + 1), i.e. log(3)
		// Which vertices are ports
		// log(binomial(2,2)) is 0. We assume that both ends will always be used as ports at least once each.

		return descriptionLength;
	}
	
	@Override
	protected void computeEncodedGraphLength(int dataVertexCount)
	{
		if (rewrittenGraph == null)
			throw new RuntimeException("Rewritten graph is null, but is needed to compute L(D|M) on multigraphs");

		SingletonStopwatchCollection.resume("MultiCodeTable.computeEncodedGraphLength");
		encodedGraphLength = 0.0;
		final int embeddingVerticesCount = Math.toIntExact(rewrittenGraph.getEmbeddingVertices().count());

		// Embedding vertices count
		encodedGraphLength += UniversalInteger.eliasDeltaWith0(embeddingVerticesCount);

		// Port vertices count
		encodedGraphLength += Utils.log2(dataVertexCount + 1);

		// Pattern of each embedding vertex
		List<Integer> allPatternsUsages = rows.stream().map(CodeTableRow::getUsage).collect(Collectors.toList());
		allPatternsUsages.addAll(singletonVertexUsages.values());
		allPatternsUsages.addAll(singletonEdgeUsages.values());
		encodedGraphLength += PrequentialCode.prequentialCodeDLIterative(allPatternsUsages, 0.5);

		// Number of edges per embedding vertex
		for (CodeTableRow row : rows)
			encodedGraphLength += Utils.log2(row.getPortUsages().size() + 1) * row.getUsage();
		for (Integer usage : singletonVertexUsages.values())
			encodedGraphLength += usage; // Equivalent to log(2) * usage
		for (Integer usage : singletonEdgeUsages.values())
			encodedGraphLength += Utils.log2(3) * usage;

		// Edge destinations
		List<Integer> portUsages = rewrittenGraph.getPortVertices()
				.map(portVertex -> rewrittenGraph.getEmbeddingHaving(portVertex).count())
				.map(Math::toIntExact)
				.collect(Collectors.toList());
		encodedGraphLength += PrequentialCode.prequentialCodeDLIterative(portUsages, 0.5);

		// Edge labels
		for (CodeTableRow row : rows)
			encodedGraphLength += PrequentialCode.prequentialCodeDLIterative(new ArrayList<>(row.getPortUsages().values()), 0.5);
		// Singleton vertices ignored as a prequential encoding for a single possible value is always 0
		for (Integer usage : singletonEdgeUsages.values())
			encodedGraphLength += PrequentialCode.prequentialCodeDLIterative(List.of(usage, usage), 0.5); // We assume that both ports are always used.

		SingletonStopwatchCollection.stop("MultiCodeTable.computeEncodedGraphLength");
	}
}
