package graphMDL.multigraphs;

import graph.Graph;

import java.util.Map;

/**
 * An implementation of {@link MultiStandardTable} or directed multigraphs.
 *
 * @see MultiStandardTable
 */
public class MultiDirectedStandardTable extends MultiStandardTable
{
	public MultiDirectedStandardTable(Graph g)
	{
		super(g);
	}

	public MultiDirectedStandardTable(Map<String, Double> vertexCodeLengths, Map<String, Double> edgeCodeLengths)
	{
		super(vertexCodeLengths, edgeCodeLengths);
	}

	@Override
	protected boolean isDirected()
	{
		return true;
	}
}
