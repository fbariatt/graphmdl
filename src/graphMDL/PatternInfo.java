package graphMDL;

import graph.automorphisms.AutomorphismInfo;
import graph.automorphisms.AutomorphismSearch;
import graph.Graph;

/**
 * Immutable class to contain information of a pattern: its structure and its automorphisms.
 */
public class PatternInfo
{
	private final Graph patternStructure;
	private final AutomorphismInfo automorphismInfo;

	public PatternInfo(Graph patternStructure, AutomorphismInfo automorphismInfo)
	{
		this.patternStructure = patternStructure;
		this.automorphismInfo = automorphismInfo;
	}

	/**
	 * Create a PatternInfo object from the given graph, filling the automorphism info with trivial information (see {@link AutomorphismInfo#trivialInfo}).
	 * Note that the resulting automorphism information is incorrect, and this method is supposed to be used only when automorphisms do not matter.
	 */
	public static PatternInfo createWithTrivialInfo(Graph patternStructure)
	{
		return new PatternInfo(patternStructure, AutomorphismInfo.trivialInfo(patternStructure));
	}

	/**
	 * Create a PatternInfo object from the given graph, filling the automorphism info by computing the <em>base</em> automorphisms of the graph (see {@link AutomorphismSearch#findBaseAutomorphisms(Graph)}).
	 */
	public static PatternInfo createComputingBaseAutomorphisms(Graph patternStructure)
	{
		return new PatternInfo(patternStructure, AutomorphismSearch.findBaseAutomorphisms(patternStructure));
	}

	/**
	 * Create a PatternInfo object from the given graph, filling the automorphism info by computing the <em>complete set</em> of automorphism of the graph (see {@link AutomorphismSearch#findAndSaturateAutomorphisms(Graph)}).
	 * Note that for some complex graphs this set may be huge, taking a lot of memory and time to compute.
	 */
	public static PatternInfo createComputingSaturatedAutomorphisms(Graph patternStructure)
	{
		return new PatternInfo(patternStructure, AutomorphismSearch.findAndSaturateAutomorphisms(patternStructure));
	}

	public Graph getStructure()
	{
		return patternStructure;
	}

	public AutomorphismInfo getAutomorphismInfo()
	{
		return automorphismInfo;
	}
}
