package graphMDL;

import graph.Edge;
import graph.Graph;
import graph.GraphFactory;
import graph.Vertex;
import patternMatching.Embedding;
import utils.GeneralUtils;
import utils.SingletonStopwatchCollection;
import utils.SortedList;

import java.io.PrintStream;
import java.util.*;
import java.util.function.Predicate;

public final class Utils
{
	private static final double LOG10OF2 = Math.log10(2);

	private Utils() {}

	/**
	 * Sort the given list of embeddings by their overlaps. The first embedding in the resulting list is
	 * the one that has the least edges that overlaps with the other embeddings.
	 * The embeddings overlapCount attribute is set appropriately.
	 *
	 * @param unsortedEmbeddings  The embeddings to sort
	 * @param pattern             The pattern of the embeddings
	 * @param data                The data in which the pattern is embedded.
	 * @param totalOrderEmbedding If true, equalities are broken by a total order on the embeddings based on the sequence of data vertices numbers of each embedding
	 * @return A sorted  list containing the given embeddings form the one that has the least overlaps to the one that has the most.
	 */
	public static SortedList<Embedding> sortEmbeddings(List<Embedding> unsortedEmbeddings, Graph pattern, Graph data, boolean totalOrderEmbedding)
	{
		SingletonStopwatchCollection.resume("graphMDL.sortEmbeddings");
		// Counting how many times each data edge is used by these embeddings
		Map<Edge, Integer> dataEdgeUsages = new HashMap<>();
		unsortedEmbeddings.stream()
				.flatMap(embedding -> // Getting data edges used by this embedding
						pattern.getEdges().map(patternEdge -> getEdgeEmbedding(pattern, patternEdge, embedding, data))
				)
				.forEach(dataEdge -> dataEdgeUsages.put(dataEdge, dataEdgeUsages.getOrDefault(dataEdge, 0) + 1)); // Counting usage of each data edge
		// Counting overlap of each embedding
		unsortedEmbeddings.forEach(embedding -> embedding.setOverlapCount(
				pattern.getEdges()
						.map(patternEdge -> getEdgeEmbedding(pattern, patternEdge, embedding, data))
						.mapToInt(dataEdge -> dataEdgeUsages.get(dataEdge) - 1)
						.sum()
		));
		// Sorting embeddings
		Comparator<Embedding> embeddingComparator = Comparator.comparingInt(Embedding::getOverlapCount);
		if (totalOrderEmbedding)
		{
			embeddingComparator = embeddingComparator.thenComparing(
					(emb1, emb2) -> GeneralUtils.lexicographicCompare(
							Arrays.stream(emb1.getMappingsByIndex())
									.map(mapping -> mapping.dataVertexIndex)
									.iterator(),
							Arrays.stream(emb2.getMappingsByIndex())
									.map(mapping -> mapping.dataVertexIndex)
									.iterator()
					));
		}
		SortedList<Embedding> sortedEmbeddings = new SortedList<>(embeddingComparator);
		sortedEmbeddings.addAll(unsortedEmbeddings);
		SingletonStopwatchCollection.stop("graphMDL.sortEmbeddings");
		return sortedEmbeddings;
	}

	/**
	 * See {@link #sortEmbeddings(List, Graph, Graph, boolean)}. Does apply total ordering to embeddings.
	 */
	public static SortedList<Embedding> sortEmbeddings(List<Embedding> unsortedEmbeddings, Graph pattern, Graph data)
	{
		return sortEmbeddings(unsortedEmbeddings, pattern, data, true);
	}

	/**
	 * Get the data edge corresponding to a pattern edge following a given embedding.
	 *
	 * @throws NoSuchElementException if the data edge can not be found (the embedding may be incorrect)
	 */
	public static Edge getEdgeEmbedding(Graph pattern, Edge patternEdge, Embedding embedding, Graph data)
	{
		Map<Vertex, Integer> patternVertexIndexMap = pattern.getVertexIndexMap();
		Edge dataEdge = data.getEdge(
				embedding.getMapping(patternVertexIndexMap.get(patternEdge.getSource())).dataVertex,
				embedding.getMapping(patternVertexIndexMap.get(patternEdge.getTarget())).dataVertex,
				patternEdge.getLabel()
		);
		if (dataEdge == null)
			throw new NoSuchElementException("Impossible to find data edge corresponding to the pattern edge following the given embedding. Is the embedding correct?");
		return dataEdge;
	}

	/**
	 * Check whether a given embedding is valid.
	 * For an embedding to be valid, none of its data edges can be already described by another embedding;
	 * Additionally, the embedding must describe at least something (i.e. it must contain any label that is not already described by other embeddings).
	 *
	 * @param pattern                The pattern graph
	 * @param embedding              The embedding
	 * @param data                   The data graph
	 * @param elementDescribedMarker Value of the vertex/edge markers that means that the element is already described by another embedding
	 */
	public static boolean isEmbeddingValid(Graph pattern, Embedding embedding, Graph data, int elementDescribedMarker)
	{
		if (pattern.getEdges().findAny().isPresent()) // If the pattern has at least one edge
		{
			// Checking if any edge overlaps with one described by another embedding
			boolean anyEdgeOverlaps = pattern.getEdges()
					.map(patternEdge -> getEdgeEmbedding(pattern, patternEdge, embedding, data)) // Getting the corresponding data edge
					.anyMatch(dataEdge -> dataEdge.getMarker() == elementDescribedMarker); // Is the data edge already described?
			return !anyEdgeOverlaps;
		}
		else // The pattern has no edges
		{
			for (int v = 0; v < pattern.getVertexCount(); ++v)
			{
				final Vertex patternVertex = pattern.getVertex(v);
				final Vertex dataVertex = embedding.getMapping(v).dataVertex;
				for (String labelString : patternVertex.getLabels().keySet())
				{
					if (dataVertex.getLabels().get(labelString).getMarker() != elementDescribedMarker) // This label is not described by another embedding, therefore it will be described by this one
					{
						// The embedding describes something, therefore it is valid
						return true;
					}
				}
			}
			// If we get here, it means that the embedding doesn't describe any non-described vertex label, therefore it is not valid
			return false;
		}
	}

	/**
	 * Mark an embeddings of a pattern in the data graph and all of its vertex labels and edges with a specific marker
	 */
	public static void markEmbedding(Graph pattern, Embedding embedding, Graph data, int marker)
	{
		embedding.setMarker(marker);
		for (int v = 0; v < pattern.getVertexCount(); ++v)
		{
			final Vertex patternVertex = pattern.getVertex(v);
			final Vertex dataVertex = embedding.getMapping(v).dataVertex;
			patternVertex.getLabels().keySet().forEach(vertexLabelString -> dataVertex.getLabels().get(vertexLabelString).setMarker(marker));
			pattern.getOutEdges(patternVertex).forEach(edge -> getEdgeEmbedding(pattern, edge, embedding, data).setMarker(marker));
		}
	}

	public static boolean vertexRequiresPort(Graph pattern, int patternVertexIndex, Embedding embedding, Graph data, int alreadyPortMarker)
	{
		Vertex dataVertex = embedding.getMapping(patternVertexIndex).dataVertex;
		int dataVertexIndex = embedding.getMapping(patternVertexIndex).dataVertexIndex;

		if (dataVertex.getIsPortMarker() == alreadyPortMarker)
			return true;
		if (dataVertex.getLabels().size() > pattern.getVertex(patternVertexIndex).getLabels().size())
			return true;
		if (data.getOutEdgeCount(dataVertexIndex) > pattern.getOutEdgeCount(patternVertexIndex))
			return true;
		if (data.getInEdgeCount(dataVertexIndex) > pattern.getInEdgeCount(patternVertexIndex))
			return true;
		return false;
	}

	/**
	 * @return The base 2 logarithm of a number
	 */
	public static double log2(double x)
	{
		return Math.log10(x) / LOG10OF2;
	}

	/**
	 * Compute the binomial coefficient for a given n and given k. Also called "n choose k".
	 * <br/>
	 * See https://math.stackexchange.com/a/927064
	 * <br/>
	 * <strong>Beware that the binomial coefficient can get very large very quickly, and might possibly cause overflows for large values.</strong>
	 */
	public static double binomial(int n, int k)
	{
		if (k > n)
			throw new IllegalArgumentException("k should be lower than n in binomial coefficient");
		if (k == 0)
			return 1;
		if (k > n / 2)
			return binomial(n, n - k);
		return n * binomial(n - 1, k - 1) / k;
	}

	/**
	 * Compute the log base 2 of the binomial coefficient for a given n and k (also called "n choose k").
	 * Since binomial coefficient can get very large, introducing log in the computation instead of after can
	 * help avoid overflows.
	 *
	 * @see #binomial(int, int)
	 * @see #log2(double)
	 */
	public static double log2Binomial(int n, int k)
	{
		if (k > n)
			throw new IllegalArgumentException("k should be lower than n in binomial coefficient");
		if (k == 0)
			return 0.0;
		if (k > n / 2)
			return log2Binomial(n, n - k);
		return log2(n) - log2(k) + log2Binomial(n - 1, k - 1);
	}

	/**
	 * Print a representation of a Code Table to a stream.
	 */
	public static void printCodeTable(CodeTable ct, PrintStream out)
	{
		int patternNumber = 1;
		for (CodeTableRow row : ct.getRows())
		{
			out.printf("Pattern P%d: %d vertices, %d edges\n", patternNumber, row.getPatternGraph().getVertexCount(), row.getPatternGraph().getEdgeCount());
			out.printf("\tL(P|ST) = %f\n", row.getPatternSTEncodingLength());
			out.printf("\tImage based support: %d\n", row.getImageBasedSupport());
			if (row.getUsage() > 0)
			{
				out.println("\tPorts:");
				for (int port : row.getPortUsages().keySet())
				{
					out.printf("\t\tPort %d: usage %d, code length %f\n", port, row.getPortUsages().get(port), row.getPortCodeLengths().get(port));
				}
				out.printf("\t\tTotal of %d ports\n", row.getPortUsageSum());
				out.printf("\tPattern usage: %d\n", row.getUsage());
				out.printf("\tPattern code length: %f\n", row.getCodeLength());
			}
			else
			{
				out.println("\t>> Unused pattern <<");
			}
			patternNumber++;
		}
		for (String vertexLabel : ct.getSingletonVertexUsages().keySet())
		{
			out.printf("Singleton vertex pattern: %s\n", vertexLabel);
			out.printf("\tUsage: %d\n", ct.getSingletonVertexUsages().get(vertexLabel));
			out.printf("\tCode length: %f\n", ct.getSingletonVertexLengths().get(vertexLabel));
			patternNumber++;
		}
		for (String edgeLabel : ct.getSingletonEdgeUsages().keySet())
		{
			out.printf("Singleton edge pattern: %s\n", edgeLabel);
			out.printf("\tUsage: %d\n", ct.getSingletonEdgeUsages().get(edgeLabel));
			out.printf("\tCode length: %f\n", ct.getSingletonEdgeLengths().get(edgeLabel));
			patternNumber++;
		}
		out.printf("Rewritten graph has %d port nodes\n", ct.getRewrittenGraphPortCount());
		out.printf("L(CT) = %f\n", ct.getModelDL());
		out.printf("L(D|CT) = %f\n", ct.getEncodedGraphLength());
		out.printf("L(D,CT) = %f\n", ct.getModelDL() + ct.getEncodedGraphLength());
	}

	public static void printCodeTable(CodeTable ct)
	{
		printCodeTable(ct, System.out);
	}

	/**
	 * @return The amount of labels that a graph has.
	 */
	public static int graphLabelCount(Graph g)
	{
		int labelCount = 0;
		Iterator<Vertex> vertices = g.getVertices().iterator();
		while (vertices.hasNext())
		{
			Vertex vertex = vertices.next();
			labelCount += vertex.getLabels().size();
			labelCount += g.getOutEdgeCount(vertex); // Each edge has exactly one label
		}
		return labelCount;
	}

	/**
	 * Create all possible graphs composed of two vertices, each with one label, connected by an edge with one label.
	 *
	 * @param vertexLabels All vertex labels that can be combined.
	 * @param edgeLabels   All edge labels that can be combined.
	 */
	public static Iterable<Graph> createTrivialGraphs(Iterable<String> vertexLabels, Iterable<String> edgeLabels)
	{
		List<Graph> result = new LinkedList<>();
		for (String vertexLabelA : vertexLabels)
		{
			for (String vertexLabelB : vertexLabels)
			{
				for (String edgeLabel : edgeLabels)
				{
					Graph pattern = GraphFactory.createSimpleGraph();
					pattern.addVertex(vertexLabelA);
					pattern.addVertex(vertexLabelB);
					pattern.addEdge(0, 1, edgeLabel);
					result.add(pattern);
				}
			}
		}
		return result;
	}

	/**
	 * Create a vertex-singleton graph for the given label
	 */
	public static Graph createVertexSingleton(String label)
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex(label);
		return g;
	}

	/**
	 * Create a {@link PatternInfo} object describing a vertex-singleton graph of the given label
	 */
	public static PatternInfo createVertexSingletonPatternInfo(String label)
	{
		return PatternInfo.createComputingSaturatedAutomorphisms(createVertexSingleton(label));
	}

	/**
	 * Create an edge-singleton graph for the given label
	 */
	public static Graph createEdgeSingleton(String label)
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex();
		g.addVertex();
		g.addEdge(0, 1, label);
		return g;
	}

	/**
	 * Create a {@link PatternInfo} object describing an edge-singleton graph of the given label
	 */
	public static PatternInfo createEdgeSingletonPatternInfo(String label)
	{
		return PatternInfo.createComputingSaturatedAutomorphisms(createEdgeSingleton(label));
	}

	/**
	 * @return Whether the given graph is a vertex-singleton graph
	 */
	public static boolean isVertexSingleton(Graph graph)
	{
		// A vertex-singleton graph is composed of a single vertex, with a single label
		Predicate<Graph> isSingleton = g ->
				g.getVertexCount() == 1
				&& g.getVertex(0).getLabels().size() == 1;
		return isSingleton.test(graph);
	}

	/**
	 * @return Whether the given graph is an edge-singleton graph
	 */
	public static boolean isEdgeSingleton(Graph graph)
	{
		// An edge-singleton graph is composed of two unlabeled vertices, connected by a single edge from the first to the second
		Predicate<Graph> isSingleton = g ->
				g.getVertexCount() == 2
				&& g.getVertex(0).getLabels().size() == 0
				&& g.getVertex(1).getLabels().size() == 0
				&& g.getEdgeCount() == 1
				&& g.getEdges(0, 1).count() == 1;
		return isSingleton.test(graph);
	}
}
