package graphMDL;

import org.apache.log4j.Logger;
import utils.FindFirstMin;
import utils.Procedure;
import utils.SingletonStopwatchCollection;

import java.util.LinkedHashSet;

/**
 * Code table pruning, as found in Krimp.
 * See J. Vreeken, M. van Leeuwen, and A. Siebes, "Krimp: mining itemsets that compress", Data Min Knowl Disc, vol. 23, no. 1, pp. 169–214, Jul. 2011, doi: 10.1007/s10618-010-0202-x.
 */
public final class CodeTablePruning
{
	private static final Logger logger = Logger.getLogger(CodeTablePruning.class);

	private CodeTablePruning() {}

	/**
	 * Prune a code table *in-place* following Krimp's pruning algorithm.
	 * To be called <em>every time</em> that a code table has been accepted (i.e. its description length is better than the previous best code table).
	 * <br/>
	 * <b>Warning!</b> The code table is not guaranteed to be in a correct state w.r.t. the cover after this function finishes!
	 *
	 * @param ct             The code table to prune. It must have been "accepted".
	 * @param applyCodeTable A function that re-computes the cover of the given ct *in-place*.
	 * @param skip0UsageRows If true, CT rows whose *new* usage after a cover is 0 will not be considered as pruning candidates, even if their usage was higher previously.
	 */
	public static void pruneCodeTable(CodeTable ct, Procedure applyCodeTable, boolean skip0UsageRows)
	{
		logger.debug("Starting code table pruning" + (skip0UsageRows ? " (skipping rows with 0 usage)" : ""));
		final int ctSizeBefore = ct.getRows().size();
		SingletonStopwatchCollection.resume("codeTablePruning.pruneCodeTable");
		double bestDL = getCodeTableDL(ct);
		// We use a LinkedHashSet to ensure that addition of new candidates will not disrupt the relative order of already-existing candidates
		LinkedHashSet<CodeTableRow> pruningCandidates = getPruningCandidates(ct, skip0UsageRows);
		logger.debug(String.format("%d initial pruning candidates", pruningCandidates.size()));
		while (!pruningCandidates.isEmpty())
		{
			CodeTableRow candidate = FindFirstMin.find(pruningCandidates, CodeTableRow::getUsage);
			pruningCandidates.remove(candidate);
			ct.removeRow(candidate);
			applyCodeTable.run();
			double newDL = getCodeTableDL(ct);
			if (newDL < bestDL) // Removing the candidate has made the CT better
			{
				bestDL = newDL;
				logger.debug("Pattern pruned from CT: " + candidate.getAutomorphisms().getCanonicalName());
				int currentPruningCandidatesCount = pruningCandidates.size();
				pruningCandidates.addAll(getPruningCandidates(ct, skip0UsageRows)); // This new code table could have generated new pruning candidates
				logger.debug(String.format("%d additional pruning candidates found", pruningCandidates.size() - currentPruningCandidatesCount));
			}
			else // Removing the candidate did not make the CT better
			{
				ct.addRow(candidate); // We add the candidate back
			}
		}
		SingletonStopwatchCollection.stop("codeTablePruning.pruneCodeTable");
		logger.debug(String.format("Pruned %d patterns from code table", ctSizeBefore - ct.getRows().size()));
		// Warning! If the last candidate did not improve the code table, the code table is now in an incorrect state since we modified it since the last cover!
	}

	/**
	 * @return The description length of a code table. A code table is considered better than another if its description length is lower than the other's.
	 */
	private static double getCodeTableDL(CodeTable ct)
	{
		return ct.getModelDL() + ct.getEncodedGraphLength();
	}

	/**
	 * To be called every time a new code table has been accepted (i.e. its DL improved).
	 * Retrieve the code table rows that are candidates for pruning, i.e. all the rows whose usage has decreased when the last accepted code table.
	 * Also for each row store the current usage as the usage of the last accepted code table (meaning that <strong>this function is <em>not</em> idempotent</strong>).
	 *
	 * @param ct             The code table from which to extract candidate rows.
	 * @param skip0UsageRows If true, the rows whose *new* usage is 0 will not be candidates, even if their usage was higher previously.
	 * @return A {@link LinkedHashSet} of rows that are candidate for pruning. The rows are in the order that they appear in the CT.
	 */
	private static LinkedHashSet<CodeTableRow> getPruningCandidates(CodeTable ct, boolean skip0UsageRows)
	{
		SingletonStopwatchCollection.resume("codeTablePruning.getPruningCandidates");
		LinkedHashSet<CodeTableRow> pruningCandidates = new LinkedHashSet<>();
		ct.getRows().stream()
				.filter(row -> (!skip0UsageRows) || row.getUsage() != 0) // If we are asked to skip rows with 0 usage, we do so, otherwise all rows are accepted by this filter
				.filter(row -> row.getUsage() < row.lastAcceptedUsage)
				.forEachOrdered(pruningCandidates::add);
		ct.getRows().forEach(row -> row.lastAcceptedUsage = row.getUsage());
		SingletonStopwatchCollection.stop("codeTablePruning.getPruningCandidates");
		return pruningCandidates;
	}
}
