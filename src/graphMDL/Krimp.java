package graphMDL;

import graph.Graph;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import org.apache.log4j.Logger;
import patternMatching.RDFGraphMatcher;
import utils.SingletonStopwatchCollection;
import utils.SortedList;

import java.util.Comparator;
import java.util.stream.Stream;

/**
 * The Krimp algorithm for computing the best code table for encoding a data graph with a set of candidate patterns, according to MDL.
 */
public abstract class Krimp
{
	private final Logger logger = Logger.getLogger(getClass());

	// Parameters
	protected final Graph data;
	protected final Comparator<CodeTableRow> candidateComparator;
	protected final Comparator<CodeTableRow> codeTableComparator;
	private final boolean useAutomorphisms;
	// Attributes computed from parameters
	private final RDFGraphMatcher patternMatcher;
	protected final StandardTable st;
	private final SortedList<CodeTableRow> orderedCandidates;

	// Attributes used during code table computation
	protected CodeTable codeTable;
	private int testedCodeTablesCount;


	/**
	 * @param data                The data graph
	 * @param candidates          Candidate patterns to be added to the code table. The candidates do not need to be in order, an ordered list will be created based on the given comparator.
	 * @param candidateComparator Candidate order. Tell whether a given candidate pattern should be considered for use before another candidate pattern.
	 * @param codeTableComparator Code table order. Tell whether a given pattern should be placed closer to the top of the code table than another.
	 * @param useAutomorphisms    If true, pattern automorphisms are used in the choice of port numbering: the automorphism which maximises port usage is chosen
	 */
	public Krimp(Graph data, Stream<Graph> candidates, Comparator<CodeTableRow> candidateComparator, Comparator<CodeTableRow> codeTableComparator, boolean useAutomorphisms)
	{
		this.data = data;
		this.candidateComparator = candidateComparator;
		this.codeTableComparator = codeTableComparator;
		this.useAutomorphisms = useAutomorphisms;

		this.patternMatcher = new RDFGraphMatcher(data);
		st = createStandardTableFromData();
		orderedCandidates = computeOrderedCandidates(candidates);
	}

	/**
	 * Create the standard table from the data
	 */
	protected abstract StandardTable createStandardTableFromData();

	/**
	 * Compute a {@link CodeTableRow} from each candidate and store them in an ordered candidate list, based on the candidate order.
	 */
	private SortedList<CodeTableRow> computeOrderedCandidates(Stream<Graph> candidates)
	{
		SingletonStopwatchCollection.resetAndStart("krimp.candidates");

		SortedList<CodeTableRow> orderedCandidates = new SortedList<>(candidateComparator);
		candidates
				.parallel()
				.filter(this::isValidCandidate)
				.map(candidate -> // Creating a CodeTableRow from each candidate pattern (cpu-intensive operation)
				{
					PatternInfo patternInfo = useAutomorphisms ? PatternInfo.createComputingSaturatedAutomorphisms(candidate) : PatternInfo.createWithTrivialInfo(candidate);
					int support = patternMatcher.imageBasedSupport(candidate);
					if (support == 0)
						logger.error(String.format("Support for pattern %s is 0!", patternInfo.getAutomorphismInfo().getCanonicalName()));

					return new CodeTableRow(
							patternInfo,
							st.encode(candidate),
							support,
							this.labelCount(candidate)
					);
				})
				.forEachOrdered(candidate -> // Adding the Row to the list of candidates
				{
					orderedCandidates.addWithoutSorting(candidate); // We will sort when all have been added to gain some time

					if (orderedCandidates.size() % 1000 == 0)
						logger.info("Loaded " + orderedCandidates.size() + " candidates in ordered list");
				});
		logger.info("Loaded " + orderedCandidates.size() + " candidates");
		orderedCandidates.sort();
		logger.info("Sorted candidates");
		SingletonStopwatchCollection.stop("krimp.candidates");
		return orderedCandidates;
	}

	/**
	 * Checks whether the given pattern is a valid candidate and should be added to the list of candidates.
	 */
	protected abstract boolean isValidCandidate(Graph pattern);

	/**
	 * @return The number of labels in the given graph
	 */
	protected abstract int labelCount(Graph g);

	/**
	 * The main computation function. Process all candidates and compute the best code table for encoding the data.
	 *
	 * @param usePruning                     Whether to prune for useless patterns after a new acceptable code table is found
	 * @param stripUnusedPatternsFromFinalCT Remove unused patterns from the best code table before returning it.
	 * @return The code table. Alternatively, getCodeTable can be used after calling this function.
	 */
	public CodeTable computeBestCodeTable(boolean usePruning, boolean stripUnusedPatternsFromFinalCT)
	{
		SingletonStopwatchCollection.resetAndStart("krimp.searchCodeTable");
		testedCodeTablesCount = 0;

		logger.info("Initialising an empty code table");
		initCodeTable();

		// Computing description length of the empty code table
		applyCodeTable();
		double bestDL = codeTable.getModelDL() + codeTable.getEncodedGraphLength();

		// Printing code table information
		logger.info("L(CT0) = " + codeTable.getModelDL());
		logger.info("L(D | CT0) = " + codeTable.getEncodedGraphLength());
		logger.info("L(D, CT0) = " + bestDL);
		logger.info("CT0 singleton vertex pattern count: " + codeTable.getSingletonVertexUsages().size());
		logger.info("CT0 singleton edge pattern count: " + codeTable.getSingletonEdgeUsages().size());

		for (CodeTableRow candidate : orderedCandidates)
		{
			candidate.setEmbeddings(Utils.sortEmbeddings(patternMatcher.getEmbeddings(candidate.getPatternGraph()), candidate.getPatternGraph(), data));
			codeTable.addRow(candidate);
			applyCodeTable();
			double DL = codeTable.getModelDL() + codeTable.getEncodedGraphLength();
			if (DL < bestDL)
			{
				bestDL = DL;

				// Pruning
				if (usePruning)
					CodeTablePruning.pruneCodeTable(codeTable, this::applyCodeTable, true);
			}
			else
			{
				codeTable.removeRow(candidate);
			}
		}
		SingletonStopwatchCollection.stop("krimp.searchCodeTable");

		// If the last candidate was not retained, the code table has incorrect values for usages, description lengths and such
		// We do one last computation to have the correct values for the final code table
		applyCodeTable();

		if (stripUnusedPatternsFromFinalCT)
		{
			logger.info("Removing CT rows with unused patterns");
			codeTable.removedUnused();
		}

		// Printing code table description lengths
		logger.info("L(CT) = " + codeTable.getModelDL());
		logger.info("L(D | CT) = " + codeTable.getEncodedGraphLength());
		logger.info("L(D, CT) = " + bestDL);

		return codeTable;
	}

	/**
	 * See {@link #computeBestCodeTable}
	 */
	public CodeTable computeBestCodeTableWithPruning(boolean stripUnusedPatternsFromFinalCT)
	{
		return computeBestCodeTable(true, stripUnusedPatternsFromFinalCT);
	}

	/**
	 * See {@link #computeBestCodeTable}
	 */
	public CodeTable computeBestCodeTableWithoutPruning(boolean stripUnusedPatternsFromFinalCT)
	{
		return computeBestCodeTable(false, stripUnusedPatternsFromFinalCT);
	}

	/**
	 * Init the code table attribute to a new empty code table.
	 */
	protected abstract void initCodeTable();

	/**
	 * Perform a new round of computation by covering the data with the current code table
	 */
	private void applyCodeTable()
	{
		SingletonStopwatchCollection.resume("krimp.applyCodeTable");
		codeTable.apply(data, new FilterEmbeddingsCoverStrategy(), false);
		++testedCodeTablesCount;
		SingletonStopwatchCollection.stop("krimp.applyCodeTable");

		if (testedCodeTablesCount % 1000 == 0)
			logger.info("Processed " + testedCodeTablesCount + " candidate code tables");
	}

	/**
	 * Compute and return the rewritten graph obtained by covering the data with the current code table.
	 * Note: this runs once {@link CodeTable#apply}, so it may take some time
	 */
	public RewrittenGraph computeRewrittenGraph()
	{
		SingletonStopwatchCollection.resume("krimp.rewrittenGraph");
		RewrittenGraph result = codeTable.apply(data, new FilterEmbeddingsCoverStrategy(), true).get();
		SingletonStopwatchCollection.stop("krimp.rewrittenGraph");
		return result;
	}

	/**
	 * @return Candidate order. Tell whether a given candidate pattern should be considered for use before another candidate pattern.
	 */
	public Comparator<CodeTableRow> getCandidateComparator()
	{
		return candidateComparator;
	}

	/**
	 * @return Code table order. Tell whether a given pattern should be placed closer to the top of the code table than another.
	 */
	public Comparator<CodeTableRow> getCodeTableComparator()
	{
		return codeTableComparator;
	}

	/**
	 * After computeBestCodeTable has been called, this returns the best code table.
	 */
	public CodeTable getCodeTable()
	{
		return codeTable;
	}

	/**
	 * @return How many candidate code tables have been tested
	 */
	public int getTestedCodeTablesCount()
	{
		return testedCodeTablesCount;
	}
}
