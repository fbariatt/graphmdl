package graphMDL.rewrittenGraphs;

import graphMDL.PatternInfo;

import java.util.Objects;

/**
 * {@link PatternCTInfo} object describing a singleton pattern of the code table.
 * A different class from {@link NonSingletonCTInfo} is needed since singletons are stored differently in {@link graphMDL.CodeTable}
 * than non-singletons (the latter as {@link graphMDL.CodeTableRow}, the former as elements in one or more maps).
 */
public class SingletonCTInfo extends PatternCTInfo
{
	private final int arity;
	private final String singletonLabel;
	private final PatternInfo patternInfo;

	/**
	 * @param arity          The arity of the singleton: 1 for vertices, 2 for edges.
	 * @param singletonLabel The label of the singleton. Together with the arity they uniquely identify an object of this type.
	 * @param patternInfo    {@link PatternInfo} object describing the pattern's structure and automorphisms. Can be null (it is not needed by this class).
	 */
	public SingletonCTInfo(int arity, String singletonLabel, PatternInfo patternInfo)
	{
		this.arity = arity;
		this.singletonLabel = singletonLabel;
		this.patternInfo = patternInfo;
	}

	public boolean isVertexSingleton()
	{
		return arity == 1;
	}

	public boolean isEdgeSingleton()
	{
		return arity == 2;
	}

	public int getArity()
	{
		return arity;
	}

	public String getSingletonLabel()
	{
		return singletonLabel;
	}

	public PatternInfo getPatternInfo()
	{
		return patternInfo;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SingletonCTInfo that = (SingletonCTInfo) o;
		return arity == that.arity &&
				singletonLabel.equals(that.singletonLabel);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(arity, singletonLabel);
	}
}
