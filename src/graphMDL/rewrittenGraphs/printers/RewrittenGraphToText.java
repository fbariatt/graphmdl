package graphMDL.rewrittenGraphs.printers;

import graphMDL.rewrittenGraphs.*;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Print a rewritten graph as a graph in text format.
 */
public final class RewrittenGraphToText
{
	private RewrittenGraphToText()
	{}

	/**
	 * @param rewrittenGraph The rewritten graph to be printed
	 * @param out            Where to print the graph
	 */
	public static void print(RewrittenGraph rewrittenGraph, PrintWriter out)
	{
		Map<EmbeddingVertex, Integer> embeddingIndexes = new HashMap<>();
		Map<PortVertex, Integer> portIndexes = new HashMap<>();
		int vertexCounter = 0;

		out.println("t");

		// Printing embedding vertices
		Iterator<EmbeddingVertex> embeddingVertices = rewrittenGraph.getEmbeddingVertices().iterator();
		while (embeddingVertices.hasNext())
		{
			EmbeddingVertex embeddingVertex = embeddingVertices.next();
			String patternName;
			if (embeddingVertex.getPatternCTInfo() instanceof NonSingletonCTInfo)
				patternName = "P" + ((NonSingletonCTInfo) embeddingVertex.getPatternCTInfo()).getPatternNumber();
			else
				patternName = ((SingletonCTInfo) embeddingVertex.getPatternCTInfo()).getSingletonLabel();

			out.printf("v %d embedding %s\n", vertexCounter, patternName);
			embeddingIndexes.put(embeddingVertex, vertexCounter);
			vertexCounter++;
		}

		// Printing port vertices
		Iterator<PortVertex> portVertices = rewrittenGraph.getPortVertices().iterator();
		while (portVertices.hasNext())
		{
			PortVertex portVertex = portVertices.next();
			out.printf("v %d data %s\n", vertexCounter, portVertex.getDataVertexNumber());
			portIndexes.put(portVertex, vertexCounter);
			vertexCounter++;
		}

		// Printing edges
		Iterator<PortEdge> edges = rewrittenGraph.getPortEdges().iterator();
		while (edges.hasNext())
		{
			PortEdge edge = edges.next();
			out.printf("e %d %d %d\n", embeddingIndexes.get(edge.getEmbedding()), portIndexes.get(edge.getPort()), edge.getPatternPortNumber());
		}
		out.flush();
	}

	/**
	 * Same as {@link #print(RewrittenGraph, PrintWriter)}, but prints to stdout.
	 */
	public static void print(RewrittenGraph rewrittenGraph)
	{
		print(rewrittenGraph, new PrintWriter(System.out, true));
	}


}
