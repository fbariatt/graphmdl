package graphMDL.rewrittenGraphs.printers;

import graphMDL.rewrittenGraphs.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Print a rewritten graph in RDF format
 */
public final class RewrittenGraphToRDF
{
	private RewrittenGraphToRDF()
	{}

	public final static String BASE_URI = "urn:GraphMDL:RewrittenGraph";
	public final static String EMBEDDING_TYPE_URI = BASE_URI + ":VertexType:Embedding";
	public final static String PORT_TYPE_URI = BASE_URI + ":VertexType:Port";
	public final static String VERTICES_URI = BASE_URI + ":Vertex";
	public final static String EMBEDDING_TO_PATTERN_URI = BASE_URI + ":ofPattern";
	public final static String PORT_TO_DATA_VERTEX_URI = BASE_URI + ":ofDataVertex";
	public final static String EMBEDDING_TO_PORT_URI = BASE_URI + ":hasPort";

	/**
	 * @param rewrittenGraph The rewritten graph to be printed
	 * @param out            Where to print the graph
	 * @param lang           The RDF language to use, see {@link Model#write(Writer, String)}
	 */
	public static void print(RewrittenGraph rewrittenGraph, PrintWriter out, String lang)
	{
		Map<EmbeddingVertex, String> embeddingsUris = new HashMap<>();
		Map<PortVertex, String> portsUris = new HashMap<>();
		Map<String, String> patternUris = new HashMap<>();
		int vertexCounter = 0;
		Model model = ModelFactory.createDefaultModel();

		// Printing embedding vertices
		final Resource embeddingTypeEntity = model.createResource(EMBEDDING_TYPE_URI);
		final Property embeddingOfPatternPredicate = model.createProperty(EMBEDDING_TO_PATTERN_URI);
		Iterator<EmbeddingVertex> embeddingVertices = rewrittenGraph.getEmbeddingVertices().iterator();
		while (embeddingVertices.hasNext())
		{
			EmbeddingVertex embeddingVertex = embeddingVertices.next();

			String embeddingVertexUri = VERTICES_URI + ":" + vertexCounter;
			embeddingsUris.put(embeddingVertex, embeddingVertexUri);

			String patternName;
			if (embeddingVertex.getPatternCTInfo() instanceof NonSingletonCTInfo)
				patternName = "P" + ((NonSingletonCTInfo) embeddingVertex.getPatternCTInfo()).getPatternNumber();
			else
				patternName = ((SingletonCTInfo) embeddingVertex.getPatternCTInfo()).getSingletonLabel();
			model.createResource(embeddingVertexUri)
					.addProperty(RDF.type, embeddingTypeEntity)
					.addLiteral(embeddingOfPatternPredicate, model.createTypedLiteral(patternName));
			vertexCounter++;
		}

		// Printing port vertices
		final Resource portTypeEntity = model.createResource(PORT_TYPE_URI);
		final Property portOfDataVertexPredicate = model.createProperty(PORT_TO_DATA_VERTEX_URI);
		Iterator<PortVertex> portVertices = rewrittenGraph.getPortVertices().iterator();
		while (portVertices.hasNext())
		{
			PortVertex portVertex = portVertices.next();

			String portUri = VERTICES_URI + ":" + vertexCounter;
			portsUris.put(portVertex, portUri);

			model.createResource(portUri)
					.addProperty(RDF.type, portTypeEntity)
					.addLiteral(portOfDataVertexPredicate, model.createTypedLiteral(portVertex.getDataVertexNumber()));
			vertexCounter++;
		}

		// Printing edges
		Iterator<PortEdge> edges = rewrittenGraph.getPortEdges().iterator();
		while (edges.hasNext())
		{
			PortEdge edge = edges.next();

			final Property embeddingHasPortPredicate = model.createProperty(EMBEDDING_TO_PORT_URI + ":" + edge.getPatternPortNumber());
			model.createResource(embeddingsUris.get(edge.getEmbedding()))
					.addProperty(embeddingHasPortPredicate, model.createResource(portsUris.get(edge.getPort())));
		}
		model.setNsPrefix("rg", BASE_URI + ":");
		model.write(out, lang, "");
		out.flush();
	}

	/**
	 * Same as {@link #print(RewrittenGraph, PrintWriter, String)}, using turtle language
	 */
	public static void print(RewrittenGraph rewrittenGraph, PrintWriter out)
	{
		print(rewrittenGraph, out, "TTL");
	}

	/**
	 * Same as {@link #print(RewrittenGraph, PrintWriter, String)}, printing to stdout
	 */
	public static void print(RewrittenGraph rewrittenGraph, String lang)
	{
		print(rewrittenGraph, new PrintWriter(System.out, true), lang);
	}

	/**
	 * Same as {@link #print(RewrittenGraph, PrintWriter, String)}, printing to stdout using turtle language
	 */
	public static void print(RewrittenGraph rewrittenGraph)
	{
		print(rewrittenGraph, new PrintWriter(System.out, true), "TTL");
	}
}
