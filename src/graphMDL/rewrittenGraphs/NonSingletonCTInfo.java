package graphMDL.rewrittenGraphs;

import graphMDL.CodeTableRow;
import graphMDL.PatternInfo;

import java.util.Objects;

/**
 * {@link PatternCTInfo} object describing a non-singleton pattern of the code table.
 */
public class NonSingletonCTInfo extends PatternCTInfo
{
	private final int patternNumber;
	private final CodeTableRow ctRow;

	/**
	 * @param patternNumber The index of the pattern in the code table (i.e. the index of the corresponding {@link CodeTableRow}).
	 *                      Two objects of this type are equal if they have the same value for this attribute.
	 * @param ctRow         The row of the code table describing this pattern. Can be null (it is not needed by this class).
	 */
	public NonSingletonCTInfo(int patternNumber, CodeTableRow ctRow)
	{
		this.patternNumber = patternNumber;
		this.ctRow = ctRow;
	}

	public int getPatternNumber()
	{
		return patternNumber;
	}

	public CodeTableRow getCtRow()
	{
		return ctRow;
	}

	@Override
	public PatternInfo getPatternInfo()
	{
		if (this.ctRow == null)
			return null;
		else
			return this.ctRow.getPatternInfo();
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		NonSingletonCTInfo that = (NonSingletonCTInfo) o;
		return patternNumber == that.patternNumber;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(patternNumber);
	}
}
