package graphMDL.rewrittenGraphs;

import graph.Vertex;

/**
 * A port vertex in the rewritten graph
 */
public class PortVertex
{
	private final Vertex rewrittenGraphVertex;
	private final int dataVertexNumber;

	/**
	 * @param rewrittenGraphVertex The corresponding vertex object in the underlying Graph object used for storage
	 * @param dataVertexNumber     The data vertex that this port vertex represents
	 */
	PortVertex(Vertex rewrittenGraphVertex, int dataVertexNumber)
	{
		this.rewrittenGraphVertex = rewrittenGraphVertex;
		this.dataVertexNumber = dataVertexNumber;
	}

	/**
	 * @return The number of the data vertex that this port vertex represents.
	 */
	public int getDataVertexNumber()
	{
		return dataVertexNumber;
	}

	Vertex getRewrittenGraphVertex()
	{
		return rewrittenGraphVertex;
	}
}
