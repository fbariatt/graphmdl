package graphMDL.rewrittenGraphs;

import graphMDL.CodeTableRow;
import graphMDL.PatternInfo;

/**
 * Class used to link information about a CT pattern to the rewritten graph embedding vertices.
 */
public abstract class PatternCTInfo
{
	/**
	 * @return The {@link PatternInfo} object of the pattern's structure and automorphisms.
	 * May be null if it was not supplied during creation of this object.
	 */
	public abstract PatternInfo getPatternInfo();

	/**
	 * Shorthand method to create a {@link NonSingletonCTInfo} object.
	 */
	public static PatternCTInfo nonSingleton(int patternNumber, CodeTableRow ctRow)
	{
		return new NonSingletonCTInfo(patternNumber, ctRow);
	}

	/**
	 * Shorthand method to create a {@link SingletonCTInfo} object of arity 1 (vertex).
	 */
	public static PatternCTInfo vertexSingleton(String singletonLabel, PatternInfo patternInfo)
	{
		return new SingletonCTInfo(1, singletonLabel, patternInfo);
	}

	/**
	 * Shorthand method to create a {@link SingletonCTInfo} object of arity 2 (edge).
	 */
	public static PatternCTInfo edgeSingleton(String singletonLabel, PatternInfo patternInfo)
	{
		return new SingletonCTInfo(2, singletonLabel, patternInfo);
	}
}
