package graphMDL.rewrittenGraphs;

import graph.Vertex;

/**
 * An embedding vertex in the rewritten graph
 */
public class EmbeddingVertex
{
	private final Vertex rewrittenGraphVertex;
	private final PatternCTInfo patternCTInfo;

	/**
	 * @param rewrittenGraphVertex The corresponding vertex object in the underlying Graph object used for storage
	 * @param patternCTInfo        Information about the pattern associated to this vertex.
	 */
	EmbeddingVertex(Vertex rewrittenGraphVertex, PatternCTInfo patternCTInfo)
	{
		this.rewrittenGraphVertex = rewrittenGraphVertex;
		this.patternCTInfo = patternCTInfo;
	}

	public PatternCTInfo getPatternCTInfo()
	{
		return patternCTInfo;
	}

	Vertex getRewrittenGraphVertex()
	{
		return rewrittenGraphVertex;
	}
}
