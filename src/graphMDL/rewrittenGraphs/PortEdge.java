package graphMDL.rewrittenGraphs;

import graph.Edge;

/**
 * An edge between an embedding vertex and a port vertex in the rewritten graph.
 */
public class PortEdge
{
	private final EmbeddingVertex embedding;
	private final PortVertex port;
	private final Edge rewrittenGraphEdge;

	/**
	 * @param embedding          The embedding vertex from which this edge starts
	 * @param port               The port vertex to which this edge ends
	 * @param rewrittenGraphEdge The corresponding edge object in the underlying Graph object used for storage
	 */
	PortEdge(EmbeddingVertex embedding, PortVertex port, Edge rewrittenGraphEdge)
	{
		this.embedding = embedding;
		this.port = port;
		this.rewrittenGraphEdge = rewrittenGraphEdge;
	}

	public EmbeddingVertex getEmbedding()
	{
		return embedding;
	}

	public PortVertex getPort()
	{
		return port;
	}

	/**
	 * @return The vertex of the pattern that corresponds to this specific port.
	 */
	public int getPatternPortNumber()
	{
		// The port number is encoded as the label of the rewritten graph edge
		return Integer.parseInt(rewrittenGraphEdge.getLabel());
	}

	Edge getRewrittenGraphEdge()
	{
		return rewrittenGraphEdge;
	}
}
