package graphMDL.rewrittenGraphs;

import graph.Edge;
import graph.Graph;
import graph.GraphFactory;
import graph.Vertex;
import graphMDL.CodeTableRow;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Represents a rewritten graph, as per GraphMDL definition: a graph where "embedding vertices" represent pattern occurrences
 * in some data, "port vertices" represent data vertices shared by more than one pattern embedding, and embedding vertices are connected to port vertices
 * via edges that identify which of the pattern vertices correspond to the port.
 */
public class RewrittenGraph
{
	private Graph graph; // The actual graph object storing the structure of the rewritten graph
	private Map<Integer, PortVertex> portVertices; // Data graph vertex number -> port vertex
	private Map<Vertex, EmbeddingVertex> embeddingVertices; // Rewritten graph Vertex object -> EmbeddingVertex object
	private Map<Edge, PortEdge> portEdges; // Rewritten graph Edge object -> PortEdge object

	public RewrittenGraph()
	{
		graph = GraphFactory.createSimpleGraph();
		portVertices = new HashMap<>();
		embeddingVertices = new HashMap<>();
		portEdges = new HashMap<>();
	}
	
	/**
	 * Add a new embedding vertex for the given pattern.
	 *
	 * @param patternCTInfo Information about the pattern for which this vertex represents an embedding.
	 * @return The newly-created embedding vertex
	 */
	public EmbeddingVertex addEmbeddingVertex(PatternCTInfo patternCTInfo)
	{
		Vertex v = graph.addVertex();
		EmbeddingVertex embeddingVertex = new EmbeddingVertex(v, patternCTInfo);
		embeddingVertices.put(v, embeddingVertex);
		return embeddingVertex;
	}

	/**
	 * Add a port for a given embedding, connecting the embedding vertex to the port vertex.
	 * The port vertex is created if not existing.
	 *
	 * @param embeddingVertex   The embedding for which the port is to be added.
	 * @param patternPortNumber The number of the port vertex in the pattern.
	 * @param dataVertexNumber  The number of the data vertex that correspond to this port.
	 * @return The newly-created port edge connecting the embedding vertex to the port vertex.
	 */
	public PortEdge addPort(EmbeddingVertex embeddingVertex, int patternPortNumber, int dataVertexNumber)
	{
		checkPortNumberIsValid(embeddingVertex.getPatternCTInfo(), patternPortNumber);

		// Getting the port vertex associated to this data graph vertex, or creating one if needed
		PortVertex portVertex = portVertices.computeIfAbsent(dataVertexNumber, k -> new PortVertex(graph.addVertex(), dataVertexNumber));
		// Creating the edge from the embedding vertex to the port vertex
		Edge e = graph.addEdge(embeddingVertex.getRewrittenGraphVertex(), portVertex.getRewrittenGraphVertex(), Integer.toString(patternPortNumber));
		PortEdge portEdge = new PortEdge(embeddingVertex, portVertex, e);
		portEdges.put(e, portEdge);
		return portEdge;
	}

	/**
	 * Check that the given port number is valid for the given pattern info.
	 * If it can not be determined, assume it is valid.
	 *
	 * @throws IllegalArgumentException if the port number is not valid for the given pattern info.
	 */
	private void checkPortNumberIsValid(PatternCTInfo ctInfo, int patternPortNumber)
	{
		int maxPortNumber;
		if (ctInfo instanceof NonSingletonCTInfo)
		{
			CodeTableRow ctRow = ((NonSingletonCTInfo) ctInfo).getCtRow();
			if (ctRow == null)
				return; // If this patterns does not have an associated code table row we can not determine if the port number is valid, so we assume it is
			maxPortNumber = ctRow.getPatternGraph().getVertexCount() - 1;
		}
		else
		{
			maxPortNumber = ((SingletonCTInfo) ctInfo).getArity() - 1;
		}
		if ((patternPortNumber < 0) || (patternPortNumber > maxPortNumber))
			throw new IllegalArgumentException(String.format("Port number out of range. Got %d expected between 0 and %d", patternPortNumber, maxPortNumber));
	}

	/**
	 * @return The number of embedding vertices in this rewritten graph
	 */
	public int getEmbeddingVerticesCount()
	{
		return embeddingVertices.size();
	}

	/**
	 * @return The number of port vertices in this rewritten graph
	 */
	public int getPortVerticesCount()
	{
		return portVertices.size();
	}

	/**
	 * @return The number of edges connecting embeddings to ports in this rewritten graph.
	 * Corresponds to the total number of ports for all patterns.
	 */
	public int getPortEdgesCount()
	{
		return portEdges.size();
	}

	/**
	 * @return All the patterns that have embedding vertices in this rewritten graph.
	 */
	public Stream<PatternCTInfo> getPatternsCTInfo()
	{
		return embeddingVertices.values().stream()
				.map(EmbeddingVertex::getPatternCTInfo)
				.distinct();
	}

	/**
	 * @return The number of different patterns that have embeddings vertices in this rewritten graph.
	 */
	public int getPatternCount()
	{
		return Math.toIntExact(getPatternsCTInfo().count());
	}

	/**
	 * @return All embedding vertices
	 */
	public Stream<EmbeddingVertex> getEmbeddingVertices()
	{
		return embeddingVertices.values().stream();
	}

	/**
	 * @return All port vertices
	 */
	public Stream<PortVertex> getPortVertices()
	{
		return portVertices.values().stream();
	}

	/**
	 * @return The ids of the data vertices corresponding to the port vertices of this graph.
	 * Note: result is not sorted.
	 */
	public Stream<Integer> getDataVerticesThatArePorts()
	{
		return portVertices.keySet().stream();
	}

	/**
	 * @return All edges of the rewritten graph. Each edge connects an embedding vertex to a port vertex.
	 */
	public Stream<PortEdge> getPortEdges()
	{
		return portEdges.values().stream();
	}

	/**
	 * @return The edge connecting the given embedding vertex to the given port vertex, if there is any.
	 */
	public Optional<PortEdge> getPortEdge(EmbeddingVertex embeddingVertex, PortVertex portVertex)
	{
		return graph.getEdges(embeddingVertex.getRewrittenGraphVertex(), portVertex.getRewrittenGraphVertex())
				.findAny()
				.map(this.portEdges::get);
	}

	public Stream<EmbeddingVertex> getEmbeddingsOf(PatternCTInfo patternCTInfo)
	{
		return embeddingVertices.values().stream()
				.filter(embeddingVertex -> embeddingVertex.getPatternCTInfo().equals(patternCTInfo));
	}

	/**
	 * @return All edges connecting the given embedding vertex to ports.
	 */
	public Stream<PortEdge> getPortEdgesOf(EmbeddingVertex embeddingVertex)
	{
		return graph.getOutEdges(embeddingVertex.getRewrittenGraphVertex())
				.map(edge -> portEdges.get(edge));
	}

	/**
	 * @return All ports associated to the given embedding vertex.
	 */
	public Stream<PortVertex> getPortsOf(EmbeddingVertex embeddingVertex)
	{
		return getPortEdgesOf(embeddingVertex).map(PortEdge::getPort);
	}

	/**
	 * @return All embeddings connected to the given port vertex
	 */
	public Stream<EmbeddingVertex> getEmbeddingHaving(PortVertex portVertex)
	{
		return graph.getInEdges(portVertex.getRewrittenGraphVertex())
				.map(edge -> embeddingVertices.get(edge.getSource()));
	}
}
