package graphMDL;

import graph.Graph;
import patternMatching.Embedding;
import utils.SingletonStopwatchCollection;

import java.util.LinkedList;
import java.util.List;

/**
 * In order to compute the cover embeddings of a code table row, compute all the embeddings of the row's pattern
 * and then go through all of them, checking which ones can be used in the cover.
 * In our tests this strategy was quicker on certain datasets, as long as the pattern did not have too many embeddings.
 * It also uses more ram because of the need to store all the embeddings.
 */
public class FilterEmbeddingsCoverStrategy implements CoverStrategy
{
	public FilterEmbeddingsCoverStrategy()
	{}

	@Override
	public boolean needsEmbeddingsSetOnCTRow()
	{
		return true;
	}

	@Override
	public List<Embedding> computeCoverEmbeddings(CodeTableRow row, Graph data, StandardTable standardTable, int elementDescribedMarker)
	{
		if (row.getEmbeddings() == null)
			throw new RuntimeException("Code table row must have embeddings set to compute cover using this strategy");

		SingletonStopwatchCollection.resume("filterEmbeddingsCoverStrategy.computeCoverEmbeddings");
		List<Embedding> coverEmbeddings = new LinkedList<>();
		for (Embedding embedding : row.getEmbeddings())
		{
			if (Utils.isEmbeddingValid(row.getPatternGraph(), embedding, data, elementDescribedMarker))
			{
				Utils.markEmbedding(row.getPatternGraph(), embedding, data, elementDescribedMarker);
				coverEmbeddings.add(embedding);
			}
		}
		SingletonStopwatchCollection.stop("filterEmbeddingsCoverStrategy.computeCoverEmbeddings");
		return coverEmbeddings;
	}
}
