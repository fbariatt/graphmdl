package graphMDL;

import graph.Graph;
import patternMatching.Embedding;

import java.util.List;

/**
 * Strategy used in order to compute the cover embeddings of a {@link CodeTableRow} in a data graph.
 */
public interface CoverStrategy
{
	/**
	 * Compute the list of embeddings that compose the cover of the given row in the given data graph.
	 *
	 * @param row                    The row for which to compute the cover embeddings.
	 * @param data                   The data, in graph form.
	 * @param standardTable          The standard table associated to the data (may be used to direct the search towards less-frequent labels first).
	 * @param elementDescribedMarker Value to use for markers on vertex and edge labels to tell whether they are described by a pattern or not.
	 * @implSpec This function marks the data vertices, edges, and label of the computed embeddings in the data graph with the given marker. This is done so that
	 * the same function called on a different row will know which parts of the data are already covered or not.
	 */
	List<Embedding> computeCoverEmbeddings(CodeTableRow row, Graph data, StandardTable standardTable, int elementDescribedMarker);

	/**
	 * @return Whether this strategy needs the list of embeddings to be set on the code table rows.
	 */
	boolean needsEmbeddingsSetOnCTRow();
}
