package mains;

import graph.Graph;
import graph.loaders.FileSystemTextGraphLoader;
import graphMDL.CodeTable;
import graphMDL.CodeTableHeuristics;
import graphMDL.CodeTableRow;
import graphMDL.ScanDataCoverStrategy;
import graphMDL.jsonSerializer.GraphMDLDeserializer;
import graphMDL.jsonSerializer.multigraphs.MultiDirectedGraphMDLDeserializer;
import graphMDL.jsonSerializer.simpleGraphs.directed.SimpleDirectedGraphMDLDeserializer;
import graphMDL.jsonSerializer.simpleGraphs.undirected.SimpleUndirectedGraphMDLDeserializer;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LabelsCoveredByPatternSize
{
	public final static String DESCRIPTION = "Compute how many data labels are covered by patterns of each size";
	private static Logger logger = Logger.getLogger(LabelsCoveredByPatternSize.class);

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		parser.addArgument("graphmdl_json").type(Arguments.fileType().verifyCanRead()).help("GraphMDL json result file containing the patterns to analyze. It is assumed that patterns are ordered by decreasing label size then decreasing support.");
		parser.addArgument("data").type(Arguments.fileType().verifyCanRead()).help("Data graph on which the patterns will be matched (text format).");
		parser.addArgument("output").type(Arguments.fileType().verifyCanWrite().or().verifyCanCreate()).help("Output histogram data in JSON format.");
		parser.addArgument("--rto", "--row-cover-timeout").dest("row_cover_timeout").required(true).metavar("ms").type(Integer.class).help("Timeout for computing the cover of each code table row.");
		parser.addArgument("-v", "--verbose").action(Arguments.storeTrue()).help("Show debug information");
	}

	public static void execute(Namespace args)
	{
		logger.info("Executing with arguments: " + args);
		final String graphMDLJSONPath = args.getString("graphmdl_json");
		final String dataPath = args.getString("data");
		final FileWriter output = openOutput(args.getString("output"));
		final int rowCoverTimeout = args.getInt("row_cover_timeout");
		if (args.getBoolean("verbose"))
			logger.setLevel(Level.DEBUG);

		ToIntFunction<Graph> graphLabelCount = g -> g.getVertices().mapToInt(v -> v.getLabels().size()).sum() + Math.toIntExact(g.getEdgeCount());
		ToIntFunction<CodeTable> singletonUsageSum = ct -> Stream.concat(
						ct.getSingletonVertexUsages().values().stream(),
						ct.getSingletonEdgeUsages().values().stream()
				)
				.mapToInt(Integer::intValue).sum();

		logger.info("Loading data...");

		Graph data = FileSystemTextGraphLoader.loadGraphFromFile(dataPath);
		final int dataLabelCount = graphLabelCount.applyAsInt(data);
		logger.info(String.format("Loaded data graph with %d vertices, %d edges and %d total labels", data.getVertexCount(), data.getEdgeCount(), dataLabelCount));

		logger.info("Loading GraphMDL json...");
		final Comparator<CodeTableRow> ctOrder = CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDescImageBasedSupportDesc);
		CodeTable ct = getDeserializer(graphMDLJSONPath).createCodeTableWithoutEmbeddings(ctOrder);
		List<CodeTableRow> originalPatterns = new ArrayList<>(ct.getRows());
		logger.info(String.format("Loaded %s non-singleton patterns", originalPatterns.size()));
		if (originalPatterns.size() == 0)
		{
			logger.error("At least one pattern is needed!");
			System.exit(1);
		}

		List<Integer> patternSizesReverseSorted = originalPatterns.stream()
				.map(CodeTableRow::getPatternGraph)
				.mapToInt(graphLabelCount)
				.distinct()
				.sorted()
				.boxed().collect(Collectors.toList());
		Collections.reverse(patternSizesReverseSorted);
		logger.info(String.format("Smallest pattern has %d labels, largest has %d labels", patternSizesReverseSorted.get(patternSizesReverseSorted.size() - 1), patternSizesReverseSorted.get(0)));

		logger.info("Computing label cover for patterns >= size with size max..min");
		JSONObject coveredPerSize = new JSONObject();
		int previous = 0;
		for (int size : patternSizesReverseSorted)
		{
			ct.getRows().clear();
			originalPatterns.stream().filter(row -> graphLabelCount.applyAsInt(row.getPatternGraph()) >= size).forEach(ct::addRow);
			ct.apply(data, new ScanDataCoverStrategy(rowCoverTimeout), false);
			final int coveredLabels = dataLabelCount - singletonUsageSum.applyAsInt(ct);
			final int unique = coveredLabels - previous;
			coveredPerSize.put(Integer.toString(size), unique);
			logger.debug(String.format("%d (%.2f%%) labels covered for size >= %d, %d (%.2f%%) unique", coveredLabels, percent(coveredLabels, dataLabelCount), size, unique, percent(unique, dataLabelCount)));
			previous = coveredLabels;
		}
		// The remaining labels are covered by singletons exclusively
		coveredPerSize.put("1", dataLabelCount - previous);

		logger.info("Writing to output file...");
		coveredPerSize.write(output);
		try
		{
			output.flush();
			output.close();
		} catch (IOException e)
		{
			logger.error("Error when writing to output, output might be incomplete: " + e);
			System.exit(1);
		}
		logger.info("Done.");
	}

	private static FileWriter openOutput(String outputPath)
	{
		try
		{
			return new FileWriter(outputPath);
		} catch (IOException e)
		{
			logger.error("Error when opening output: " + e);
			System.exit(1);
			return null;
		}
	}

	private static GraphMDLDeserializer getDeserializer(String jsonPath)
	{
		try
		{
			JSONObject serialization = new JSONObject(new JSONTokener(new FileInputStream(jsonPath)));
			final String kind = serialization.getString("kind");
			switch (kind)
			{
				case "simple_undirected":
					return new SimpleUndirectedGraphMDLDeserializer(serialization);
				case "simple_directed":
					return new SimpleDirectedGraphMDLDeserializer(serialization);
				case "multi_directed":
					return new MultiDirectedGraphMDLDeserializer(serialization);
				default:
					logger.error(String.format("Impossible to deserialize json: unknown serialization kind '%s'", kind));
					System.exit(1);
					return null;
			}
		} catch (FileNotFoundException e)
		{
			logger.error("Json file not found: ", e);
			System.exit(1);
			return null;
		}
	}

	private static double percent(int value, int tot)
	{
		return ((double) value) / tot * 100;
	}
}
