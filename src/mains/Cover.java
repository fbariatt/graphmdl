package mains;

import graph.Graph;
import graph.loaders.FileSystemTextGraphLoader;
import graph.loaders.SingleFileTextGraphCollectionLoader;
import graphMDL.*;
import graphMDL.jsonSerializer.GraphMDLSerializer;
import graphMDL.jsonSerializer.PatternInfoDeserializer;
import graphMDL.jsonSerializer.SerializationUtils;
import graphMDL.jsonSerializer.multigraphs.MultiDirectedGraphMDLSerializer;
import graphMDL.jsonSerializer.simpleGraphs.directed.SimpleDirectedGraphMDLSerializer;
import graphMDL.jsonSerializer.simpleGraphs.directed.SimpleDirectedPatternInfoDeserializer;
import graphMDL.jsonSerializer.simpleGraphs.undirected.SimpleUndirectedGraphMDLSerializer;
import graphMDL.jsonSerializer.simpleGraphs.undirected.SimpleUndirectedPatternInfoDeserializer;
import graphMDL.multigraphs.MultiDirectedCodeTable;
import graphMDL.multigraphs.MultiDirectedStandardTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedCodeTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedIndependentVertexAndEdgeST;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedCodeTable;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedIndependentVertexAndEdgeST;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedUtils;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.MutuallyExclusiveGroup;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import utils.GeneralUtils;
import utils.GraphType;

import java.io.*;
import java.nio.file.Path;
import java.util.EnumSet;
import java.util.stream.Stream;

public class Cover
{
	public final static String DESCRIPTION = "Cover a given data graph with a given code table.";
	private final static Logger logger = Logger.getLogger(Cover.class);

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		GraphType.addCliArguments(parser);
		parser.addArgument("data_graph").type(Arguments.fileType().verifyIsFile().verifyCanRead()).help("File containing the data graph, in text format.");
		parser.addArgument("ct_patterns").type(Arguments.fileType().verifyIsFile().verifyCanRead()).help("File containing the list of code table patterns. Either a list of graphs in text format, or JSON object containing a list of Pattern serializations in its 'patterns' property (GraphMDL result files can be used)");
		parser.addArgument("out").type(Arguments.fileType().verifyCanCreate()).help("File where the output will be saved");

		MutuallyExclusiveGroup fileFormat = parser.addMutuallyExclusiveGroup().required(false);
		fileFormat.addArgument("-t", "--text").action(Arguments.storeTrue()).help("Specify that the CT file is in text format");
		fileFormat.addArgument("-j", "--json").action(Arguments.storeTrue()).help("Specify that the CT file is in JSON format");

		parser.addArgument("--timeout", "--row-cover-timeout").dest("row_cover_timeout").metavar("ms").type(Integer.class).help("Maximum duration (in milliseconds) allocated to compute the cover of each code table pattern.");
	}

	public static void execute(Namespace args)
	{
		Logger.getRootLogger().setLevel(Level.INFO);
		System.out.printf("Computing cover of a data graph using a code table. CLI arguments: %s\n", args);
		final EnumSet<GraphType> graphType = GraphType.fromCliArguments(args);
		System.out.printf("Graphs are considered %s\n", graphType.contains(GraphType.DIRECTED) ? "directed" : "undirected");
		System.out.printf("Using %s encoding\n", graphType.contains(GraphType.MULTIGRAPH) ? "multigraph" : "simple graph (non-multigraph)");

		// Loading data
		logger.info("Loading data graph and computing Standard Tables...");
		Graph data = FileSystemTextGraphLoader.loadGraphFromFile(args.getString("data_graph"));
		StandardTable st = computeStandardTable(data, graphType);

		// Loading CT patterns and creating CT
		logger.info("Creating CT...");
		final String patternsFilePath = args.getString("ct_patterns");
		FileFormat ctFileFormat = determineCTFileFormat(args);
		CodeTable ct = createCT(st, graphType);

		if (ctFileFormat == FileFormat.TEXT)
		{
			try
			{
				logger.info("Loading patterns from text file...");
				Stream<Graph> ctPatterns = SingleFileTextGraphCollectionLoader.loadGraphsFromFile(patternsFilePath);
				ctPatterns.parallel()
						.map(pattern -> new CodeTableRow(
								PatternInfo.createComputingSaturatedAutomorphisms(pattern),
								st.encode(pattern),
								0, // We don't need the support because the selected CT heuristic does not take it into account and is not present in the output
								graphLabelCount(pattern, graphType)
						))
						.forEachOrdered(ct::addRow);
			} catch (IOException e)
			{
				logger.error("Error when loading patterns from text file", e);
				System.exit(1);
			}
		}
		else
		{ // If it is not text, it is json
			try
			{
				logger.info("Loading patterns from JSON file...");
				JSONObject serialization = new JSONObject(new JSONTokener(new FileInputStream(patternsFilePath)));
				SerializationUtils.<JSONObject>castJSONArrayElements(serialization.getJSONArray("patterns"))
						.filter(patternSerialization -> !patternSerialization.getBoolean("singleton")) // We only load non-singleton patterns
						.map(patternSerialization -> deserializePatternInfo(patternSerialization.getJSONObject("structure"), graphType))
						.map(patternInfo -> new CodeTableRow(
								patternInfo,
								st.encode(patternInfo.getStructure()),
								0, // We don't need the support because the selected CT heuristic does not take it into account and is not present in the output
								graphLabelCount(patternInfo.getStructure(), graphType)
						))
						.forEachOrdered(ct::addRow);
			} catch (FileNotFoundException | JSONException e)
			{
				logger.error("Error when loading patterns from json file", e);
				System.exit(1);
			} catch (StandardTable.StandardTableEncodingException e)
			{
				logger.error("Error when loading patterns: impossible to encode a pattern with the standard tables computed on the data graph: " + e.getMessage());
				System.exit(1);
			}
		}
		logger.info("CT created.");

		// Cover computation
		logger.info("Computing cover...");
		ct.apply(data, new ScanDataCoverStrategy(GeneralUtils.defaultIfNull(args.getInt("row_cover_timeout"), 0)), false);

		final String outFilePath = args.getString("out");
		logger.info(String.format("Saving result as json to %s", outFilePath));
		try
		{
			GraphMDLSerializer serializer = createGraphMDLSerializer(ct, data, graphType);
			FileWriter out = new FileWriter(outFilePath);
			serializer.getJSONObject().write(out, 2, 0);
			out.flush();
			out.close();
		} catch (IOException e)
		{
			logger.error("Error when saving cover result to file", e);
			System.exit(1);
		}

		logger.info("Finished.");
	}

	private enum FileFormat
	{
		TEXT, JSON
	}

	private static FileFormat determineCTFileFormat(Namespace args)
	{
		if (args.getBoolean("text")) // If the user specified that the file is text
			return FileFormat.TEXT;
		if (args.getBoolean("json")) // If the user specified that the file is json
			return FileFormat.JSON;

		// If the user did not specify a format, we determine it based on the file extension
		final Path filePath = new File(args.getString("ct_patterns")).toPath();
		final String extension = filePath.getFileName().toString().substring(filePath.getFileName().toString().lastIndexOf(".") + 1);
		switch (extension) {
			case "txt":
				return FileFormat.TEXT;
			case "json":
				return FileFormat.JSON;
			default:
				logger.error("Error: Code Table file format has not been specified as a command-line argument and can not be determined automatically");
				System.exit(1);
				return null; // Needed to compile the program
		}
	}

	private static StandardTable computeStandardTable(Graph data, EnumSet<GraphType> graphType)
	{
		if (graphType.equals(GraphType.SIMPLE_DIRECTED))
			return new SimpleDirectedIndependentVertexAndEdgeST(data);
		else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
			return new SimpleUndirectedIndependentVertexAndEdgeST(data);
		else if (graphType.equals(GraphType.MULTI_DIRECTED))
			return new MultiDirectedStandardTable(data);
		else // Multi undirected
		{
			System.err.println("Undirected multigraphs are not supported for now");
			System.exit(1);
			return null;
		}
	}

	private static CodeTable createCT(StandardTable st, EnumSet<GraphType> graphType)
	{
		if (graphType.equals(GraphType.SIMPLE_DIRECTED))
			return new SimpleDirectedCodeTable((SimpleDirectedIndependentVertexAndEdgeST) st, CodeTableHeuristics.PartialOrder.noOrder);
		else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
			return new SimpleUndirectedCodeTable((SimpleUndirectedIndependentVertexAndEdgeST) st, CodeTableHeuristics.PartialOrder.noOrder);
		else if (graphType.equals(GraphType.MULTI_DIRECTED))
			return new MultiDirectedCodeTable((MultiDirectedStandardTable) st, CodeTableHeuristics.PartialOrder.noOrder);
		else // Multi undirected
		{
			System.err.println("Undirected multigraphs are not supported for now");
			System.exit(1);
			return null;
		}
	}

	static int graphLabelCount(Graph g, EnumSet<GraphType> graphType)
	{
		if (graphType.contains(GraphType.DIRECTED))
			return Utils.graphLabelCount(g);
		else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
			return SimpleUndirectedUtils.graphLabelCount(g);
		else // Multi undirected
		{
			System.err.println("Undirected multigraphs are not supported for now");
			System.exit(1);
			return 0;
		}
	}

	private static PatternInfo deserializePatternInfo(JSONObject serialization, EnumSet<GraphType> graphType)
	{
		if (graphType.equals(GraphType.SIMPLE_DIRECTED))
			return new SimpleDirectedPatternInfoDeserializer(serialization).createPatternInfo();
		else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
			return new SimpleUndirectedPatternInfoDeserializer(serialization).createPatternInfo();
		else if (graphType.equals(GraphType.MULTI_DIRECTED))
			return new PatternInfoDeserializer(serialization).createPatternInfo();
		else // Multi undirected
		{
			System.err.println("Undirected multigraphs are not supported for now");
			System.exit(1);
			return null;
		}
	}

	static GraphMDLSerializer createGraphMDLSerializer(CodeTable ct, Graph data, EnumSet<GraphType> graphType)
	{
		if (graphType.equals(GraphType.SIMPLE_DIRECTED))
			return new SimpleDirectedGraphMDLSerializer((SimpleDirectedCodeTable) ct, data);
		else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
			return new SimpleUndirectedGraphMDLSerializer((SimpleUndirectedCodeTable) ct, data);
		else if (graphType.equals(GraphType.MULTI_DIRECTED))
			return new MultiDirectedGraphMDLSerializer((MultiDirectedCodeTable) ct, data);
		else // Multi undirected
		{
			System.err.println("Undirected multigraphs are not supported for now");
			System.exit(1);
			return null;
		}
	}


	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(Cover.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));
		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
