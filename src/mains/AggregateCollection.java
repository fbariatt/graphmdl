package mains;

import graph.Graph;
import graph.GraphFactory;
import graph.Vertex;
import graph.loaders.SingleFileTextGraphCollectionLoader;
import graph.printers.GraphToText;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Aggregate a collection of graphs into one single graph
 */
public class AggregateCollection
{

	public static final String DESCRIPTION = "Aggregate a collection of graphs into a single graph";


	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		parser.addArgument("output").help("Where to write the aggregated output. Should be different from the inputs.");
		parser.addArgument("input").nargs("+").help("The graphs to aggregate, in text format. Each file can contain multiple graphs.");
		parser.addArgument("-k", "--keep-names").action(Arguments.storeTrue()).help("Use the same vertex names in the output as they have in the input. If selected vertices should all have different names across all graphs.");
	}

	public static void execute(Namespace args)
	{
		final Stream<Path> inputFilePaths = args.<String>getList("input").stream()
				.map(filename -> Paths.get(filename).toAbsolutePath())
				.sorted();
		final Path outputFilePath = Paths.get(args.getString("output")).toAbsolutePath();
		final boolean keepVertexNames = args.getBoolean("keep_names");

		// We create the output at the start so that if there are errors later it will be an empty file
		OutputStream outputStream = createOutput(outputFilePath);

		System.out.println("Loading input graphs...");
		Graph aggregateGraph = GraphFactory.createDefaultGraph();
		inputFilePaths
				.flatMap(AggregateCollection::graphCollectionFromTextFileExitOnException)
				.forEach(aggregateGraph::concat);
		System.out.printf("Aggregate graph has %d vertices and %d edges\n", aggregateGraph.getVertexCount(), aggregateGraph.getEdgeCount());

		// Verifying vertex names
		if (keepVertexNames)
		{
			// All vertex names should be different
			Set<String> vertexNames = new HashSet<>();
			aggregateGraph.getVertices()
					.map(Vertex::getName)
					.forEach(name -> {
						if (!vertexNames.add(name))
						{
							System.err.printf("Error: multiple vertices have the same name '%s'\n", name);
							System.exit(1);
						}
					});
		}
		else
		{
			// Removing all names
			aggregateGraph.getVertices()
					.forEach(v -> v.setName(null));
		}

		System.out.printf("Writing aggregated graph to %s\n", outputFilePath);
		GraphToText.print(aggregateGraph, new PrintWriter(outputStream), keepVertexNames);
		try
		{
			outputStream.flush();
			outputStream.close();
		} catch (IOException e)
		{
			System.err.printf("Error when finalizing the output stream: %s\n", e);
			System.exit(1);
		}
		System.out.println("Done.");
	}

	private static OutputStream createOutput(Path output)
	{
		try
		{
			System.out.printf("Output will be written to %s\n", output);
			return Files.newOutputStream(output);
		} catch (IOException e)
		{
			System.err.printf("Error when trying to create output file: %s\n", e);
			System.exit(1);
			return null;
		}
	}

	private static Stream<Graph> graphCollectionFromTextFileExitOnException(Path filePath)
	{
		try
		{
			return SingleFileTextGraphCollectionLoader.loadGraphsFromFile(filePath.toFile());
		} catch (IOException e)
		{
			System.err.printf("Error when loading %s: %s\n", filePath, e);
			e.printStackTrace();
			System.exit(1);
			return null;
		}
	}
}
