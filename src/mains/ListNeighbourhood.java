package mains;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import graph.loaders.FileSystemTextGraphLoader;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.MutuallyExclusiveGroup;
import net.sourceforge.argparse4j.inf.Namespace;
import utils.GeneralUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListNeighbourhood
{
	public static final String DESCRIPTION = "List the vertices that are closer than a certain distance from some starting vertices.";

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		parser.addArgument("graph").type(Arguments.fileType().verifyCanRead()).help("The graph to process, in text format.");
		parser.addArgument("distance").type(Integer.class).help("The maximum distance allowed from the starting vertices. The starting vertices have a distance of 0.");
		parser.addArgument("-n", "--names").nargs("*").metavar("N").help("The names of the starting vertices");
		parser.addArgument("-v", "--numbers").nargs("*").metavar("V").type(Integer.class).help("The numbers of the starting vertices");
		parser.addArgument("-l", "--labels").nargs("*").metavar("LABEL").help("The labels of the starting vertices");

		MutuallyExclusiveGroup outFormat = parser.addMutuallyExclusiveGroup();
		outFormat.addArgument("--print-numbers").dest("print_numbers").action(Arguments.storeTrue()).help("Print the resulting vertices' numbers (default)");
		outFormat.addArgument("--print-names").dest("print_numbers").action(Arguments.storeFalse()).help("Print the resulting vertices' names");
	}

	public static void execute(Namespace args)
	{
		String graphPath = args.getString("graph");
		System.err.printf("Loading graph from %s...\n", graphPath);
		Graph g = FileSystemTextGraphLoader.loadGraphFromFile(graphPath);
		System.err.printf("Loaded graph with %d vertices and %d edges\n", g.getVertexCount(), g.getEdgeCount());

		final int maxDistance = args.getInt("distance");
		Map<Vertex, VertexWithDistance> toVisit = new HashMap<>();
		Set<Vertex> visited = new HashSet<>();

		// Getting vertices specified as numbers on the command line
		{
			List<Integer> vertexNumbersArg = GeneralUtils.defaultIfNull(args.getList("numbers"), Collections.emptyList());
			for (int v : vertexNumbersArg)
			{
				if (v < 0 || v >= g.getVertexCount())
				{
					System.err.printf("Error: invalid vertex number %d, expected between 0 and %d\n", v, g.getVertexCount() - 1);
					System.exit(1);
				}
				toVisit.put(g.getVertex(v), new VertexWithDistance(g.getVertex(v), 0));
			}
		}
		// Getting vertices specified with names on the command line
		{
			List<String> vertexNamesArg = GeneralUtils.defaultIfNull(args.getList("names"), Collections.emptyList());
			for (String name : vertexNamesArg)
			{
				List<Vertex> verticesWithName = g.getVertices()
						.filter(v -> v.getName().equals(name))
						.collect(Collectors.toList());
				if (verticesWithName.isEmpty())
				{
					System.err.printf("Error: no vertex with name %s\n", name);
					System.exit(1);
				}
				verticesWithName.forEach(v -> toVisit.put(v, new VertexWithDistance(v, 0)));
			}
		}
		// Getting vertices specified with labels on the command line
		{
			List<String> vertexLabelsArg = GeneralUtils.defaultIfNull(args.getList("labels"), Collections.emptyList());
			for (String label : vertexLabelsArg)
			{
				List<Vertex> verticesWithLabel = g.getVerticesWithLabel(label).collect(Collectors.toList());
				if (verticesWithLabel.isEmpty())
				{
					System.err.printf("Error: no vertex with label %s\n", label);
					System.exit(1);
				}
				verticesWithLabel.forEach(v -> toVisit.put(v, new VertexWithDistance(v, 0)));
			}
		}
		if (toVisit.isEmpty())
		{
			System.err.println("Error: no starting vertices specified");
			System.exit(1);
		}

		// Finding vertices with a BFS
		while (!toVisit.isEmpty())
		{
			VertexWithDistance v = toVisit.values().stream().min(Comparator.naturalOrder()).get();
			visited.add(v.vertex);
			toVisit.remove(v.vertex);
			if (v.distance + 1 > maxDistance)
				continue;
			// Process neighbouring vertices following out-edges and in-edges
			g.getOutEdges(v.vertex)
					.map(Edge::getTarget)
					.forEach(neighbour -> visitVertex(neighbour, v.distance + 1, toVisit, visited));
			g.getInEdges(v.vertex)
					.map(Edge::getSource)
					.forEach(neighbour -> visitVertex(neighbour, v.distance + 1, toVisit, visited));
		}

		Stream<String> resultVertices;
		if (args.getBoolean("print_numbers"))
			resultVertices = visited.stream()
					.mapToInt(v -> g.getVertexIndexMap().get(v))
					.sorted()
					.mapToObj(Integer::toString);
		else
			resultVertices = visited.stream()
					.map(Vertex::getName)
					.sorted();
		System.out.println("[" + resultVertices.collect(Collectors.joining(", ")) + "]");
	}

	private static void visitVertex(Vertex v, int newDistance, Map<Vertex, VertexWithDistance> toVisit, Set<Vertex> visited)
	{
		if (visited.contains(v))
			return;
		if (toVisit.containsKey(v))
			toVisit.get(v).updateIfLower(newDistance);
		else
			toVisit.put(v, new VertexWithDistance(v, newDistance));
	}

	private static class VertexWithDistance implements Comparable<VertexWithDistance>
	{
		public Vertex vertex;
		public int distance;

		public VertexWithDistance(Vertex vertex, int distance)
		{
			this.vertex = vertex;
			this.distance = distance;
		}

		public void updateIfLower(int newDistance)
		{
			if (newDistance < this.distance)
				this.distance = newDistance;
		}


		@Override
		public int compareTo(VertexWithDistance other)
		{
			return Comparator.<VertexWithDistance>comparingDouble(v -> v.distance).compare(this, other);
		}
	}
}
