package mains;

import graph.Graph;
import graph.loaders.FileSystemTextGraphLoader;
import graphMDL.jsonSerializer.SerializationUtils;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.impl.type.FileArgumentType;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.MutuallyExclusiveGroup;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import utils.GeneralUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PrintEmbeddings
{
	public static final String DESCRIPTION = "Print embeddings of a pattern contained in a GraphMDL JSON result file";

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		parser.addArgument("json").type(new FileArgumentType().verifyCanRead()).help("GraphMDL JSON result file");
		parser.addArgument("-d", "--data").type(new FileArgumentType().verifyCanRead()).help("Data on which GraphMDL was executed. Needed if names are requested. In text format");
		parser.addArgument("-n", "--names").action(Arguments.storeTrue()).help("Print the names of the data vertices (needs to specify --data)");

		MutuallyExclusiveGroup printUsed = parser.addMutuallyExclusiveGroup();
		printUsed.addArgument("-u", "--used").action(Arguments.storeTrue()).dest("print_used").help("Only print embeddings that are used in the cover (default).");
		printUsed.addArgument("-a", "--all").action(Arguments.storeFalse()).dest("print_used").help("Print all embeddings of the patterns found in the json file, even the ones not used in the cover.");

		parser.addArgument("-v", "--vertices").nargs("+").metavar("V").type(Integer.class).dest("vertex_subset").help("Only print those pattern vertices for each embedding.");

		parser.addArgument("pattern").type(Integer.class).help("The number of the pattern for which to print the embeddings (starts at 0)");
	}

	public static void execute(Namespace args)
	{
		try
		{
			final int patternNumber = args.getInt("pattern");
			final String jsonPath = args.getString("json");

			JSONObject serialization = new JSONObject(new JSONTokener(new FileInputStream(jsonPath)));
			JSONArray patterns = serialization.getJSONArray("patterns");
			if (patternNumber < 0 || patternNumber >= patterns.length())
			{
				System.err.printf("Error: pattern %d requested, but serialization only contains %d patterns\n", patternNumber, patterns.length());
				System.exit(1);
			}
			System.out.printf("Printing embeddings of pattern %d out of %d in %s\n", patternNumber, patterns.length(), jsonPath);
			JSONObject patternSerialization = patterns.getJSONObject(patternNumber);

			// Getting embeddings
			final boolean usedEmbeddingsOnly = args.getBoolean("print_used");
			Stream<Stream<Integer>> embeddings;
			if (usedEmbeddingsOnly)
			{
				System.out.println("Printing only embeddings used in the cover");
				embeddings = SerializationUtils.<JSONObject>castJSONArrayElements(patternSerialization.getJSONArray("used_embeddings"))
						.map(embeddingSerialization -> SerializationUtils.<Integer>castJSONArrayElements(embeddingSerialization.getJSONArray("mapping")));
			}
			else
			{
				System.out.println("Printing all embeddings of the pattern");
				embeddings = SerializationUtils.<JSONArray>castJSONArrayElements(patternSerialization.getJSONArray("embeddings"))
						.map(SerializationUtils::<Integer>castJSONArrayElements);
			}

			// Loading data graph if needed
			final boolean printDataNames = args.getBoolean("names");
			Graph data = null;
			if (printDataNames)
			{
				final String dataPath = args.getString("data");
				if (dataPath == null)
				{
					System.err.println("Error: Names are requested, but data is not specified");
					System.exit(1);
				}
				data = FileSystemTextGraphLoader.loadGraphFromFile(dataPath);
			}

			// Is only a subset of the pattern's vertices requested?
			List<Integer> vertexSubset = null;
			if (args.getList("vertex_subset") != null)
				vertexSubset = args.getList("vertex_subset");


			// Printing embeddings
			Iterator<Stream<Integer>> embeddingsIt = embeddings.iterator();
			for (int embeddingNb = 0; embeddingsIt.hasNext(); ++embeddingNb)
			{
				List<Integer> embedding = embeddingsIt.next().collect(Collectors.toList());
				System.out.printf("==== Embedding %d ====\n", embeddingNb);

				if (vertexSubset != null)
				{
					for (int v : vertexSubset)
						printVertexEmbedding(v, embedding.get(v), printDataNames, data);
				}
				else
				{
					for (int v = 0; v < embedding.size(); ++v)
						printVertexEmbedding(v, embedding.get(v), printDataNames, data);
				}
			}
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("==== Done ====");
	}

	/**
	 * Print the embedding of a pattern vertex.
	 *
	 * @param patternVertex  The pattern vertex number
	 * @param dataVertex     The number of the data vertex to which the pattern vertex is mapped
	 * @param printDataNames Whether to print data vertices' names
	 * @param data           If printDataNames is true, the data graph, in order to get the name
	 */
	private static void printVertexEmbedding(int patternVertex, int dataVertex, boolean printDataNames, Graph data)
	{
		System.out.printf("%5d -> %5d", patternVertex, dataVertex);
		if (printDataNames)
			System.out.printf(" < %s >", GeneralUtils.defaultIfNull(data.getVertex(dataVertex).getName(), "N/A"));
		System.out.println();
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(PrintEmbeddings.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));
		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
