package mains;

import graph.Graph;
import graph.loaders.FileSystemTextGraphLoader;
import graph.loaders.SingleFileTextGraphCollectionLoader;
import graphMDL.*;
import graphMDL.jsonSerializer.GraphMDLSerializer;
import graphMDL.jsonSerializer.simpleGraphs.directed.SimpleDirectedGraphMDLSerializer;
import graphMDL.jsonSerializer.simpleGraphs.undirected.SimpleUndirectedGraphMDLSerializer;
import graphMDL.simpleGraphs.directed.SimpleDirectedCodeTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedKrimp;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedCodeTable;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedKrimp;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedUtils;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.*;
import utils.GeneralUtils;
import utils.Quartiles;
import utils.SingletonStopwatchCollection;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleKrimp
{
	public static final String DESCRIPTION = "Execute GraphMDL algorithm for simple graphs (directed or undirected)";
	public static final String GRAPHMDL_VISUALIZER_ADVERTISEMENT_MESSAGE = "Did you know? The JSON file produced by this method can be used with the GraphMDL Visualizer tool (https://gitlab.inria.fr/fbariatt/graphmdl-visualizer) for an easier analysis of the results.";
	private static final Map<String, Comparator<CodeTableRow>> heuristicsOptions = makeHeuristicsOptions();

	public static Map<String, Comparator<CodeTableRow>> makeHeuristicsOptions()
	{
		Map<String, Comparator<CodeTableRow>> map = new HashMap<>();
		map.put("supportLabel", CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.imageBasedSupportDescLabelCountDesc));
		map.put("labelSupport", CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDescImageBasedSupportDesc));
		map.put("vertexCountDesc", CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.vertexCountDesc));
		map.put("supportDesc", CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.imageBasedSupportDesc));
		map.put("labelDesc", CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDesc));
		return map;
	}

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		//Graph type
		MutuallyExclusiveGroup directed = parser.addMutuallyExclusiveGroup("Graph type").required(true);
		directed.addArgument("-d", "--directed").dest("directed").action(Arguments.storeTrue()).help("Graphs are directed.");
		directed.addArgument("-u", "--undirected").dest("directed").action(Arguments.storeFalse()).help("Graph are undirected.");

		// Input/output
		ArgumentGroup ioGroup = parser.addArgumentGroup("Input/Output");
		ioGroup.addArgument("data").help("The data graph file path (must have doubled edges if undirected).");
		ioGroup.addArgument("patterns").help("Path to candidate patterns. If a directory, all files directly inside the directory (non recursively) will be loaded. If a file, all graphs in the file will be loaded. Graphs must have doubled edges if undirected.");
		ioGroup.addArgument("-o", "--out-file").required(true).help("File where information about the selected patterns and cover will be saved (json format).");

		// Parameters for the search
		ArgumentGroup searchGroup = parser.addArgumentGroup("Search");
		searchGroup.addArgument("--candidate-order").required(true).choices(heuristicsOptions.keySet()).help("Order to use for ordering candidate patterns.");
		searchGroup.addArgument("--table-order").required(true).choices(heuristicsOptions.keySet()).help("Order to use for ordering patterns in the code table.");
		searchGroup.addArgument("--no-pruning").dest("pruning").action(Arguments.storeFalse()).help("Do not use Krimp's code table pruning.");
		searchGroup.addArgument("--no-automorphisms").dest("automorphisms").action(Arguments.storeFalse()).help("Do not use automorphism computation to optimise port usages.");
	}

	public static void execute(Namespace args)
	{

		System.out.printf("Executing simple undirected GraphMDL with arguments %s\n", args);

		// Graph type
		final boolean directed = args.getBoolean("directed");
		System.out.printf("Graphs are considered %s\n", directed ? "directed" : "undirected");

		// Input/output files
		final Graph data = FileSystemTextGraphLoader.loadGraphFromFile(args.getString("data"));
		final Stream<Graph> patterns = patterns(args);
		final FileWriter outputFile = jsonOutputFile(args);

		// Parameters for the search
		final Comparator<CodeTableRow> candidateOrder = heuristicsOptions.get(args.getString("candidate_order"));
		final Comparator<CodeTableRow> codeTableOrder = heuristicsOptions.get(args.getString("table_order"));
		final boolean pruning = args.getBoolean("pruning");
		final boolean automorphisms = args.getBoolean("automorphisms");

		// Creating GraphMDL object and performing the search
		SingletonStopwatchCollection.resetAndStart("globalGraphMDL");
		Krimp krimp;
		if (directed)
			krimp = new SimpleDirectedKrimp(data, patterns, candidateOrder, codeTableOrder, automorphisms);
		else
			krimp = new SimpleUndirectedKrimp(data, patterns, candidateOrder, codeTableOrder, automorphisms);
		CodeTable codeTable = krimp.computeBestCodeTable(pruning, true);
		SingletonStopwatchCollection.stop("globalGraphMDL");

		// Printing search results
		System.out.printf("Tested %d candidate code tables\n", krimp.getTestedCodeTablesCount());
		System.out.printf("Code table contain %d singleton vertex patterns\n", codeTable.getSingletonVertexUsages().size());
		System.out.printf("Code table contain %d singleton edge patterns\n", codeTable.getSingletonEdgeUsages().size());
		System.out.printf("Code table contains %d non-singleton patterns\n", codeTable.getRows().size());
		System.out.printf("Code table contains a total of %d patterns\n", codeTable.getRows().size() + codeTable.getSingletonVertexUsages().size() + codeTable.getSingletonEdgeUsages().size());

		System.out.println("Writing detailed output to output file");
		System.out.println(SimpleKrimp.GRAPHMDL_VISUALIZER_ADVERTISEMENT_MESSAGE);
		GraphMDLSerializer serializer = directed ? new SimpleDirectedGraphMDLSerializer((SimpleDirectedCodeTable) codeTable, data) : new SimpleUndirectedGraphMDLSerializer((SimpleUndirectedCodeTable) codeTable, data);
		serializer.getJSONObject().write(outputFile, 2, 0);
		try
		{
			outputFile.flush();
			outputFile.close();
		} catch (IOException e)
		{
			System.err.println("Error when writing output file to disk: it may be incomplete.");
			e.printStackTrace();
		}

		// Printing code table statistics
		printCTStatistics(codeTable, directed);

		// Timing statistics
		System.out.println("=== Timing information ===");
		System.out.printf("%ds Global GraphMDL time\n", SingletonStopwatchCollection.getElapsedTime("globalGraphMDL").asSeconds());
		System.out.printf("* %ds spent loading candidates\n", SingletonStopwatchCollection.getElapsedTime("krimp.candidates").asSeconds());
		System.out.printf("* %ds spent computing best CT from candidates\n", SingletonStopwatchCollection.getElapsedTime("krimp.searchCodeTable").asSeconds());
		if (SingletonStopwatchCollection.allKeys().contains("codeTablePruning.pruneCodeTable"))
			System.out.printf("** %ds spent in pruning CT\n", SingletonStopwatchCollection.getElapsedTime("codeTablePruning.pruneCodeTable").asSeconds());
		System.out.printf("** %ds spent in computing CT cover and DL\n", SingletonStopwatchCollection.getElapsedTime("krimp.applyCodeTable").asSeconds());
		System.out.printf("*** %ds spent computing non-singletons covers\n", SingletonStopwatchCollection.getElapsedTime("codeTable.nonSingletonPatternsCover").asSeconds());
		System.out.printf("**** %ds spent computing pattern embeddings used in cover\n", SingletonStopwatchCollection.getElapsedTime("codeTableRow.computeCoverEmbeddings").asSeconds());
		System.out.printf("**** %ds spent checking if a vertex needs to be a port\n", SingletonStopwatchCollection.getElapsedTime("codeTableRow.vertexRequiresPort").asSeconds());
		System.out.printf("**** %ds spent computing the best port numbers to use\n", SingletonStopwatchCollection.getElapsedTime("codeTableRow.automorphismPortNumberSearch").asSeconds());
		System.out.printf("*** %ds spent computing singletons cover and port vertices\n", SingletonStopwatchCollection.getElapsedTime("codeTable.SingletonsCoverAndPorts").asSeconds());
		System.out.println("Timers dump (in seconds):");
		SingletonStopwatchCollection.allKeys().stream()
				.sorted()
				.forEachOrdered(key -> System.out.printf("[%s] -> %ds\n", key, SingletonStopwatchCollection.getElapsedTime(key).asSeconds()));
	}


	/**
	 * Loads the candidate patterns from the source defined in command-line arguments.
	 */
	private static Stream<Graph> patterns(Namespace args)
	{
		try
		{
			File patternsFile = new File(args.getString("patterns"));
			if (patternsFile.isDirectory()) // If patterns argument point to a directory containing pattern files
				return FileSystemTextGraphLoader.loadGraphsFromDirectory(patternsFile);
			else // If patterns argument point to a single file (which describes multiple graphs)
			{
				return SingleFileTextGraphCollectionLoader.loadGraphsFromFile(patternsFile);
			}
		} catch (IOException e)
		{
			System.err.printf("Error when loading candidate patterns: %s\n", e);
			System.exit(1);
		}
		return null; // Only so that it compiles: never reached since program exits on error
	}

	/**
	 * Creates the json output file and returns an open writer to it.
	 */
	private static FileWriter jsonOutputFile(Namespace args)
	{
		String outFilePath = args.getString("out_file");
		if (outFilePath == null)
			throw new IllegalArgumentException("Missing required parameter: output file");
		System.out.printf("Detailed output will be written to %s\n", outFilePath);
		try
		{
			return new FileWriter(outFilePath);
		} catch (IOException e)
		{
			System.err.printf("Impossible to open output file: %s\n", e);
			System.exit(1);
		}
		// Should never be reached as in case of exception we exit the program,
		// but needed so that it compiles
		return null;
	}

	/**
	 * Print some statistics about the distribution of different attributes of the CT non-singleton patterns.
	 */
	private static void printCTStatistics(CodeTable codeTable, boolean directed)
	{
		System.out.println("=== Code table statistics ===");
		if (codeTable.getRows().size() > 0)
		{
			List<Integer> sortedPatternUsages = codeTable.getRows().stream()
					.filter(pattern -> pattern.getUsage() > 0)
					.mapToInt(CodeTableRow::getUsage)
					.sorted()
					.boxed()
					.collect(Collectors.toList());
			System.out.printf("Code table non-singleton patterns usage distribution: %s\n", quartilesToString(Quartiles.fromSorted(sortedPatternUsages)));

			List<Integer> sortedPatternVertexCounts = codeTable.getRows().stream()
					.filter(pattern -> pattern.getUsage() > 0)
					.mapToInt(pattern -> pattern.getPatternGraph().getVertexCount())
					.sorted()
					.boxed()
					.collect(Collectors.toList());
			System.out.printf("Code table non-singleton patterns vertex count distribution: %s\n", quartilesToString(Quartiles.fromSorted(sortedPatternVertexCounts)));

			List<Integer> sortedPatternEdgeCounts = codeTable.getRows().stream()
					.filter(pattern -> pattern.getUsage() > 0)
					.mapToInt(pattern -> (int) (directed ? pattern.getPatternGraph().getEdgeCount() : pattern.getPatternGraph().getEdgeCount() / 2))
					.sorted()
					.boxed()
					.collect(Collectors.toList());
			System.out.printf("Code table non-singleton patterns edge count distribution: %s\n", quartilesToString(Quartiles.fromSorted(sortedPatternEdgeCounts)));

			List<Integer> sortedPatternLabelCounts = codeTable.getRows().stream()
					.filter(pattern -> pattern.getUsage() > 0)
					.mapToInt(pattern -> directed ? Utils.graphLabelCount(pattern.getPatternGraph()) : SimpleUndirectedUtils.graphLabelCount(pattern.getPatternGraph()))
					.sorted()
					.boxed()
					.collect(Collectors.toList());
			System.out.printf("Code table non-singleton patterns label count distribution: %s\n", quartilesToString(Quartiles.fromSorted(sortedPatternLabelCounts)));
		}
		else
		{
			System.out.println("Code table contains 0 non-singleton patterns. Skipping statistics computation.");
		}
	}

	private static String quartilesToString(Quartiles<Integer> quartiles)
	{
		return String.format("%d(min), %d(25%%), %d(50%%), %d(75%%), %d(max)",
				quartiles.min,
				quartiles.q25,
				quartiles.q50,
				quartiles.q75,
				quartiles.max
		);
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(SimpleKrimp.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));

		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
