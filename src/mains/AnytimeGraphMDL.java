package mains;

import graph.Graph;
import graph.loaders.FileSystemTextGraphLoader;
import graphMDL.*;
import graphMDL.anytime.MultiDirectedAnytimeGraphMDL;
import graphMDL.anytime.SimpleDirectedAnytimeGraphMDL;
import graphMDL.anytime.SimpleUndirectedAnytimeGraphMDL;
import graphMDL.jsonSerializer.GraphMDLSerializer;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentGroup;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import sun.misc.Signal;
import utils.GeneralUtils;
import utils.GraphType;
import utils.SingletonStopwatchCollection;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class AnytimeGraphMDL
{
	public static final String DESCRIPTION = "Execute Anytime versions of GraphMDL algorithm (a.k.a. GraphMDL+ for simple graphs and KG-MDL for multigraphs).";
	private static final Map<String, Comparator<CodeTableRow>> heuristicsOptions = SimpleKrimp.makeHeuristicsOptions();

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		// Graph type
		GraphType.addCliArguments(parser);

		// Input/output files
		ArgumentGroup ioGroup = parser.addArgumentGroup("Input/Output");
		ioGroup.addArgument("data").help("The data graph file path (must have doubled edges if undirected).");
		ioGroup.addArgument("-o", "--out-file").required(true).help("File where information about the selected patterns and cover will be saved (json format).");
		ioGroup.addArgument("--dl-log").help("File where the description lengths of accepted code tables will be logged.");

		// Parameters for the search
		ArgumentGroup searchGroup = parser.addArgumentGroup("Search");
		searchGroup.addArgument("--table-order").choices(heuristicsOptions.keySet()).setDefault("labelSupport").help("Order to use for ordering patterns in the code table. Defaults to labelSupport.");
		searchGroup.addArgument("--no-strip-unused").dest("strip_unused").action(Arguments.storeFalse()).help("Do not remove unused patterns from the final CT.");
		searchGroup.addArgument("--no-pruning").dest("pruning").action(Arguments.storeFalse()).help("Do not use pruning after a new code table is found.");
		searchGroup.addArgument("--timeout", "--row-cover-timeout").dest("row_cover_timeout").metavar("ms").type(Integer.class).help("If not specified, when a pattern is added to the code table all of its embeddings are computed, and they are just filtered for each cover. If specified, only the cover embeddings are computed from the data for each cover. If specified, precise how many milliseconds are allocated to compute the cover embeddings of each code table row (0 meaning no limit).");
		searchGroup.addArgument("--dl", "--dl-threshold").type(Double.class).help("If specified, the computation will stop after finding a CT yielding a description lower or equal to this value.");
		searchGroup.addArgument("-t", "--max-time").help("If specified, the computation will stop after this much time. Must be an integer followed by either s(seconds), m(minutes), or h(hours).");

		// Pattern constraints
		ArgumentGroup constraintGroup = parser.addArgumentGroup("Pattern constraints");
		constraintGroup.addArgument("--max-same-edges").metavar("k").type(Integer.class).help("Force generated patterns to have k edges or less of same label around any vertex. E.g. with k = 2 the pattern 'a book has three authors' will not be generated.");
		constraintGroup.addArgument("--forbidden-labels-file").metavar("file").type(Arguments.fileType().verifyCanRead()).help("Force generated patterns to not have any of the labels specified in this file (one label per line).");
		constraintGroup.addArgument("--required-labels-file").metavar("file").type(Arguments.fileType().verifyCanRead()).help("Force all generated patterns to have *at least one* label specified in this file (one label per line).");
		constraintGroup.addArgument("--max-embeddings").metavar("e").type(Integer.class).help("When all pattern embeddings are computed, limit the number of embeddings computed for each pattern to this value.");
		constraintGroup.addArgument("--minsup", "--minsupp", "--minimum-support").metavar("s").type(Integer.class).help("Force generated patterns to have a minimum image based support greater or equal to the given value. Only possible if pattern embeddings are computed (i.e. no row cover timeout specified).");
	}

	public static void execute(Namespace args)
	{

		System.out.printf("Executing anytime GraphMDL with arguments %s\n", args);

		final EnumSet<GraphType> graphType = GraphType.fromCliArguments(args);
		System.out.printf("Graphs are considered %s\n", graphType.contains(GraphType.DIRECTED) ? "directed" : "undirected");
		System.out.printf("Using %s encoding\n", graphType.contains(GraphType.MULTIGRAPH) ? "multigraph" : "simple graph (non-multigraph)");

		// Input/Output files
		final Graph data = FileSystemTextGraphLoader.loadGraphFromFile(args.getString("data"));
		final FileWriter outputFile = jsonOutputFile(args);
		final Optional<PrintWriter> dlLogfile = dlLogFile(args);

		// Parameters for the search
		final Comparator<CodeTableRow> ctOrder = heuristicsOptions.get(args.getString("table_order"));
		final boolean stripUnusedPatterns = args.getBoolean("strip_unused");
		final boolean usePruning = args.getBoolean("pruning");
		final Optional<Integer> ctRowCoverTimeout = Optional.ofNullable(args.getInt("row_cover_timeout"));
		final Optional<Double> dlThreshold = Optional.ofNullable(args.getDouble("dl"));
		final Optional<Integer> searchTimeout = searchTimeout(args);

		// Pattern constraints
		final PatternConstraints patternConstraints = patternConstraints(args);

		// Creating the actual GraphMDL+ object
		graphMDL.anytime.AnytimeGraphMDL anytimeGraphMDL = createAnytimeGraphMDLObject(
				graphType,
				data,
				ctOrder,
				patternConstraints,
				ctRowCoverTimeout.<CoverStrategy>map(ScanDataCoverStrategy::new).orElse(new FilterEmbeddingsCoverStrategy())
		);

		// Graceful shutdown on SIGINT (CTRL+C)
		// Note: for an instant shutdown, it is therefore necessary to use SIGTERM or SIGKILL
		Signal.handle(new Signal("INT"), signal -> {
			System.out.println("=== Received SIGINT, asking for graceful shutdown (may take a while) ===");
			anytimeGraphMDL.askToStopSearch();
		});

		// Performing the search
		CodeTable codeTable = anytimeGraphMDL.computeBestCodeTable(
				dlThreshold.orElse(0.0),
				searchTimeout.orElse(0),
				stripUnusedPatterns,
				usePruning,
				dlLogfile.orElse(null)
		);
		dlLogfile.ifPresent(writer -> { // Make sure that all the content is written to disk
			writer.flush();
			writer.close();
		});

		// Printing search results
		System.out.printf("Tested %d candidate code tables\n", anytimeGraphMDL.getTestedCodeTablesCount());
		System.out.printf("Code table contain %d singleton vertex patterns\n", codeTable.getSingletonVertexUsages().size());
		System.out.printf("Code table contain %d singleton edge patterns\n", codeTable.getSingletonEdgeUsages().size());
		System.out.printf("Code table contains %d non-singleton patterns\n", codeTable.getRows().size());
		System.out.printf("Code table contains a total of %d patterns\n", codeTable.getRows().size() + codeTable.getSingletonVertexUsages().size() + codeTable.getSingletonEdgeUsages().size());

		System.out.println("Writing detailed output to output file");
		System.out.println(SimpleKrimp.GRAPHMDL_VISUALIZER_ADVERTISEMENT_MESSAGE);
		GraphMDLSerializer serializer = Cover.createGraphMDLSerializer(codeTable, data, graphType);
		serializer.getJSONObject().write(outputFile, 2, 0);
		try
		{
			outputFile.flush();
			outputFile.close();
		} catch (IOException e)
		{
			System.err.println("Error when writing output file to disk: it may be incomplete.");
			e.printStackTrace();
		}


		// Timing statistics
		System.out.println("=== Timing information ===");
		System.out.printf("%ds Anytime GraphMDL global search time\n", SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.ctSearch").asSeconds());
		System.out.printf("* %ds Merge candidate generation\n", SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.candidateGeneration").asSeconds());
		System.out.printf("** %ds Computing common ports of embeddings\n", SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.candidateGeneration.createPortSet").asSeconds());
		System.out.printf("** %ds Computing if two candidates are equivalent\n", SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.candidateGeneration.findEquivalent").asSeconds());
		System.out.printf("* %ds Computing merge candidate support estimation\n", SingletonStopwatchCollection.getElapsedTime("mergeCandidate.usageEstimation").asSeconds());
		System.out.printf("* %ds Creating the actual pattern graphs\n", SingletonStopwatchCollection.getElapsedTime("mergeCandidate.patternCreation").asSeconds());
		System.out.printf("* %ds Testing if candidate improves description length\n", SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.candidateTesting").asSeconds());
		System.out.printf("** %ds Computing Candidate|ST\n", SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.candidateSTEncoding").asSeconds());
		System.out.printf("** %ds Creating CT rows from candidates\n", SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.createCTRowFromCandidate").asSeconds());
		System.out.printf("** %ds Computing code table cover and rewritten graph\n", SingletonStopwatchCollection.getElapsedTime("anytimeGraphMDL.applyCT").asSeconds());
		System.out.println("---");
		System.out.println("Timers dump (in seconds):");
		SingletonStopwatchCollection.allKeys().stream()
				.sorted()
				.forEachOrdered(key -> System.out.printf("[%s] -> %ds\n", key, SingletonStopwatchCollection.getElapsedTime(key).asSeconds()));

		System.out.println("Done.");
	}

	/**
	 * Creates the json output file and returns an open writer to it.
	 */
	private static FileWriter jsonOutputFile(Namespace args)
	{
		String outFilePath = args.getString("out_file");
		if (outFilePath == null)
			throw new IllegalArgumentException("Missing required parameter: output file");
		System.out.printf("Detailed output will be written to %s\n", outFilePath);
		try
		{
			return new FileWriter(outFilePath);
		} catch (IOException e)
		{
			System.err.printf("Impossible to open output file: %s\n", e);
			System.exit(1);
		}
		// Should never be reached as in case of exception we exit the program,
		// but needed so that it compiles
		return null;
	}

	/**
	 * Create the description length log file and return a writer to it if the user requested it.
	 */
	private static Optional<PrintWriter> dlLogFile(Namespace args)
	{
		return Optional.ofNullable(args.getString("dl_log"))
				.flatMap(logFilePath -> {
					try
					{
						PrintWriter dlLogFile = new PrintWriter(new FileOutputStream(logFilePath), true);
						dlLogFile.println("time(ms),DL(bits)");
						return Optional.of(dlLogFile);
					} catch (FileNotFoundException e)
					{
						System.err.printf("Impossible to open description length log file: %s\n", e);
						System.exit(1);
					}
					// Should never be reached as in case of exception we exit the program
					// but needed so that it compiles.
					return Optional.empty();
				});
	}

	/**
	 * Parse the maximum time allocated to the search and return it as a number of seconds, if the user supplied a value for it.
	 */
	private static Optional<Integer> searchTimeout(Namespace args)
	{
		return Optional.ofNullable(args.getString("max_time"))
				.map(timeoutString -> {
					int value = Integer.parseInt(timeoutString.substring(0, timeoutString.length() - 1));
					switch (timeoutString.charAt(timeoutString.length() - 1))
					{
						case 's':
							return value;
						case 'm':
							return value * 60;
						case 'h':
							return value * 60 * 60;
						default:
							System.err.printf("Invalid time specification for max time of search: should be an integer followed by either s,m or h, got %s\n", timeoutString);
							System.exit(1);
							return 0; // So that it compiles, since we exit the program before reaching this.
					}
				});
	}

	/**
	 * Create a set of constraints that should be applied to the generated patterns based on user-supplied options.
	 */
	private static PatternConstraints patternConstraints(Namespace args)
	{
		PatternConstraints patternConstraints = PatternConstraints.noConstraints();
		Optional.ofNullable(args.getInt("max_same_edges"))
				.ifPresent(maxEdges -> patternConstraints.addPatternStructureConstraint(PatternConstraints.makeVerticesHaveLessThanKSameEdges(maxEdges)));
		Optional.ofNullable(args.getString("forbidden_labels_file"))
				.map(AnytimeGraphMDL::getFileLinesWithoutSpacesAsSet)
				.ifPresent(forbiddenLabels -> patternConstraints.addCandidateGenerationEmbeddingConstraint(PatternConstraints.makeCandidateHasNoForbiddenLabel(forbiddenLabels, forbiddenLabels)));
		Optional.ofNullable(args.getString("required_labels_file"))
				.map(AnytimeGraphMDL::getFileLinesWithoutSpacesAsSet)
				.ifPresent(requiredLabels -> patternConstraints.addPatternStructureConstraint(PatternConstraints.makePatternContainsRequiredLabel(requiredLabels)));
		Optional.ofNullable(args.getInt("max_embeddings"))
				.ifPresent(patternConstraints::setMaxEmbeddingsPerPattern);
		Optional.ofNullable(args.getInt("minsup"))
				.ifPresent(minSupport -> {
					if (args.getInt("row_cover_timeout") != null)
					{
						System.err.println("Minsup constraint is incompatible with row cover timeout: when the latter is specified, complete pattern embeddings (and thus support) are not computed.");
						System.exit(1);
					}
					patternConstraints.addCodeTableRowConstraint(PatternConstraints.makePatternMinSupport(minSupport));
				});
		return patternConstraints;
	}

	/**
	 * Open the given file and return the lines it contains as a set.
	 * All spaces and carriage returns are removed.
	 * If an error is generated, the program exits.
	 */
	private static Set<String> getFileLinesWithoutSpacesAsSet(String filePath)
	{
		try
		{
			return new BufferedReader(new FileReader(filePath))
					.lines()
					.map(line -> line.replace("\n", "").replace(" ", ""))
					.collect(Collectors.toSet());
		} catch (FileNotFoundException e)
		{
			System.err.printf("Impossible to open file %s : %s", filePath, e);
			System.exit(1);
			return Collections.emptySet(); // So that it compiles. Never reached because we exit the program.
		}
	}

	/**
	 * Create the correct GraphMDL+ object based on the graph type.
	 * All parameters other than the graph type are simply passed to the constructor.
	 */
	private static graphMDL.anytime.AnytimeGraphMDL createAnytimeGraphMDLObject(EnumSet<GraphType> graphType, Graph data, Comparator<CodeTableRow> ctOrder, PatternConstraints patternConstraints, CoverStrategy coverStrategy)
	{
		if (graphType.equals(GraphType.SIMPLE_DIRECTED))
			return new SimpleDirectedAnytimeGraphMDL(data, ctOrder, patternConstraints, coverStrategy);
		else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
			return new SimpleUndirectedAnytimeGraphMDL(data, ctOrder, patternConstraints, coverStrategy);
		else if (graphType.equals(GraphType.MULTI_DIRECTED))
			return new MultiDirectedAnytimeGraphMDL(data, ctOrder, patternConstraints, coverStrategy);
		else // Multi undirected
		{
			System.err.println("Undirected multigraphs are not supported for now");
			System.exit(1);
			return null;
		}
	}
}
