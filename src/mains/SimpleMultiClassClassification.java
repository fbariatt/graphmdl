package mains;

import graph.Graph;
import graph.loaders.SingleFileTextGraphCollectionLoader;
import graphMDL.CodeTable;
import graphMDL.CodeTableHeuristics;
import graphMDL.classification.SimpleClassificationCT;
import graphMDL.classification.ClassificationUtils;
import graphMDL.jsonSerializer.GraphMDLDeserializer;
import graphMDL.jsonSerializer.simpleGraphs.directed.SimpleDirectedGraphMDLDeserializer;
import graphMDL.jsonSerializer.simpleGraphs.undirected.SimpleUndirectedGraphMDLDeserializer;
import graphMDL.simpleGraphs.SimpleCodeTable;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import utils.Average;
import utils.ConfusionMatrix;
import utils.GeneralUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class SimpleMultiClassClassification
{
	public final static String DESCRIPTION = "Perform a classification using results of GraphMDL. Can support any number of classes (min 2).";

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);
		parser.addArgument("-v", "--vertex-alphabet").required(true).help("File containing the alphabet of vertex labels common to all classes");
		parser.addArgument("-e", "--edge-alphabet").required(true).help("File containing the alphabet of edge labels common to all classes");
		parser.addArgument("--threshold").type(Double.class).help("Confidence threshold: if the chosen class is less than threshold bits from the second choice, no classification is made");
		MutuallyExclusiveGroup directed = parser.addMutuallyExclusiveGroup().required(true);
		directed.addArgument("-d", "--directed").dest("directed").action(Arguments.storeTrue()).help("The graph is directed");
		directed.addArgument("-u", "--undirected").dest("directed").action(Arguments.storeFalse()).help("The graph is undirected");
		ArgumentGroup classInformation = parser.addArgumentGroup("Class files").description("Information required for each class. Can be repeated as much as there are classes");
		classInformation.addArgument("-j", "--json").required(true).action(Arguments.append()).help("GraphMDL JSON result file for this class");
		classInformation.addArgument("-t", "--test").required(true).action(Arguments.append()).help("File containing the test set of this class");
		classInformation.addArgument("-n", "--name").required(false).action(Arguments.append()).metavar("NAME").dest("class_names").help("Name of this class (used only in display)");
	}

	public static void execute(Namespace args)
	{
		System.out.printf("Executing multi-class classification with arguments %s\n", args);

		final boolean directed = args.getBoolean("directed");
		System.out.println("Graphs are " + (directed ? "directed" : "undirected"));

		List<String> jsonFiles = args.getList("json");
		List<String> testSetsFiles = args.getList("test");
		List<String> classNames = args.getList("class_names");
		if (jsonFiles.size() != testSetsFiles.size())
		{
			System.err.println("Error: each class must have exactly one json file and one test set");
			System.exit(1);
		}

		final int nbClasses = jsonFiles.size();

		if ((classNames != null) && (classNames.size() != nbClasses))
		{
			System.err.println("Error: if class names are specified, there must be as much names as there are classes.");
			System.err.printf("Got names %s for %d classes\n", classNames, nbClasses);
			System.exit(1);
		}
		if (nbClasses < 2)
		{
			System.err.printf("Error: at least two classes should be specified but %d are given.\n", nbClasses);
			System.exit(1);
		}

		System.out.printf("Performing a %d-way classification\n", nbClasses);

		// Loading alphabets
		Set<String> vertexAlphabet = loadAlphabetExitOnError(args.getString("vertex_alphabet"));
		System.out.printf("Loaded an alphabet of %d vertex labels\n", vertexAlphabet.size());
		Set<String> edgeAlphabet = loadAlphabetExitOnError(args.getString("edge_alphabet"));
		System.out.printf("Loaded an alphabet of %d edge labels\n", edgeAlphabet.size());

		// Loading information for each class
		List<SimpleClassificationCT> codeTables = new ArrayList<>(nbClasses);
		List<Stream<Graph>> testSets = new ArrayList<>(nbClasses);
		for (int i = 0; i < nbClasses; ++i)
		{
			try
			{
				final JSONObject serialization = new JSONObject(new JSONTokener(new FileInputStream(jsonFiles.get(i))));
				final String kind = serialization.getString("kind");
				GraphMDLDeserializer deserializer;
				if (directed)
				{
					if (!kind.equals("simple_directed"))
					{
						System.err.printf("Error: expected simple directed graphs and got 'kind = %s' in json file for class %s\n", kind, ClassificationUtils.getDisplayNameForClass(classNames, i));
						System.exit(1);
					}
					deserializer = new SimpleDirectedGraphMDLDeserializer(serialization);
				}
				else
				{
					if (!kind.equals("simple_undirected"))
					{
						System.err.printf("Error: expected simple undirected graphs and got 'kind = %s' in json file for class %s\n", kind, ClassificationUtils.getDisplayNameForClass(classNames, i));
						System.exit(1);
					}
					deserializer = new SimpleUndirectedGraphMDLDeserializer(serialization);
				}

				CodeTable baseCT = deserializer.createCodeTable(CodeTableHeuristics.PartialOrder.noOrder, false, null);
				if (!(baseCT instanceof SimpleCodeTable)) // This should never happen since we use SimpleGraphMDL deserializers
					throw new RuntimeException("Deserialized code table not of class SimpleCodeTable. This should not happen");
				codeTables.add(new SimpleClassificationCT((SimpleCodeTable) baseCT, vertexAlphabet, edgeAlphabet));
				testSets.add(SingleFileTextGraphCollectionLoader.loadGraphsFromFile(testSetsFiles.get(i)));
				System.out.printf("Loaded class %s information\n", ClassificationUtils.getDisplayNameForClass(classNames, i));
			} catch (IOException | JSONException e)
			{
				System.err.printf("Error when loading class %s information\n", ClassificationUtils.getDisplayNameForClass(classNames, i));
				e.printStackTrace();
				System.exit(1);
			}
		}

		// Performing classification
		ConfusionMatrix confusionMatrix = new ConfusionMatrix();
		Average[][] averageDLDifferences = new Average[nbClasses][nbClasses]; // Average difference in DL for each cell of the confusion matrix
		for (int i = 0; i < nbClasses; ++i)
			for (int j = 0; j < nbClasses; ++j)
				averageDLDifferences[i][j] = new Average();
		Double confidenceThreshold = args.getDouble("threshold");
		if (confidenceThreshold != null)
			System.out.printf("Using a confidence threshold of %f bits\n", confidenceThreshold);

		for (int classId = 0; classId < nbClasses; ++classId)
		{
			Iterator<Graph> testSet = testSets.get(classId).iterator();
			while (testSet.hasNext())
			{
				Graph toClassify = testSet.next();
				// Computing description length with each CT
				double[] descriptionLengths = new double[nbClasses];
				for (int i = 0; i < descriptionLengths.length; ++i)
				{
					descriptionLengths[i] = codeTables.get(i).computeGraphDescriptionLength(toClassify);
				}

				// Finding the CT yielding the smallest description length, which will be the selected class
				int chosenClass = 0;
				double minDL = descriptionLengths[0];
				double secondMinDL = -1;
				for (int i = 1; i < descriptionLengths.length; ++i)
				{
					if (descriptionLengths[i] < secondMinDL || secondMinDL == -1)
						secondMinDL = descriptionLengths[i];

					if (descriptionLengths[i] < minDL)
					{
						chosenClass = i;
						secondMinDL = minDL;
						minDL = descriptionLengths[i];
					}
				}

				if (confidenceThreshold != null && (secondMinDL - minDL) < confidenceThreshold) // If we are under the confidence threshold
				{
					confusionMatrix.increaseValue(ClassificationUtils.getDisplayNameForClass(classNames, classId), "unknown"); // We choose not to classify this element
				}
				else // We are over the confidence threshold
				{
					// We classify the element as the class with the lowest DL
					confusionMatrix.increaseValue(ClassificationUtils.getDisplayNameForClass(classNames, classId), ClassificationUtils.getDisplayNameForClass(classNames, chosenClass));
					if (chosenClass == classId) // Classification was correctly performed
						averageDLDifferences[classId][classId].addValue(secondMinDL - minDL); // The cell contains the minimum difference between the class and any other class
					else
						averageDLDifferences[classId][chosenClass].addValue(descriptionLengths[classId] - minDL); // The cell contains the difference between the correct class and the chosen class
				}
			}
			System.out.printf("Performed classification on class %s test set\n", ClassificationUtils.getDisplayNameForClass(classNames, classId));
		}

		if (confidenceThreshold != null)
			confusionMatrix.increaseValue("unknown", "unknown", 0); // This just makes sure that there will be a line and a column for the "unknown" class in the matrix

		// Printing confusion matrix
		System.out.println("==== Classification results ====");
		System.out.println(confusionMatrix);
		System.out.println(confusionMatrix.printLabelPrecRecFm());
		System.out.printf("Avg precision: %f, avg recall: %f\n", confusionMatrix.getAvgPrecision(), confusionMatrix.getAvgRecall());
		System.out.println(confusionMatrix.printNiceResults());

		// Printing DL difference matrix
		System.out.println();
		System.out.println("==== Average DL differences ====");
		// Header
		System.out.print("↓gold\\pred→");
		for (int i = 0; i < nbClasses; ++i)
			System.out.printf("%12s", ClassificationUtils.getDisplayNameForClass(classNames, i));
		System.out.println();
		// Lines
		for (int i = 0; i < nbClasses; ++i)
		{
			System.out.printf("%11s", ClassificationUtils.getDisplayNameForClass(classNames, i));
			for (int j = 0; j < nbClasses; ++j)
				System.out.printf("%12.3f", averageDLDifferences[i][j].getAverage());
			System.out.println();
		}
		System.out.println("Classified correctly: average difference between DL of that class and second-best DL");
		System.out.println("Classified incorrectly: average difference between DL of chosen class and DL of correct class");
	}

	private static Set<String> loadAlphabetExitOnError(String alphabetFile)
	{
		Set<String> alphabet = null;
		try
		{
			alphabet = ClassificationUtils.loadAlphabet(alphabetFile);
		} catch (FileNotFoundException e)
		{
			System.err.println("Error when trying to load alphabet");
			e.printStackTrace();
			System.exit(1);
		}
		return alphabet;
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(SimpleMultiClassClassification.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));
		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
