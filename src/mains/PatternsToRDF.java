package mains;

import graph.Graph;
import graph.printers.GraphToRDF;
import graphMDL.CodeTable;
import graphMDL.CodeTableHeuristics;
import graphMDL.CodeTableRow;
import graphMDL.jsonSerializer.GraphMDLDeserializer;
import graphMDL.jsonSerializer.multigraphs.MultiDirectedGraphMDLDeserializer;
import graphMDL.jsonSerializer.simpleGraphs.directed.SimpleDirectedGraphMDLDeserializer;
import graphMDL.jsonSerializer.simpleGraphs.undirected.SimpleUndirectedGraphMDLDeserializer;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.impl.type.FileArgumentType;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.MutuallyExclusiveGroup;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.json.JSONObject;
import org.json.JSONTokener;
import utils.GeneralUtils;
import utils.GraphType;
import utils.RDFUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class PatternsToRDF
{
	public static final String DESCRIPTION = "Convert GraphMDL patterns to SPARQL queries";

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		// I/O arguments
		parser.addArgument("json").type(new FileArgumentType().verifyCanRead()).help("GraphMDL JSON result file.");
		parser.addArgument("rdf_data").type(Arguments.fileType().verifyCanRead()).help("RDF graph used to retrieve literals datatypes. Pattern vertices with such labels will be converted to literals.");
		parser.addArgument("-l", "--lang").required(true).choices(RDFUtils.commandLineRDFFormats.keySet()).help("The format of the RDF graph.");
		parser.addArgument("output").nargs("?").help("Where the patterns should be written. Omit for stdout");

		// Graph type
		GraphType.addCliArguments(parser);

		// Select part of query
		MutuallyExclusiveGroup selectVars = parser.addMutuallyExclusiveGroup("SELECT variables");
		selectVars.addArgument("-v", "--select-all").dest("select_ports").action(Arguments.storeFalse()).help("Use all pattern vertices as variables in the SELECT part of the query (default).");
		selectVars.addArgument("-p", "--select-ports").dest("select_ports").action(Arguments.storeTrue()).help("Use port vertices as variables in the SELECT part of the query.");

		// Other arguments
		parser.addArgument("--no-isomorphisms").dest("use_isomorphism").action(Arguments.storeFalse()).help("By default, clauses are added to the FILTER part of the query to ensure an isomorphic mapping of the pattern in the data. Specify this option to avoid doing so.");
	}

	public static void execute(Namespace args)
	{
		org.apache.jena.query.ARQ.init(); // Making sure that SPARQL subsystem is started

		// Parsing parameters and loading inputs
		System.out.printf("Executing with arguments %s\n", args);
		final EnumSet<GraphType> graphType = GraphType.fromCliArguments(args);
		System.out.printf("Graphs are considered %s and %s\n", graphType.contains(GraphType.DIRECTED) ? "directed" : "undirected", graphType.contains(GraphType.MULTIGRAPH) ? "multigraphs" : "simple (non-multigraph)");
		final CodeTable ct = loadCT(args.getString("json"), graphType);
		final Model rdfData = getRDFData(args);
		final PrintWriter output = getOutput(args);
		final boolean selectPorts = getSelectPorts(args);
		final boolean useIsomorphism = args.getBoolean("use_isomorphism");

		// Loading literal datatypes
		System.out.println("Computing datatypes in data graph");
		Set<String> dataTypes = new HashSet<>();
		try (QueryExecution queryExecution = QueryExecutionFactory.create(RDFUtils.createDatatypeSelectQuery(), rdfData))
		{
			queryExecution.execSelect().forEachRemaining(
					row -> dataTypes.add(row.get("dataType").asResource().getURI())
			);
		}
		System.out.println("The following datatypes have been found:");
		dataTypes.stream()
				.sorted()
				.forEachOrdered(System.out::println);

		// Performing the conversion
		for (int patternNb = 0; patternNb < ct.getRows().size(); ++patternNb)
		{
			output.println("-".repeat(10) + " " + (patternNb + 1) + " " + "-".repeat(10));
			CodeTableRow patternRow = ct.getRows().get(patternNb);
			Graph patternGraph = patternRow.getPatternGraph();
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(patternGraph, GraphToRDF::isURIIfHasColon, GraphToRDF.makeIsLiteralIfHasValueOrLabelIn(dataTypes));

			if (patternQuery.isEmpty()) // The query is empty: the pattern is completely described by the conversion of vertices to query nodes.
			{
				for (int v = 0; v < patternGraph.getVertexCount(); ++v)
				{
					output.println(patternQuery.getVerticesAsNodes().get(patternGraph.getVertex(v)));
				}
			}
			else
			{
				if (selectPorts)
				{
					output.println(patternQuery.getSelectQuery(
							useIsomorphism,
							patternRow.getPortUsages().keySet().stream()
									.map(v -> patternQuery.getVerticesAsNodes().get(patternGraph.getVertex(v)))
									.collect(Collectors.toList())
					));
				}
				else
				{
					output.println(patternQuery.getSelectQuery(useIsomorphism));
				}
			}
		}
		output.flush();
		output.close();
	}

	/**
	 * Load the RDF graph specified in the command-line arguments and return it.
	 */
	private static Model getRDFData(Namespace args)
	{
		try
		{
			String rdfDataPath = args.getString("rdf_data");
			System.out.printf("Loading rdf file %s ...\n", rdfDataPath);
			Model result = ModelFactory.createDefaultModel();
			result.read(new FileInputStream(rdfDataPath), null, RDFUtils.commandLineRDFFormats.get(args.getString("lang")));
			return result;
		} catch (FileNotFoundException e)
		{
			System.err.printf("Error when loading RDF data: %s\n", e);
			System.exit(1);
			return null;
		}
	}

	/**
	 * @return An open writer to the output specified in the command-line arguments.
	 */
	private static PrintWriter getOutput(Namespace args)
	{
		try
		{
			final String outputPath = args.getString("output");
			if (outputPath == null)
			{

				return new PrintWriter(System.out, true);
			}
			else
			{
				System.out.printf("Output will be saved to %s\n", outputPath);
				return new PrintWriter(new FileOutputStream(outputPath), true);
			}
		} catch (FileNotFoundException e)
		{
			System.err.printf("Impossible to open output file: %s\n", e);
			System.exit(1);
			return null;
		}
	}


	/**
	 * Deserialize the code table from the json file.
	 */
	private static CodeTable loadCT(String jsonPath, EnumSet<GraphType> graphType)
	{
		try
		{
			System.out.printf("Loading json file %s\n", jsonPath);
			JSONObject jsonSerialization = new JSONObject(new JSONTokener(new FileInputStream(jsonPath)));
			GraphMDLDeserializer deserializer = null;
			if (graphType.equals(GraphType.SIMPLE_DIRECTED))
				deserializer = new SimpleDirectedGraphMDLDeserializer(jsonSerialization);
			else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
				deserializer = new SimpleUndirectedGraphMDLDeserializer(jsonSerialization);
			else if (graphType.equals(GraphType.MULTI_DIRECTED))
				deserializer = new MultiDirectedGraphMDLDeserializer(jsonSerialization);
			else // Multi undirected
			{
				System.err.println("Undirected multigraphs are not supported for now");
				System.exit(1);
			}
			return deserializer.createCodeTableWithoutEmbeddings(CodeTableHeuristics.PartialOrder.noOrder);
		} catch (FileNotFoundException e)
		{
			System.err.printf("Error when loading CT %s\n", e);
			System.exit(1);
			return null;
		}
	}

	private static boolean getSelectPorts(Namespace args)
	{
		boolean result = args.getBoolean("select_ports");
		if (result)
			System.out.println("Only port vertices will appear as variables in the queries");
		return result;
	}


	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(PatternsToRDF.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));
		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
