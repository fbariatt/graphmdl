package mains;

import graph.Graph;
import graph.loaders.SingleFileTextGraphCollectionLoader;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import utils.GeneralUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class Alphabet
{
	public final static String DESCRIPTION = "Process one or multiple data files and output the global alphabet of vertex and edge labels";

	public static void addCliArgs(ArgumentContainer parser)
	{
		parser.description(DESCRIPTION);
		parser.addArgument("-v", "--vertex-labels").required(true).help("File where the alphabet of vertex labels will be stored");
		parser.addArgument("-e", "--edge-labels").required(true).help("File where the alphabet of edge labels will be stored");
		parser.addArgument("data").nargs("+").help("Data files from which the alphabet is extracted. Each one can contain multiple files");
	}

	public static void execute(Namespace args)
	{
		try
		{
			// Creating output files
			PrintWriter vertexLabelsOut = new PrintWriter(new FileOutputStream(args.getString("vertex_labels")), true);
			PrintWriter edgeLabelsOut = new PrintWriter(new FileOutputStream(args.getString("edge_labels")), true);

			// Computing alphabet
			Set<String> vertexLabels = new HashSet<>();
			Set<String> edgeLabels = new HashSet<>();
			for (String dataFile : args.<String>getList("data"))
			{
				System.out.printf("Loading graphs from %s...\n", dataFile);
				Stream<Graph> graphs = SingleFileTextGraphCollectionLoader.loadGraphsFromFile(dataFile);
				graphs.forEach(g -> {
					g.getVertices().forEach(vertex -> vertexLabels.addAll(vertex.getLabels().keySet()));
					g.getEdges().forEach(edge -> edgeLabels.add(edge.getLabel()));
				});
			}
			System.out.printf("Alphabet is composed of %d vertex labels and %d edge labels\n", vertexLabels.size(), edgeLabels.size());

			// Saving alphabet
			vertexLabels.stream()
					.sorted()
					.forEach(vertexLabelsOut::println);
			edgeLabels.stream()
					.sorted()
					.forEach(edgeLabelsOut::println);
			vertexLabelsOut.close();
			edgeLabelsOut.close();
		} catch (IOException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(Alphabet.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));

		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
