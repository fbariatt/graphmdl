package mains.classificationBaselines;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import graph.loaders.SingleFileTextGraphCollectionLoader;
import graphMDL.classification.ClassificationUtils;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentGroup;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import utils.ConfusionMatrix;
import utils.GeneralUtils;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class AlphabetClassificationBaseline
{
	public final static String DESCRIPTION = "Perform a classification in which each element is classified based on its label alphabet";

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		ArgumentGroup classInformation = parser.addArgumentGroup("Class information").description("Files required for each class. Can be repeated as much as there are classes");
		classInformation.addArgument("--train").required(true).action(Arguments.append()).help("File containing the graphs that will be used to compute this class' alphabet");
		classInformation.addArgument("--test").required(true).action(Arguments.append()).help("File containing the test set of this class");
		classInformation.addArgument("-n", "--name").required(false).action(Arguments.append()).metavar("NAME").dest("class_names").help("Name of this class (used only for display)");
	}

	public static void execute(Namespace args)
	{
		System.out.printf("Executing alphabet baseline classification with arguments %s\n", args);

		try
		{
			List<String> trainFiles = args.getList("train");
			List<String> testFiles = args.getList("test");
			List<String> classNames = args.getList("class_names");
			if (trainFiles.size() != testFiles.size())
			{
				System.err.println("Error: number of train files and number of test files do not match!");
				System.err.println(trainFiles.size() + " train files and " + testFiles.size() + " test files");
				System.exit(1);
			}
			final int nbClasses = trainFiles.size();
			if ((classNames != null) && (classNames.size() != nbClasses))
			{
				System.err.println("Error: if class names are specified, there must be as much names as there are classes.");
				System.err.println(String.format("Got names %s for %d classes", classNames, nbClasses));
				System.exit(1);
			}

			// Loading alphabets
			List<Set<String>> vertexAlphabets = new ArrayList<>(nbClasses);
			List<Set<String>> edgeAlphabets = new ArrayList<>(nbClasses);
			List<Integer> classSizes = new ArrayList<>(nbClasses);
			for (int classId = 0; classId < nbClasses; ++classId)
			{
				Set<String> vertexAlphabet = new HashSet<>();
				Set<String> edgeAlphabet = new HashSet<>();
				AtomicInteger classTrainSize = new AtomicInteger();
				SingleFileTextGraphCollectionLoader.loadGraphsFromFile(trainFiles.get(classId)).forEach(graph -> {
					graph.getVertices().flatMap(vertex -> vertex.getLabels().keySet().stream())
							.forEach(vertexAlphabet::add);
					graph.getEdges().map(Edge::getLabel)
							.forEach(edgeAlphabet::add);
					classTrainSize.incrementAndGet();
				});
				System.out.printf("Class %s has an alphabet of %d vertex labels and %d edge labels, extracted from a train set of %d graphs\n", ClassificationUtils.getDisplayNameForClass(classNames, classId), vertexAlphabet.size(), edgeAlphabet.size(), classTrainSize.intValue());
				vertexAlphabets.add(vertexAlphabet);
				edgeAlphabets.add(edgeAlphabet);
				classSizes.add(classTrainSize.intValue());
			}

			// Filtering alphabets
			List<Set<String>> exclusiveVertexAlphabets = new ArrayList<>(nbClasses);
			List<Set<String>> exclusiveEdgeAlphabets = new ArrayList<>(nbClasses);
			for (int classId = 0; classId < nbClasses; ++classId)
			{
				Set<String> exclusiveVertexAlphabet = new HashSet<>(vertexAlphabets.get(classId));
				Set<String> exclusiveEdgeAlphabet = new HashSet<>(edgeAlphabets.get(classId));
				for (int otherClass = 0; otherClass < nbClasses; ++otherClass)
				{
					if (otherClass == classId)
						continue;
					exclusiveVertexAlphabet.removeAll(vertexAlphabets.get(otherClass));
					exclusiveEdgeAlphabet.removeAll(edgeAlphabets.get(otherClass));
				}
				System.out.printf("Class %s has %d unique vertex labels and %d unique edge labels\n", ClassificationUtils.getDisplayNameForClass(classNames, classId), exclusiveVertexAlphabet.size(), exclusiveEdgeAlphabet.size());
				exclusiveVertexAlphabets.add(exclusiveVertexAlphabet);
				exclusiveEdgeAlphabets.add(exclusiveEdgeAlphabet);
			}

			// Choosing default class: the largest class
			// Finding the largest class
			int largestClass = -1;
			long largestClassSize = -1;
			for (int classId = 0; classId < nbClasses; ++classId)
			{
				int classSize = classSizes.get(classId);
				if (classSize >= largestClassSize) // If multiple classes have the same size, we choose the last of them
				{
					largestClassSize = classSize;
					largestClass = classId;
				}
			}
			System.out.printf("The largest class is class %s. It will be used as a default choice\n", ClassificationUtils.getDisplayNameForClass(classNames, largestClass));

			// Performing classification
			ConfusionMatrix confusionMatrix = new ConfusionMatrix();
			for (int classId = 0; classId < nbClasses; ++classId)
			{
				Iterator<Graph> testSet = SingleFileTextGraphCollectionLoader.loadGraphsFromFile(testFiles.get(classId)).iterator();
				while (testSet.hasNext())
				{
					Graph toClassify = testSet.next();
					int classification = classify(toClassify, exclusiveVertexAlphabets, exclusiveEdgeAlphabets).orElse(largestClass);
					confusionMatrix.increaseValue(ClassificationUtils.getDisplayNameForClass(classNames, classId), ClassificationUtils.getDisplayNameForClass(classNames, classification));
				}
				System.out.printf("Performed classification on class %s test set\n", ClassificationUtils.getDisplayNameForClass(classNames, classId));
			}

			System.out.println(confusionMatrix);
			System.out.println(confusionMatrix.printLabelPrecRecFm());
			System.out.printf("Avg precision: %f, avg recall: %f\n", confusionMatrix.getAvgPrecision(), confusionMatrix.getAvgRecall());
			System.out.println(confusionMatrix.printNiceResults());

		} catch (IOException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Classify a graph g if it can be classified based on each class' exclusive labels.
	 *
	 * @return An optional with the class if it can be decided, otherwise an empty optional.
	 */
	private static Optional<Integer> classify(Graph g, List<Set<String>> exclusiveVertexLabels, List<Set<String>> exclusiveEdgeLabels)
	{
		// Can vertex labels surely determine which class it is?
		Iterator<Vertex> vertexIterator = g.getVertices().iterator();
		while (vertexIterator.hasNext())
		{
			Vertex v = vertexIterator.next();
			for (String label : v.getLabels().keySet())
			{
				for (int classId = 0; classId < exclusiveVertexLabels.size(); ++classId)
				{
					if (exclusiveVertexLabels.get(classId).contains(label))
						return Optional.of(classId);
				}
			}
		}

		// Can edge labels surely determine which class it is?
		Iterator<Edge> edgeIterator = g.getEdges().iterator();
		while (edgeIterator.hasNext())
		{
			String label = edgeIterator.next().getLabel();
			for (int classId = 0; classId < exclusiveEdgeLabels.size(); ++classId)
			{
				if (exclusiveEdgeLabels.get(classId).contains(label))
					return Optional.of(classId);
			}
		}

		// Otherwise, we can not choose
		return Optional.empty();
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(AlphabetClassificationBaseline.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));

		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
