package mains.classificationBaselines;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparsers;
import utils.GeneralUtils;

public class ClassificationBaselines
{
	public final static String DESCRIPTION = "Baseline classification methods.";

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		Subparsers subparsers = parser.addSubparsers().dest("baseline").metavar("METHOD").help("Baseline method");
		LargestClassClassificationBaseline.addCliArgs(subparsers.addParser("largest").aliases("large", "biggest", "big").help(LargestClassClassificationBaseline.DESCRIPTION));
		AlphabetClassificationBaseline.addCliArgs(subparsers.addParser("alphabet").help(AlphabetClassificationBaseline.DESCRIPTION));
		RandomClassificationBaseline.addCliArgs(subparsers.addParser("random").help(RandomClassificationBaseline.DESCRIPTION));
	}

	public static void execute(Namespace args)
	{
		switch (args.getString("baseline"))
		{
			case "largest":
				LargestClassClassificationBaseline.execute(args);
				break;
			case "alphabet":
				AlphabetClassificationBaseline.execute(args);
				break;
			case "random":
				RandomClassificationBaseline.execute(args);
				break;
		}
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(ClassificationBaselines.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));

		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
