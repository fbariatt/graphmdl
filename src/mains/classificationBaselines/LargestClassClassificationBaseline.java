package mains.classificationBaselines;

import graph.Graph;
import graph.loaders.SingleFileTextGraphCollectionLoader;
import graphMDL.classification.ClassificationUtils;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentGroup;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import utils.ConfusionMatrix;
import utils.GeneralUtils;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class LargestClassClassificationBaseline
{
	public final static String DESCRIPTION = "Perform a classification in which each element is classified in the class with the most train examples";

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		ArgumentGroup classInformation = parser.addArgumentGroup("Class information").description("Information required for each class. Can be repeated as much as there are classes");
		classInformation.addArgument("--train").required(true).action(Arguments.append()).help("File containing the training set of this class");
		classInformation.addArgument("--test").required(true).action(Arguments.append()).help("File containing the test set of this class");
		classInformation.addArgument("-n", "--name").required(false).action(Arguments.append()).metavar("NAME").dest("class_names").help("Name of this class (used only for display)");
	}

	public static void execute(Namespace args)
	{
		System.out.printf("Executing largest class baseline classification with arguments %s\n", args);

		try
		{
			List<String> trainFiles = args.getList("train");
			List<String> testFiles = args.getList("test");
			List<String> classNames = args.getList("class_names");
			if (trainFiles.size() != testFiles.size())
			{
				System.err.println("Error: number of train files and number of test files do not match!");
				System.err.println(trainFiles.size() + " train files and " + testFiles.size() + " test files");
				System.exit(1);
			}
			final int nbClasses = trainFiles.size();
			if ((classNames != null) && (classNames.size() != nbClasses))
			{
				System.err.println("Error: if class names are specified, there must be as much names as there are classes.");
				System.err.println(String.format("Got names %s for %d classes", classNames, nbClasses));
				System.exit(1);
			}

			// Finding the largest class
			int largestClass = -1;
			long largestClassSize = -1;
			for (int classId = 0; classId < nbClasses; ++classId)
			{
				long classSize = SingleFileTextGraphCollectionLoader.loadGraphsFromFile(trainFiles.get(classId)).count();
				System.out.printf("Class %s has size %d\n", ClassificationUtils.getDisplayNameForClass(classNames, classId), classSize);
				if (classSize >= largestClassSize) // If multiple classes have the same size, we choose the last of them
				{
					largestClassSize = classSize;
					largestClass = classId;
				}
			}
			System.out.printf("The largest class is class %s. All graphs will be classified as this class\n", ClassificationUtils.getDisplayNameForClass(classNames, largestClass));

			// Performing classification
			ConfusionMatrix confusionMatrix = new ConfusionMatrix();
			for (int classId = 0; classId < nbClasses; ++classId)
			{
				Iterator<Graph> testSet = SingleFileTextGraphCollectionLoader.loadGraphsFromFile(testFiles.get(classId)).iterator();
				while (testSet.hasNext())
				{
					testSet.next();
					confusionMatrix.increaseValue(ClassificationUtils.getDisplayNameForClass(classNames, classId), ClassificationUtils.getDisplayNameForClass(classNames, largestClass));
				}
				System.out.printf("Performed classification on class %s test set\n", ClassificationUtils.getDisplayNameForClass(classNames, classId));
			}

			System.out.println(confusionMatrix);
			System.out.println(confusionMatrix.printLabelPrecRecFm());
			System.out.printf("Avg precision: %f, avg recall: %f\n", confusionMatrix.getAvgPrecision(), confusionMatrix.getAvgRecall());
			System.out.println(confusionMatrix.printNiceResults());
		} catch (IOException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(LargestClassClassificationBaseline.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));

		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
