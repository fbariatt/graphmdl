package mains.classificationBaselines;

import graph.loaders.SingleFileTextGraphCollectionLoader;
import graphMDL.classification.ClassificationUtils;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import utils.ConfusionMatrix;
import utils.GeneralUtils;

import java.io.IOException;
import java.util.List;
import java.util.Random;

public class RandomClassificationBaseline
{
	public final static String DESCRIPTION = "Perform a classification in which each element is classified randomly";

	public static void addCliArgs(ArgumentContainer parser)
	{
		parser.description(DESCRIPTION);

		parser.addArgument("testset").nargs("+").help("File containing the test set of a class. Repeated as many times as there are classes");
		parser.addArgument("-n", "--name").required(false).action(Arguments.append()).metavar("NAME").dest("class_names").help("Class names (used only for display). Can be repeated as many times as there are classes");
		parser.addArgument("--seed").type(Long.class).help("Set the seed for the random number generator");
	}

	public static void execute(Namespace args)
	{
		System.out.printf("Executing classification random baseline with arguments %s\n", args);

		try
		{
			List<String> testFiles = args.getList("testset");
			List<String> classNames = args.getList("class_names");
			final int nbClasses = testFiles.size();
			if ((classNames != null) && (classNames.size() != nbClasses))
			{
				System.err.println("Error: if class names are specified, there must be as much names as there are classes.");
				System.err.println(String.format("Got names %s for %d classes", classNames, nbClasses));
				System.exit(1);
			}
			System.out.printf("Performing %d-way classification\n", nbClasses);

			// Initialising random number generator
			Random rng = new Random();
			Long seed = args.getLong("seed");
			if (seed == null)
				seed = rng.nextLong();
			System.out.printf("Random number generator seed: %d\n", seed);
			rng.setSeed(seed);

			// Performing classification
			ConfusionMatrix confusionMatrix = new ConfusionMatrix();
			for (int classId = 0; classId < nbClasses; ++classId)
			{
				final long setSize = SingleFileTextGraphCollectionLoader.loadGraphsFromFile(testFiles.get(classId)).count();
				for (int i = 0; i < setSize; ++i)
					confusionMatrix.increaseValue(ClassificationUtils.getDisplayNameForClass(classNames, classId), ClassificationUtils.getDisplayNameForClass(classNames, rng.nextInt(nbClasses)));
				System.out.printf("Performed classification on class %s test set\n", ClassificationUtils.getDisplayNameForClass(classNames, classId));
			}

			System.out.println(confusionMatrix);
			System.out.println(confusionMatrix.printLabelPrecRecFm());
			System.out.printf("Avg precision: %f, avg recall: %f\n", confusionMatrix.getAvgPrecision(), confusionMatrix.getAvgRecall());
			System.out.println(confusionMatrix.printNiceResults());
		} catch (IOException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(RandomClassificationBaseline.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));

		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
