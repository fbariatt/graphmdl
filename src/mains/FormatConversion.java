package mains;

import graph.Graph;
import graph.GraphFactory;
import graph.loaders.FileSystemTextGraphLoader;
import graph.loaders.RDFToGraph;
import graph.printers.GraphToRDF;
import graph.printers.GraphToText;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.impl.type.FileArgumentType;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import utils.MapUtils;
import utils.RDFUtils;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public final class FormatConversion
{
	public static final String DESCRIPTION = "Convert a data graph between different formats. Note that the graph structure is modified when converting between text and RDF, following KG-MDL data modeling (e.g. rdf:type <-> vertex labels).";

	private static final Map<String, String> formatOptions = makeFormatOptions();

	private static Map<String, String> makeFormatOptions()
	{
		Map<String, String> options = new HashMap<>(RDFUtils.commandLineRDFFormats);
		options.put("txt", "txt");
		return options;
	}

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		parser.addArgument("-i", "--in").required(true).choices(formatOptions.keySet()).help("Input file format.");
		parser.addArgument("-o", "--out").required(true).choices(formatOptions.keySet()).help("Output file format.");
		parser.addArgument("in_file").type(new FileArgumentType().verifyCanRead()).help("Input file path. If in text format, it is assumed that it contains a single graph.");
		parser.addArgument("out_file").type(new FileArgumentType().verifyCanCreate()).help("Output file path. May be the same as input file.");
		parser.addArgument("--np", "--no-prefixes").dest("use_prefixes").action(Arguments.storeFalse()).help("Do not use RDf prefixes in the output (only if output format is RDF and supports prefixes, e.g. turtle).");
	}

	public static void execute(Namespace args)
	{
		System.out.printf("Executing with arguments %s\n", args);
		final String inputFormat = formatOptions.get(args.getString("in"));
		final String outputFormat = formatOptions.get(args.getString("out"));
		final Path inputFilePath = Paths.get(args.getString("in_file")).toAbsolutePath();
		final Path outputFilePath = Paths.get(args.getString("out_file")).toAbsolutePath();
		final boolean usePrefixes = args.getBoolean("use_prefixes");

		System.out.printf("Converting %s (%s) to %s (%s)\n", inputFilePath, inputFormat, outputFilePath, outputFormat);
		if ((inputFormat.equals("txt") || outputFormat.equals("txt")) && (!inputFormat.equals(outputFormat)))
		{
			System.out.println("Note: when converting between text format and RDF the graph structure is modified according to the KG-MDL data model.");
		}

		// We open the output file as soon as possible, which resets it to empty if it exists.
		// This way if there is an error during processing the output will be empty.
		// Of course, we do not do that if the output is the same as the input, as it would clear the file.
		Optional<FileOutputStream> outputStream = Optional.empty();
		if (!inputFilePath.equals(outputFilePath))
		{
			outputStream = Optional.of(openOutputFile(outputFilePath));
		}

		System.out.println("Loading graph from input file...");
		Graph g;
		Map<String, String> inputPrefixes = Collections.emptyMap(); // The RDF prefixes of the input, we leave it empty if the input is not in RDF
		if (inputFormat.equals("txt"))
		{
			g = FileSystemTextGraphLoader.loadGraphFromFile(inputFilePath.toFile());
		}
		else
		{
			Model rdf = ModelFactory.createDefaultModel();
			rdf.read(openInputFile(inputFilePath), null, inputFormat);
			inputPrefixes = rdf.getNsPrefixMap();
			g = RDFToGraph.fromRDFModel(rdf, GraphFactory::createDefaultGraph);
			rdf.close();
		}
		System.out.printf("Loaded graph has %d vertices and %d edges\n", g.getVertexCount(), g.getEdgeCount());

		System.out.println("Writing graph to output file...");
		if (outputStream.isEmpty())
			outputStream = Optional.of(openOutputFile(outputFilePath));
		if (outputFormat.equals("txt"))
		{
			GraphToText.print(g, new PrintWriter(outputStream.get()), true);
		}
		else
		{
			Map<String, String> outputPrefixes = usePrefixes ? MapUtils.union(GraphToRDF.defaultPrefixes, inputPrefixes) : Collections.emptyMap();
			Model rdf = new GraphToRDF.GraphToModel(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel).getModel();
			rdf.setNsPrefixes(outputPrefixes);
			rdf.write(outputStream.get(), outputFormat);
		}
		try
		{
			outputStream.get().flush();
			outputStream.get().close();
		} catch (IOException e)
		{
			System.err.printf("Error when finalizing writing to output: %s\n", e);
			System.exit(1);
		}
	}

	/**
	 * Open the given path for writing.
	 * This empties the file it is exists.
	 * Exits program on errors.
	 */
	private static FileOutputStream openOutputFile(Path outputFilePath)
	{
		try
		{
			return new FileOutputStream(outputFilePath.toFile());
		} catch (FileNotFoundException e)
		{
			System.err.printf("Error when trying to create output file: %s\n", e);
			System.exit(1);
			return null; // Just so that it compiles, never reached because of exit
		}
	}

	/**
	 * Open the given path for reading.
	 * Exits program on errors.
	 */
	private static FileInputStream openInputFile(Path inputFilePath)
	{
		try
		{
			return new FileInputStream(inputFilePath.toFile());
		} catch (FileNotFoundException e)
		{
			System.err.printf("Error when trying to open input file: %s\n", e);
			System.exit(1);
			return null; // Just so that it compiles, never reached because of exit
		}
	}
}
