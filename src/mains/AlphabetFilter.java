package mains;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import graph.loaders.SingleFileTextGraphCollectionLoader;
import graph.printers.GraphToText;
import graphMDL.classification.ClassificationUtils;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import utils.GeneralUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class AlphabetFilter
{
	public final static String DESCRIPTION = "Load a list of graphs and only output the ones that belong to a certain alphabet";

	public static void addCliArgs(ArgumentContainer parser)
	{
		parser.description(DESCRIPTION);

		parser.addArgument("-o", "--output").help("File where to output the graphs passing the filter");
		parser.addArgument("-v", "--vertex-alphabet").help("File containing the alphabet for vertex labels");
		parser.addArgument("-e", "--edge-alphabet").help("File containing the alphabet for edge labels");
		parser.addArgument("graphs").metavar("graph").nargs("+").help("File(s) containing the graphs to filter. Multiple graphs per file are allowed.");
	}

	public static void execute(Namespace args)
	{
		try
		{
			String vertexAlphabetPath = args.getString("vertex_alphabet");
			String edgeAlphabetPath = args.getString("edge_alphabet");
			if ((vertexAlphabetPath == null) && (edgeAlphabetPath == null))
			{
				System.err.println("At least one alphabet (vertex or edge labels) must be specified");
				System.exit(1);
			}

			// Loading vertex alphabet and using it to create vertex validity predicate
			Predicate<String> vertexLabelIsValid;
			if (vertexAlphabetPath == null) // If no vertex label alphabet is specified
			{
				vertexLabelIsValid = label -> true; // We accept any label
			}
			else
			{
				Set<String> vertexAlphabet = ClassificationUtils.loadAlphabet(vertexAlphabetPath);
				System.out.printf("Loaded %d labels in vertex alphabet\n", vertexAlphabet.size());
				vertexLabelIsValid = vertexAlphabet::contains;
			}
			Predicate<Vertex> vertexIsValid = vertex -> vertex.getLabels().keySet().stream().allMatch(vertexLabelIsValid);

			// Loading edge alphabet and using it to create edge validity predicate
			Predicate<String> edgeLabelIsValid;
			if (edgeAlphabetPath == null) // If no edge label alphabet is specified
			{
				edgeLabelIsValid = label -> true; // We accept any label
			}
			else
			{
				Set<String> edgeAlphabet = ClassificationUtils.loadAlphabet(edgeAlphabetPath);
				System.out.printf("Loaded %d labels in edge alphabet\n", edgeAlphabet.size());
				edgeLabelIsValid = edgeAlphabet::contains;
			}
			Predicate<Edge> edgeIsValid = edge -> edgeLabelIsValid.test(edge.getLabel());

			// Loading all graphs and saving valid graphs to output
			PrintWriter out;
			if (args.getString("output") == null)
				out = new PrintWriter(System.out, true);
			else
				out = new PrintWriter(new FileOutputStream(args.getString("output")));

			args.<String>getList("graphs").stream()
					.flatMap(AlphabetFilter::loadGraphsFromFile)
					.filter(graph -> graph.getVertices().allMatch(vertexIsValid) && graph.getEdges().allMatch(edgeIsValid))
					.forEachOrdered(graph -> GraphToText.print(graph, out, false));

			System.out.println("Done.");
		} catch (IOException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	private static Stream<Graph> loadGraphsFromFile(String filePath)
	{
		try
		{
			return SingleFileTextGraphCollectionLoader.loadGraphsFromFile(filePath);
		} catch (IOException e)
		{
			System.err.println("Error when loading graphs from file " + filePath);
			e.printStackTrace();
			System.exit(1);
			return Stream.empty(); // Useless since we exit, but necessary for compilation
		}
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(AlphabetFilter.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));

		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
