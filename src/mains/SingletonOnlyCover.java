package mains;

import graph.Graph;
import graph.loaders.FileSystemTextGraphLoader;
import graphMDL.CodeTable;
import graphMDL.CodeTableHeuristics;
import graphMDL.ScanDataCoverStrategy;
import graphMDL.StandardTable;
import graphMDL.jsonSerializer.GraphMDLSerializer;
import graphMDL.jsonSerializer.multigraphs.MultiDirectedGraphMDLSerializer;
import graphMDL.jsonSerializer.simpleGraphs.directed.SimpleDirectedGraphMDLSerializer;
import graphMDL.jsonSerializer.simpleGraphs.undirected.SimpleUndirectedGraphMDLSerializer;
import graphMDL.multigraphs.MultiDirectedCodeTable;
import graphMDL.multigraphs.MultiDirectedStandardTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedCodeTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedIndependentVertexAndEdgeST;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedCodeTable;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedIndependentVertexAndEdgeST;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import utils.GeneralUtils;
import utils.GraphType;

import java.io.FileWriter;
import java.io.IOException;
import java.util.EnumSet;

public class SingletonOnlyCover
{
	public static final String DESCRIPTION = "Compute the cover of a data graph using CT0, the simplest code table composed of singletons only.";


	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		GraphType.addCliArguments(parser);
		parser.addArgument("data").help("The data graph file path (must be in text format and have doubled edges if undirected).");
		parser.addArgument("output").help("File where the cover result will be saved (json format).");
	}

	public static void execute(Namespace args)
	{
		System.out.printf("Computing CT0 cover with arguments %s\n", args);
		final EnumSet<GraphType> graphType = GraphType.fromCliArguments(args);
		System.out.printf("The graph is considered %s\n", graphType.contains(GraphType.DIRECTED) ? "directed" : "undirected");
		System.out.printf("Using %s encoding\n", graphType.contains(GraphType.MULTIGRAPH) ? "multigraph" : "simple graph (non-multigraph)");

		Graph data = FileSystemTextGraphLoader.loadGraphFromFile(args.getString("data"));

		StandardTable st;
		if (graphType.equals(GraphType.SIMPLE_DIRECTED))
			st = new SimpleDirectedIndependentVertexAndEdgeST(data);
		else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
			st = new SimpleUndirectedIndependentVertexAndEdgeST(data);
		else if (graphType.equals(GraphType.MULTI_DIRECTED))
			st = new MultiDirectedStandardTable(data);
		else // Multi undirected
		{
			System.err.println("Undirected multigraphs are not supported for now");
			System.exit(1);
			st = null;
		}
		System.out.printf("Vertex standard code table contains %d labels\n", st.getVertexLabelsCodeLengths().size());
		System.out.printf("Edge standard code table contains %d labels\n", st.getEdgeLabelsCodeLengths().size());

		CodeTable codeTable;
		if (graphType.equals(GraphType.SIMPLE_DIRECTED))
			codeTable = new SimpleDirectedCodeTable((SimpleDirectedIndependentVertexAndEdgeST) st, CodeTableHeuristics.PartialOrder.noOrder);
		else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
			codeTable = new SimpleUndirectedCodeTable((SimpleUndirectedIndependentVertexAndEdgeST) st, CodeTableHeuristics.PartialOrder.noOrder);
		else if (graphType.equals(GraphType.MULTI_DIRECTED))
			codeTable = new MultiDirectedCodeTable((MultiDirectedStandardTable) st, CodeTableHeuristics.PartialOrder.noOrder);
		else // Multi undirected
			return;

		codeTable.apply(data, new ScanDataCoverStrategy(0), false);

		final String outputFile = args.getString("output");
		System.out.printf("Writing output to %s\n", outputFile);
		try (FileWriter out = new FileWriter(outputFile))
		{
			GraphMDLSerializer serializer;
			if (graphType.equals(GraphType.SIMPLE_DIRECTED))
				serializer = new SimpleDirectedGraphMDLSerializer((SimpleDirectedCodeTable) codeTable, data);
			else if (graphType.equals(GraphType.SIMPLE_UNDIRECTED))
				serializer = new SimpleUndirectedGraphMDLSerializer((SimpleUndirectedCodeTable) codeTable, data);
			else if (graphType.equals(GraphType.MULTI_DIRECTED))
				serializer = new MultiDirectedGraphMDLSerializer((MultiDirectedCodeTable) codeTable, data);
			else // Multi undirected
				return;

			serializer
					.getJSONObject()
					.write(out, 2, 0);
			out.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(SingletonOnlyCover.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));

		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
