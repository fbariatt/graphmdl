package mains;

import graph.Graph;
import graph.Vertex;
import graph.WeaklyConnectedComponentSearch;
import graph.loaders.FileSystemTextGraphLoader;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.GeneralUtils;

import java.util.List;
import java.util.Set;

public class ConnectedComponents
{
	public static final String DESCRIPTION = "Compute the Weakly Connected Components of a graph.";

	public static void addCliArgs(ArgumentParser parser)
	{
		parser.description(DESCRIPTION);

		parser.addArgument("graph").type(Arguments.fileType().verifyCanRead()).help("The graph to process, in text format");
	}

	public static void execute(Namespace args)
	{
		String graphPath = args.getString("graph");
		System.err.printf("Loading graph from %s...\n", graphPath);
		Graph g = FileSystemTextGraphLoader.loadGraphFromFile(graphPath);
		System.err.printf("Loaded graph with %d vertices and %d edges, searching WCC...\n", g.getVertexCount(), g.getEdgeCount());

		List<Set<Vertex>> components = WeaklyConnectedComponentSearch.computeWCCs(g);

		System.err.printf("Writing %d connected components information to stdout...\n", components.size());
		JSONArray output = new JSONArray();
		for (int i = 0; i < components.size(); ++i)
		{
			JSONObject componentSerialization = new JSONObject();
			componentSerialization.put("name", String.format("Component %d", i + 1));
			JSONArray componentVertices = new JSONArray();
			components.get(i).stream()
					.mapToInt(v -> g.getVertexIndexMap().get(v))
					.sorted()
					.forEach(componentVertices::put);
			componentSerialization.put("vertices", componentVertices);
			output.put(componentSerialization);
		}
		System.out.println(output.toString(2));
	}

	public static void main(String... args)
	{
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor(ConnectedComponents.class.getName()).build();
		addCliArgs(argParser);
		try
		{
			execute(argParser.parseArgs(args));
		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
