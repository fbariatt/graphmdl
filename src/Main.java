import mains.*;
import mains.classificationBaselines.ClassificationBaselines;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparsers;
import utils.GeneralUtils;

public class Main
{
	public static void main(String[] args)
	{
		// Log4j configuration
		GeneralUtils.configureLogging();

		ArgumentParser argParser = ArgumentParsers.newFor("GraphMDL").singleMetavar(true).build();
		Subparsers subparsers = argParser.addSubparsers().dest("command").title("Commands").metavar("COMMAND").help("Choose which command to execute");
		// Subparsers
		FormatConversion.addCliArgs(subparsers.addParser("convert").aliases("format").help(FormatConversion.DESCRIPTION));
		AggregateCollection.addCliArgs(subparsers.addParser("aggregate").help(AggregateCollection.DESCRIPTION));
		SimpleKrimp.addCliArgs(subparsers.addParser("graphMDL").aliases("krimp").help(SimpleKrimp.DESCRIPTION));
		Alphabet.addCliArgs(subparsers.addParser("alphabet").help(Alphabet.DESCRIPTION));
		AlphabetFilter.addCliArgs(subparsers.addParser("alphabetFilter").help(AlphabetFilter.DESCRIPTION));
		SimpleMultiClassClassification.addCliArgs(subparsers.addParser("classify").aliases("multiclass").help(SimpleMultiClassClassification.DESCRIPTION));
		ClassificationBaselines.addCliArgs(subparsers.addParser("classificationBaselines").aliases("baselines").help(ClassificationBaselines.DESCRIPTION));
		AnytimeGraphMDL.addCliArgs(subparsers.addParser("graphMDL+").aliases("anytime").help(AnytimeGraphMDL.DESCRIPTION));
		subparsers.addParser("KG-MDL").help("GraphMDL+ for knowledge graphs");
		Cover.addCliArgs(subparsers.addParser("cover").help(Cover.DESCRIPTION));
		SingletonOnlyCover.addCliArgs(subparsers.addParser("ct0").aliases("singletons").help(SingletonOnlyCover.DESCRIPTION));
		PrintEmbeddings.addCliArgs(subparsers.addParser("embeddings").help(PrintEmbeddings.DESCRIPTION));
		PatternsToRDF.addCliArgs(subparsers.addParser("rdfPatterns").help(PatternsToRDF.DESCRIPTION));
		ListNeighbourhood.addCliArgs(subparsers.addParser("neighbourList").aliases("neighborList").help(ListNeighbourhood.DESCRIPTION));
		ConnectedComponents.addCliArgs(subparsers.addParser("connectedComponents").aliases("WCC").help(ConnectedComponents.DESCRIPTION));
		LabelsCoveredByPatternSize.addCliArgs(subparsers.addParser("coveredLabels").help(LabelsCoveredByPatternSize.DESCRIPTION));

		try
		{
			Namespace ARGS = argParser.parseArgs(args);
			switch (ARGS.getString("command"))
			{
				case "convert":
					FormatConversion.execute(ARGS);
					break;
				case "aggregate":
					AggregateCollection.execute(ARGS);
					break;
				case "graphMDL":
					SimpleKrimp.execute(ARGS);
					break;
				case "alphabet":
					Alphabet.execute(ARGS);
					break;
				case "alphabetFilter":
					AlphabetFilter.execute(ARGS);
					break;
				case "classify":
					SimpleMultiClassClassification.execute(ARGS);
					break;
				case "classificationBaselines":
					ClassificationBaselines.execute(ARGS);
					break;
				case "graphMDL+":
					AnytimeGraphMDL.execute(ARGS);
					break;
				case "KG-MDL":
					System.err.println("Use graphMDL+ command instead, and select multigraphs option: KG-MDL encodings are automatically used in that case.");
					System.exit(1);
					break;
				case "cover":
					Cover.execute(ARGS);
					break;
				case "ct0":
					SingletonOnlyCover.execute(ARGS);
					break;
				case "embeddings":
					PrintEmbeddings.execute(ARGS);
					break;
				case "rdfPatterns":
					PatternsToRDF.execute(ARGS);
					break;
				case "neighbourList":
					ListNeighbourhood.execute(ARGS);
					break;
				case "connectedComponents":
					ConnectedComponents.execute(ARGS);
					break;
				case "coveredLabels":
					LabelsCoveredByPatternSize.execute(ARGS);
					break;
			}

		} catch (ArgumentParserException e)
		{
			argParser.handleError(e);
			System.exit(1);
		}
	}
}
