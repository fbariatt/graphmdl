package graph;

/**
 * Graph class that verifies that the graph is a simple graph, i.e. it contains no self-loops and is not a multigraph.
 */
public class SimpleGraphImpl extends GraphImpl
{
	private static IllegalArgumentException createSelfLoopException()
	{
		return new IllegalArgumentException("Self-loops are not allowed in a simple graph!");
	}

	private static IllegalArgumentException createMultigraphException()
	{
		return new IllegalArgumentException("More than one edge between two vertices is not allowed in a simple graph!");
	}

	@Override
	public Edge addEdge(int source, int target, String label)
	{
		if (source == target)
			throw createSelfLoopException();
		if (this.getEdges(source, target).findAny().isPresent()) // If there's already an edge between the source and the target
			throw createMultigraphException();

		return super.addEdge(source, target, label);
	}

	@Override
	public Edge addEdge(Vertex source, Vertex target, String label)
	{
		if (source == target)
			throw createSelfLoopException();
		if (this.getEdges(source, target).findAny().isPresent()) // If there's already an edge between the source and the target
			throw createMultigraphException();

		return super.addEdge(source, target, label);
	}

	@Override
	public boolean isEffectivelySimple()
	{
		// This graph will *always* be a simple graph, so no need to check
		return true;
	}
}
