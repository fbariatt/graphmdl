package graph;

public class GraphSerializationException extends RuntimeException
{
	public GraphSerializationException()
	{
	}

	public GraphSerializationException(String s)
	{
		super(s);
	}

	public GraphSerializationException(String s, Throwable throwable)
	{
		super(s, throwable);
	}

	public GraphSerializationException(Throwable throwable)
	{
		super(throwable);
	}
}
