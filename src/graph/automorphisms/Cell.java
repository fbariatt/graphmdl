package graph.automorphisms;

import graph.automorphisms.vertexDescriptions.VertexDescription;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Represent a cell, element composed of one or multiple vertex indices.
 * This is a base block of the automorphism search algorithm.
 */
public class Cell
{
	private final List<Integer> elements;

	public Cell(List<Integer> elements)
	{
		if (elements.size() < 1)
			throw new IllegalArgumentException("A cell must have at least one element");
		this.elements = List.copyOf(elements);
	}

	/**
	 * @return Whether the cell is singleton, i.e. it has only one element
	 */
	public boolean isSingleton()
	{
		return elements.size() == 1;
	}

	/**
	 * Split the cell into two sub-cells: the first contains the element at splitPosition, the second contains the rest of the elements (without altering their respective order)
	 */
	public CellPair split(int splitPosition)
	{
		if (splitPosition < 0 || splitPosition >= elements.size())
			throw new IndexOutOfBoundsException("Trying to split a cell outside of its bounds. Index: " + splitPosition + ", Size: " + elements.size());

		List<Integer> firstCell = new ArrayList<>(1);
		firstCell.add(elements.get(splitPosition));

		List<Integer> secondCell = new ArrayList<>(elements.size() - 1);
		secondCell.addAll(elements.subList(0, splitPosition));
		secondCell.addAll(elements.subList(splitPosition + 1, elements.size()));

		return new CellPair(new Cell(firstCell), new Cell(secondCell));
	}

	/**
	 * "Refine" the cell into subcells.
	 * Compute the description of the elements of the cell, based on vertexDescriptions and with every vertex v mapped to mapping[v].
	 * Then, sort the descriptions and form subcells containing vertices with the same description.
	 * The cells are returned in order of their vertex description.
	 *
	 * @param vertexDescriptions The base vertex descriptions from which the vertex descriptions are derived. vertexDescription[v] must be the description of vertex with index v.
	 * @param mapping            every vertex v is replaced with mapping[v] in the vertex descriptions for computing their new values
	 */
	public List<Cell> refine(VertexDescription[] vertexDescriptions, int[] mapping)
	{
		List<Cell> result = new ArrayList<>();

		// Computing the vertex descriptions of the elements of this cell, with the given mapping
		Iterator<VertexDescription> mappedDescriptions = elements.stream()
				.map(vertex -> vertexDescriptions[vertex].mapVertices(mapping))
				.sorted()
				.iterator();

		VertexDescription currentVertexDescription = mappedDescriptions.next();
		List<Integer> currentCellElements = new ArrayList<>();
		currentCellElements.add(currentVertexDescription.getOriginalVertexNumber());
		while (mappedDescriptions.hasNext())
		{
			VertexDescription vertexDescription = mappedDescriptions.next();
			if (vertexDescription.equals(currentVertexDescription)) // The element has the same description as the previous, therefore it must stay in the same cell
			{
				currentCellElements.add(vertexDescription.getOriginalVertexNumber());
			}
			else // The element has a different description, therefore we must start a new cell
			{
				result.add(new Cell(currentCellElements));
				currentVertexDescription = vertexDescription;
				currentCellElements = new ArrayList<>();
				currentCellElements.add(vertexDescription.getOriginalVertexNumber());
			}
		}
		result.add(new Cell(currentCellElements));
		return result;
	}

	/**
	 * @return The number of elements in the cell
	 */
	public int size()
	{
		return elements.size();
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Cell cell = (Cell) o;
		return elements.equals(cell.elements);
	}

	@Override
	public String toString()
	{
		StringBuilder res = new StringBuilder();
		for (Integer element : elements)
		{
			res.append(String.format("%d ", element));
		}
		return res.toString();
	}

	public List<Integer> getElements()
	{
		return elements;
	}

	/**
	 * A pair of cells, as returned by {@link Cell#split}
	 */
	public static class CellPair
	{
		public final Cell first;
		public final Cell second;

		CellPair(Cell first, Cell second)
		{
			this.first = first;
			this.second = second;
		}
	}
}
