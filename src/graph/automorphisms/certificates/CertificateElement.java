package graph.automorphisms.certificates;

/**
 * An element of a graph certificate. E.g. an edge certificate.
 */
public interface CertificateElement extends Comparable<CertificateElement>
{
	/**
	 * Create a new certificate element based on this one, where vertex u is replaced by mapping[u]
	 */
	CertificateElement mapVertices(int[] mapping);
}
