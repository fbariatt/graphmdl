package graph.automorphisms.certificates;

import java.util.Comparator;
import java.util.Objects;

/**
 * Certificate element describing one label of a vertex
 */
public class VertexLabelCertificateElement implements CertificateElement
{
	private final String label;
	private final int vertex;

	public VertexLabelCertificateElement(String label, int vertex)
	{
		this.label = label;
		this.vertex = vertex;
	}

	@Override
	public CertificateElement mapVertices(int[] mapping)
	{
		return new VertexLabelCertificateElement(this.label, mapping[this.vertex]);
	}

	@Override
	public int compareTo(CertificateElement other)
	{
		if (other instanceof VertexLabelCertificateElement)
		{
			return Comparator
					.comparing(VertexLabelCertificateElement::getLabel)
					.thenComparingInt(VertexLabelCertificateElement::getVertex)
					.compare(this, (VertexLabelCertificateElement) other);
		}
		else if (other instanceof EdgeCertificateElement)
		{
			return -1; // A vertex certificate is always considered smaller than an edge certificate
		}
		else
		{
			throw new ClassCastException("Impossible to compare " + this.getClass() + " with " + other.getClass());
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		VertexLabelCertificateElement that = (VertexLabelCertificateElement) o;
		return vertex == that.vertex &&
				label.equals(that.label);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(label, vertex);
	}

	@Override
	public String toString()
	{
		return String.format("%s%d", label, vertex);
	}

	public String getLabel()
	{
		return label;
	}

	public int getVertex()
	{
		return vertex;
	}
}
