package graph.automorphisms.certificates;

import java.util.Comparator;
import java.util.Objects;

public class EdgeCertificateElement implements CertificateElement
{
	private final String edgeLabel;
	private final int source;
	private final int target;

	public EdgeCertificateElement(String edgeLabel, int source, int target)
	{
		this.edgeLabel = edgeLabel;
		this.source = source;
		this.target = target;
	}

	@Override
	public CertificateElement mapVertices(int[] mapping)
	{
		return new EdgeCertificateElement(this.edgeLabel, mapping[this.source], mapping[this.target]);
	}

	@Override
	public int compareTo(CertificateElement other)
	{
		if (other instanceof EdgeCertificateElement)
		{
			return Comparator
					.comparing(EdgeCertificateElement::getEdgeLabel)
					.thenComparingInt(EdgeCertificateElement::getSource)
					.thenComparingInt(EdgeCertificateElement::getTarget)
					.compare(this, (EdgeCertificateElement) other);
		}
		else if (other instanceof VertexLabelCertificateElement)
		{
			return 1; // An edge certificate is always considered greater than a vertex certificate
		}
		else
		{
			throw new ClassCastException("Impossible to compare " + this.getClass() + " with " + other.getClass());
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EdgeCertificateElement that = (EdgeCertificateElement) o;
		return source == that.source &&
				target == that.target &&
				edgeLabel.equals(that.edgeLabel);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(edgeLabel, source, target);
	}

	@Override
	public String toString()
	{
		return String.format("%s%d%d", edgeLabel, source, target);
	}

	public String getEdgeLabel()
	{
		return edgeLabel;
	}

	public int getSource()
	{
		return source;
	}

	public int getTarget()
	{
		return target;
	}
}
