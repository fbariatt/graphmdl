package graph.automorphisms.certificates;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import utils.GeneralUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A graph certificate, used in the automorphism search algorithm.
 * If two graphs have the same certificate, they are automorphic.
 * This implementation creates a certificate by describing the labels of each vertex and all edges of the graph.
 */
public class GraphCertificate implements Comparable<GraphCertificate>
{
	private final List<CertificateElement> certificateElements;
	private final int vertexCount;
	private Integer hashCode = null; // Lazy result of hashCode()
	private String stringRepresentation = null; // Lazy result of toString()

	/**
	 * @param g              The graph for which the certificate is prepared
	 * @param vertexIndexMap See {@link Graph#getVertexIndexMap()}
	 */
	public GraphCertificate(Graph g, Map<Vertex, Integer> vertexIndexMap)
	{
		List<CertificateElement> elements = new LinkedList<>();
		int vertexCount = 0;

		Iterator<Vertex> vertices = g.getVertices().iterator();
		while (vertices.hasNext())
		{
			Vertex vertex = vertices.next();
			vertexCount++;
			int vertexIndex = vertexIndexMap.get(vertex);

			for (String label : vertex.getLabels().keySet())
				elements.add(new VertexLabelCertificateElement(label, vertexIndex));

			Iterator<Edge> edges = g.getOutEdges(vertex).iterator();
			while (edges.hasNext())
			{
				Edge edge = edges.next();
				elements.add(new EdgeCertificateElement(edge.getLabel(), vertexIndex, vertexIndexMap.get(edge.getTarget())));
			}
		}

		Collections.sort(elements);
		this.certificateElements = Collections.unmodifiableList(elements);
		this.vertexCount = vertexCount;
	}

	/**
	 * See {@link #GraphCertificate(Graph, Map)}
	 */
	public GraphCertificate(Graph g)
	{
		this(g, g.getVertexIndexMap());
	}

	/**
	 * Create a new certificate using the given certificate elements.
	 * The list must be already sorted.
	 * The list is stored directly so any modification to it will modify this certificate, potentially invalidating it.
	 */
	public GraphCertificate(List<CertificateElement> certificateElements, int vertexCount)
	{
		this.certificateElements = Collections.unmodifiableList(certificateElements);
		this.vertexCount = vertexCount;
	}

	/**
	 * Create a new certificate based on this one, where each vertex u is replaced by mapping[u]
	 */
	public GraphCertificate mapVertices(int[] mapping)
	{
		if (mapping.length != vertexCount)
			throw new IllegalArgumentException("MapVertices: there should be a mapping for each vertex. Expected " + vertexCount + " got " + mapping.length);
		LinkedList<CertificateElement> mappedElements = this.certificateElements.stream()
				.map(certificateElement -> certificateElement.mapVertices(mapping))
				.collect(LinkedList::new, LinkedList::add, LinkedList::addAll);
		Collections.sort(mappedElements);
		return new GraphCertificate(mappedElements, vertexCount);
	}

	/**
	 * @return The number of vertices in the graph represented by this certificate.
	 */
	public int getVertexCount()
	{
		return vertexCount;
	}

	/**
	 * @return The elements composing this certificate.
	 */
	public List<CertificateElement> getCertificateElements()
	{
		return Collections.unmodifiableList(this.certificateElements);
	}

	@Override
	public String toString()
	{
		if (stringRepresentation == null)
		{
			stringRepresentation = Stream.concat(
					Stream.of(vertexCount + "vertices"),
					certificateElements.stream()
					// .sorted() // Not needed since the list is sorted on creation and never modified
			)
					.map(Object::toString)
					.collect(Collectors.joining(" "));
		}
		return stringRepresentation;
	}

	@Override
	public int compareTo(GraphCertificate other)
	{
		// Comparing two certificates means comparing their elements one by one
		final int lexicographicCompare = GeneralUtils.lexicographicCompare(this.certificateElements, other.certificateElements);

		if (lexicographicCompare == 0)
			// If they have the same elements we still order them by number of vertices (in case of vertices not described by vertex labels or edges)
			return this.vertexCount - other.vertexCount;
		else
			return lexicographicCompare;
	}

	@Override
	public boolean equals(Object other)
	{
		if (this == other) return true;
		if (other == null || getClass() != other.getClass()) return false;
		return this.compareTo((GraphCertificate) other) == 0;
	}

	@Override
	public int hashCode()
	{
		if (hashCode == null)
			hashCode = Objects.hash(this.vertexCount, this.certificateElements.hashCode());
		return hashCode;
	}
}
