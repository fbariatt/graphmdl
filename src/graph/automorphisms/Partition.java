package graph.automorphisms;

import graph.automorphisms.vertexDescriptions.VertexDescription;

import java.util.*;

/**
 * A partition is a list of cells.
 * A partition can have an optional parent (used for constructing a search tree of partitions).
 * This is a base block of the automorphism search algorithm.
 */
public class Partition
{
	private final List<Cell> cells;
	private final Partition parent;
	private int[] vertexToCellMapping = null; // Lazy result of getVertexToCellMapping
	private int[] cellToVertexMapping = null; // Lazy result of getCellToVertexMapping
	private List<Partition> ancestors = null; // Lazy result of getAncestors

	/**
	 * A partition with a single cell
	 */
	public Partition(Cell cell, Partition parent)
	{
		this.cells = List.of(cell);
		this.parent = parent;
	}

	/**
	 * A partition with multiple cells
	 */
	public Partition(List<Cell> cells, Partition parent)
	{
		List<Cell> cellList = new ArrayList<>(cells);
		this.cells = Collections.unmodifiableList(cellList);
		this.parent = parent;
	}

	/**
	 * @return Whether the partition is discrete. A discrete partition is a partition with only singleton cells.
	 */
	public boolean isDiscrete()
	{
		return cells.stream().allMatch(Cell::isSingleton);
	}

	/**
	 * @return An array in which element i contains the cell number of vertex i in this partition.
	 */
	public int[] getVertexToCellMapping()
	{
		if (vertexToCellMapping == null)
		{
			vertexToCellMapping = new int[cells.stream().mapToInt(Cell::size).sum()];
			for (int cellNb = 0; cellNb < cells.size(); ++cellNb)
			{
				for (int element : cells.get(cellNb).getElements())
				{
					vertexToCellMapping[element] = cellNb;
				}
			}
		}
		return vertexToCellMapping;
	}

	/**
	 * Applicable to discrete partitions only.
	 * @return An array in which element i contains the vertex number of the vertex in cell i.
	 */
	public int[] getCellToVertexMapping()
	{
		if (!isDiscrete())
			throw new RuntimeException("Cell to vertex mapping is only available for discrete partitions");

		if (cellToVertexMapping == null)
		{
			cellToVertexMapping = new int[cells.size()];
			for (int cellNb = 0; cellNb < cells.size(); ++cellNb)
				cellToVertexMapping[cellNb] = cells.get(cellNb).getElements().get(0);
		}
		return cellToVertexMapping;
	}

	/**
	 * Get all ancestors of this partition, starting from the root of partition down to the parent of this partition.
	 */
	public List<Partition> getAncestors()
	{
		if (ancestors == null)
		{
			LinkedList<Partition> anc = new LinkedList<>();
			Partition p = this.parent;
			while (p != null)
			{
				anc.addFirst(p);
				p = p.parent;
			}
			ancestors = Collections.unmodifiableList(anc);
		}
		return ancestors;
	}

	/**
	 * Refine the partition: see if the cells can be split into subcells because their elements have different descriptions.
	 *
	 * @param vertexDescriptions Base description of the vertices of the graph.
	 * @return A new partition which is a refined version of this one if possible, or an empty Optional.
	 */
	public Optional<Partition> refine(VertexDescription[] vertexDescriptions)
	{
		int[] mapping = this.getVertexToCellMapping();
		List<Cell> newCells = new ArrayList<>();
		for (Cell cell : this.cells)
		{
			if (cell.isSingleton())
				newCells.add(cell);
			else
				newCells.addAll(cell.refine(vertexDescriptions, mapping));
		}
		if (newCells.size() > cells.size()) // If the number of cells increased, the partition has been refined into a new form
			return Optional.of(new Partition(newCells, this));
		else
			return Optional.empty();
	}

	/**
	 * Two partitions are equal if they contain equal cells in the same order.
	 */
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Partition that = (Partition) o;
		return cells.equals(that.cells);
	}

	@Override
	public String toString()
	{
		StringBuilder res = new StringBuilder();
		for (Cell cell : cells)
		{
			res.append("| ");
			res.append(cell.toString());
		}
		res.append("|");
		return res.toString();
	}

	public List<Cell> getCells()
	{
		return cells;
	}

	public Partition getParent()
	{
		return parent;
	}
}
