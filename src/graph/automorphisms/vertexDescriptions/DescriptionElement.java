package graph.automorphisms.vertexDescriptions;

/**
 * En element part of the description of a vertex used in the refinement part of automorphism search.
 */
public interface DescriptionElement extends Comparable<DescriptionElement>
{
	/**
	 * Create a new vertex description element based on this one, where vertex u is replaced by mapping[u]
	 */
	DescriptionElement mapVertices(int[] mapping);
}
