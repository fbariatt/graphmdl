package graph.automorphisms.vertexDescriptions;

import java.util.Objects;

/**
 * An element describing the label of a vertex.
 */
public class VertexLabelDescriptionElement implements DescriptionElement
{
	private final String label;

	public VertexLabelDescriptionElement(String label)
	{
		this.label = label;
	}

	@Override
	public DescriptionElement mapVertices(int[] mapping)
	{
		// The description doesn't change if the vertex name changes
		return this;
	}

	@Override
	public int compareTo(DescriptionElement other)
	{
		if (other instanceof VertexLabelDescriptionElement)
		{
			return this.label.compareTo(((VertexLabelDescriptionElement) other).label);
		}
		else if ((other instanceof InEdgeDescriptionElement) || (other instanceof OutEdgeDescriptionElement))
		{
			return -1; // A vertex description is always considered smaller than an edge description
		}
		else
		{
			throw new ClassCastException("Impossible to compare " + this.getClass() + " with " + other.getClass());
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		VertexLabelDescriptionElement that = (VertexLabelDescriptionElement) o;
		return label.equals(that.label);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(label);
	}

	@Override
	public String toString()
	{
		return label;
	}

	public String getLabel()
	{
		return label;
	}
}
