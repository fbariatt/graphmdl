package graph.automorphisms.vertexDescriptions;

import java.util.Comparator;
import java.util.Objects;

/**
 * An element describing an in-edge of a vertex
 */
public class InEdgeDescriptionElement implements DescriptionElement
{
	private final String edgeLabel;
	private final int source;

	public InEdgeDescriptionElement(String edgeLabel, int source)
	{
		this.edgeLabel = edgeLabel;
		this.source = source;
	}

	@Override
	public DescriptionElement mapVertices(int[] mapping)
	{
		return new InEdgeDescriptionElement(this.edgeLabel, mapping[this.source]);
	}

	@Override
	public int compareTo(DescriptionElement other)
	{
		if (other instanceof InEdgeDescriptionElement)
		{
			return Comparator.comparing(InEdgeDescriptionElement::getEdgeLabel)
					.thenComparingInt(InEdgeDescriptionElement::getSource)
					.compare(this, (InEdgeDescriptionElement) other);
		}
		else if (other instanceof OutEdgeDescriptionElement)
		{
			return -1; // An in-edge description is always considered smaller than an out-edge description
		}
		else if (other instanceof VertexLabelDescriptionElement)
		{
			return 1; // An edge description is always considered greater than a vertex label description
		}
		else
		{
			throw new ClassCastException("Impossible to compare " + this.getClass() + " with " + other.getClass());
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		InEdgeDescriptionElement that = (InEdgeDescriptionElement) o;
		return source == that.source &&
				edgeLabel.equals(that.edgeLabel);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(edgeLabel, source);
	}

	@Override
	public String toString()
	{
		return String.format("inEdge(%s, %d)", edgeLabel, source);
	}

	public String getEdgeLabel()
	{
		return edgeLabel;
	}

	public int getSource()
	{
		return source;
	}
}
