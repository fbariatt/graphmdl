package graph.automorphisms.vertexDescriptions;

import graph.Graph;
import graph.Vertex;
import utils.GeneralUtils;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The description of a vertex, used in the refinement step of the automorphism search algorithm.
 * If two vertices have a different description, they certainly can not be swapped.
 * This implementation describes the labels of the vertex, its in-edges and out-edges.
 */
public class VertexDescription implements Comparable<VertexDescription>
{
	private final List<DescriptionElement> descriptionElements;
	private final int originalVertexNumber; // The number of the vertex in the graph, not affected by remappings and ignored in ordering.
	private Integer hashCode = null; // Lazy result of hashCode()
	private String stringRepresentation = null; // Lazy result of toString()

	/**
	 * Compute the description of a vertex v of a graph g
	 * @param g The graph of which the vertex is part of
	 * @param vertexNumber the index of the vertex in the graph
	 * @param vertexIndexMap See {@link Graph#getVertexIndexMap()}
	 */
	public VertexDescription(Graph g, int vertexNumber, Map<Vertex, Integer> vertexIndexMap)
	{
		this.originalVertexNumber = vertexNumber;

		List<DescriptionElement> elements = new LinkedList<>();
		Vertex vertex = g.getVertex(vertexNumber);

		vertex.getLabels().keySet().stream()
				.map(VertexLabelDescriptionElement::new)
				.forEach(elements::add);
		g.getOutEdges(vertex)
				.map(edge -> new OutEdgeDescriptionElement(edge.getLabel(), vertexIndexMap.get(edge.getTarget())))
				.forEach(elements::add);
		g.getInEdges(vertex)
				.map(edge -> new InEdgeDescriptionElement(edge.getLabel(), vertexIndexMap.get(edge.getSource())))
				.forEach(elements::add);

		Collections.sort(elements);
		this.descriptionElements = Collections.unmodifiableList(elements);
	}

	/**
	 * @param descriptionElements  List of description elements describing this vertex. The list must already be sorted. The list is stored directly so any modification to it will modify this description as well.
	 * @param originalVertexNumber Initial index of the vertex in the graph
	 */
	private VertexDescription(List<DescriptionElement> descriptionElements, int originalVertexNumber)
	{
		this.descriptionElements = Collections.unmodifiableList(descriptionElements);
		this.originalVertexNumber = originalVertexNumber;
	}

	/**
	 * Create a new description based on this one, where each vertex u in the description elements is replaced by mapping[u]
	 */
	public VertexDescription mapVertices(int[] mapping)
	{
		LinkedList<DescriptionElement> mappedElements = this.descriptionElements.stream()
				.map(descriptionElement -> descriptionElement.mapVertices(mapping))
				.collect(LinkedList::new, LinkedList::add, LinkedList::addAll);
		Collections.sort(mappedElements);
		return new VertexDescription(mappedElements, originalVertexNumber);
	}

	@Override
	public int compareTo(VertexDescription other)
	{
		return GeneralUtils.lexicographicCompare(this.descriptionElements, other.descriptionElements);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		VertexDescription that = (VertexDescription) o;
		return descriptionElements.equals(that.descriptionElements);
	}

	@Override
	public int hashCode()
	{
		if (hashCode == null)
			hashCode = this.descriptionElements.hashCode();
		return hashCode;
	}


	@Override
	public String toString()
	{
		if (stringRepresentation == null)
		{
			StringBuilder res = new StringBuilder();
			descriptionElements.stream()
					.map(element -> element.toString() + " ")
					.forEachOrdered(res::append);
			stringRepresentation = res.toString();
		}
		return stringRepresentation;
	}

	/**
	 * @return The number of the vertex in the graph as it was when the description was first created. Not affected by remappings.
	 * Storing this information eases computation in the automorphism search algorithm
	 */
	public int getOriginalVertexNumber()
	{
		return originalVertexNumber;
	}
}
