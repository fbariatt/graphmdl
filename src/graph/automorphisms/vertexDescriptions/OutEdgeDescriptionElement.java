package graph.automorphisms.vertexDescriptions;

import java.util.Comparator;
import java.util.Objects;

/**
 * An element describing an out-edge of a vertex
 */
public class OutEdgeDescriptionElement implements DescriptionElement
{
	private final String edgeLabel;
	private final int target;

	public OutEdgeDescriptionElement(String edgeLabel, int target)
	{
		this.edgeLabel = edgeLabel;
		this.target = target;
	}

	@Override
	public DescriptionElement mapVertices(int[] mapping)
	{
		return new OutEdgeDescriptionElement(this.edgeLabel, mapping[this.target]);
	}

	@Override
	public int compareTo(DescriptionElement other)
	{
		if (other instanceof OutEdgeDescriptionElement)
		{
			return Comparator.comparing(OutEdgeDescriptionElement::getEdgeLabel)
					.thenComparingInt(OutEdgeDescriptionElement::getTarget)
					.compare(this, (OutEdgeDescriptionElement) other);
		}
		else if ((other instanceof InEdgeDescriptionElement) || (other instanceof VertexLabelDescriptionElement))
		{
			return 1; // An out-edge description is always considered greater than a vertex label or in-edge description
		}
		else
		{
			throw new ClassCastException("Impossible to compare " + this.getClass() + " with " + other.getClass());
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		OutEdgeDescriptionElement that = (OutEdgeDescriptionElement) o;
		return target == that.target &&
				edgeLabel.equals(that.edgeLabel);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(edgeLabel, target);
	}

	@Override
	public String toString()
	{
		return String.format("outEdge(%s, %d)", edgeLabel, target);
	}

	public String getEdgeLabel()
	{
		return edgeLabel;
	}

	public int getTarget()
	{
		return target;
	}
}
