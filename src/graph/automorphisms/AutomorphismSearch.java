package graph.automorphisms;

import graph.Graph;
import graph.Vertex;
import graph.automorphisms.certificates.GraphCertificate;
import graph.automorphisms.vertexDescriptions.VertexDescription;
import utils.SingletonStopwatchCollection;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Algorithm that finds graph automorphisms.
 * Adaptation of T. Junttila and P. Kaski, "Engineering an Efficient Canonical Labeling Tool for Large and Sparse Graphs" in 2007 Proceedings of the Ninth Workshop on Algorithm Engineering and Experiments (ALENEX), 2007, pp. 135–149.
 */
public class AutomorphismSearch
{
	private final GraphCertificate certificateSkeleton;
	private final VertexDescription[] vertexDescriptions;
	private Map<GraphCertificate, Partition> firstPartitions; // First partition found for every certificate
	private Map<GraphCertificate, List<Partition>> automorphisms; // All automorphisms found for every certificate

	public AutomorphismSearch(Graph g)
	{
		Map<Vertex, Integer> vertexIndexMap = g.getVertexIndexMap();
		this.certificateSkeleton = new GraphCertificate(g, vertexIndexMap);
		vertexDescriptions = new VertexDescription[g.getVertexCount()];
		for (int v = 0; v < vertexDescriptions.length; ++v)
			vertexDescriptions[v] = new VertexDescription(g, v, vertexIndexMap);
		this.firstPartitions = new HashMap<>();
		this.automorphisms = new HashMap<>();
	}

	/**
	 * Run the algorithm in order to find a set of "base" automorphisms that allow to generate all possible automorphisms of the graph.
	 * The returned set does not contain the identity automorphism (i.e. mapping each vertex to itself) unless it is the only one that is found (i.e. the
	 * pattern has no symmetries).
	 * The returned set is not guaranteed to be the smallest possible generative set, but the algorithm tries as much as possible to do so, in order to avoid redundant computation.
	 * Computation is deterministic, automorphisms will always be returned in the same order.
	 */
	public AutomorphismInfo findAutomorphisms()
	{
		SingletonStopwatchCollection.resume("AutomorphismSearch.findBaseAutomorphisms");
		// Performing automorphism search
		Partition root = new Partition(new Cell(IntStream.range(0, vertexDescriptions.length).boxed().collect(Collectors.toList())), null);
		explorePartition(root);

		// Computing search results
		LinkedHashSet<Automorphism> automorphisms = new LinkedHashSet<>();
		GraphCertificate minCertificate = null;

		for (GraphCertificate certificate : this.firstPartitions.keySet()) // For every certificate found
		{
			// Is this certificate the canonical certificate?
			if (minCertificate == null || certificate.compareTo(minCertificate) < 0)
				minCertificate = certificate;

			// Parsing automorphisms associated to this certificate (if there are any)
			List<Partition> automorphismForCertificate = this.automorphisms.get(certificate);
			if (automorphismForCertificate != null)
			{
				int[] firstPartitionVertexToCellMapping = firstPartitions.get(certificate).getVertexToCellMapping();
				for (Partition automorphismPartition : automorphismForCertificate)
				{
					int[] automorphismCellToVertexMapping = automorphismPartition.getCellToVertexMapping();
					int[] automorphism = new int[automorphismCellToVertexMapping.length];
					for (int i = 0; i < automorphismCellToVertexMapping.length; ++i)
						automorphism[i] = automorphismCellToVertexMapping[firstPartitionVertexToCellMapping[i]];

					automorphisms.add(new Automorphism(automorphism));
				}
			}
		}
		if (automorphisms.isEmpty())
			automorphisms.add(Automorphism.identity(vertexDescriptions.length));
		SingletonStopwatchCollection.stop("AutomorphismSearch.findBaseAutomorphisms");
		return new AutomorphismInfo(minCertificate, automorphisms, false);
	}

	/**
	 * Explore a given partition.
	 *
	 * @return If a partition is returned, it means that a backjump to that partition is needed.
	 * Backjump: resume computation from the given partition instead of continuing normally.
	 */
	private Optional<Partition> explorePartition(Partition p)
	{
		if (p.isDiscrete())
			return exploreDiscretePartition(p);
		else
			return exploreNonDiscretePartition(p);
	}

	/**
	 * Explore a discrete partition.
	 * See {@link #explorePartition}
	 */
	private Optional<Partition> exploreDiscretePartition(Partition p)
	{
		GraphCertificate certificate = certificateSkeleton.mapVertices(p.getVertexToCellMapping());

		if (firstPartitions.containsKey(certificate)) // This certificate has already been seen: store the automorphism and perform a backjump
		{
			automorphisms.computeIfAbsent(certificate, k -> new LinkedList<>()).add(p); // This partition is an automorphism for the certificate
			return Optional.of(findBackjumpTarget(p, firstPartitions.get(certificate)));
		}
		else // This is the first time we see this certificate: store it and do no backjump
		{
			firstPartitions.put(certificate, p);
			return Optional.empty();
		}
	}

	/**
	 * To be called when a backjump needs to be performed to find where to backjump.
	 * The backjump target is the lowest common ancestor of the two partitions.
	 */
	private static Partition findBackjumpTarget(Partition currentPartition, Partition otherPartition)
	{
		// Reminder: the ancestor of a partition p are a list that starts with the root partition and ends with the parent of p
		ListIterator<Partition> ancestorsOfCurrent = currentPartition.getAncestors().listIterator();
		ListIterator<Partition> ancestorsOfOther = otherPartition.getAncestors().listIterator();

		boolean ancestorsAreTheSame = true; // Are the current ancestors the same or are they different?
		while (ancestorsOfCurrent.hasNext() && ancestorsOfOther.hasNext() && ancestorsAreTheSame)
		{
			if (ancestorsOfCurrent.next() != ancestorsOfOther.next())
				ancestorsAreTheSame = false;
		}
		Partition backjumpTarget;
		if (ancestorsAreTheSame) // We did not find a differing ancestor (i.e. we stopped because the lists ended)
		{
			backjumpTarget = ancestorsOfCurrent.previous(); // The partition we just inspected (i.e. the last partition in the list) is the lowest common ancestor
		}
		else // We stopped because we found two different ancestors
		{
			ancestorsOfCurrent.previous(); // We go back to the differing ancestor
			backjumpTarget = ancestorsOfCurrent.previous(); // The ancestor just before was the lowest common one

		}
		return backjumpTarget;
	}

	/**
	 * Explore a non discrete partition.
	 * See {@link #explorePartition}
	 */
	private Optional<Partition> exploreNonDiscretePartition(Partition p)
	{
		// If the partition can be refined, we explore the refined partition instead
		Optional<Partition> refinedPartition = p.refine(vertexDescriptions);
		if (refinedPartition.isPresent())
			return explorePartition(refinedPartition.get());

		// Since the partition is not discrete, it will generate some child partition
		List<Cell> cellsOfChildPartition = new ArrayList<>(p.getCells());

		// Finding the cell to split
		// We choose the first non-singleton cell of minimum size
		Cell cellToSplit = utils.FindFirstMin.find(
				cellsOfChildPartition.stream().filter(cell -> !cell.isSingleton()),
				Cell::size
		);
		ListIterator<Cell> cellToSplitIterator = cellsOfChildPartition.listIterator(); // Iterator pointing on the selected cell in the list of cells of the child partition
		while (true)
		{
			if (cellToSplitIterator.next() == cellToSplit)
				break;
		}

		// Trying all splits
		cellToSplitIterator.remove(); // Removing the cell to split
		for (int i = 0; i < cellToSplit.size(); ++i)
		{
			Cell.CellPair subCells = cellToSplit.split(i);
			cellToSplitIterator.add(subCells.first);
			cellToSplitIterator.add(subCells.second);

			// Create a child partition and explore it
			Optional<Partition> backjump = explorePartition(new Partition(cellsOfChildPartition, p));
			if (backjump.isPresent()) // A backjump is needed: we should abandon the search of the current subtree and go back to the given partition
			{
				if (backjump.get() != p) // Are we the given partition? If yes computation continues, otherwise we jump
					return backjump;
			}

			// Remove the added sub-cells so that next iteration can add the next split
			cellToSplitIterator.previous();
			cellToSplitIterator.remove();
			cellToSplitIterator.previous();
			cellToSplitIterator.remove();
		}
		return Optional.empty(); // We completely explored this partition and no backjump is to be performed
	}

	/**
	 * Find a base set of automorphisms of the given graph.
	 *
	 * @see #findAutomorphisms()
	 */
	public static AutomorphismInfo findBaseAutomorphisms(Graph g)
	{
		return new AutomorphismSearch(g).findAutomorphisms();
	}

	/**
	 * Find <em>all</em> automorphisms of the given graph.
	 * This method first finds all base automorphisms and then combines them together in all
	 * possible ways to find the complete set of automorphisms.
	 * @see #findAutomorphisms()
	 * @see AutomorphismInfo#saturateAutomorphisms()
	 */
	public static AutomorphismInfo findAndSaturateAutomorphisms(Graph g)
	{
		AutomorphismInfo automorphisms = findBaseAutomorphisms(g);
		SingletonStopwatchCollection.resume("AutomorphismSearch.saturateAutomorphisms");
		automorphisms.saturateAutomorphisms();
		SingletonStopwatchCollection.stop("AutomorphismSearch.saturateAutomorphisms");
		return automorphisms;
	}
}
