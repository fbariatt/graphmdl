package graph.automorphisms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class representing a graph automorphism
 */
public class Automorphism
{
	private final int[] mapping;

	/**
	 * @param mapping To which vertex is each vertex mapped to. This array should contain a permutation of integers between 0 and mapping.length -1. This is not checked.
	 *                The object stores the array as-is, and does not make a copy. Later modification of the array will modify the object as well.
	 */
	public Automorphism(int[] mapping)
	{
		this.mapping = mapping;
	}

	/**
	 * @return An identity automorphism, i.e. an automorphism that maps each vertex to itself
	 */
	public static Automorphism identity(int size)
	{
		int[] mapping = new int[size];
		for (int i = 0; i < size; ++i)
			mapping[i] = i;
		return new Automorphism(mapping);
	}

	/**
	 * @return The number of vertices that this automorphism maps
	 */
	public int getVertexCount()
	{
		return mapping.length;
	}

	/**
	 * @return The mapping of the given vertex by this automorphism
	 */
	public int getMapping(int vertex)
	{
		return mapping[vertex];
	}

	/**
	 * @return The mapping for each of the given vertices by this automorphism
	 * See {@link #getMapping(int)}.
	 */
	public int[] getMapping(int[] vertices)
	{
		int[] result = new int[vertices.length];
		for (int i = 0; i < vertices.length; ++i)
		{
			result[i] = mapping[vertices[i]];
		}
		return result;
	}

	/**
	 * See {@link #getMapping(int[])}
	 */
	public List<Integer> getMapping(List<Integer> vertices)
	{
		List<Integer> result = new ArrayList<>(vertices.size());
		for (Integer vertex : vertices)
		{
			result.add(mapping[vertex]);
		}
		return result;
	}

	/**
	 * @return The automorphism equivalent of applying this automorphism and then another
	 */
	public Automorphism composeWith(Automorphism other)
	{
		if (other.getVertexCount() != this.getVertexCount())
			throw new IllegalArgumentException("Trying to compose automorphisms of different sizes");
		int[] newMapping = new int[mapping.length];
		for (int i = 0; i < mapping.length; ++i)
			newMapping[i] = mapping[other.mapping[i]];
		return new Automorphism(newMapping);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Automorphism that = (Automorphism) o;
		return Arrays.equals(mapping, that.mapping);
	}

	@Override
	public int hashCode()
	{
		return Arrays.hashCode(mapping);
	}

	@Override
	public String toString()
	{
		return String.format("Automorphism{%s}", Arrays.toString(mapping));
	}
}
