package graph.automorphisms;

import graph.Graph;
import graph.automorphisms.certificates.GraphCertificate;

import java.util.*;

/**
 * This class contains information about the automorphisms of a graph.
 */
public class AutomorphismInfo
{
	private final GraphCertificate canonicalName;
	private LinkedHashSet<Automorphism> automorphisms;
	private boolean isSaturated;

	public AutomorphismInfo(GraphCertificate canonicalName, LinkedHashSet<Automorphism> automorphisms, boolean isSaturated)
	{
		this.canonicalName = canonicalName;
		this.automorphisms = automorphisms;
		this.isSaturated = isSaturated;

	}

	/**
	 * Saturate the set of automorphisms of this instance, if not already saturated.
	 * @see  #saturateAutomorphisms(Collection, int)
	 */
	public void saturateAutomorphisms()
	{
		if (!isSaturated)
		{
			this.automorphisms = saturateAutomorphisms(this.automorphisms, canonicalName.getVertexCount());
			this.isSaturated = true;
		}
	}

	/**
	 * From a set of base automorphism, generate all possible automorphisms of the graph by combining them in all possible ways.
	 */
	public static LinkedHashSet<Automorphism> saturateAutomorphisms(Collection<Automorphism> baseAutomorphisms, int automorphismVertexCount)
	{
		LinkedHashSet<Automorphism> result = new LinkedHashSet<>();
		Deque<Automorphism> toExplore = new LinkedList<>();

		toExplore.add(Automorphism.identity(automorphismVertexCount));
		while (!toExplore.isEmpty())
		{
			Automorphism automorphism = toExplore.pollFirst();
			if (!result.add(automorphism)) // We add the automorphism to the result set and check if it wasn't already there
				continue;
			for (Automorphism baseAutomorphism : baseAutomorphisms) // We try to compose this state with all the base transformations
			{
				Automorphism newAutomorphism = automorphism.composeWith(baseAutomorphism);
				if (!result.contains(newAutomorphism)) // If it is a new state that we never explored
					toExplore.addLast(newAutomorphism);
			}
		}
		return result;
	}

	/**
	 * Return an instance of AutomorphismInfo with only the identity automorphism and the certificate derived by the graph as it is given.
	 * Warning: no automorphism search is performed, so the certificate probably won't be the canonical label for the graph.
	 * Do not rely on the result of this method to tell whether two graphs are the same.
	 * This method is intended as a fill-in argument for methods that expect an instance of AutomorphismInfo.
	 */
	public static AutomorphismInfo trivialInfo(Graph g)
	{
		LinkedHashSet<Automorphism> automorphisms = new LinkedHashSet<>(1);
		automorphisms.add(Automorphism.identity(g.getVertexCount()));
		return new AutomorphismInfo(new GraphCertificate(g), automorphisms, false);
	}

	public GraphCertificate getCanonicalName()
	{
		return canonicalName;
	}

	/**
	 * @return An unmodifiable view of the automorphisms.
	 * @see Collections#unmodifiableSet(Set)
	 */
	public Set<Automorphism> getAutomorphisms()
	{
		return Collections.unmodifiableSet(automorphisms);
	}

	/**
	 * @return Whether this collection of automorphism is saturated.
	 * @see #saturateAutomorphisms(Collection, int)
	 */
	public boolean isSaturated()
	{
		return isSaturated;
	}
}
