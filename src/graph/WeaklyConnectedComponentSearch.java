package graph;

import java.util.*;
import java.util.function.Predicate;

/**
 * Simple algorithm for finding weakly connected components in a graph.
 * A weakly connected component is a set of vertices such that there is a path between any two of them, ignoring edge direction.
 * <a href="https://en.wikipedia.org/wiki/Component_(graph_theory)">https://en.wikipedia.org/wiki/Component_(graph_theory)</a>
 * <br/>
 * Starting from a vertex, the algorithm simply visits all neighbours recursively, following edges in any direction.
 */
public class WeaklyConnectedComponentSearch
{

	/**
	 * @param g               A graph.
	 * @param ignoredVertices Vertices in this set are considered as if they weren't part of the graph for the WCC computation.
	 * @param ignoredEdges    Edges in this set are considered as if they weren't part of the graph for the WCC computation.
	 * @return The vertices in each WCC of the graph. WCCs may be returned in any order.
	 */
	public static List<Set<Vertex>> computeWCCs(Graph g, Set<Vertex> ignoredVertices, Set<Edge> ignoredEdges)
	{
		Set<Vertex> visited = new HashSet<>();
		List<Set<Vertex>> connectedComponents = new ArrayList<>();

		Iterator<Vertex> vertexIterator = g.getVertices().iterator();
		while (vertexIterator.hasNext())
		{
			Vertex v = vertexIterator.next();
			if (!visited.contains(v) && !ignoredVertices.contains(v))
			{
				Set<Vertex> component = computeWCCAround(g, v, ignoredVertices, ignoredEdges);
				connectedComponents.add(component);
				visited.addAll(component);
			}
		}
		return connectedComponents;
	}

	/**
	 * Compute all the weakly connected components in a graph.
	 */
	public static List<Set<Vertex>> computeWCCs(Graph g)
	{
		return computeWCCs(g, Collections.emptySet(), Collections.emptySet());
	}


	/**
	 * Compute the weakly connected component around a given vertex.
	 * The vertices (resp. edges) provided are considered as if they weren't part of the graph.
	 * The starting vertex should not be in the set of ignored vertices.
	 */
	public static Set<Vertex> computeWCCAround(Graph g, Vertex v, Set<Vertex> ignoredVertices, Set<Edge> ignoredEdges)
	{
		if (ignoredVertices.contains(v))
			throw new IllegalArgumentException("Starting vertex for WCC computation is in the ignored vertices");

		Set<Vertex> component = new HashSet<>();
		Set<Vertex> toVisit = new HashSet<>();
		toVisit.add(v);

		while (!toVisit.isEmpty())
		{
			Vertex visiting = toVisit.stream().findAny().get();
			toVisit.remove(visiting);
			component.add(visiting);

			g.getOutEdges(visiting)
					.filter(Predicate.not(ignoredEdges::contains))
					.map(Edge::getTarget)
					.filter(Predicate.not(ignoredVertices::contains))
					.filter(Predicate.not(component::contains))
					.forEach(toVisit::add);
			g.getInEdges(visiting)
					.filter(Predicate.not(ignoredEdges::contains))
					.map(Edge::getSource)
					.filter(Predicate.not(ignoredVertices::contains))
					.filter(Predicate.not(component::contains))
					.forEach(toVisit::add);
		}
		return component;
	}

	/**
	 * Compute the weakly connected component around a given vertex.
	 */
	public static Set<Vertex> computeWCCAround(Graph g, Vertex v)
	{
		return computeWCCAround(g, v, Collections.emptySet(), Collections.emptySet());
	}
}
