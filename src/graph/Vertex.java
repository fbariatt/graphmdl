package graph;

import java.util.Map;

/**
 * A graph vertex
 */
public interface Vertex
{
	/**
	 * @return The node's labels
	 */
	Map<String, VertexLabel> getLabels();

	/**
	 * Add a label to the node
	 *
	 * @return This vertex, so it is possible to chain adds.
	 */
	Vertex addLabel(String label);

	/**
	 * Add a label to the node
	 *
	 * @return This vertex, so it is possible to chain adds.
	 */
	Vertex addLabel(VertexLabel label);

	/**
	 * Remove a label from this vertex
	 *
	 * @throws java.util.NoSuchElementException if the vertex does not currently have the specified label
	 */
	void delLabel(String label);

	/**
	 * Remove a label from this vertex
	 *
	 * @throws java.util.NoSuchElementException if the vertex does not currently have the specified label
	 */
	void delLabel(VertexLabel label);

	/**
	 * Remove all labels from this vertex
	 */
	void clearLabels();

	/**
	 * @return A string associated with the vertex. It is supposed to be unique, but this is not enforced since
	 * vertices are identified by their number in the graph
	 */
	String getName();

	/**
	 * @param name A string associated with the vertex. It is supposed to be unique, but this is not enforced since
	 *             vertices are identified by their number in the graph
	 */
	void setName(String name);

	/**
	 * @return A marker used to indicate if this edge needs to be a port in the MDL-rewritten graph
	 */
	int getIsPortMarker();

	/**
	 * Set the marker used to indicate if this edge needs to be a port in the MDL-rewritten graph
	 */
	void setIsPortMarker(int isPortMarker);
}
