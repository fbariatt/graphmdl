package graph;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GraphImpl implements Graph
{
	private List<VertexImpl> vertices;
	private int marker = -1;
	// Lazy result of getVertexIndexMap
	private Map<Vertex, Integer> vertexIndexMap = null;

	GraphImpl()
	{
		this.vertices = new ArrayList<>();
	}

	@Override
	public int getMarker()
	{
		return this.marker;
	}

	@Override
	public void setMarker(int marker)
	{
		this.marker = marker;
	}

	@Override
	public int getVertexCount()
	{
		return this.vertices.size();
	}

	@Override
	public long getEdgeCount()
	{
		return vertices.stream().mapToLong(VertexImpl::getOutEdgeCount).sum();
	}

	@Override
	public void clearEdges()
	{
		vertices.forEach(VertexImpl::clearEdges);
	}

	@Override
	public void clear()
	{
		this.clearEdges();
		this.vertices.clear();
		this.vertexIndexMap = null;
	}

	@Override
	public Vertex getVertex(int i)
	{
		return vertices.get(i);
	}

	@Override
	public Vertex addVertex(Iterable<String> labels)
	{
		VertexImpl v = new VertexImpl();
		labels.forEach(v::addLabel);
		vertices.add(v);
		// Adding a vertex does not shift the indices of the others (since it is added at the end)
		// So if a vertexIndexMap exists, it can just be updated
		if (this.vertexIndexMap != null)
			vertexIndexMap.put(v, this.vertices.size() - 1);
		return v;
	}

	@Override
	public Vertex addVertex(String label)
	{
		return this.addVertex(Collections.singletonList(label));
	}

	@Override
	public Vertex addVertex()
	{
		return this.addVertex(Collections.emptyList());
	}

	@Override
	public void delVertex(int i)
	{
		VertexImpl v = vertices.remove(i);
		v.clearEdges();
		this.vertexIndexMap = null; // Invalidating lazy result since vertex numbers will be shifted
	}

	@Override
	public void delVertex(Vertex v)
	{
		delVertex(getVertexIndex(v));
	}

	@Override
	public Stream<Vertex> getVertices()
	{
		return vertices.stream().map(vertex -> (Vertex) vertex);
	}

	@Override
	public Stream<Vertex> getVerticesWithLabel(String label)
	{
		return vertices.stream()
				.filter(vertex -> vertex.getLabels().containsKey(label))
				.map(vertex -> (Vertex) vertex);
	}

	@Override
	public int getVertexIndex(Vertex v)
	{
		if (this.vertexIndexMap != null)
			return this.vertexIndexMap.get(v);
		else
			return this.vertices.indexOf(v);
	}

	@Override
	public Map<Vertex, Integer> getVertexIndexMap()
	{
		if (this.vertexIndexMap == null)
		{
			this.vertexIndexMap = new HashMap<>();
			for (int v = 0; v < vertices.size(); ++v)
				this.vertexIndexMap.put(vertices.get(v), v);
		}
		return Collections.unmodifiableMap(this.vertexIndexMap);
	}

	@Override
	public Edge getEdge(int source, int target, String label)
	{
		return this.vertices.get(source).getEdge(this.vertices.get(target), label);
	}

	@Override
	public Edge getEdge(Vertex source, Vertex target, String label)
	{
		return ((VertexImpl) source).getEdge((VertexImpl) target, label);
	}

	@Override
	public Stream<Edge> getEdges(int source, int target)
	{
		return getEdges(this.vertices.get(source), this.vertices.get(target));
	}

	@Override
	public Stream<Edge> getEdges(Vertex source, Vertex target)
	{
		return ((VertexImpl) source).getOutEdgesWith((VertexImpl) target)
				.map(edge -> (Edge) edge);
	}

	@Override
	public Edge addEdge(int source, int target, String label)
	{
		return this.vertices.get(source).addEdge(this.vertices.get(target), label);
	}

	@Override
	public Edge addEdge(Vertex source, Vertex target, String label)
	{
		return ((VertexImpl) source).addEdge((VertexImpl) target, label);
	}

	@Override
	public void delEdge(Edge e)
	{
		((VertexImpl) e.getSource()).delEdge((VertexImpl) e.getTarget(), e.getLabel());
	}

	@Override
	public void delEdge(int source, int target, String label)
	{
		this.vertices.get(source).delEdge(this.vertices.get(target), label);
	}

	@Override
	public void delEdge(Vertex source, Vertex target, String label)
	{
		((VertexImpl) source).delEdge((VertexImpl) target, label);
	}

	@Override
	public long getOutEdgeCount(int vertex)
	{
		return this.vertices.get(vertex).getOutEdgeCount();
	}

	@Override
	public long getOutEdgeCount(Vertex v)
	{
		return ((VertexImpl) v).getOutEdgeCount();
	}

	@Override
	public long getInEdgeCount(int vertex)
	{
		return this.vertices.get(vertex).getInEdgeCount();
	}

	@Override
	public long getInEdgeCount(Vertex v)
	{
		return ((VertexImpl) v).getInEdgeCount();
	}

	@Override
	public Stream<Edge> getOutEdges(int vertex)
	{
		return getOutEdges(this.vertices.get(vertex));
	}

	@Override
	public Stream<Edge> getOutEdges(Vertex v)
	{
		return ((VertexImpl) v).getOutEdges()
				.map(edge -> (Edge) edge);
	}

	@Override
	public Stream<Edge> getInEdges(int vertex)
	{
		return getInEdges(this.vertices.get(vertex));
	}

	@Override
	public Stream<Edge> getInEdges(Vertex v)
	{
		return ((VertexImpl) v).getInEdges()
				.map(edge -> (Edge) edge);
	}

	@Override
	public Stream<Edge> getInOutEdges(int vertex)
	{
		return Stream.concat(this.getInEdges(vertex), this.getOutEdges(vertex));
	}

	@Override
	public Stream<Edge> getInOutEdges(Vertex v)
	{
		return Stream.concat(this.getInEdges(v), this.getOutEdges(v));
	}

	@Override
	public Stream<Edge> getEdges()
	{
		return this.vertices.stream()
				.flatMap(VertexImpl::getOutEdges)
				.map(edge -> (Edge) edge);
	}

	@Override
	public void clearOutEdges(int vertex)
	{
		this.vertices.get(vertex).clearOutEdges();
	}

	@Override
	public void clearOutEdges(Vertex v)
	{
		((VertexImpl) v).clearOutEdges();
	}

	@Override
	public void clearInEdges(int vertex)
	{
		this.vertices.get(vertex).clearInEdges();
	}

	@Override
	public void clearInEdges(Vertex v)
	{
		((VertexImpl) v).clearInEdges();
	}

	@Override
	public void clearEdges(int vertex)
	{
		this.vertices.get(vertex).clearEdges();
	}

	@Override
	public void clearEdges(Vertex v)
	{
		((VertexImpl) v).clearEdges();
	}

	@Override
	public void concat(Graph other)
	{
		Map<Vertex, Vertex> otherToThis = new HashMap<>();
		// Adding vertices
		Iterator<Vertex> otherVertices = other.getVertices().iterator();
		while (otherVertices.hasNext())
		{
			Vertex otherVertex = otherVertices.next();
			Vertex v = this.addVertex();
			otherToThis.put(otherVertex, v);
			v.setName(otherVertex.getName());
			for (VertexLabel label : otherVertex.getLabels().values())
			{
				v.addLabel(new VertexLabel(label));
			}
		}
		// Adding edges
		other.getEdges().forEach(edge -> this.addEdge(otherToThis.get(edge.getSource()), otherToThis.get(edge.getTarget()), edge.getLabel()));
	}

	@Override
	public InducedSubgraph inducedSubGraph(Set<Vertex> vertices, Supplier<Graph> subGraphCreator)
	{
		Graph induced = subGraphCreator.get();
		Map<Vertex, Vertex> originalToInduced = new HashMap<>();

		// Copying vertices to result
		for (Vertex v : vertices)
		{
			Vertex inducedVertex = induced.addVertex();
			for (String label : v.getLabels().keySet())
				inducedVertex.addLabel(label);
			originalToInduced.put(v, inducedVertex);
		}

		// Copying edges to result
		for (Vertex v : vertices)
		{
			getOutEdges(v)
					.filter(edge -> vertices.contains(edge.getTarget()))
					.forEach(edge -> induced.addEdge(originalToInduced.get(v), originalToInduced.get(edge.getTarget()), edge.getLabel()));
		}

		return new InducedSubgraph(induced, originalToInduced);
	}

	@Override
	public InducedSubgraph inducedSubGraphByIndices(Set<Integer> vertices, Supplier<Graph> subGraphCreator)
	{
		return inducedSubGraph(
				vertices.stream()
						.map(v -> this.vertices.get(v))
						.collect(Collectors.toSet()),
				subGraphCreator);
	}

	@Override
	public boolean isEffectivelySimple()
	{
		return this.vertices.stream().allMatch(VertexImpl::isEffectivelySimpleVertex);
	}
}
