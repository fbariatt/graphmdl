package graph;

/**
 * A graph directed edge connecting two nodes.
 *
 * Note that edge properties can not be modified. Instead, a new vertex need to be created and the old one removed.
 */
public interface Edge
{
	/**
	 * @return The source vertex
	 */
	Vertex getSource();

	/**
	 * @return The target vertex
	 */
	Vertex getTarget();

	/**
	 * @return The edge's label, or "type"
	 */
	String getLabel();

	/**
	 * Mark the edge with a specific value
	 */
	void setMarker(int marker);

	/**
	 * @return The value with which the edge is marked
	 */
	int getMarker();
}
