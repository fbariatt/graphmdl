package graph.loaders;

import graph.Graph;
import graph.GraphFactory;
import graph.GraphSerializationException;
import graph.Vertex;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Methods to load a graph from text format
 */
public final class TextToGraph
{

	/**
	 * @param in               Input where the graph is read from
	 * @param graphInitializer Function used to initialize an empty graph that is then filled based on the input
	 * @return A graph which corresponds to the text description found in the input
	 * @throws IOException        If the input throws such an exception when reading.
	 * @throws GraphSerializationException If the input can not be interpreted correctly
	 * @implSpec This method assumes that the input only describes one graph, i.e. lines starting with 't' are simply ignored
	 * (this allows the input to start with some comment lines before the start of the actual graph)
	 */
	public static Graph read(BufferedReader in, Supplier<Graph> graphInitializer) throws IOException, GraphSerializationException
	{
		Graph g = graphInitializer.get();
		Map<String, Vertex> vertexNamesToVertices = new HashMap<>();

		String line;
		while ((line = in.readLine()) != null)
		{
			if (line.isBlank()) // Ignore empty lines
				continue;
			if (line.strip().charAt(0) == '#') // Ignore comment lines
				continue;

			String[] tokens = line.split(" ");
			switch (tokens[0])
			{
				case "t": // Line starts a graph
				{
					continue; // We simply ignore it as we assume that all the content describes a single graph
				}
				case "v": // Line describes a vertex
				{
					if (tokens.length < 2)
						throw new GraphSerializationException(String.format("Not enough elements for vertex definition on line %s: expected at least a vertex name.", line));
					final String vertexName = tokens[1];

					// Parsing vertex name
					if (vertexNamesToVertices.containsKey(vertexName))
						throw new GraphSerializationException(String.format("Duplicate vertex name %s found on line: %s", vertexName, line));
					Vertex vertex = g.addVertex();
					vertex.setName(vertexName);
					vertexNamesToVertices.put(vertexName, vertex);

					// Parsing vertex labels
					for (int i = 2; i < tokens.length; ++i)
					{
						final String vertexLabel = tokens[i];
						if (vertexLabel.length() > 0)
							vertex.addLabel(vertexLabel);
					}
				}
				break;
				case "e": // Line describes an edge
				{
					if (tokens.length < 4)
						throw new GraphSerializationException(String.format("Not enough elements for edge definition on line %s: expected source, target and label.", line));
					final String sourceName = tokens[1];
					final String targetName = tokens[2];
					final String label = tokens[3];

					if (!vertexNamesToVertices.containsKey(sourceName))
						throw new GraphSerializationException(String.format("Source vertex %s not found on line %s", sourceName, line));
					if (!vertexNamesToVertices.containsKey(targetName))
						throw new GraphSerializationException(String.format("Target vertex %s not found on line %s", targetName, line));

					g.addEdge(vertexNamesToVertices.get(sourceName), vertexNamesToVertices.get(targetName), label);
					break;
				}
				default:
				{
					throw new GraphSerializationException(String.format("Unknown token %s on line %s", tokens[0], line));
				}
			}
		}
		return g;
	}

	/**
	 * Same as {@link #read(BufferedReader, Supplier)}, with a default value for the graph initializer.
	 */
	public static Graph read(BufferedReader in) throws IOException, GraphSerializationException
	{
		return read(in, GraphFactory::createDefaultGraph);
	}

}
