package graph.loaders;

import graph.Graph;
import graph.GraphFactory;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Load a collection of graphs from a single text file
 */
public final class SingleFileTextGraphCollectionLoader
{
	private SingleFileTextGraphCollectionLoader()
	{}

	/**
	 * Load a collection of graphs from a single text file. Each graph definition must start with a "t" line.
	 * All graphs are parsed and loaded upon call, no lazy loading is performed.
	 *
	 * @throws IOException on errors
	 */
	public static Stream<Graph> loadGraphsFromFile(File graphFile) throws IOException
	{
		List<Graph> graphs = new LinkedList<>();
		BufferedReader reader = new SingleFileTextGraphCollectionBufferedReader(new FileReader(graphFile));
		while (reader.ready())
			graphs.add(TextToGraph.read(reader, GraphFactory::createDefaultGraph));
		return graphs.stream();
	}

	/**
	 * See {@link #loadGraphsFromFile}
	 */
	public static Stream<Graph> loadGraphsFromFile(String graphFilePath) throws IOException
	{
		return loadGraphsFromFile(new File(graphFilePath));
	}

	/**
	 * Internal class used to "trick" the graph class into thinking that the input file ends when a new graph definition is encountered.
	 */
	private static class SingleFileTextGraphCollectionBufferedReader extends BufferedReader
	{
		SingleFileTextGraphCollectionBufferedReader(Reader reader) throws IOException
		{
			super(reader);

			// We skip to the beginning of the first graph definition
			String line;
			do
			{
				line = super.readLine();
				if (line == null)
					throw new IOException("SingleFileTextGraphCollectionLoader: reached end of file without encountering any graph");
			}
			while (!line.startsWith("t"));
		}

		@Override
		public String readLine() throws IOException
		{
			String line = super.readLine();

			if (line == null)
				return null;

			if (line.startsWith("t"))
			{
				return null;
			}
			else
			{
				return line;
			}
		}
	}
}
