package graph.loaders;

import graph.Graph;
import graph.GraphFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Utility class for loading graphs from text files on the filesystem
 */
public final class FileSystemTextGraphLoader
{
	private FileSystemTextGraphLoader()
	{}

	private static Graph parseGraphFile(File graphFile)
	{
		if (!graphFile.isFile())
			throw new IllegalArgumentException(String.format("Graph file %s should be a regular file", graphFile.getPath()));

		try
		{
			return TextToGraph.read(new BufferedReader(new FileReader(graphFile)), GraphFactory::createDefaultGraph);
		} catch (IOException e)
		{
			e.printStackTrace();
			System.exit(1);
			return null;
		}
	}

	/**
	 * See {@link FileSystemGraphLoaderUtils#loadGraphsFromDirectory(File, Function)}
	 */
	public static Stream<Graph> loadGraphsFromDirectory(File graphDir)
	{
		return FileSystemGraphLoaderUtils.loadGraphsFromDirectory(graphDir, FileSystemTextGraphLoader::parseGraphFile);
	}

	/**
	 * See {@link FileSystemGraphLoaderUtils#loadGraphsFromDirectory(File, Function)}
	 */
	public static Stream<Graph> loadGraphsFromDirectory(String graphDirPath)
	{
		return loadGraphsFromDirectory(new File(graphDirPath));
	}

	/**
	 * Load a graph from a file.
	 */
	public static Graph loadGraphFromFile(File graphFile)
	{
		return parseGraphFile(graphFile);
	}

	/**
	 * Load a graph from a file.
	 */
	public static Graph loadGraphFromFile(String graphFilePath)
	{
		return parseGraphFile(new File(graphFilePath));
	}

}
