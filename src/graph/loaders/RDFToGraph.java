package graph.loaders;

import graph.Graph;
import graph.GraphFactory;
import graph.GraphSerializationException;
import graph.Vertex;
import graph.printers.RDFGraphPrefixes;
import org.apache.jena.rdf.model.*;
import org.apache.jena.shared.JenaException;
import org.apache.jena.vocabulary.RDF;

import java.io.InputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Methods to load a graph object from RDF.
 */
public final class RDFToGraph
{
	/**
	 * @param in               Input where the graph is read from.
	 * @param lang             RDF language to use for loading. See {@link Model#read(Reader, String, String)}
	 * @param graphInitializer Function used to initialize an empty graph that is then filled based on the input.
	 * @return A graph loaded from the RDF input.
	 * @throws GraphSerializationException If the input can not be interpreted correctly
	 */
	public static Graph read(InputStream in, String lang, Supplier<Graph> graphInitializer) throws GraphSerializationException
	{
		Model rdf = ModelFactory.createDefaultModel();
		try
		{
			rdf.read(in, null, lang);
		} catch (JenaException e)
		{
			throw new GraphSerializationException("Error when loading rdf", e);
		}
		Graph g = fromRDFModel(rdf, graphInitializer);
		rdf.close();
		return g;
	}

	/**
	 * @param rdf              Jena RDF Model from which the graph should be parsed
	 * @param graphInitializer Function used to initialize an empty graph that is then filled based on the rdf model.
	 * @return A graph loaded from the RDF model
	 */
	public static Graph fromRDFModel(Model rdf, Supplier<Graph> graphInitializer)
	{
		Graph g = graphInitializer.get();
		Map<Resource, Vertex> rdfResourcesToVertices = new HashMap<>();
		StmtIterator statements = rdf.listStatements();
		while (statements.hasNext())
		{
			final Statement statement = statements.next();
			final Resource subject = statement.getSubject();
			final Property predicate = statement.getPredicate();
			final RDFNode object = statement.getObject();

			Vertex subjectVertex = rdfResourcesToVertices.computeIfAbsent(subject, _k -> createVertexForEntity(subject, g));

			if (predicate.equals(RDF.type)) // rdf:type edges are transformed to vertex labels
			{
				subjectVertex.addLabel(object.toString());
			}
			else // All edges except rdf:type
			{
				Vertex objectVertex;
				if (object.isLiteral()) // The object of the predicate is a literal
				{
					final Literal literal = object.asLiteral();
					// Each literal instance is transformed into a different vertex, with labels indicating value, type and (if defined) language
					objectVertex = g.addVertex();
					objectVertex.addLabel(literal.getDatatypeURI());
					objectVertex.addLabel(RDFGraphPrefixes.LITERAL_VALUE_URI + literal.getLexicalForm());
					if (!literal.getLanguage().isEmpty())
						objectVertex.addLabel(RDFGraphPrefixes.LITERAL_LANG_URI + literal.getLanguage());
				}
				// The object of the predicate is not a literal: it is either an entity or a blank node
				else if (object.isURIResource() && object.asResource().equals(RDF.nil)) // The object is rdf:nil
				{
					// Each rdf:nil instance is modeled by a different vertex, to avoid creating artificial connections between all lists
					objectVertex = g.addVertex();
					objectVertex.addLabel(RDF.nil.getURI());
				}
				else // Any other entity or blank node
				{
					objectVertex = rdfResourcesToVertices.computeIfAbsent(object.asResource(), _k -> createVertexForEntity(object.asResource(), g));
				}
				g.addEdge(subjectVertex, objectVertex, predicate.getURI());
			}
		}
		return g;
	}

	/**
	 * Create a graph vertex for the given resource.
	 * Resource must be either an entity (with a URI), or a blank node.
	 * If the resource is a URI entity, the URI is saved in the graph as the name of the node.
	 */
	private static Vertex createVertexForEntity(Resource entity, Graph g)
	{
		Vertex entityVertex = g.addVertex();
		// If the entity has a URI (and is not one of our automatically-generated URIs) we save it as the vertex name
		if (entity.isURIResource() && !entity.getURI().startsWith(RDFGraphPrefixes.VERTEX_URI))
		{
			entityVertex.setName(entity.getURI());
		}
		return entityVertex;
	}

	/**
	 * Same as {@link #read(InputStream, String, Supplier)}, with a default value for the graph initializer.
	 */
	public static Graph read(InputStream in, String lang) throws GraphSerializationException
	{
		return read(in, lang, GraphFactory::createDefaultGraph);
	}
}
