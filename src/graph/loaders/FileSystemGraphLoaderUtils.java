package graph.loaders;

import graph.Graph;

import java.io.File;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Utility functions for loading graphs from files on the filesystem
 */
public final class FileSystemGraphLoaderUtils
{
	private FileSystemGraphLoaderUtils()
	{}

	/**
	 * Create a stream of graphs, a graph loaded from each file in the given directory.
	 * Files in the directory are sorted by their name, in order to have a deterministic behaviour.
	 * Subdirectories are not explored
	 *
	 * @param graphDir       The directory in which the files reside
	 * @param parseGraphFile Function to create a graph from a file
	 */
	public static Stream<Graph> loadGraphsFromDirectory(File graphDir, Function<File, Graph> parseGraphFile)
	{
		if (!graphDir.isDirectory())
			throw new IllegalArgumentException("Graph directory (" + graphDir.getPath() + ") is not a directory");

		return Arrays.stream(graphDir.listFiles())
				.filter(File::isFile)
				.sorted()
				.map(parseGraphFile);
	}
}
