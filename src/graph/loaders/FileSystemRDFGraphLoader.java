package graph.loaders;

import graph.Graph;
import graph.GraphFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Utility class for loading graphs from RDF files on the filesystem
 */
public final class FileSystemRDFGraphLoader
{
	private String rdfLang;

	private FileSystemRDFGraphLoader(String rdfLang)
	{
		this.rdfLang = rdfLang;
	}

	private Graph parseGraphFile(File graphFile)
	{
		if (!graphFile.isFile())
			throw new IllegalArgumentException(String.format("Graph file %s should be a regular file", graphFile.getPath()));

		try
		{
			return RDFToGraph.read(new FileInputStream(graphFile), rdfLang, GraphFactory::createDefaultGraph);
		} catch (IOException e)
		{
			e.printStackTrace();
			System.exit(1);
			return null;
		}
	}

	/**
	 * See {@link FileSystemGraphLoaderUtils#loadGraphsFromDirectory(File, Function)}
	 */
	public static Stream<Graph> loadGraphsFromDirectory(File graphDir, String rdfLang)
	{
		FileSystemRDFGraphLoader loader = new FileSystemRDFGraphLoader(rdfLang);
		return FileSystemGraphLoaderUtils.loadGraphsFromDirectory(graphDir, loader::parseGraphFile);
	}

	/**
	 * See {@link FileSystemGraphLoaderUtils#loadGraphsFromDirectory(File, Function)}
	 */
	public static Stream<Graph> loadGraphsFromDirectory(String graphDirPath, String rdfLang)
	{
		return loadGraphsFromDirectory(new File(graphDirPath), rdfLang);
	}

	/**
	 * Load a graph from a file.
	 */
	public static Graph loadGraphFromFile(File graphFile, String rdfLang)
	{
		return new FileSystemRDFGraphLoader(rdfLang).parseGraphFile(graphFile);
	}

	/**
	 * Load a graph from a file.
	 */
	public static Graph loadGraphFromFile(String graphFilePath, String rdfLang)
	{
		return new FileSystemRDFGraphLoader(rdfLang).parseGraphFile(new File(graphFilePath));
	}


}
