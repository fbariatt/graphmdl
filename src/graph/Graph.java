package graph;

import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * A directed multigraph without self loops.
 */
public interface Graph
{
	/**
	 * @return The number of vertices in the graph
	 */
	int getVertexCount();

	/**
	 * @return The number of edges in the graph
	 */
	long getEdgeCount();

	/**
	 * Remove all edges from the graph
	 */
	void clearEdges();

	/**
	 * Remove all vertices and edges from the graph
	 */
	void clear();

	/**
	 * @return i-th vertex in the graph
	 */
	Vertex getVertex(int i);

	/**
	 * Add a vertex without labels to the graph
	 *
	 * @return The newly created vertex
	 */
	Vertex addVertex();

	/**
	 * Add a vertex with one label to the graph.
	 *
	 * @return The newly created vertex
	 */
	Vertex addVertex(String label);

	/**
	 * Add a vertex with a series of labels to the graph.
	 *
	 * @return The newly created vertex
	 */
	Vertex addVertex(Iterable<String> labels);

	/**
	 * Delete vertex i from the graph, and all of its associated edges
	 */
	void delVertex(int i);

	/**
	 * Delete vertex v from the graph, and all of its associated edges
	 */
	void delVertex(Vertex v);

	/**
	 * @return Iterator on all vertices in the graph
	 */
	Stream<Vertex> getVertices();

	/**
	 * @return Iterator on all vertices in the graph with a certain label
	 */
	Stream<Vertex> getVerticesWithLabel(String label);

	/**
	 * Retrieve the current index of a vertex in the graph.
	 * Note that the result of this function is only valid until a vertex is deleted, since vertex deletion shifts all indices.
	 */
	int getVertexIndex(Vertex v);

	/**
	 * Retrieve a map associating each vertex in the graph with its index.
	 * Note that the result of this function is only valid until a vertex is deleted, since vertex deletion shifts all indices.
	 */
	Map<Vertex, Integer> getVertexIndexMap();

	/**
	 * @return A specific edge between two vertices. Null if the edge does not exist.
	 */
	Edge getEdge(int source, int target, String label);

	/**
	 * @return A specific edge between two vertices. Null if the edge does not exist.
	 */
	Edge getEdge(Vertex source, Vertex target, String label);

	/**
	 * @return All edges between two vertices
	 */
	Stream<Edge> getEdges(int source, int target);

	/**
	 * @return All edges between two vertices
	 */
	Stream<Edge> getEdges(Vertex source, Vertex target);

	/**
	 * @return All edges in the graph
	 */
	Stream<Edge> getEdges();

	/**
	 * Add a new edge in the graph, between vertices 'source' and 'target', with label 'label'
	 *
	 * @return The newly created edge
	 */
	Edge addEdge(int source, int target, String label);

	/**
	 * Add a new edge in the graph, between vertices 'source' and 'target', with label 'label'
	 *
	 * @return The newly created edge
	 */
	Edge addEdge(Vertex source, Vertex target, String label);

	/**
	 * Delete a specific edge from the graph
	 */
	void delEdge(Edge e);

	/**
	 * Delete a specific edge from the graph
	 */
	void delEdge(int source, int target, String label);

	/**
	 * Delete a specific edge from the graph
	 */
	void delEdge(Vertex source, Vertex target, String label);

	/**
	 * @return The amount of edges starting at a specific vertex
	 */
	long getOutEdgeCount(int vertex);

	/**
	 * @return The amount of edges starting at a specific vertex
	 */
	long getOutEdgeCount(Vertex v);

	/**
	 * @return The amount of edges ending at a specific vertex
	 */
	long getInEdgeCount(int vertex);

	/**
	 * @return The amount of edges ending at a specific vertex
	 */
	long getInEdgeCount(Vertex v);

	/**
	 * @return The edges starting at a specific vertex
	 */
	Stream<Edge> getOutEdges(int vertex);

	/**
	 * @return The edges starting at a specific vertex
	 */
	Stream<Edge> getOutEdges(Vertex v);

	/**
	 * @return The edges ending at a specific vertex
	 */
	Stream<Edge> getInEdges(int vertex);

	/**
	 * @return The edges ending at a specific vertex
	 */
	Stream<Edge> getInEdges(Vertex v);

	/**
	 * @return Edges that either start or end at a specific vertex
	 */
	Stream<Edge> getInOutEdges(int vertex);

	/**
	 * @return Edges that either start or end at a specific vertex
	 */
	Stream<Edge> getInOutEdges(Vertex v);

	/**
	 * Delete all edges starting at a specific vertex
	 */
	void clearOutEdges(int vertex);

	/**
	 * Delete all edges starting at a specific vertex
	 */
	void clearOutEdges(Vertex v);

	/**
	 * Delete all edges ending at a specific vertex
	 */
	void clearInEdges(int vertex);

	/**
	 * Delete all edges ending at a specific vertex
	 */
	void clearInEdges(Vertex v);

	/**
	 * Delete all edges starting or ending at a specific vertex
	 */
	void clearEdges(int vertex);

	/**
	 * Delete all edges starting or ending at a specific vertex
	 */
	void clearEdges(Vertex v);

	/**
	 * Mark the graph with a specific value
	 */
	void setMarker(int marker);

	/**
	 * @return The value with which the graph is marked
	 */
	int getMarker();

	/**
	 * Add all vertices and edges of another graph to this one, effectively integrating the other graph as a component of this one.
	 */
	void concat(Graph other);

	/**
	 * Return a graph which correspond to the subgraph induced by the given vertices.
	 * The returned graph is an independent copy.
	 *
	 * @param vertices        The vertices that define the induced subgraph
	 * @param subGraphCreator Function used to create a graph instance to be used as the result graph
	 */
	InducedSubgraph inducedSubGraph(Set<Vertex> vertices, Supplier<Graph> subGraphCreator);

	/**
	 * See {@link #inducedSubGraph(Set, Supplier)}
	 */
	InducedSubgraph inducedSubGraphByIndices(Set<Integer> vertices, Supplier<Graph> subGraphCreator);

	/**
	 * Return value of {@link #inducedSubGraph}, containing both the subgraph and a map between the original graph's vertices
	 * and the corresponding subgraph's vertices.
	 */
	class InducedSubgraph
	{
		public final Graph subgraph;
		public final Map<Vertex, Vertex> originalToInduced;

		public InducedSubgraph(Graph subgraph, Map<Vertex, Vertex> originalToInduced)
		{
			this.subgraph = subgraph;
			this.originalToInduced = originalToInduced;
		}
	}

	/**
	 * Check if the graph in its current state is effectively a simple graph.
	 * A simple graph is a graph which has a maximum of one edge between each pair of vertices and that does not contain self-loops.
	 * Note that this method only checks the current state of the graph. A return value of true does not imply that the graph <em>will remain</em> simple,
	 * but just check that it currently is.
	 */
	boolean isEffectivelySimple();
}
