package graph.printers;

/**
 * URI prefixes to be used when loading/writing graph from/to RDF
 */
public final class RDFGraphPrefixes
{
	private static final String BASE_URI = "urn:GraphMDL:";
	/**
	 * To make URIs to uniquely identify vertices in a graph. E.g. "vertex 1", "vertex 2".
	 *
	 * @see graph.printers.GraphToRDF
	 */
	public static final String VERTEX_URI = BASE_URI + "Vertex:";
	/**
	 * To make URIs for vertex labels that are not URIs.
	 *
	 * @see graph.printers.GraphToRDF
	 */
	public static final String VERTEX_LABEL_URI = BASE_URI + "VertexLabel:";
	/**
	 * To make URIs for edge labels that are not URIs
	 *
	 * @see graph.printers.GraphToRDF
	 */
	public static final String EDGE_LABEL_URI = BASE_URI + "EdgeLabel:";
	/**
	 * When a literal is loaded from an RDF file, a vertex label is generated with its value and added to the vertex that represents the literal.
	 *
	 * @see graph.loaders.RDFToGraph
	 */
	public static final String LITERAL_VALUE_URI = BASE_URI + "Value:";
	/**
	 * When a literal with a language tag is loaded from an RDF file, a vertex label with the language value is generated and added to the vertex that represents the literal.
	 *
	 * @see graph.loaders.RDFToGraph
	 */
	public static final String LITERAL_LANG_URI = BASE_URI + "Lang:";

}
