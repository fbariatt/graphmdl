package graph.printers;

import graph.*;
import org.apache.log4j.Logger;
import utils.GeneralUtils;

import java.io.PrintWriter;
import java.util.*;

/**
 * Methods to print a graph in text format
 */
public final class GraphToText
{
	private static final Logger logger = Logger.getLogger(GraphToText.class);

	/**
	 * Print the given graph to the given printer, as text
	 *
	 * @param g              The graph to print
	 * @param out            Where to print the graph
	 * @param useVertexNames Whether vertices should be identified by their number or their name (when available).
	 *                       See {@link Vertex#setName}.
	 * @throws GraphSerializationException in case of problems
	 */
	public static void print(Graph g, PrintWriter out, boolean useVertexNames)
	{
		final int vertexCount = g.getVertexCount();
		out.println("t");
		out.printf("# %d vertices\n", vertexCount);
		out.printf("# %d edges\n", g.getEdgeCount());

		// Printing vertices
		Map<Vertex, String> vertexToName = new HashMap<>();
		Set<String> vertexNames = new HashSet<>(); // To make sure that no two vertices are mapped to the same name
		for (int v = 0; v < vertexCount; ++v)
		{
			final Vertex vertex = g.getVertex(v);

			String vertexName;
			if (useVertexNames && vertex.getName() != null)
			{
				vertexName = vertex.getName();
				if (vertexName.indexOf(' ') != -1)
					logger.warn(String.format("Vertex name '%s' contains spaces: replacing them with underscore to make it safe to re-parse", vertexName));
				vertexName = vertexName.replace(' ', '_');
			}
			else
			{
				vertexName = Integer.toString(v);
			}
			if (!vertexNames.add(vertexName))
				throw new GraphSerializationException(String.format("Duplicate vertex name '%s'", vertexName));
			vertexToName.put(vertex, vertexName);
			out.printf("v %s", vertexName);
			// Printing vertex labels
			vertex.getLabels().keySet().stream()
					.sorted() // If the labels are sorted, it is nicer to read
					.forEachOrdered(label -> {
						if (label.indexOf(' ') != -1)
						{
							logger.warn(String.format("Vertex label '%s' contains spaces: replacing them with underscore to make it safe to re-parse", label));
							label = label.replace(' ', '_');
						}
						out.printf(" %s", label);
					});
			out.write("\n");
		}


		// Printing edges
		g.getEdges()
				.sorted( // If the edges are sorted, it is nicer to read
						// In case vertex names are actually vertex numbers (e.g. when useVertexNames is false) we compare them numerically (so that 10 is after 9 and not after 1)
						Comparator.<Edge, String>comparing(edge -> vertexToName.get(edge.getSource()), GeneralUtils::numericallyCompareStrings)
								.thenComparing(edge -> vertexToName.get(edge.getTarget()), GeneralUtils::numericallyCompareStrings)
								.thenComparing(Edge::getLabel)
				)
				.forEachOrdered(edge -> {
					String edgeLabel = edge.getLabel();
					if (edgeLabel.indexOf(' ') != -1)
					{
						logger.warn(String.format("Edge label '%s' contains spaces: replacing them with underscore to make it safe to re-parse", edgeLabel));
						edgeLabel = edgeLabel.replace(' ', '_');
					}
					out.printf("e %s %s %s\n", vertexToName.get(edge.getSource()), vertexToName.get(edge.getTarget()), edgeLabel);
				});
		out.flush();
	}

	/**
	 * Same as {@link #print(Graph, PrintWriter, boolean)}, but printing to stdout and without using vertex names.
	 */
	public static void print(Graph g)
	{
		print(g, new PrintWriter(System.out, true), false);
	}
}
