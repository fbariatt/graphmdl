package graph.printers;

import graph.Edge;
import graph.Graph;
import graph.GraphSerializationException;
import graph.Vertex;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.datatypes.TypeMapper;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.rdf.model.*;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.expr.*;
import org.apache.jena.sparql.expr.nodevalue.NodeValueNode;
import org.apache.jena.sparql.expr.nodevalue.NodeValueString;
import org.apache.jena.sparql.syntax.ElementFilter;
import org.apache.jena.sparql.syntax.ElementGroup;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import utils.GeneralUtils;

import java.io.OutputStream;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Methods to print a graph in RDF format
 */
public final class GraphToRDF
{
	/**
	 * The default prefixes used when printing a graph in an RDF format that supports prefixes.
	 */
	public static final Map<String, String> defaultPrefixes = Map.of(
			"v", RDFGraphPrefixes.VERTEX_URI,
			"vl", RDFGraphPrefixes.VERTEX_LABEL_URI,
			"e", RDFGraphPrefixes.EDGE_LABEL_URI,
			"xsd", XSD.NS
	);

	/**
	 * Converter to transform a {@link Graph} to a Jena RDF {@link Model}.
	 * This object un-does the transformations that are done by {@link graph.loaders.RDFToGraph}:
	 * <ul>
	 *     <li>Graph vertices that have rdf:nil URI as label are mapped to rdf:nil</li>
	 *     <li>Graph vertices that correspond to literals are mapped to literals</li>
	 *     <li>
	 *         For all other graph vertices:
	 *         <ul>
	 *             <li>If {@link Vertex#getName()} returns a value, that value is used as the node's URI (if it is a URI) or converted to a URI by adding a prefix. Otherwise, a URI is generated for the vertex.</li>
	 *             <li>Each label is transformed into a rdf:type edge</li>
	 *         </ul>
	 *     </li>
	 *     <li>Edges are transformed into triples, and the edge label is the triple's predicate URI</li>
	 * </ul>
	 *
	 * @see RDFGraphPrefixes
	 * @see PatternToQuery
	 */
	public static class GraphToModel
	{
		private final Model model;
		private final Map<Vertex, RDFNode> verticesAsNodes;


		/**
		 * @param g         The graph from which to create the model.
		 * @param isURI     A predicate to deciding which strings (vertex labels and names) may be used as URIs as they are.
		 * @param isLiteral A predicate deciding whether a particular vertex should be transformed into a literal.
		 * @throws GraphSerializationException in case of problems.
		 */
		public GraphToModel(Graph g, Predicate<String> isURI, Predicate<Vertex> isLiteral)
		{
			this.model = ModelFactory.createDefaultModel();
			this.verticesAsNodes = new HashMap<>();

			// Parsing vertices
			Set<String> vertexURIs = new HashSet<>(); // All URIs used to represent vertices, to make sure that no two vertices are matched to the same URI
			for (int v = 0; v < g.getVertexCount(); ++v)
			{
				final Vertex vertex = g.getVertex(v);
				if (vertex.getLabels().size() == 1 && vertex.getLabels().keySet().stream().anyMatch(label -> label.equals(RDF.nil.getURI()))) // Vertex corresponds to rdf:nil
				{
					if (g.getOutEdges(vertex).findAny().isPresent())
						throw new GraphSerializationException("Vertex that is to be mapped to rdf:nil has outgoing edges");
					verticesAsNodes.put(vertex, RDF.nil);
				}
				else if (isLiteral.test(vertex)) // Vertex corresponds to a literal
				{
					if (g.getOutEdges(vertex).findAny().isPresent())
						throw new GraphSerializationException("Impossible to create literal from vertex: it has outgoing edges");
					verticesAsNodes.put(vertex, vertexToLiteral(vertex));
				}
				else // Vertex is neither rdf:nil nor a literal
				{
					// We always create a URI for the vertex, since in our tests Jena seems to have problems
					// sometimes when serializing blank nodes to turtle
					String vertexURI;
					if (vertex.getName() == null)
					{
						vertexURI = RDFGraphPrefixes.VERTEX_URI + v;
					}
					else
					{
						if (isURI.test(vertex.getName()))
							vertexURI = vertex.getName();
						else
							vertexURI = RDFGraphPrefixes.VERTEX_URI + vertex.getName(); // If the vertex name is not a URI, we make one from it
					}
					if (!vertexURIs.add(vertexURI))
						throw new GraphSerializationException(String.format("Two vertices map to the same URI %s when serializing to RDF", vertexURI));

					Resource resource = model.createResource(vertexURI);
					verticesAsNodes.put(vertex, resource);
					// Adding vertex labels
					vertex.getLabels().keySet().stream()
							.map(label -> isURI.test(label) ? label : RDFGraphPrefixes.VERTEX_LABEL_URI + label) // If the vertex label does not correspond to a URI, we make it into one
							.forEach(label -> resource.addProperty(RDF.type, model.createResource(label))); // Vertex labels are added as rdf:type edges of the vertex
				}
			}

			// Parsing edges
			g.getEdges().forEach(edge -> {
				String label = edge.getLabel();
				if (!isURI.test(label))
					label = RDFGraphPrefixes.EDGE_LABEL_URI + label;
				model.add(
						verticesAsNodes.get(edge.getSource()).asResource(), // If the vertex has out edges we can be sure that it is not a literal
						model.createProperty(label),
						verticesAsNodes.get(edge.getTarget())
				);
			});
		}

		/**
		 * Convert the given vertex to a literal, assuming that it can be done.
		 * Un-does the transformations of {@link graph.loaders.RDFToGraph}.
		 *
		 * @throws GraphSerializationException if the conversion is impossible.
		 */
		private Literal vertexToLiteral(Vertex v)
		{
			String value = GeneralUtils.exactlyOneElement(
					v.getLabels().keySet().stream()
							.filter(label -> label.startsWith(RDFGraphPrefixes.LITERAL_VALUE_URI))
							.map(valueLabel -> valueLabel.substring(RDFGraphPrefixes.LITERAL_VALUE_URI.length())),
					() -> new GraphSerializationException("Impossible to create literal from vertex: can not correctly find label describing the value."),
					() -> new GraphSerializationException("Impossible to create literal from vertex: found multiple labels that describe the value")
			);
			Optional<String> language = v.getLabels().keySet().stream()
					.filter(label -> label.startsWith(RDFGraphPrefixes.LITERAL_LANG_URI))
					.map(languageLabel -> languageLabel.substring(RDFGraphPrefixes.LITERAL_LANG_URI.length()))
					.findAny();
			if (language.isPresent())
			{
				return model.createLiteral(value, language.get());
			}
			else
			{
				// If the literal does not have a language, then it has a type (represented by a label that is not the value label)
				String dataType = GeneralUtils.exactlyOneElement(
						v.getLabels().keySet().stream()
								.filter(label -> !label.startsWith(RDFGraphPrefixes.LITERAL_VALUE_URI)),
						() -> new GraphSerializationException("Impossible to create literal from vertex: can not find label describing the datatype"),
						() -> new GraphSerializationException("Impossible to create literal from vertex: found multiple labels that could be the datatype")
				);
				return model.createTypedLiteral(value, dataType);
			}
		}

		/**
		 * @return The RDF representation of the data.
		 */
		public Model getModel()
		{
			return model;
		}

		/**
		 * @return A conversion between each vertex in the original graph and the corresponding node in the RDF model.
		 * Note that multiple graph vertices may be mapped to the same model node (e.g. if multiple vertices are converted to the same literal, or multiple vertices correspond to rdf:nil).
		 */
		public Map<Vertex, RDFNode> getVerticesAsNodes()
		{
			return verticesAsNodes;
		}
	}

	/**
	 * Convert the given graph to RDF and print it.
	 * Mainly intended for debugging.
	 *
	 * @param g          The graph to convert
	 * @param out        Where the converted graph will be printed
	 * @param lang       RDF lang to use. See {@link Model#write(OutputStream, String)}
	 * @param nsPrefixes Namespace prefixes that should be used in the serialization, ignored if the serialization does not support prefixes.
	 * @implNote This method simply calls {@link GraphToModel} and some methods on the resulting model.
	 */
	public static void print(Graph g, OutputStream out, String lang, Map<String, String> nsPrefixes)
	{
		Model graphAsModel = new GraphToModel(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel).getModel();
		graphAsModel.setNsPrefixes(nsPrefixes);
		graphAsModel.write(out, lang);
	}

	/**
	 * Convert a graph to RDF and print its Turtle representation on standard output.
	 * Mainly intended for debugging.
	 */
	public static void print(Graph g)
	{
		print(g, System.out, "TTL", defaultPrefixes);
	}


	/**
	 * Possible implementation of a {@link Predicate} on strings that says that a string is a valid URI if it contains a colon (':').
	 * It is simple but it works quite well.
	 */
	public static boolean isURIIfHasColon(String s)
	{
		return s.indexOf(':') != -1;
	}


	/**
	 * Possible implementation of a {@link Predicate} on vertices that says that a graph vertex should be converted to an RDF literal
	 * if it has a label that starts with {@link RDFGraphPrefixes#LITERAL_VALUE_URI} (which is how literals are transformed by {@link graph.loaders.RDFToGraph}).
	 */
	public static boolean isLiteralIfHasValueLabel(Vertex v)
	{
		return v.getLabels().keySet().stream().anyMatch(label -> label.startsWith(RDFGraphPrefixes.LITERAL_VALUE_URI));
	}

	/**
	 * Creates a {@link Predicate} on vertices that will say that a graph vertex should be converted to an RDF literal if it has a label
	 * that is contained in the given set of labels.
	 * Intended to be used when converting patterns to RDF queries, where vertices may not have a "value" label but may have a label that corresponds to a literal datatype.
	 */
	public static Predicate<Vertex> makeIsLiteralIfHasLabelIn(Set<String> literalLabels)
	{
		return v -> v.getLabels().keySet().stream().anyMatch(literalLabels::contains);
	}

	/**
	 * Create a {@link Predicate} on vertices that is equivalent to the combination of {@link #isLiteralIfHasValueLabel} and {@link #makeIsLiteralIfHasLabelIn(Set)}.
	 */
	public static Predicate<Vertex> makeIsLiteralIfHasValueOrLabelIn(Set<String> literalLabels)
	{
		return v -> v.getLabels().keySet().stream().anyMatch(label -> label.startsWith(RDFGraphPrefixes.LITERAL_VALUE_URI) || literalLabels.contains(label));
	}

	/**
	 * Converter to transform a {@link Graph} to a Jena RDF {@link Query}, un-doing the transformations done by {@link graph.loaders.RDFToGraph}.
	 * This is the companion class of {@link GraphToModel}: the latter converts the data and this class converts patterns so that they can be matched on the data.
	 *
	 * @see GraphToModel
	 */
	public static class PatternToQuery
	{
		private List<Node> selectVars; // Variables that would appear in the SELECT part of the query
		private List<Triple> triplePatterns; // Triple patterns corresponding to the body of the query
		private Optional<Expr> bodyFilterExpr; // FILTER expression for constraints that complete the body triple pattern
		private Optional<Expr> isomorphismFilterExp; // FILTER expression to make matching isomorphic
		private Map<Vertex, Node> verticesAsNodes; // Pattern vertex -> query node

		/**
		 * @param pattern         The graph that should be converted to a query.
		 * @param isUri           A predicate to deciding which strings (vertex labels and names) may be used as URIs as they are.
		 * @param isLiteral       A predicate deciding whether a particular vertex should be transformed into a literal.
		 * @param ignoredVertices The vertices in this set will be considered as not present in the graph for the purpose of creating the pattern.
		 * @param ignoredEdges    The edges in this set will be considered as not present in the graph for the purpose of creating the pattern.
		 * @throws GraphSerializationException in case of problems.
		 */
		public PatternToQuery(Graph pattern, Predicate<String> isUri, Predicate<Vertex> isLiteral, Set<Vertex> ignoredVertices, Set<Edge> ignoredEdges)
		{
			this.selectVars = new ArrayList<>();
			triplePatterns = new ArrayList<>();
			this.bodyFilterExpr = Optional.empty();
			this.isomorphismFilterExp = Optional.empty();
			this.verticesAsNodes = new HashMap<>();

			// Parsing pattern vertices
			List<Integer> isomorphismFilterVertices = new ArrayList<>(); // The vertices that should appear in the filter expression if the match is to be isomorphic
			for (int v = 0; v < pattern.getVertexCount(); ++v)
			{
				final Vertex vertex = pattern.getVertex(v);
				if (ignoredVertices.contains(vertex))
					continue;

				if (vertex.getLabels().size() == 1 && vertex.getLabels().containsKey(RDF.nil.getURI())) // Vertex corresponds to rdf:nil
				{
					if (pattern.getOutEdges(vertex).findAny().isPresent())
						throw new GraphSerializationException("Vertex that is to be mapped to rdf:nil has outgoing edges");

					verticesAsNodes.put(vertex, NodeFactory.createURI(RDF.nil.getURI()));
					// rdf:nil nodes should not appear in the select variables and the disjoint filter
				}
				else if (isLiteral.test(vertex)) // The pattern vertex corresponds to a literal
				{
					if (pattern.getOutEdges(vertex).findAny().isPresent())
						throw new GraphSerializationException("Vertex that is to be mapped to literal has outgoing edges");

					Optional<String> value = vertex.getLabels().keySet().stream()
							.filter(label -> label.startsWith(RDFGraphPrefixes.LITERAL_VALUE_URI))
							.map(valueLabel -> valueLabel.substring(RDFGraphPrefixes.LITERAL_VALUE_URI.length()))
							.findAny();
					Optional<String> language = vertex.getLabels().keySet().stream()
							.filter(label -> label.startsWith(RDFGraphPrefixes.LITERAL_LANG_URI))
							.map(languageLabel -> languageLabel.substring(RDFGraphPrefixes.LITERAL_LANG_URI.length()))
							.findAny();
					Optional<RDFDatatype> dataType = vertex.getLabels().keySet().stream()
							.filter(label -> !label.startsWith(RDFGraphPrefixes.LITERAL_VALUE_URI) && !label.startsWith(RDFGraphPrefixes.LITERAL_LANG_URI))
							.findAny()
							.flatMap(label -> Optional.ofNullable(TypeMapper.getInstance().getTypeByName(label)));
					if (value.isPresent()) // The literal has a value (it can simply be inserted in the body as a literal constant)
					{
						if (language.isPresent())
							verticesAsNodes.put(vertex, NodeFactory.createLiteral(value.get(), language.get()));
						else if (dataType.isPresent())
							verticesAsNodes.put(vertex, NodeFactory.createLiteral(value.get(), dataType.get()));
						else
							verticesAsNodes.put(vertex, NodeFactory.createLiteral(value.get()));
					}
					else // The literal does not have a value (it must be inserted in the body as a variable plus some eventual constraints)
					{
						// Creating a variable
						Node vertexNode = Var.alloc(Integer.toString(v));
						verticesAsNodes.put(vertex, vertexNode);
						// Since this node is a variable, it should appear in the select part of the query
						selectVars.add(vertexNode);
						// If it has a language or a datatype, we must add those constraints to the body's filter
						if (language.isPresent())
							this.addToBodyFilter(new E_Equals(new E_Lang(new ExprVar(vertexNode)), new NodeValueString(language.get())));
						else if (dataType.isPresent())
							this.addToBodyFilter(new E_Equals(new E_Datatype(new ExprVar(vertexNode)), new NodeValueNode(NodeFactory.createURI(dataType.get().getURI()))));
					}
				}
				else // The pattern vertex is neither rdf:nil nor a literal: converted to a variable plus rdf:type edges
				{
					Node vertexNode = Var.alloc(Integer.toString(v));
					verticesAsNodes.put(vertex, vertexNode);
					selectVars.add(vertexNode); // Since it is a variable, it should appear in the select part of the query
					isomorphismFilterVertices.add(v); // In case of isomorphism, this vertex should be mapped to a different value to other vertices
					// Vertex labels will be matched to rdf:type triple
					vertex.getLabels().keySet().stream()
							.map(label -> isUri.test(label) ? label : RDFGraphPrefixes.VERTEX_LABEL_URI + label) // Making sure that labels are valid URIs
							.map(uriLabel -> new Triple(vertexNode, NodeFactory.createURI(RDF.type.getURI()), NodeFactory.createURI(uriLabel)))
							.forEach(triplePatterns::add);
				}
			}

			// Creating filter expression that can make mapping isomorphic
			if (isomorphismFilterVertices.size() >= 2)
			{
				Function<Integer, ExprVar> varOfVertex = v -> new ExprVar(verticesAsNodes.get(pattern.getVertex(isomorphismFilterVertices.get(v))));

				// For each vertex x, add an expression ?x NOT IN(?x+1, ?x+2, ...)
				for (int i = 0; i <= isomorphismFilterVertices.size() - 3; ++i)
				{
					ExprVar current = varOfVertex.apply(i);
					ExprList following = new ExprList();
					for (int j = i + 1; j < isomorphismFilterVertices.size(); j++)
					{
						following.add(varOfVertex.apply(j));
					}
					this.addToIsomorphismFilter(new E_NotOneOf(current, following));
				}
				// For the last two vertices, add an expression ?x != ?y
				this.addToIsomorphismFilter(new E_NotEquals(varOfVertex.apply(isomorphismFilterVertices.size() - 2), varOfVertex.apply(isomorphismFilterVertices.size() - 1)));
			}

			// Parsing pattern edges as triples
			Map<String, Node> edgeLabelsAsNodes = new HashMap<>();
			pattern.getEdges()
					.filter(e -> !ignoredEdges.contains(e) && !ignoredVertices.contains(e.getSource()) && !ignoredVertices.contains(e.getTarget()))
					.map(e -> new Triple(
							verticesAsNodes.get(e.getSource()),
							edgeLabelsAsNodes.computeIfAbsent(e.getLabel(), label -> NodeFactory.createURI(edgeLabelToUri(label, isUri))),
							verticesAsNodes.get(e.getTarget())
					))
					.forEach(triplePatterns::add);
		}

		/**
		 * @param pattern   The graph that should be converted to a query.
		 * @param isUri     A predicate to deciding which strings (vertex labels and names) may be used as URIs as they are.
		 * @param isLiteral A predicate deciding whether a particular vertex should be transformed into a literal.
		 * @throws GraphSerializationException in case of problems.
		 */
		public PatternToQuery(Graph pattern, Predicate<String> isUri, Predicate<Vertex> isLiteral)
		{
			this(pattern, isUri, isLiteral, Collections.emptySet(), Collections.emptySet());
		}

		/**
		 * Transform an edge label to a URI if it is not one already.
		 */
		public static String edgeLabelToUri(String edgeLabel, Predicate<String> isUri)
		{
			return isUri.test(edgeLabel) ? edgeLabel : RDFGraphPrefixes.EDGE_LABEL_URI + edgeLabel;
		}

		/**
		 * Modify the body filter expression to be the previous expression AND the given one.
		 */
		private void addToBodyFilter(Expr e)
		{
			this.bodyFilterExpr = this.bodyFilterExpr
					.<Expr>map(expr -> new E_LogicalAnd(expr, e))
					.or(() -> Optional.of(e));
		}

		/**
		 * Modify the isomorphism filter expression to be the previous expression AND the given one.
		 */
		private void addToIsomorphismFilter(Expr e)
		{
			this.isomorphismFilterExp = this.isomorphismFilterExp
					.<Expr>map(expr -> new E_LogicalAnd(expr, e))
					.or(() -> Optional.of(e));
		}

		/**
		 * Create a <em>new</em> query instances that corresponds to the pattern.
		 *
		 * @param useIsomorphism If true, a FILTER clause is added to the query to make sure that each pattern vertex is mapped to a different data vertex.
		 * @param selectVars     The variables to use in the SELECT part of the query
		 */
		public Query getSelectQuery(boolean useIsomorphism, List<Node> selectVars)
		{
			Query query = new Query();
			query.setQuerySelectType();

			// Adding variables in SELECT
			selectVars.forEach(query::addResultVar);

			// Creating WHERE body
			ElementGroup queryBody = new ElementGroup();
			this.triplePatterns.forEach(queryBody::addTriplePattern);

			// Adding FILTER to body
			if (useIsomorphism && this.isomorphismFilterExp.isPresent()) // If isomorphism is requested and a filter is needed to make isomorphism
			{
				queryBody.addElementFilter(new ElementFilter(
						this.bodyFilterExpr // Combining the two filters if both present or just using the isomorphism one
								.<Expr>map(bodyFilter -> new E_LogicalAnd(bodyFilter, this.isomorphismFilterExp.get()))
								.orElse(this.isomorphismFilterExp.get())
				));
			}
			else // Isomorphism is not requested or a filter is not needed to achieve it
			{
				// We just add the body filter if one is needed
				this.bodyFilterExpr.ifPresent(e -> queryBody.addElementFilter(new ElementFilter(e)));
			}

			// Adding body to query
			query.setQueryPattern(queryBody);

			return query;
		}

		/**
		 * Create a <em>new</em> query instances that corresponds to the pattern.
		 *
		 * @param useIsomorphism If true, a FILTER clause is added to the query to make sure that each pattern vertex is mapped to a different data vertex.
		 */
		public Query getSelectQuery(boolean useIsomorphism)
		{
			return getSelectQuery(useIsomorphism, this.selectVars);
		}

		/**
		 * The pattern corresponds to an empty query.
		 * This can happen if all vertices can be mapped to literal expressions.
		 * In this case, it is not meaningful to match the pattern to the data, as the mapping of its vertices
		 * can be retrieved directly from this object.
		 */
		public boolean isEmpty()
		{
			return this.selectVars.isEmpty() && this.triplePatterns.isEmpty() && this.bodyFilterExpr.isEmpty();
		}

		/**
		 * @return The variables will be included in the SELECT part of the query.
		 */
		public List<Node> getSelectVars()
		{
			return selectVars;
		}

		/**
		 * @return The triples in the WHERE part of the query.
		 */
		public List<Triple> getTriplePattern()
		{
			return triplePatterns;
		}

		/**
		 * @return FILTER expression that completes the triple patterns for the WHERE part of the query.
		 */
		public Optional<Expr> getBodyFilterExpr()
		{
			return bodyFilterExpr;
		}

		/**
		 * @return FILTER expression that can be optionally added to the query to make sure that each pattern vertex gets mapped to a different data vertex.
		 */
		public Optional<Expr> getIsomorphismFilterExp()
		{
			return isomorphismFilterExp;
		}

		/**
		 * @return A mapping between each graph vertex and its corresponding query node.
		 * Note that multiple graph vertices may be mapped to the same query node (e.g. if multiple vertices are converted to the same literal,
		 * or multiple vertices correspond to rdf:nil).
		 */
		public Map<Vertex, Node> getVerticesAsNodes()
		{
			return verticesAsNodes;
		}
	}
}
