package graph;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

public class VertexImpl implements Vertex
{
	private Map<String, VertexLabel> labels;
	private String name;
	// In and out edges: mapping each neighbour to the edges between this vertex and the neighbour, edges are stored by label
	private Map<VertexImpl, Map<String, EdgeImpl>> outEdges;
	private Map<VertexImpl, Map<String, EdgeImpl>> inEdges;
	private int isPortMarker = -1;

	/**
	 * @param name An optional vertex name, to be used when displaying the vertex
	 */
	VertexImpl(String name)
	{
		this.labels = new HashMap<>();
		this.name = name;
		this.outEdges = new HashMap<>();
		this.inEdges = new HashMap<>();
	}

	VertexImpl()
	{
		this(null);
	}

	@Override
	public Map<String, VertexLabel> getLabels()
	{
		return this.labels;
	}

	@Override
	public Vertex addLabel(VertexLabel label)
	{
		this.labels.put(label.getLabelString(), label);
		return this;
	}

	@Override
	public Vertex addLabel(String label)
	{
		return this.addLabel(new VertexLabel(label));
	}

	@Override
	public void delLabel(String label)
	{
		if (this.labels.remove(label) == null)
			throw new NoSuchElementException("Trying to remove a non-existing label from a vertex");
	}

	@Override
	public void delLabel(VertexLabel label)
	{
		delLabel(label.getLabelString());
	}

	@Override
	public void clearLabels()
	{
		this.labels.clear();
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Add a new edge connecting to a specified vertex, with a specified label.
	 * If a duplicate edge already existed, nothing is done.
	 *
	 * @return The newly created edge
	 */
	EdgeImpl addEdge(VertexImpl target, String label)
	{
		EdgeImpl edge = this.outEdges
				.computeIfAbsent(target, k -> new HashMap<>())
				.get(label);
		if (edge == null) // Edge doesn't exist, we create it and add it
		{
			edge = new EdgeImpl(this, target, label);
			this.outEdges
					.get(target)
					.put(label, edge);
			target.inEdges
					.computeIfAbsent(this, k -> new HashMap<>())
					.put(label, edge);
		}
		return edge;
	}

	/**
	 * Get a specific edge between this vertex and another.
	 */
	EdgeImpl getEdge(VertexImpl target, String label)
	{
		Map<String, EdgeImpl> edgesWithTarget = this.outEdges.get(target);
		if (edgesWithTarget == null)
			return null;
		return edgesWithTarget.get(label);
	}

	/**
	 * @return All edges connecting this vertex to a specified vertex
	 */
	Stream<EdgeImpl> getOutEdgesWith(VertexImpl target)
	{
		if (this.outEdges.get(target) != null)
			return this.outEdges.get(target).values().stream();
		else
			return Stream.empty();
	}

	/**
	 * Delete a specific edge between this vertex and another
	 */
	void delEdge(VertexImpl target, String label)
	{
		EdgeImpl edge = getEdge(target, label);
		if (edge == null)
			throw new IllegalArgumentException("Error: trying to delete an edge that doesn't exist");
		this.outEdges.get(target).remove(label);
		target.inEdges.get(this).remove(label);
	}

	/**
	 * @return All edges connecting this vertex to another
	 */
	Stream<EdgeImpl> getOutEdges()
	{
		return this.outEdges.values().stream().flatMap(edgesWithTarget -> edgesWithTarget.values().stream());
	}

	/**
	 * @return All edges connecting another vertex to this one.
	 * Note that this vertex will therefore be in the "target" field of the edges.
	 */
	Stream<EdgeImpl> getInEdges()
	{
		return this.inEdges.values().stream().flatMap(edgesWithTarget -> edgesWithTarget.values().stream());
	}

	/**
	 * @return Number of edges starting at this vertex
	 */
	long getOutEdgeCount()
	{
		return this.outEdges.values().stream().mapToLong(Map::size).sum();
	}

	/**
	 * @return Number of edges ending at this vertex
	 */
	long getInEdgeCount()
	{
		return this.inEdges.values().stream().mapToLong(Map::size).sum();
	}

	/**
	 * Delete all outgoing edges
	 */
	void clearOutEdges()
	{
		// Removing corresponding in-edges from neighbours
		this.outEdges.keySet().iterator().forEachRemaining(vertex -> vertex.inEdges.remove(this));
		this.outEdges.clear();
	}

	/**
	 * Delete all ingoing edges
	 */
	void clearInEdges()
	{
		// Removing corresponding out-edges from neighbours
		this.inEdges.keySet().iterator().forEachRemaining(vertex -> vertex.outEdges.remove(this));
		this.inEdges.clear();
	}

	/**
	 * Delete both in and out edges
	 */
	void clearEdges()
	{
		this.clearOutEdges();
		this.clearInEdges();
	}

	@Override
	public int getIsPortMarker()
	{
		return isPortMarker;
	}

	@Override
	public void setIsPortMarker(int isPortMarker)
	{
		this.isPortMarker = isPortMarker;
	}

	@Override
	public String toString()
	{
		StringBuilder labels = new StringBuilder();
		this.labels.keySet().forEach(labelString -> {
			labels.append(labelString);
			labels.append(", ");
		});
		return "VertexImpl{" +
				"labels={" + labels.toString() +
				"}, name='" + name + '\'' +
				'}';
	}

	/**
	 * Function to help computing {@link Graph#isEffectivelySimple()} for {@link GraphImpl}.
	 */
	boolean isEffectivelySimpleVertex()
	{
		return this.outEdges.entrySet().stream().allMatch(outEdgesByTarget ->
				outEdgesByTarget.getKey() != this // The edge is not a self-loop
						&& outEdgesByTarget.getValue().size() <= 1 // There is no more than one edge with this neighbour
		);
	}
}
