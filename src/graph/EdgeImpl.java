package graph;

import java.util.Objects;

public class EdgeImpl implements Edge
{
	private Vertex source;
	private Vertex target;
	private String label;
	private int marker = -1;

	public EdgeImpl(Vertex source, Vertex target, String label)
	{
		this.source = source;
		this.target = target;
		this.label = label;
	}

	@Override
	public Vertex getSource()
	{
		return this.source;
	}

	@Override
	public Vertex getTarget()
	{
		return this.target;
	}

	@Override
	public String getLabel()
	{
		return this.label;
	}

	@Override
	public int getMarker()
	{
		return marker;
	}

	@Override
	public void setMarker(int marker)
	{
		this.marker = marker;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EdgeImpl edge = (EdgeImpl) o;
		return Objects.equals(source, edge.source) &&
				Objects.equals(target, edge.target) &&
				Objects.equals(label, edge.label);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(source, target, label);
	}

	@Override
	public String toString()
	{
		return "EdgeImpl{" +
				"source=" + source +
				", target=" + target +
				", label='" + label + '\'' +
				", marker=" + marker +
				'}';
	}
}
