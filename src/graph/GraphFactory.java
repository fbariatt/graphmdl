package graph;

import graph.automorphisms.Automorphism;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class GraphFactory
{
	private GraphFactory() {}

	/**
	 * Create a graph using the default implementation
	 */
	public static Graph createDefaultGraph()
	{
		return new GraphImpl();
	}

	public static Graph createSimpleGraph()
	{
		return new SimpleGraphImpl();
	}

	/**
	 * Create a graph that is a copy of another graph (i.e. it has the same structure).
	 * Warning: it may take a long time since every vertex and edge need to be re-created.
	 */
	public static Graph copy(Graph g)
	{
		Graph newGraph = supplierOfSameType(g).get();
		newGraph.concat(g);
		return newGraph;
	}

	public static Graph fromGraphWithAutomorphism(Graph g, Automorphism automorphism)
	{
		Graph newGraph = supplierOfSameType(g).get();
		Map<Vertex, Vertex> originalToNew = new HashMap<>();
		for (int v = 0; v < g.getVertexCount(); ++v)
		{
			final int vertexIndexFollowingAutomorphism = automorphism.getMapping(v);
			final Vertex vertexFollowingAutomorphism = g.getVertex(vertexIndexFollowingAutomorphism);
			Vertex newVertex = newGraph.addVertex(vertexFollowingAutomorphism.getLabels().keySet());
			originalToNew.put(vertexFollowingAutomorphism, newVertex);
		}
		g.getEdges().forEach(originalEdge -> newGraph.addEdge(
				originalToNew.get(originalEdge.getSource()),
				originalToNew.get(originalEdge.getTarget()),
				originalEdge.getLabel()
		));
		return newGraph;
	}

	/**
	 * @return A supplier instantiating graphs of the same type as the given one
	 */
	private static Supplier<Graph> supplierOfSameType(Graph g)
	{
		if (g.getClass() == SimpleGraphImpl.class)
			return SimpleGraphImpl::new;
		if (g.getClass() == GraphImpl.class)
			return GraphImpl::new;
		throw new IllegalArgumentException(String.format("Impossible to create supplier of a %s graph. Is a case missing in this function?", g.getClass()));
	}

}
