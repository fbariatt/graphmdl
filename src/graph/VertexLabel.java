package graph;

import java.util.Objects;

public class VertexLabel
{
	private final String labelString;
	private int marker;

	public VertexLabel(String labelString, int marker)
	{
		this.labelString = labelString;
		this.marker = marker;
	}

	public VertexLabel(String labelString)
	{
		this.labelString = labelString;
		this.marker = -1;
	}

	public VertexLabel(VertexLabel vl)
	{
		this.labelString = vl.labelString;
		this.marker = vl.marker;
	}

	public String getLabelString()
	{
		return labelString;
	}

	public int getMarker()
	{
		return marker;
	}

	public void setMarker(int marker)
	{
		this.marker = marker;
	}

	/**
	 * @return Whether this label is equal to another. Two labels are considered equal if their label strings are equal.
	 */
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		VertexLabel that = (VertexLabel) o;
		return Objects.equals(labelString, that.labelString);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(labelString);
	}

	@Override
	public String toString()
	{
		return "VertexLabel{" +
				"labelString='" + labelString + '\'' +
				", marker=" + marker +
				'}';
	}
}
