package utils;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * This class contains utility functions for interacting with maps.
 */
public final class MapUtils
{
	private MapUtils() {}


	/**
	 * Reverse the key/value mapping of a map.
	 *
	 * @return A new map that associates to each original value all the original keys that pointed to that value.
	 * @implNote Current implementation uses {@link HashMap} for the map and {@link HashSet} for the sets.
	 */
	public static <Key, Value> Map<Value, Set<Key>> reverse(Map<Key, Value> map)
	{
		Map<Value, Set<Key>> result = new HashMap<>();
		for (Map.Entry<Key, Value> entry : map.entrySet())
		{
			result.computeIfAbsent(entry.getValue(), _k -> new HashSet<>()).add(entry.getKey());
		}
		return result;
	}

	/**
	 * Reverse the key/value mapping of a map.
	 * This method assumes that the original map represents an injection, i.e. each original value is associated
	 * to only one original key.
	 *
	 * @return A new map that associates to each original value the original key that pointed to that value.
	 * @throws IllegalArgumentException if multiple keys have the same value in the original map.
	 * @implNote Current implementation returns an instance of {@link HashMap}.
	 */
	public static <Key, Value> Map<Value, Key> reverseInjective(Map<Key, Value> map)
	{
		Map<Value, Key> result = new HashMap<>();
		for (Map.Entry<Key, Value> entry : map.entrySet())
		{
			if (result.containsKey(entry.getValue()))
				throw new IllegalArgumentException("Impossible to create inverse map: multiple keys have the same value " + entry.getValue());
			result.put(entry.getValue(), entry.getKey());
		}
		return result;
	}

	/**
	 * Creates a new map whose content is the union of the two given maps.
	 * If maps share some keys, the values of the second map are used.
	 *
	 * @implNote Current implementation returns an instance of {@link HashMap}.
	 */
	public static <K, V> Map<K, V> union(Map<K, V> mapA, Map<K, V> mapB)
	{
		Map<K, V> result = new HashMap<>();
		result.putAll(mapA);
		result.putAll(mapB);
		return result;
	}

	/**
	 * Try to get a value from a map. If the value can not be retrieved, throws a runtime exception using the given supplier.
	 *
	 * @implNote <b>A value of null is considered as missing</b> and an exception will be thrown. Therefore, the map should not
	 * contain null values for this method to work correctly.
	 */
	public static <K, V> V mapGetOrThrowRuntime(Map<K, V> map, K key, Supplier<RuntimeException> exceptionSupplier)
	{
		V result = map.get(key);
		if (result == null)
			throw exceptionSupplier.get();
		return result;
	}

	/**
	 * @return A <em>sorted</em> stream of the keys of a map, where the order of the keys is determined by the value associated to each key.
	 * @implNote Note that this function calls {@link Stream#sorted(Comparator)}, which is a stateful intermediate operation.
	 */
	public static <K, V> Stream<K> mapKeysSortedByValues(Map<K, V> map, Comparator<V> comparator)
	{
		return map.keySet().stream()
				.sorted(Comparator.comparing(map::get, comparator));
	}

	/**
	 * Transform a K->V map into a K->NewV map keeping the same keys and computing the associated valued with the given function.
	 *
	 * @param original        The map to transform. It is not modified.
	 * @param mappingFunction Function that computes a new value for each original key/value pair.
	 * @implNote Currently uses a {@link HashMap} for the new map.
	 */
	public static <K, V, NewV> Map<K, NewV> mapFromMap(Map<K, V> original, BiFunction<K, V, NewV> mappingFunction)
	{
		Map<K, NewV> newMap = new HashMap<>();
		original.forEach((k, v) -> newMap.put(k, mappingFunction.apply(k, v)));
		return newMap;
	}

	/**
	 * Given a map, increment the integer value associated with the given key by the given amount.
	 * If there is no value associated to the given key, treat it as if 0 was present.
	 *
	 * @deprecated It is actually just as easy to use {@link Map#merge} (see implementation).
	 */
	@Deprecated
	public static <K> void incrementOrCreateMapValue(Map<K, Integer> map, K key, int incrementAmount)
	{
		map.merge(key, incrementAmount, Integer::sum);
	}
}
