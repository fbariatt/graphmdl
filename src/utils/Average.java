package utils;

/**
 * Average and standard deviation computation.
 * This class uses Welford's algorithm to compute a running average and SD.
 * See https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
 */
public class Average
{
	private int count = 0;
	private double average = 0.0;
	private double squaredDelta = 0.0;

	public void addValue(double v)
	{
		count++;
		double delta1 = v - average;
		average += delta1 / count;
		double delta2 = v - average;
		squaredDelta += delta1 * delta2;
	}

	public double getAverage()
	{
		return average;
	}

	public double getVariance()
	{
		return squaredDelta / (count - 1);
	}

	public double getStandardDeviation()
	{
		return Math.sqrt(getVariance());
	}
}
