package utils;

/**
 * Functional interface for a function that takes no arguments and produces no result.
 * In practice this class is the same as {@link Runnable}, but semantically it is not related to multithreaded code.
 */
@FunctionalInterface
public interface Procedure
{
	/**
	 * Do something, potentially producing some side effects.
	 * Implementation-defined.
	 */
	void run();

	/**
	 * @return A new procedure which correspond to executing this procedure and then the given one.
	 */
	default Procedure andThen(Procedure following)
	{
		return () -> {
			this.run();
			following.run();
		};
	}
}
