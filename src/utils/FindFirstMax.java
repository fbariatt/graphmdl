package utils;

import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

/**
 * Utility functions to find the first of the elements of an Iterable/Stream that compare the highest for a given comparison criterion
 * Also see {@link FindFirstMin}.
 * Implementation detail: this class actually uses {@link FindFirstMin} and just inverses the comparators.
 */
public final class FindFirstMax
{
	private FindFirstMax()
	{}

	/**
	 * Find the first of the elements in the given iterable that compare the highest with the given comparator
	 *
	 * @throws NoSuchElementException if the iterable is empty
	 */
	public static <T> T find(Iterable<T> iterable, Comparator<T> comparator)
	{
		return FindFirstMin.find(iterable, comparator.reversed());
	}

	/**
	 * Find the first of the elements in the given stream that compare the highest with the given comparator.
	 * This is a terminal operation that consumes the stream.
	 *
	 * @throws NoSuchElementException if the stream is empty
	 */
	public static <T> T find(Stream<T> stream, Comparator<T> comparator)
	{
		return FindFirstMin.find(stream, comparator.reversed());
	}

	/**
	 * Same as {@link #find(Iterable, Comparator)}, but the elements are converted to integer with the given function for the comparison.
	 */
	public static <T> T find(Iterable<T> iterable, ToIntFunction<T> toIntFunction)
	{
		return FindFirstMin.find(iterable, Comparator.comparingInt(toIntFunction).reversed());
	}

	/**
	 * Same as {@link #find(Stream, Comparator)}, but the elements are converted to integer with the given function for the comparison.
	 */
	public static <T> T find(Stream<T> stream, ToIntFunction<T> toIntFunction)
	{
		return FindFirstMin.find(stream, Comparator.comparingInt(toIntFunction).reversed());
	}
}
