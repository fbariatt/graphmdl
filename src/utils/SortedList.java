package utils;

import java.util.*;

/**
 * A list that is automatically sorted when a new element is added.
 *
 * The sorting is done by a comparator given as parameter.
 * In case of ties elements are kept in insertion order (implementation detail: this is due to {@link List#sort(Comparator)} being a stable algorithm)
 */
public class SortedList<E> extends AbstractList<E>
{
	private List<E> internalList;
	private Comparator<E> comparator;

	public SortedList(Comparator<E> comparator)
	{
		this.internalList = new ArrayList<>();
		this.comparator = comparator;
	}

	@Override
	public boolean add(E e)
	{
		internalList.add(e);
		internalList.sort(comparator);
		return true;
	}

	public boolean addWithoutSorting(E e)
	{
		internalList.add(e);
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends E> collection)
	{
		boolean result = internalList.addAll(collection);
		internalList.sort(comparator);
		return result;
	}

	public void sort()
	{
		internalList.sort(comparator);
	}

	@Override
	public E get(int i)
	{
		return internalList.get(i);
	}

	@Override
	public int size()
	{
		return internalList.size();
	}

	@Override
	public E remove(int i)
	{
		return internalList.remove(i);
	}

	@Override
	public void clear()
	{
		internalList.clear();
	}

	/**
	 * @return The comparator currently used to sort the list.
	 */
	public Comparator<E> getComparator()
	{
		return comparator;
	}

	/**
	 * Set a new comparator for sorting the list. The list is subsequently sorted following this new comparator.
	 */
	public void setComparator(Comparator<E> comparator)
	{
		this.comparator = comparator;
		this.sort();
	}
}
