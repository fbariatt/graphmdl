package utils;

import java.util.*;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

/**
 * Utility functions to find all elements of an Iterable/Stream that compare the lowest for a given comparison criterion
 * Also see {@link FindAllMax}.
 */
public final class FindAllMin
{
	private FindAllMin()
	{}

	/**
	 * Find all elements in the given iterable that compare the lowest with the given comparator.
	 * The elements are returned in the order they appear in the iterable.
	 *
	 * @throws NoSuchElementException if the iterable is empty
	 */
	public static <T> List<T> find(Iterable<T> iterable, Comparator<T> comparator)
	{
		return find(iterable.iterator(), comparator);
	}

	/**
	 * Find all elements in the given stream that compare the lowest with the given comparator.
	 * The elements are returned in the order they appear in the iterable.
	 * This is a terminal operation that consumes the stream.
	 *
	 * @throws NoSuchElementException if the stream is empty
	 */
	public static <T> List<T> find(Stream<T> stream, Comparator<T> comparator)
	{
		return find(stream.iterator(), comparator);
	}

	/**
	 * Same as {@link #find(Iterable, Comparator)}, but the elements are converted to integer with the given function for the comparison.
	 */
	public static <T> List<T> find(Iterable<T> iterable, ToIntFunction<T> toIntFunction)
	{
		return find(iterable.iterator(), Comparator.comparingInt(toIntFunction));
	}

	/**
	 * Same as {@link #find(Stream, Comparator)}, but the elements are converted to integer with the given function for the comparison.
	 */
	public static <T> List<T> find(Stream<T> stream, ToIntFunction<T> toIntFunction)
	{
		return find(stream.iterator(), Comparator.comparingInt(toIntFunction));
	}

	/**
	 * Actual implementation of the find function
	 */
	private static <T> List<T> find(Iterator<T> iterator, Comparator<T> comparator)
	{
		if (!iterator.hasNext())
			throw new NoSuchElementException("Impossible to find minimum elements if no elements are given!");
		List<T> result = new LinkedList<>();
		result.add(iterator.next());
		while (iterator.hasNext())
		{
			T el = iterator.next();
			final int comparison = comparator.compare(el, result.get(0));
			if (comparison < 0) // The new element is smaller
			{
				result.clear();
				result.add(el);
			}
			else if (comparison == 0) // The new element compare the same as the minimum
			{
				result.add(el);
			}
		}
		return result;
	}
}
