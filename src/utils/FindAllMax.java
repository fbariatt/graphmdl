package utils;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

/**
 * Utility functions to find all elements of an Iterable/Stream that compare the highest for a given comparison criterion
 * Also see {@link FindAllMin}.
 * Implementation detail: this class actually uses {@link FindAllMin} and just inverses the comparators.
 */
public final class FindAllMax
{
	private FindAllMax()
	{}

	/**
	 * Find all elements in the given iterable that compare the highest with the given comparator.
	 * The elements are returned in the order they appear in the iterable.
	 *
	 * @throws NoSuchElementException if the iterable is empty
	 */
	public static <T> List<T> find(Iterable<T> iterable, Comparator<T> comparator)
	{
		return FindAllMin.find(iterable, comparator.reversed());
	}

	/**
	 * Find all elements in the given stream that compare the highest with the given comparator.
	 * The elements are returned in the order they appear in the iterable.
	 * This is a terminal operation that consumes the stream.
	 *
	 * @throws NoSuchElementException if the stream is empty
	 */
	public static <T> List<T> find(Stream<T> stream, Comparator<T> comparator)
	{
		return FindAllMin.find(stream, comparator.reversed());
	}

	/**
	 * Same as {@link #find(Iterable, Comparator)}, but the elements are converted to integer with the given function for the comparison.
	 */
	public static <T> List<T> find(Iterable<T> iterable, ToIntFunction<T> toIntFunction)
	{
		return FindAllMin.find(iterable, Comparator.comparingInt(toIntFunction).reversed());
	}

	/**
	 * Same as {@link #find(Stream, Comparator)}, but the elements are converted to integer with the given function for the comparison.
	 */
	public static <T> List<T> find(Stream<T> stream, ToIntFunction<T> toIntFunction)
	{
		return FindAllMin.find(stream, Comparator.comparingInt(toIntFunction).reversed());
	}
}
