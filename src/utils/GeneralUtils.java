package utils;

import org.apache.log4j.*;
import org.apache.log4j.varia.LevelRangeFilter;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This class contains general utility functions that may be useful to perform common operations.
 */
public final class GeneralUtils
{
	private GeneralUtils()
	{}

	/**
	 * Sensible configuration for log4j
	 *
	 * @param minLevel The minimum level to set on the root logger. Log messages under this level will not be printed.
	 */
	public static void configureLogging(Level minLevel)
	{
		// Layout common to all appenders (TTCCLayout class specifies that a different instance must be used for each appender)
		Supplier<Layout> logLayout = () -> new TTCCLayout("DATE");

		// Root logger: logging INFO level and below to System.out
		ConsoleAppender sysOutAppender = new ConsoleAppender(logLayout.get(), "System.out");
		LevelRangeFilter sysOutFilter = new LevelRangeFilter();
		sysOutFilter.setLevelMax(Level.INFO);
		sysOutFilter.setAcceptOnMatch(true);
		sysOutAppender.addFilter(sysOutFilter);
		Logger.getRootLogger().addAppender(sysOutAppender);

		// Root logger: logging WARN level and above to System.err
		ConsoleAppender sysErrAppender = new ConsoleAppender(logLayout.get(), "System.err");
		LevelRangeFilter sysErrFilter = new LevelRangeFilter();
		sysErrFilter.setLevelMin(Level.WARN);
		sysErrFilter.setAcceptOnMatch(true);
		sysErrAppender.addFilter(sysErrFilter);
		Logger.getRootLogger().addAppender(sysErrAppender);

		// Disabling Jena debug messages
		Logger.getLogger("org.apache.jena").setLevel(Level.INFO);

		// Setting minimum level for root logger
		Logger.getRootLogger().setLevel(minLevel);
	}

	/**
	 * Same as {@link #configureLogging(Level)}. Set minimum level of root logger to INFO.
	 */
	public static void configureLogging()
	{
		configureLogging(Level.INFO);
	}

	/**
	 * Perform a lexicographic comparison of two iterators.
	 * Elements of the two iterators are compared one by one, the first differing element determines the result of the comparison.
	 * If one iterator ends before the other and elements were all equal up until that point, that iterator compares "less than" the other.
	 * This method moves the iterator (if the iterator is from a stream, it consumes -all or part of- the stream).
	 * Also see {@link #lexicographicCompare(Iterable, Iterable)}.
	 *
	 * @return -1 if a is lexicographically less than b, 1 if it is greater than b, 0 if a and b are equal (same size, same elements).
	 */
	public static <T extends Comparable<T>> int lexicographicCompare(Iterator<T> iteratorA, Iterator<T> iteratorB)
	{
		while (iteratorA.hasNext() && iteratorB.hasNext())
		{
			int compareResult = iteratorA.next().compareTo(iteratorB.next());
			if (compareResult != 0)
				return compareResult;
		}
		if (!iteratorA.hasNext() && !iteratorB.hasNext()) // We arrived at the end of both without detecting any difference
			return 0; // They are the same
		// Else, one is longer than the other, so it is greater
		return iteratorA.hasNext() ? 1 : -1;
	}

	/**
	 * Perform a lexicographic comparison of two iterables.
	 * Elements of a and b are compared one by one, the first differing element determines the result of the comparison.
	 * If one iterable is a prefix of the other (i.e. they are the same until a certain point then the iterable ends), it compares "less than" the other.
	 * If the two iterables are the same object (a==b), they are considered equal (short-circuit comparison).
	 * Also see {@link #lexicographicCompare(Iterator, Iterator)}.
	 *
	 * @return -1 if a is lexicographically less than b, 1 if it is greater than b, 0 if a and b are equal (same size, same elements).
	 */
	public static <T extends Comparable<T>> int lexicographicCompare(Iterable<T> a, Iterable<T> b)
	{
		if (a == b) // If they are the exact same object, there is no need to compare them
			return 0;
		return lexicographicCompare(a.iterator(), b.iterator());
	}

	/**
	 * String comparison function extending {@link Comparator<String>}.
	 * If the two strings represent numbers, then compare them numerically, otherwise perform the default string comparison.
	 * This can be used to ensure that '10' will come after '9' and not after '1'.
	 */
	public static int numericallyCompareStrings(String a, String b)
	{
		try
		{
			int aAsInt = Integer.parseInt(a);
			int bAsInt = Integer.parseInt(b);
			return aAsInt - bAsInt;
		} catch (NumberFormatException e)
		{
			return a.compareTo(b);
		}
	}

	/**
	 * @return If the given object is not null, return it. Otherwise, return the default value.
	 */
	public static <T> T defaultIfNull(T object, T defaultValue)
	{
		return object == null ? defaultValue : object;
	}

	/**
	 * @return If the given object is not null, return it. Otherwise, call the given supplier for a value to return.
	 */
	public static <T> T defaultIfNull(T object, Supplier<T> defaultSupplier)
	{
		return object == null ? defaultSupplier.get() : object;
	}

	/**
	 * Compute the intersection between two given sets, i.e. the set whose elements are contained in both the given sets.
	 *
	 * @implNote Current implementation returns an instance of {@link HashSet}.
	 */
	public static <T> Set<T> intersection(Set<T> setA, Set<T> setB)
	{
		Set<T> smaller = setA.size() <= setB.size() ? setA : setB;
		Set<T> bigger = setA.size() > setB.size() ? setA : setB;
		Set<T> intersection = new HashSet<>();

		for (T element : smaller)
		{
			if (bigger.contains(element))
				intersection.add(element);
		}

		return intersection;
	}

	/**
	 * @return Whether the intersection between the two given sets if empty.
	 * This is a short-circuit method that stops at the first common element, without needing to compute the whole intersection.
	 */
	public static <T> boolean isIntersectionEmpty(Set<T> setA, Set<T> setB)
	{
		Set<T> smaller = setA.size() <= setB.size() ? setA : setB;
		Set<T> bigger = setA.size() > setB.size() ? setA : setB;

		for (T element : smaller)
		{
			if (bigger.contains(element))
				return false;
		}
		return true;
	}

	/**
	 * Verifies whether in the given list of values, each distinct value appears k times or less.
	 *
	 * @param values The values to verify
	 * @param k      The maximum amount of times each single values is allowed to appear in the list
	 * @return true if each distinct value appears k times or less, false if at least one value appears more than k times
	 * @implSpec The values must be hashable since a map is used to count the occurrences.
	 */
	public static <T> boolean hasMaxKTimesEachValue(Iterator<T> values, int k)
	{
		Map<T, Integer> occurrencesCounts = new HashMap<>();
		while (values.hasNext())
		{
			final T value = values.next();
			int countOfValue = occurrencesCounts.getOrDefault(value, 0);
			if (countOfValue >= k) // We already counted k times this value, so this one is over the limit
				return false;
			occurrencesCounts.put(value, countOfValue + 1);
		}
		// We read all the values without ever going over the limit,
		// so there is k or fewer times each value
		return true;
	}

	/**
	 * Create a list that is a shuffled copy of the given collection.
	 *
	 * @param coll                 The collection to copy and shuffle
	 * @param copyCollectionToList Function that given a collection creates a list that is a copy of the collection.
	 * @see #shuffledCopy(Collection)
	 */
	public static <T> List<T> shuffledCopy(Collection<T> coll, Function<Collection<T>, List<T>> copyCollectionToList)
	{
		List<T> collectionCopy = copyCollectionToList.apply(coll);
		Collections.shuffle(collectionCopy);
		return collectionCopy;
	}

	/**
	 * Same as {@link #shuffledCopy(Collection, Function)}, but creates an {@link ArrayList}.
	 *
	 * @return An {@link ArrayList} that is a shuffled copy of the given collection.
	 */
	public static <T> List<T> shuffledCopy(Collection<T> coll)
	{
		return shuffledCopy(coll, ArrayList::new);
	}

	/**
	 * Concatenate multiple streams. This allows to write concatStreams(stream1, stream2, stream3) instead of
	 * nested {@link Stream#concat} such as Stream.concat(Stream.concat(stream1, stream2), stream3).
	 *
	 * @return A stream which is the lazy concatenation of the given streams.
	 * @see Stream#concat
	 */
	@SafeVarargs
	public static <T> Stream<T> streamConcat(Stream<T>... streams)
	{
		Stream<T> s = streams[0];
		for (int i = 1; i < streams.length; ++i)
			s = Stream.concat(s, streams[i]);
		return s;
	}

	/**
	 * Create a stream from an iterator, something that java does not have, surprisingly.
	 * <em>The given iterator should not be used afterwards or the stream may get corrupted!</em>
	 *
	 * @see <a href="https://stackoverflow.com/a/24511534">https://stackoverflow.com/a/24511534</a>
	 */
	public static <T> Stream<T> streamFromIterator(Iterator<T> it)
	{
		// Since creating a stream using StreamSupport requires a spliterator, we trick java by
		// making an iterable out of the iterator in order to get a spliterator from it.
		// Since Iterable is a functional interface, it can be simply instantiated with a lambda
		Iterable<T> iterable = () -> it;
		return StreamSupport.stream(iterable.spliterator(), false);
	}

	/**
	 * If the given stream has exactly one element, return it. Otherwise, (if it has none or more than one)
	 * throw an exception using the given suppliers.
	 * This method consumes one element of the stream if available.
	 *
	 * @param stream          The stream from which the element is retrieved.
	 * @param onStreamEmpty   If the stream has no elements, throw this exception.
	 * @param onStreamHasMore If the stream has more than one element, throw this exception.
	 */
	public static <T> T exactlyOneElement(Stream<T> stream, Supplier<RuntimeException> onStreamEmpty, Supplier<RuntimeException> onStreamHasMore)
	{
		Iterator<T> it = stream.iterator();
		if (it.hasNext())
		{
			T result = it.next();
			if (it.hasNext())
				throw onStreamHasMore.get();
			else
				return result;
		}
		else
		{
			throw onStreamEmpty.get();
		}
	}

	/**
	 * Same as {@link #exactlyOneElement(Stream, Supplier, Supplier)}, but throw the same exception in both error cases.
	 */
	public static <T> T exactlyOneElement(Stream<T> stream, Supplier<RuntimeException> exceptionSupplier)
	{
		return exactlyOneElement(stream, exceptionSupplier, exceptionSupplier);
	}

	/**
	 * Create a simple string representation of tabular data.
	 * Note that the implementation is very simple, and it does not verify that data and headers sizes are coherent.
	 *
	 * @param header The column headers
	 * @param data   The content of the table, by line.
	 */
	public static String tabulate(Iterable<String> header, Iterable<Iterable<String>> data)
	{
		StringBuilder sb = new StringBuilder();
		final int maxCellSize = Stream.concat(
						StreamSupport.stream(header.spliterator(), false),
						StreamSupport.stream(data.spliterator(), false).flatMap(iter -> StreamSupport.stream(iter.spliterator(), false))
				)
				.mapToInt(String::length)
				.max()
				.orElse(0);

		// Printing header
		header.forEach(h -> sb.append(String.format(String.format("| %%%ds ", maxCellSize), h)));
		sb.append("|\n");

		// Printing separator
		header.forEach(_h -> sb.append("|").append("-".repeat(maxCellSize + 2)));
		sb.append("|\n");

		// Printing data
		for (Iterable<String> line : data)
		{
			line.forEach(cell -> sb.append(String.format(String.format("| %%%ds ", maxCellSize), cell)));
			sb.append("|\n");
		}

		return sb.toString();
	}
}
