package utils;

import java.util.List;

/**
 * Utility class for packing quartile information of a distribution.
 */
public class Quartiles<T>
{
	public T min;
	public T q25;
	public T q50;
	public T q75;
	public T max;

	private Quartiles() {}

	/**
	 * Compute quartile information from the values in a given list of <em>already sorted</em> data.
	 */
	public static <T> Quartiles<T> fromSorted(List<T> sortedData)
	{
		Quartiles<T> res = new Quartiles<>();
		res.min = sortedData.get(0);
		res.q25 = sortedData.get((int) Math.floor((sortedData.size() - 1) * 0.25));
		res.q50 = sortedData.get((int) Math.floor((sortedData.size() - 1) * 0.50));
		res.q75 = sortedData.get((int) Math.floor((sortedData.size() - 1) * 0.75));
		res.max = sortedData.get(sortedData.size() - 1);
		return res;
	}
}
