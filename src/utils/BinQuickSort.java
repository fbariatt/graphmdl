package utils;

import java.util.*;

/**
 * Utility function to sort a list into bins (sublists) of equivalent elements using quicksort.
 */
public final class BinQuickSort
{
	private BinQuickSort()
	{}

	/**
	 * Create a sorted list of bins from the given list.
	 * Each bin of the result will contain elements that compare equal.
	 * The elements of the first bin will all compare less than the elements of the second bin, and so on.
	 * This implementation is recursive and uses quicksort.
	 *
	 * @param toSort     The list to sort.
	 * @param comparator Comparator used for comparing elements of the list.
	 */
	public static <T> List<List<T>> sort(List<T> toSort, Comparator<T> comparator)
	{
		// Recursion stop case
		if (toSort.size() == 0)
		{
			return new LinkedList<>();
		}
		else if (toSort.size() == 1)
		{
			List<List<T>> result = new LinkedList<>();
			result.add(Collections.singletonList(toSort.get(0)));
			return result;
		}

		// General case
		Iterator<T> it = toSort.iterator();
		T pivot = it.next();
		List<T> smaller = new LinkedList<>();
		List<T> equivalent = new LinkedList<>();
		equivalent.add(pivot);
		List<T> larger = new LinkedList<>();
		while (it.hasNext())
		{
			T element = it.next();
			final int comparison = comparator.compare(element, pivot);
			if (comparison < 0)
				smaller.add(element);
			else if (comparison == 0)
				equivalent.add(element);
			else
				larger.add(element);
		}
		List<List<T>> result = sort(smaller, comparator);
		result.add(equivalent);
		result.addAll(sort(larger, comparator));
		return result;
	}

	/**
	 * Same as {@link #sort(List, Comparator)}, but uses natural ordering to compare elements
	 */
	public static <T extends Comparable<T>> List<List<T>> sort(List<T> toSort)
	{
		return sort(toSort, Comparator.naturalOrder());
	}

}
