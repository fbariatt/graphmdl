package utils;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.expr.E_Datatype;
import org.apache.jena.sparql.expr.E_IsLiteral;
import org.apache.jena.sparql.expr.ExprVar;
import org.apache.jena.sparql.syntax.ElementFilter;
import org.apache.jena.sparql.syntax.ElementGroup;

import java.util.Map;

public final class RDFUtils
{
	/**
	 * Choices for a command-line argument requiring to choose a RDF format supported by Jena.
	 * The map consists of "cli value" -> "jena value" entries.
	 * The map is an unmodifiable map.
	 */
	public static final Map<String, String> commandLineRDFFormats = Map.of(
			"rdf", "RDF/XML",
			"ttl", "TURTLE",
			"n3", "N3",
			"nt", "N-TRIPLE"
	);


	/**
	 * @return A query that selects all datatypes used in a given knowledge graph. The datatypes will be saved
	 * as matches of the ?dataType variable.
	 */
	public static Query createDatatypeSelectQuery()
	{
		Query query = new Query();
		query.setQuerySelectType();
		query.setDistinct(true);

		Node dataTypeVar = Var.alloc("dataType");
		Node literalVar = Var.alloc("l");

		query.addResultVar(dataTypeVar, new E_Datatype(new ExprVar(literalVar)));
		ElementGroup queryBody = new ElementGroup();
		queryBody.addTriplePattern(new Triple(Var.alloc("s"), Var.alloc("p"), literalVar));
		queryBody.addElementFilter(new ElementFilter(new E_IsLiteral(new ExprVar(literalVar))));
		query.setQueryPattern(queryBody);

		return query;
	}


}
