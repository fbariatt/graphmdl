package utils;

/**
 * Simple class representing a stopwatch that can compute elapsed real time.
 *
 * @implNote This implementation is not thread safe.
 */
public class Stopwatch
{
	private long startTime = 0;
	private long stopTime = 0;
	private boolean isRunning = false;

	/**
	 * Start the stopwatch.
	 * Only a stopped stopwatch can be started.
	 */
	public void resetAndStart()
	{
		if (isRunning)
			throw new RuntimeException("Trying to start a running stopwatch");

		isRunning = true;
		stopTime = 0;
		startTime = System.nanoTime();
	}

	/**
	 * Stop the stopwatch and store elapsed time.
	 * Only a running stopwatch can be stopped.
	 */
	public void stop()
	{
		if (!isRunning)
			throw new RuntimeException("Trying to stop a non-running stopwatch");

		stopTime = System.nanoTime();
		isRunning = false;
	}

	/**
	 * Start the stopwatch, but instead of starting from 0, it starts from the previous elapsed time.
	 * If the stopwatch has never been started, it is equivalent to {@link #resetAndStart()}.
	 * Only a stopped stopwatch can be resumed.
	 */
	public void resume()
	{
		if (isRunning)
			throw new RuntimeException("Trying to resume a running stopwatch");

		startTime = System.nanoTime() - getElapsedTime().asNanoSeconds();
		stopTime = 0;
		isRunning = true;
	}

	/**
	 * @return Whether the stopwatch is running.
	 */
	public boolean isRunning()
	{
		return isRunning;
	}

	/**
	 * If the stopwatch is running: return elapsed time since the stopwatch has been started.
	 * If the stopwatch is stopped: return duration for which the stopwatch has been running.
	 * Note: the returned {@link Duration} object is immutable and will *not* update with the stopwatch.
	 */
	public Duration getElapsedTime()
	{
		if (isRunning)
			return new Duration(System.nanoTime() - startTime);
		else
			return new Duration(stopTime - startTime);
	}

	/**
	 * Represent a certain value of elapsed time.
	 * Objects of this class are immutable.
	 */
	public static class Duration
	{
		private final long nanoseconds;

		/**
		 * @param nanoseconds Elapsed time in nanoseconds
		 */
		public Duration(long nanoseconds)
		{
			this.nanoseconds = nanoseconds;
		}

		public long asNanoSeconds()
		{
			return nanoseconds;
		}

		public long asMilliseconds()
		{
			return nanoseconds / 1000000;
		}

		public long asSeconds()
		{
			return nanoseconds / 1000000000;
		}

		public Duration add(Duration other)
		{
			return new Duration(this.nanoseconds + other.nanoseconds);
		}
	}
}
