package utils.tuples;

import org.apache.commons.lang3.builder.CompareToBuilder;

import java.util.Objects;
import java.util.function.Function;

/**
 * Implementation of a two-element tuple because java doesn't have one and the ones that I could find did not suit
 * my necessities (e.g. missing a map method or difficult comparisons).
 * <br/>
 * {@link Object#equals} and {@link Object#hashCode} are overridden so that tuples can be used as keys in sets or maps.
 * They defer the computation to the elements of the tuple.
 */
public class Tuple2<A, B> implements Comparable<Tuple2<A, B>>
{
	public final A first;
	public final B second;

	public Tuple2(A first, B second)
	{
		this.first = first;
		this.second = second;
	}

	/**
	 * A tuple is equal to another object if the other object is also a tuple of the same size, and the corresponding elements are equal.
	 */
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof Tuple2)) return false;
		Tuple2<?, ?> tuple2 = (Tuple2<?, ?>) o;
		return first.equals(tuple2.first) && second.equals(tuple2.second);
	}

	/**
	 * The hashcode of a tuple is computed from its elements.
	 */
	@Override
	public int hashCode()
	{
		return Objects.hash(first, second);
	}

	/**
	 * Compare this tuple with another.
	 * This is done by comparing the elements of the tuples in order, effectively a lexicographic comparison.
	 * This only works if the tuple's elements extend {@link Comparable}.
	 *
	 * @throws ClassCastException if the tuple's elements do not extend {@link Comparable}.
	 */
	@Override
	public int compareTo(Tuple2<A, B> other)
	{
		return new CompareToBuilder()
				.append(this.first, other.first)
				.append(this.second, other.second)
				.toComparison();
	}

	/**
	 * Map the tuple to a different one, possibly changing the types of the elements.
	 * Note: {@link Function#identity()} can be used to map an element to itself.
	 */
	public <ANew, BNew> Tuple2<ANew, BNew> map(Function<A, ANew> mapFirst, Function<B, BNew> mapSecond)
	{
		return new Tuple2<>(mapFirst.apply(first), mapSecond.apply(second));
	}

	/**
	 * @return A new tuple that corresponds to this one read back-to-front.
	 */
	public Tuple2<B, A> reverse()
	{
		return new Tuple2<>(second, first);
	}
}
