package utils.tuples;

import org.apache.commons.lang3.builder.CompareToBuilder;

import java.util.Objects;
import java.util.function.Function;

/**
 * Same as {@link Tuple2}, but with three elements.
 *
 * @see Tuple2
 */
public class Tuple3<A, B, C> implements Comparable<Tuple3<A, B, C>>
{
	public final A first;
	public final B second;
	public final C third;

	public Tuple3(A first, B second, C third)
	{
		this.first = first;
		this.second = second;
		this.third = third;
	}

	/**
	 * See {@link Tuple2#equals}.
	 */
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof Tuple3)) return false;
		Tuple3<?, ?, ?> tuple3 = (Tuple3<?, ?, ?>) o;
		return first.equals(tuple3.first) && second.equals(tuple3.second) && third.equals(tuple3.third);
	}

	/**
	 * See {@link Tuple2#hashCode()}.
	 */
	@Override
	public int hashCode()
	{
		return Objects.hash(first, second, third);
	}

	/**
	 * See {@link Tuple2#compareTo}.
	 */
	@Override
	public int compareTo(Tuple3<A, B, C> other)
	{
		return new CompareToBuilder()
				.append(this.first, other.first)
				.append(this.second, other.second)
				.append(this.third, other.third)
				.toComparison();
	}

	/**
	 * See {@link Tuple2#map}.
	 */
	public <Anew, BNew, CNew> Tuple3<Anew, BNew, CNew> map(Function<A, Anew> mapFirst, Function<B, BNew> mapSecond, Function<C, CNew> mapThird)
	{
		return new Tuple3<>(mapFirst.apply(first), mapSecond.apply(second), mapThird.apply(third));
	}

	/**
	 * See {@link Tuple2#reverse()}.
	 */
	public Tuple3<C, B, A> reverse()
	{
		return new Tuple3<>(third, second, first);
	}
}
