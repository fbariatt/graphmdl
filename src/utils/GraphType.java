package utils;

import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.MutuallyExclusiveGroup;
import net.sourceforge.argparse4j.inf.Namespace;

import java.util.EnumSet;

/**
 * Enum to describe a graph type, to be used in main methods, to make a link between command-line arguments and java classes
 */
public enum GraphType
{
	DIRECTED, UNDIRECTED, MULTIGRAPH, SIMPLE;

	public static EnumSet<GraphType> SIMPLE_DIRECTED = EnumSet.of(SIMPLE, DIRECTED);
	public static EnumSet<GraphType> SIMPLE_UNDIRECTED = EnumSet.of(SIMPLE, UNDIRECTED);
	public static EnumSet<GraphType> MULTI_DIRECTED = EnumSet.of(MULTIGRAPH, DIRECTED);
	public static EnumSet<GraphType> MULTI_UNDIRECTED = EnumSet.of(MULTIGRAPH, UNDIRECTED);

	/**
	 * Add command-line arguments to an {@link ArgumentParser} that allow to specify values for this enum.
	 */
	public static void addCliArguments(ArgumentParser parser)
	{
		MutuallyExclusiveGroup directed = parser.addMutuallyExclusiveGroup().required(true);
		directed.addArgument("-d", "--directed").dest("graphs_directed").help("Graphs are directed").action(Arguments.storeConst()).setConst(GraphType.DIRECTED);
		directed.addArgument("-u", "--undirected").dest("graphs_directed").help("Graphs are undirected").action(Arguments.storeConst()).setConst(GraphType.UNDIRECTED);

		MutuallyExclusiveGroup multigraph = parser.addMutuallyExclusiveGroup().required(true);
		multigraph.addArgument("-m", "--multigraph").dest("graphs_multigraph").help("Use multigraph encoding").action(Arguments.storeConst()).setConst(GraphType.MULTIGRAPH);
		multigraph.addArgument("-s", "--simple").dest("graphs_multigraph").help("Use simple graph encoding").action(Arguments.storeConst()).setConst(GraphType.SIMPLE);
	}

	/**
	 * Create a graph type EnumSet from command-line arguments. The arguments must have been created using {@link #addCliArguments}.
	 */
	public static EnumSet<GraphType> fromCliArguments(Namespace args)
	{
		return EnumSet.of((GraphType) args.get("graphs_directed"), (GraphType) args.get("graphs_multigraph"));
	}
}
