package utils;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

/**
 * A global collection of stopwatches that can be accessed from anywhere in the code.
 * Each stopwatch is associated to a key, be careful to use unique keys to avoid side effects.
 * <br/>
 * This class is thread safe in the sense that each thread has its own collection of stopwatches, and it can therefore start and stop stopwatches without
 * impacting other threads. This class also contains methods to get the elapsed time across all threads.
 *
 * @see Stopwatch
 */
public final class SingletonStopwatchCollection
{
	/**
	 * This variable stores all key->stopwatch maps for all threads.
	 * It is used to compute elapsed times across all threads.
	 */
	private static final ConcurrentLinkedQueue<Map<Object, Stopwatch>> allStopWatchCollections = new ConcurrentLinkedQueue<>();
	/**
	 * Stopwatch collection local to the current thread.
	 * When a thread requests a value, a new map is created and added to {@link #allStopWatchCollections}.
	 */
	private static final ThreadLocal<Map<Object, Stopwatch>> threadLocalStopwatchCollection = ThreadLocal.withInitial(() -> {
		Map<Object, Stopwatch> stopWatches = new HashMap<>();
		allStopWatchCollections.add(stopWatches);
		return stopWatches;
	});

	private SingletonStopwatchCollection()
	{}

	/**
	 * Start the stopwatch associated with the given key for the current thread, creating it if it doesn't exist.
	 *
	 * @see Stopwatch#resetAndStart()
	 */
	public static void resetAndStart(Object key)
	{
		threadLocalStopwatchCollection.get().computeIfAbsent(key, _k -> new Stopwatch()).resetAndStart();
	}

	/**
	 * Resume the stopwatch associated with the given key for the current thread.
	 * If no stopwatch is associated with the given key, a new one is created (in this case this call is equivalent to {@link #resetAndStart}).
	 *
	 * @see Stopwatch#resume()
	 */
	public static void resume(Object key)
	{
		threadLocalStopwatchCollection.get().computeIfAbsent(key, _k -> new Stopwatch()).resume();
	}

	/**
	 * Stop a running stopwatch associated with the given key for the current thread.
	 *
	 * @throws NoSuchElementException if the current thread has no stopwatch for the given key.
	 * @see Stopwatch#stop()
	 */
	public static void stop(Object key)
	{
		Stopwatch stopWatch = threadLocalStopwatchCollection.get().get(key);
		if (stopWatch == null)
			throw new NoSuchElementException(String.format("Requested stopwatch %s does not exist in collection.", key));
		stopWatch.stop();
	}

	/**
	 * Get the sum of all times elapsed across all threads for the stopwatch with the given key.
	 * <em>If the key does not exist, a duration of 0 is returned</em>.
	 *
	 * @see Stopwatch#getElapsedTime()
	 */
	public static Stopwatch.Duration getElapsedTime(Object key)
	{
		return allStopWatchCollections.stream()
				.map(stopWatches -> Optional.ofNullable(stopWatches.get(key)))
				.map(stopWatch -> stopWatch.map(Stopwatch::getElapsedTime))
				.map(duration -> duration.orElse(new Stopwatch.Duration(0)))
				.reduce(new Stopwatch.Duration(0), Stopwatch.Duration::add);
	}

	/**
	 * @return The set of all stopwatch keys across all threads.
	 * The returned set is a new instance, modifying it will not modify this object.
	 */
	public static Set<Object> allKeys()
	{
		return allStopWatchCollections.stream()
				.flatMap(stopWatches -> stopWatches.keySet().stream())
				.collect(Collectors.toSet());
	}

	/**
	 * @return The stopwatches of the current thread. Note that this directly returns an internal value,
	 * and modifying it can affect this object.
	 */
	public static Map<Object, Stopwatch> currentThreadStopWatches()
	{
		return threadLocalStopwatchCollection.get();
	}
}
