package utils;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

/**
 * Utility functions to find the first of the elements of an Iterable/Stream that compare the lowest for a given comparison criterion
 * Also see {@link FindFirstMax}
 */
public final class FindFirstMin
{
	private FindFirstMin()
	{}

	/**
	 * Find the first of the elements in the given iterable that compare the lowest with the given comparator
	 *
	 * @throws NoSuchElementException if the iterable is empty
	 */
	public static <T> T find(Iterable<T> iterable, Comparator<T> comparator)
	{
		return find(iterable.iterator(), comparator);
	}

	/**
	 * Find the first of the elements in the given stream that compare the lowest with the given comparator.
	 * This is a terminal operation that consumes the stream.
	 *
	 * @throws NoSuchElementException if the stream is empty
	 */
	public static <T> T find(Stream<T> stream, Comparator<T> comparator)
	{
		return find(stream.iterator(), comparator);
	}

	/**
	 * Same as {@link #find(Iterable, Comparator)}, but the elements are converted to integer with the given function for the comparison.
	 */
	public static <T> T find(Iterable<T> iterable, ToIntFunction<T> toIntFunction)
	{
		return find(iterable.iterator(), Comparator.comparingInt(toIntFunction));
	}

	/**
	 * Same as {@link #find(Stream, Comparator)}, but the elements are converted to integer with the given function for the comparison.
	 */
	public static <T> T find(Stream<T> stream, ToIntFunction<T> toIntFunction)
	{
		return find(stream.iterator(), Comparator.comparingInt(toIntFunction));
	}

	/**
	 * Actual implementation of the find function.
	 */
	private static <T> T find(Iterator<T> iterator, Comparator<T> comparator)
	{
		if (!iterator.hasNext())
			throw new NoSuchElementException("Trying to find a minimum element in an empty Iterable");
		T min = iterator.next();
		while (iterator.hasNext())
		{
			T el = iterator.next();
			if (comparator.compare(el, min) < 0) // The new element is smaller
				min = el;
		}
		return min;
	}
}
