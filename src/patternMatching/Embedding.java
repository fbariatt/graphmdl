package patternMatching;

import graph.Vertex;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Represent one embedding of a pattern into the data.
 * An embedding is a list containing a mapping for each pattern vertex.
 */
public class Embedding
{
	private Mapping[] mappingsByIndex;
	private Integer overlapCount = null;
	private int marker = -1; // Marker that can be set used to tell which embeddings are used in the cover

	public Embedding(int patternSize)
	{
		this.mappingsByIndex = new Mapping[patternSize];
	}

	public void setMapping(Vertex patternVertex, Vertex dataVertex, int patternVertexIndex, int dataVertexIndex)
	{
		mappingsByIndex[patternVertexIndex] = new Mapping(patternVertex, dataVertex, patternVertexIndex, dataVertexIndex);
	}

	/**
	 * @return The mapping of the pattern vertex number 'patternVertexIndex'
	 */
	public Mapping getMapping(int patternVertexIndex)
	{
		return mappingsByIndex[patternVertexIndex];
	}


	public Mapping[] getMappingsByIndex()
	{
		return mappingsByIndex;
	}

	public Integer getOverlapCount()
	{
		return overlapCount;
	}

	public void setOverlapCount(Integer overlapCount)
	{
		this.overlapCount = overlapCount;
	}

	/**
	 * Mark this embedding with a specific value.
	 * Can be used for example to mark which embeddings are used in the GraphMDL cover.
	 */
	public void setMarker(int marker)
	{
		this.marker = marker;
	}

	/**
	 * @return The marker previously set with {@link #setMarker}.
	 */
	public int getMarker()
	{
		return marker;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Embedding embedding = (Embedding) o;
		return Arrays.equals(mappingsByIndex, embedding.mappingsByIndex);
	}

	@Override
	public int hashCode()
	{
		return Arrays.hashCode(mappingsByIndex);
	}

	@Override
	public String toString()
	{
		return Arrays.stream(mappingsByIndex)
				.filter(Objects::nonNull)
				.map(Mapping::toString)
				.collect(Collectors.joining(", "));
	}
}
