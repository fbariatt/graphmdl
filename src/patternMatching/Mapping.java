package patternMatching;

import graph.Vertex;

import java.util.Objects;

/**
 * Represent a mapping of a pattern node onto a data node.
 */
public class Mapping
{
	public final Vertex patternVertex;
	public final Vertex dataVertex;
	public final int patternVertexIndex;
	public final int dataVertexIndex;

	public Mapping(Vertex patternVertex, Vertex dataVertex, int patternVertexIndex, int dataVertexIndex)
	{
		this.patternVertex = patternVertex;
		this.dataVertex = dataVertex;
		this.patternVertexIndex = patternVertexIndex;
		this.dataVertexIndex = dataVertexIndex;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Mapping mapping = (Mapping) o;
		return patternVertexIndex == mapping.patternVertexIndex &&
				dataVertexIndex == mapping.dataVertexIndex;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(patternVertexIndex, dataVertexIndex);
	}

	@Override
	public String toString()
	{
		return String.format("%d -> %d", patternVertexIndex, dataVertexIndex);
	}
}
