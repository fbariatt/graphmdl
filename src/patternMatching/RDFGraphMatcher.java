package patternMatching;

import graph.Graph;
import graph.Vertex;
import graph.printers.GraphToRDF;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.sparql.expr.ExprVar;
import org.apache.jena.sparql.expr.aggregate.AggCountVarDistinct;
import utils.GeneralUtils;
import utils.MapUtils;
import utils.SingletonStopwatchCollection;

import java.util.*;

/**
 * Compute pattern embeddings by converting the data to RDF and the patterns to SPARQL queries.
 * Note that patterns are matched in an <em>isomorphic</em> manner (which is not the usual in RDF).
 */
public class RDFGraphMatcher
{
	private final Model dataAsModel;
	private final Map<RDFNode, Vertex> dataModelNodesToVertices;
	private final Map<Vertex, Integer> dataVerticesToIndices;

	/**
	 * Initialize the matcher on some data graph.
	 * This converts the data to RDF and store the results, so that the object can be used to match multiple patterns, without
	 * the need to re-compute the conversion.
	 * <strong>If the data is changed in any way, a new object should be created</strong>.
	 */
	public RDFGraphMatcher(Graph data)
	{
		SingletonStopwatchCollection.resume("RDFGraphMatcher");

		// We make sure that the query subsystem is initialised, since we use it to compute embeddings
		// Otherwise an exception is thrown. See https://stackoverflow.com/a/54922781
		org.apache.jena.query.ARQ.init();

		// We convert the data to an RDF representation
		// In order to perform an isomorphic matching, we never convert a vertex to a literal.
		GraphToRDF.GraphToModel dataConversion = new GraphToRDF.GraphToModel(data, GraphToRDF::isURIIfHasColon, _v -> false);
		this.dataAsModel = dataConversion.getModel();
		this.dataModelNodesToVertices = MapUtils.reverseInjective(dataConversion.getVerticesAsNodes()); // We know that each data vertex is mapped to a different model node since we made sure that there are no literals
		this.dataVerticesToIndices = data.getVertexIndexMap();

		SingletonStopwatchCollection.stop("RDFGraphMatcher");
	}

	/**
	 * Compute all the embeddings of the given pattern into the data.
	 */
	public List<Embedding> getEmbeddings(Graph pattern)
	{
		SingletonStopwatchCollection.resume("RDFGraphMatcher.getEmbeddings");
		List<Embedding> embeddings = new LinkedList<>();
		// Similar to the data, in order to perform an isomorphic matching we never convert a vertex to a literal.
		GraphToRDF.PatternToQuery patternToQuery = new GraphToRDF.PatternToQuery(pattern, GraphToRDF::isURIIfHasColon, _v -> false);
		try (QueryExecution qexec = QueryExecutionFactory.create(patternToQuery.getSelectQuery(true), dataAsModel))
		{
			final int patternSize = pattern.getVertexCount();
			ResultSet querySolutions = qexec.execSelect();
			while (querySolutions.hasNext())
			{
				QuerySolution querySolution = querySolutions.next();
				Embedding embedding = new Embedding(patternSize);
				// For each pattern vertex
				for (int patternVertexIndex = 0; patternVertexIndex < patternSize; ++patternVertexIndex)
				{
					final Vertex patternVertex = pattern.getVertex(patternVertexIndex);
					final RDFNode dataNode = querySolution.get(patternToQuery.getVerticesAsNodes().get(patternVertex).getName());
					final Vertex dataVertex = this.dataModelNodesToVertices.get(dataNode);
					final int dataVertexIndex = this.dataVerticesToIndices.get(dataVertex);

					embedding.setMapping(patternVertex, dataVertex, patternVertexIndex, dataVertexIndex);
				}
				embeddings.add(embedding);
			}
		}
		SingletonStopwatchCollection.stop("RDFGraphMatcher.getEmbeddings");
		return embeddings;
	}

	/**
	 * Compute the "minimum image-based" support of the given pattern in the data.
	 * <br/>
	 * See B. Bringmann and S. Nijssen, "What Is Frequent in a Single Graph?" in Advances in Knowledge Discovery and Data Mining, 2008, vol. 5012, pp. 858–863.
	 */
	public int imageBasedSupport(Graph pattern)
	{
		// Creating a query where pattern vertices are not converted to literals and with no variables in select
		GraphToRDF.PatternToQuery patternToQuery = new GraphToRDF.PatternToQuery(pattern, GraphToRDF::isURIIfHasColon, _v -> false);
		Query query = patternToQuery.getSelectQuery(true);

		// Creating COUNT(DISTINCT ?variable) for each ?variable of the pattern and adding it to to query
		patternToQuery.getVerticesAsNodes().values().stream()
				.map(var -> new AggCountVarDistinct(new ExprVar(var)))
				.forEach(count -> query.addResultVar(query.allocAggregate(count)));

		try (QueryExecution queryExecution = QueryExecutionFactory.create(query, dataAsModel))
		{
			QuerySolution solution = queryExecution.execSelect().next();// The query will have exactly one result

			return GeneralUtils.streamFromIterator(solution.varNames())
					.map(solution::getLiteral)
					.mapToInt(Literal::getInt)
					.min()
					.orElse(0);
		}
	}
}
