package patternMatching;

import utils.SingletonStopwatchCollection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class MinimumImageBasedSupportFromEmbeddings
{
	private MinimumImageBasedSupportFromEmbeddings()
	{}

	/**
	 * Compute the "minimum image-based support" of a pattern from its embeddings.
	 * See B. Bringmann and S. Nijssen, "What Is Frequent in a Single Graph?" in Advances in Knowledge Discovery and Data Mining, 2008, vol. 5012, pp. 858–863.
	 *
	 * @param embeddings  The embeddings of the pattern for which to compute the support
	 * @param patternSize The size of the pattern (i.e. the size of each embedding)
	 */
	public static int imageBasedSupport(List<Embedding> embeddings, int patternSize)
	{
		SingletonStopwatchCollection.resume("MinimumImageBasedSupportFromEmbeddings");
		ArrayList<Set<Integer>> embeddingsOfPatternVertices = new ArrayList<>(patternSize); // For each pattern vertex, the data vertices to which it is mapped
		for (int i = 0; i < patternSize; ++i)
			embeddingsOfPatternVertices.add(new HashSet<>());

		for (Embedding embedding : embeddings)
		{
			for (int v = 0; v < patternSize; ++v)
			{
				embeddingsOfPatternVertices.get(v).add(embedding.getMapping(v).dataVertexIndex);
			}
		}

		int result = embeddingsOfPatternVertices.stream()
				.mapToInt(Set::size)
				.min().getAsInt();
		SingletonStopwatchCollection.stop("MinimumImageBasedSupportFromEmbeddings");
		return result;
	}
}
