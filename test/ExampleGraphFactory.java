import graph.Edge;
import graph.Graph;
import graph.GraphFactory;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.junit.Test;
import utils.GeneralUtils;
import utils.RDFUtils;

import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Create instances of our classic example graph for testing purposes
 */
public class ExampleGraphFactory
{
	public static Graph createSimpleUndirected()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex("y");
		g.addVertex("x");
		g.addVertex("z");
		g.addVertex("x");
		g.addVertex("z");
		g.addVertex("x");
		g.addVertex("z");
		g.addVertex("x")
				.addLabel("w");

		g.addEdge(0, 1, "a");
		g.addEdge(0, 3, "a");
		g.addEdge(0, 5, "a");
		g.addEdge(1, 0, "a");
		g.addEdge(3, 0, "a");
		g.addEdge(5, 0, "a");
		g.addEdge(0, 2, "b");
		g.addEdge(0, 4, "b");
		g.addEdge(0, 6, "b");
		g.addEdge(2, 0, "b");
		g.addEdge(4, 0, "b");
		g.addEdge(6, 0, "b");
		g.addEdge(5, 7, "a");
		g.addEdge(7, 5, "a");
		return g;
	}

	@Test
	public void testSimpleUndirected()
	{
		Graph g = createSimpleUndirected();
		assertEquals(8, g.getVertexCount());
		assertEquals(14, g.getEdgeCount());

		assertEquals(4, g.getVerticesWithLabel("x").count());
		assertEquals(1, g.getVerticesWithLabel("y").count());
		assertEquals(3, g.getVerticesWithLabel("z").count());
		assertEquals(1, g.getVerticesWithLabel("w").count());

		assertEquals(8, g.getEdges().filter(edge -> edge.getLabel().equals("a")).count());
		assertEquals(6, g.getEdges().filter(edge -> edge.getLabel().equals("b")).count());

	}

	public static Graph createSimpleDirected()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex("y");
		g.addVertex("x");
		g.addVertex("z");
		g.addVertex("x");
		g.addVertex("z");
		g.addVertex("x");
		g.addVertex("z");
		g.addVertex("x")
				.addLabel("w");

		g.addEdge(1, 0, "a");
		g.addEdge(3, 0, "a");
		g.addEdge(5, 0, "a");
		g.addEdge(0, 2, "b");
		g.addEdge(0, 4, "b");
		g.addEdge(0, 6, "b");
		g.addEdge(5, 7, "a");
		g.addEdge(7, 5, "a");
		return g;
	}

	@Test
	public void testSimpleDirected()
	{
		Graph g = createSimpleDirected();
		assertEquals(8, g.getVertexCount());
		assertEquals(8, g.getEdgeCount());

		assertEquals(4, g.getVerticesWithLabel("x").count());
		assertEquals(1, g.getVerticesWithLabel("y").count());
		assertEquals(3, g.getVerticesWithLabel("z").count());
		assertEquals(1, g.getVerticesWithLabel("w").count());

		assertEquals(5, g.getEdges().filter(edge -> edge.getLabel().equals("a")).count());
		assertEquals(3, g.getEdges().filter(edge -> edge.getLabel().equals("b")).count());

		// Vertex 5 is especially complicated
		assertEquals(2, g.getOutEdgeCount(5));
		assertEquals(1, g.getInEdgeCount(5));
	}

	public static Graph createMultiDirected()
	{
		Graph g = GraphFactory.createDefaultGraph();

		for (int i = 0; i < 3; ++i)
			g.addVertex("Book");
		g.addVertex("Type:String").addLabel("Value:Alice");
		for (int i = 0; i < 2; ++i)
			g.addVertex("Person");
		g.addVertex("Type:String").addLabel("Value:Bob");
		for (int i = 0; i < 2; ++i)
			g.addVertex("City");
		for (int i = 0; i < 2; ++i)
			g.addVertex("Monument");
		for (int i = 0; i < 2; ++i)
			g.addVertex("Type:Integer").addLabel("Value:123");

		g.addEdge(0, 4, "author");
		g.addEdge(1, 4, "author");
		g.addEdge(2, 4, "author");
		g.addEdge(2, 5, "author");
		g.addEdge(4, 3, "name");
		g.addEdge(5, 6, "name");
		g.addEdge(4, 7, "born_in");
		g.addEdge(4, 7, "died_in");
		g.addEdge(5, 8, "born_in");
		g.addEdge(5, 8, "died_in");
		g.addEdge(9, 7, "is_located");
		g.addEdge(10, 7, "is_located");
		g.addEdge(9, 10, "near");
		g.addEdge(10, 9, "near");
		g.addEdge(9, 11, "height");
		g.addEdge(10, 12, "height");

		return g;
	}

	@Test
	public void testMultiDirected()
	{
		Graph g = createMultiDirected();

		assertEquals(13, g.getVertexCount());
		assertEquals(16, g.getEdgeCount());

		// Verifying that it is a multigraph
		assertEquals(2, g.getOutEdges(4).filter(edge -> edge.getTarget().equals(g.getVertex(7))).count());

		assertEquals(9, g.getVertices().flatMap(v -> v.getLabels().keySet().stream()).distinct().count());
		assertEquals(7, g.getEdges().map(Edge::getLabel).distinct().count());
	}

	public static Model createExampleModel()
	{
		Model model = ModelFactory.createDefaultModel();
		model.read(ClassLoader.getSystemResourceAsStream("exampleMultigraph.ttl"), null, "ttl");
		return model;
	}

	@SuppressWarnings("HttpUrlsUsage")
	@Test
	public void testExampleModel()
	{
		Model m = createExampleModel();

		// Just a few test to make sure that the model has been loaded correctly
		assertEquals(4, GeneralUtils.streamFromIterator(m.listStatements(m.getResource("http://example.org/Author1"), null, (RDFNode) null)).count());
		assertEquals(4, GeneralUtils.streamFromIterator(m.listStatements(null, null, m.getResource("http://example.org/City1"))).count());
		assertEquals(
				Set.of(XSD.xstring, XSD.integer),
				GeneralUtils.streamFromIterator(QueryExecutionFactory.create(RDFUtils.createDatatypeSelectQuery(), m).execSelect()).map(solution -> solution.get("dataType")).collect(Collectors.toSet())
		);
	}

	public static Model createModelWithLiteralsAndLists()
	{
		Model model = ModelFactory.createDefaultModel();
		//noinspection HttpUrlsUsage
		final String prefix = "http://example.org/testModel#";
		model.add(model.createResource(prefix + "p1"), RDF.type, model.createResource(prefix + "Person"));
		model.add(model.createResource(prefix + "p2"), RDF.type, model.createResource(prefix + "Person"));
		model.add(model.createResource(prefix + "p1"), model.createProperty(prefix + "age"), model.createTypedLiteral(30));
		model.add(model.createResource(prefix + "book1"), model.createProperty(prefix + "price"), model.createTypedLiteral(30));
		model.add(model.createResource(prefix + "book1"), model.createProperty(prefix + "authors"), model.createList(model.createResource(prefix + "p2")));
		model.add(model.createResource(prefix + "region1"), model.createProperty(prefix + "cities"), model.createList(model.createResource(prefix + "city1")));

		return model;
	}
}
