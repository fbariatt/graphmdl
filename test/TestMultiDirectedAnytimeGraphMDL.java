import graphMDL.*;
import graphMDL.anytime.AnytimeGraphMDL;
import graphMDL.anytime.MultiDirectedAnytimeGraphMDL;
import graphMDL.rewrittenGraphs.PatternCTInfo;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import org.junit.Before;
import org.junit.Test;
import utils.GeneralUtils;

import static junit.framework.TestCase.assertEquals;

public class TestMultiDirectedAnytimeGraphMDL
{
	private static AnytimeGraphMDL kgMDL;

	@Before
	public void setUp()
	{
		GeneralUtils.configureLogging();
		kgMDL = new MultiDirectedAnytimeGraphMDL(
				ExampleGraphFactory.createMultiDirected(),
				CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDescImageBasedSupportDesc),
				PatternConstraints.noConstraints(),
				new ScanDataCoverStrategy(0)
		);
	}

	/**
	 * Running KG-MDL without pruning.
	 * Not tested on paper, only verifies if future modifications modify the results.
	 * <br/>
	 * Expected values based on commit 561e0f3.
	 */
	@Test
	public void noPruning()
	{
		CodeTable ct = kgMDL.computeBestCodeTableWithoutPruning(0, 0, true, null);

		// CT non-singleton patterns
		assertEquals(3, ct.getRows().size());
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createMultiDirectedNearMonumentsOfHeight123LocatedInBlank()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(0).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createMultiDirectedBlankWithStringNameBornDiedInCity()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(1).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPerson()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(2).getAutomorphisms().getCanonicalName()
		);

		// Description lengths
		assertEquals(232.949, ct.getModelDL(), 0.001);
		assertEquals(116.584, ct.getEncodedGraphLength(), 0.001);
		assertEquals(349.533, ct.getModelDL() + ct.getEncodedGraphLength(), 0.001);

		// Rewritten graph
		RewrittenGraph rewrittenGraph = kgMDL.getRewrittenGraph();
		assertEquals(6, rewrittenGraph.getPortVertices().count());
		assertEquals(9, rewrittenGraph.getEmbeddingVerticesCount());
		assertEquals(14, rewrittenGraph.getPortEdgesCount());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(1, null)).count());
		assertEquals(2, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(2, null)).count());
		assertEquals(4, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(3, null)).count());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.vertexSingleton("Value:Alice", null)).count());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.vertexSingleton("Value:Bob", null)).count());
	}

	/**
	 * Running KG-MDL without pruning.
	 * Not tested on paper, only verifies if future modifications modify the results.
	 * <br/>
	 * Expected values based on commit 561e0f3.
	 * <br/>
	 * The results are actually the same as without pruning (except more CT are tested).
	 * Probably the example data that we use is too simple to make a difference between pruning and not pruning.
	 */
	@Test
	public void withPruning()
	{
		CodeTable ct = kgMDL.computeBestCodeTableWithPruning(0, 0, true, null);

		// CT non-singleton patterns
		assertEquals(3, ct.getRows().size());
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createMultiDirectedNearMonumentsOfHeight123LocatedInBlank()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(0).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createMultiDirectedBlankWithStringNameBornDiedInCity()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(1).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPerson()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(2).getAutomorphisms().getCanonicalName()
		);

		// Description lengths
		assertEquals(232.949, ct.getModelDL(), 0.001);
		assertEquals(116.584, ct.getEncodedGraphLength(), 0.001);
		assertEquals(349.533, ct.getModelDL() + ct.getEncodedGraphLength(), 0.001);

		// Rewritten graph
		RewrittenGraph rewrittenGraph = kgMDL.getRewrittenGraph();
		assertEquals(6, rewrittenGraph.getPortVertices().count());
		assertEquals(9, rewrittenGraph.getEmbeddingVerticesCount());
		assertEquals(14, rewrittenGraph.getPortEdgesCount());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(1, null)).count());
		assertEquals(2, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(2, null)).count());
		assertEquals(4, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(3, null)).count());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.vertexSingleton("Value:Alice", null)).count());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.vertexSingleton("Value:Bob", null)).count());
	}
}
