import graphMDL.*;
import graphMDL.anytime.AnytimeGraphMDL;
import graphMDL.anytime.SimpleDirectedAnytimeGraphMDL;
import graphMDL.rewrittenGraphs.PatternCTInfo;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import org.junit.Before;
import org.junit.Test;
import utils.GeneralUtils;

import static org.junit.Assert.assertEquals;

public class TestSimpleDirectedAnytimeGraphMDL
{
	private static AnytimeGraphMDL anytimeGraphMDL;

	@Before
	public void setUp()
	{
		GeneralUtils.configureLogging();
		anytimeGraphMDL = new SimpleDirectedAnytimeGraphMDL(
				ExampleGraphFactory.createSimpleDirected(),
				CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDescImageBasedSupportDesc),
				PatternConstraints.noConstraints(),
				new FilterEmbeddingsCoverStrategy()
		);
	}

	/**
	 * Running GraphMDL+ without pruning. Verified on paper.
	 */
	@Test
	public void noPruningTestedOnPaper()
	{
		CodeTable ct = anytimeGraphMDL.computeBestCodeTableWithoutPruning(0, -1, true, null);
		assertEquals(14, anytimeGraphMDL.getTestedCodeTablesCount()); // 12 actual + the first and last run

		// CT
		assertEquals(2, ct.getRows().size());
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleDirectedYBZ()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(0).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleDirectedXABlank()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(1).getAutomorphisms().getCanonicalName()
		);

		// Description lengths
		assertEquals(59.05, ct.getModelDL(), 0.01);
		assertEquals(55.75, ct.getEncodedGraphLength(), 0.01);
		assertEquals(114.80, ct.getModelDL() + ct.getEncodedGraphLength(), 0.01);

		// Rewritten graph
		RewrittenGraph rewrittenGraph = anytimeGraphMDL.getRewrittenGraph();
		assertEquals(3, rewrittenGraph.getPortVertices().count());
		assertEquals(9, rewrittenGraph.getEmbeddingVerticesCount());
		assertEquals(12, rewrittenGraph.getPortEdgesCount());
		assertEquals(3, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(1, null)).count());
		assertEquals(5, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(2, null)).count());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.vertexSingleton("w", null)).count());
	}

	/**
	 * RunningGraphMDL+ with pruning enabled.
	 * **Not** verified on paper.
	 * - Expected values were initially based on commit d0af976.
	 * - Description lengths changed when, on commit just after 61d876e, we decided that using the "universal encoding" of prefix codes was not a good idea.
	 * Nothing changes from the version without pruning, as pruning does not seem to make an impact on the example data that we use.
	 */
	@Test
	public void withPruning()
	{
		CodeTable ct = anytimeGraphMDL.computeBestCodeTableWithPruning(0, -1, true, null);
		assertEquals(17, anytimeGraphMDL.getTestedCodeTablesCount());

		// CT
		assertEquals(2, ct.getRows().size());
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleDirectedYBZ()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(0).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleDirectedXABlank()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(1).getAutomorphisms().getCanonicalName()
		);

		// Description lengths
		assertEquals(59.054, ct.getModelDL(), 0.001);
		assertEquals(55.745, ct.getEncodedGraphLength(), 0.001);
		assertEquals(114.798, ct.getModelDL() + ct.getEncodedGraphLength(), 0.001);

		// Rewritten graph
		RewrittenGraph rewrittenGraph = anytimeGraphMDL.getRewrittenGraph();
		assertEquals(3, rewrittenGraph.getPortVertices().count());
		assertEquals(9, rewrittenGraph.getEmbeddingVerticesCount());
		assertEquals(12, rewrittenGraph.getPortEdgesCount());
		assertEquals(3, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(1, null)).count());
		assertEquals(5, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(2, null)).count());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.vertexSingleton("w", null)).count());
	}
}
