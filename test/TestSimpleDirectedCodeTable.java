import graph.Graph;
import graphMDL.*;
import graphMDL.simpleGraphs.directed.SimpleDirectedCodeTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedIndependentVertexAndEdgeST;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.GeneralUtils;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class TestSimpleDirectedCodeTable
{
	@BeforeClass
	public static void beforeClass()
	{
		GeneralUtils.configureLogging();
	}

	@Test
	public void TestOnPaperSingletonsOnly()
	{
		final Graph data = ExampleGraphFactory.createSimpleDirected();
		final SimpleDirectedIndependentVertexAndEdgeST st = new SimpleDirectedIndependentVertexAndEdgeST(data);
		CodeTable ct = new SimpleDirectedCodeTable(st, CodeTableHeuristics.PartialOrder.noOrder);

		ct.apply(data, false);

		assertEquals(0, ct.getRows().size());
		assertEquals(4, ct.getSingletonVertexUsages().size());
		assertEquals(4, (int) ct.getSingletonVertexUsages().get("x"));
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("y"));
		assertEquals(3, (int) ct.getSingletonVertexUsages().get("z"));
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("w"));
		assertEquals(2, ct.getSingletonEdgeUsages().size());
		assertEquals(5, (int) ct.getSingletonEdgeUsages().get("a"));
		assertEquals(3, (int) ct.getSingletonEdgeUsages().get("b"));

		assertEquals(8, ct.getRewrittenGraphPortCount());


		assertEquals(89.40, ct.getModelDL(), 0.01);
		assertEquals(161.07, ct.getEncodedGraphLength(), 0.03);
		assertEquals(250.47, ct.getModelDL() + ct.getEncodedGraphLength(), 0.04);
	}

	@Test
	public void TestOnPaper1()
	{
		final Graph data = ExampleGraphFactory.createSimpleDirected();
		final SimpleDirectedIndependentVertexAndEdgeST st = new SimpleDirectedIndependentVertexAndEdgeST(data);
		CodeTable ct = new SimpleDirectedCodeTable(st, CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDesc));

		final Graph P1 = ExamplePatternsFactory.createSimpleDirectedXYZ();
		ct.addRow(new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(P1),
				st.encode(P1),
				0,
				Utils.graphLabelCount(P1)
		));

		ct.apply(data, false);

		assertEquals(3, ct.getRows().get(0).getUsage());
		assertEquals(2, ct.getSingletonVertexUsages().size());
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("x"));
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("w"));
		assertEquals(1, ct.getSingletonEdgeUsages().size());
		assertEquals(2, (int) ct.getSingletonEdgeUsages().get("a"));

		assertEquals(3, ct.getRewrittenGraphPortCount());

		// Delta may be quite high since I round numbers when I do computation on paper, which results in compound errors
		assertEquals(83.20, ct.getModelDL(), 0.01);
		assertEquals(50.93, ct.getEncodedGraphLength(), 0.02);
		assertEquals(134.13, ct.getModelDL() + ct.getEncodedGraphLength(), 0.03);
	}

	@Test
	public void TestOnPaper2()
	{
		final Graph data = ExampleGraphFactory.createSimpleDirected();
		final SimpleDirectedIndependentVertexAndEdgeST st = new SimpleDirectedIndependentVertexAndEdgeST(data);
		CodeTable ct = new SimpleDirectedCodeTable(st, CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDesc));

		final Graph P1 = ExamplePatternsFactory.createSimpleDirectedXAAX();
		ct.addRow(new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(P1),
				st.encode(P1),
				0,
				Utils.graphLabelCount(P1)
		));

		final Graph P2 = ExamplePatternsFactory.createSimpleDirectedXYZ();
		ct.addRow(new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(P2),
				st.encode(P2),
				0,
				Utils.graphLabelCount(P2)
		));

		ct.apply(data, false);

		assertEquals(3, ct.getRows().get(0).getUsage());
		assertEquals(1, ct.getRows().get(1).getUsage());
		assertEquals(1, ct.getSingletonVertexUsages().size());
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("w"));
		assertEquals(0, ct.getSingletonEdgeUsages().size());

		assertEquals(3, ct.getRewrittenGraphPortCount());

		// Delta may be quite high since I round numbers when I do computation on paper, which results in compound errors
		assertEquals(79.78, ct.getModelDL(), 0.01);
		assertEquals(35.55, ct.getEncodedGraphLength(), 0.02);
		assertEquals(115.33, ct.getModelDL() + ct.getEncodedGraphLength(), 0.03);
	}

	/**
	 * If one of the labels in the data graph is not covered by a pattern, it means that it should be covered by a singleton.
	 * Therefore, that label should exist in the Standard Tables associated to the Code Table.
	 * Otherwise, an error should be raised.
	 */
	@Test
	public void nonDescribedDataLabelsShouldBeDefinedInST()
	{
		Graph data = ExampleGraphFactory.createSimpleDirected();
		final SimpleDirectedIndependentVertexAndEdgeST st = new SimpleDirectedIndependentVertexAndEdgeST(data);
		CodeTable ct = new SimpleDirectedCodeTable(st, CodeTableHeuristics.PartialOrder.noOrder);

		// Let's add a vertex label that does not exist in the ST
		data.getVertex(0).addLabel("notExists");
		assertThrows(RuntimeException.class, () -> ct.apply(data, false));
		// We remove the label and now it does not throw an exception
		data.getVertex(0).delLabel("notExists");
		ct.apply(data, false);

		// Let's add an edge label that does not exist in the ST
		data.addEdge(1, 2, "notExists");
		assertThrows(RuntimeException.class, () -> ct.apply(data, false));
		// We remove the label and now it does not throw an exception
		data.delEdge(1, 2, "notExists");
		ct.apply(data, false);
	}

	@Test
	public void forbidSameLabelOnVerticesAndEdges()
	{
		Map<String, Double> vertexST = new HashMap<>();
		vertexST.put("aVertexLabel", 1.0);
		vertexST.put("aVertexLabelThatIsAlsoAnEdgeLabel", 2.0);

		Map<String, Double> edgeST = new HashMap<>();
		edgeST.put("anEdgeLabel", 3.0);
		edgeST.put("aVertexLabelThatIsAlsoAnEdgeLabel", 4.0);

		assertThrows(IllegalArgumentException.class, () -> new SimpleDirectedCodeTable(new SimpleDirectedIndependentVertexAndEdgeST(vertexST, edgeST), CodeTableHeuristics.PartialOrder.noOrder));
	}
}
