import graphMDL.UniversalInteger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class TestUniversalInteger
{
	@Test
	public void testBinaryEncoding()
	{
		assertEquals(1, UniversalInteger.numberOfBinaryBits(1));
		assertEquals(2, UniversalInteger.numberOfBinaryBits(2));
		assertEquals(2, UniversalInteger.numberOfBinaryBits(3));
		assertEquals(3, UniversalInteger.numberOfBinaryBits(4));
		assertEquals(5, UniversalInteger.numberOfBinaryBits(19));
	}

	@Test
	public void testEliasGamma()
	{
		assertThrows(IllegalArgumentException.class, () -> UniversalInteger.eliasGamma(0));
		assertEquals(1, UniversalInteger.eliasGamma(1));
		assertEquals(3, UniversalInteger.eliasGamma(2));
		assertEquals(3, UniversalInteger.eliasGamma(3));
		assertEquals(5, UniversalInteger.eliasGamma(4));
		assertEquals(9, UniversalInteger.eliasGamma(19));

		for (int x = 0; x <= 100; ++x)
			assertEquals(UniversalInteger.eliasGamma(x + 1), UniversalInteger.eliasGammaWith0(x));
	}

	@Test
	public void testEliasDelta()
	{
		assertThrows(IllegalArgumentException.class, () -> UniversalInteger.eliasDelta(0));
		assertEquals(1, UniversalInteger.eliasDelta(1));
		assertEquals(4, UniversalInteger.eliasDelta(2));
		assertEquals(4, UniversalInteger.eliasDelta(3));
		assertEquals(5, UniversalInteger.eliasDelta(4));
		assertEquals(9, UniversalInteger.eliasDelta(19));

		for (int x = 0; x <= 100; ++x)
			assertEquals(UniversalInteger.eliasDelta(x + 1), UniversalInteger.eliasDeltaWith0(x));
	}

	@Test
	public void testEliasOmega()
	{
		assertThrows(IllegalArgumentException.class, () -> UniversalInteger.eliasOmega(0));
		assertEquals(1, UniversalInteger.eliasOmega(1));
		assertEquals(3, UniversalInteger.eliasOmega(2));
		assertEquals(3, UniversalInteger.eliasOmega(3));
		assertEquals(6, UniversalInteger.eliasOmega(4));
		assertEquals(11, UniversalInteger.eliasOmega(19));

		for (int x = 0; x <= 100; ++x)
			assertEquals(UniversalInteger.eliasOmega(x + 1), UniversalInteger.eliasOmegaWith0(x));
	}
}
