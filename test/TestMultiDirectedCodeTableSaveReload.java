import graph.Graph;
import graphMDL.*;
import graphMDL.jsonSerializer.multigraphs.MultiDirectedGraphMDLDeserializer;
import graphMDL.jsonSerializer.multigraphs.MultiDirectedGraphMDLSerializer;
import graphMDL.multigraphs.MultiDirectedCodeTable;
import graphMDL.multigraphs.MultiDirectedStandardTable;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import patternMatching.RDFGraphMatcher;
import utils.GeneralUtils;

import java.util.Arrays;
import java.util.Comparator;

public class TestMultiDirectedCodeTableSaveReload
{
	private final Graph data;
	private final MultiDirectedCodeTable originalCT;
	private final JSONObject serialization;
	private final Comparator<CodeTableRow> ctHeuristic;

	@BeforeClass
	public static void beforeClass()
	{
		GeneralUtils.configureLogging();
	}

	public TestMultiDirectedCodeTableSaveReload()
	{
		this.data = ExampleGraphFactory.createMultiDirected();
		final MultiDirectedStandardTable st = new MultiDirectedStandardTable(data);

		this.ctHeuristic = CodeTableHeuristics.PartialOrder.noOrder;
		originalCT = new MultiDirectedCodeTable(st, CodeTableHeuristics.PartialOrder.noOrder);

		Graph[] patterns = {
				ExamplePatternsFactory.createMultiNearMonumentsInBlank(),
				ExamplePatternsFactory.createMultiDirectedBookHasAuthorPersonBornDiedInCity(),
				ExamplePatternsFactory.createMultiMonumentIsLocatedInCity(), // This one won't be used since it is covered by the other monument pattern
				ExamplePatternsFactory.createMultiMonumentHeight123(),
				ExamplePatternsFactory.createMultiPersonNameNoValue()
		};

		Arrays.stream(patterns)
				.map(pattern -> new CodeTableRow(
						PatternInfo.createComputingSaturatedAutomorphisms(pattern),
						st.encode(pattern),
						new RDFGraphMatcher(data).imageBasedSupport(pattern),
						Utils.graphLabelCount(pattern)
				))
				.forEach(originalCT::addRow);
		originalCT.apply(data, false);

		originalCT.apply(data, true);
		serialization = new MultiDirectedGraphMDLSerializer(originalCT, data).getJSONObject();
	}

	@Test
	public void loadCodeTable()
	{
		// When we tell to load the code table, but not embeddings and image-based support
		TestingUtils.assertCodeTablesAreEqual(
				this.originalCT,
				new MultiDirectedGraphMDLDeserializer(serialization).createCodeTableWithoutEmbeddings(ctHeuristic),
				false
		);
		// When we tell to load everything
		TestingUtils.assertCodeTablesAreEqual(
				this.originalCT,
				new MultiDirectedGraphMDLDeserializer(serialization).createCodeTableWithEmbeddings(ctHeuristic, data),
				true
		);
		// The loaded version stays the same even after we apply it to the data
		{
			CodeTable loadedCT = new MultiDirectedGraphMDLDeserializer(serialization).createCodeTableWithEmbeddings(ctHeuristic, data);
			loadedCT.apply(data, false);
			TestingUtils.assertCodeTablesAreEqual(this.originalCT, loadedCT, true);
		}
	}
}
