import org.junit.Test;
import utils.tuples.Tuple2;

import static org.junit.Assert.*;


public class TestTuples
{

	private static class NonComparableObj
	{
		public int x;

		public NonComparableObj(int x)
		{
			this.x = x;
		}
	}

	private static class ComparableObj implements Comparable<ComparableObj>
	{
		public int x;

		public ComparableObj(int a)
		{
			this.x = a;
		}

		@Override
		public int compareTo(ComparableObj other)
		{
			return this.x - other.x;
		}
	}

	@Test
	public void tuple2EqualsAndCompare()
	{
		{
			Tuple2<Integer, Integer> a = new Tuple2<>(1, 2);
			Tuple2<Integer, Integer> b = new Tuple2<>(1, 2);
			Tuple2<Integer, Integer> c = new Tuple2<>(1, 3);

			assertEquals(a, b);
			assertEquals(b, a);
			assertNotEquals(a, c);
			assertNotEquals(c, a);
			assertNotEquals(b, c);
			assertNotEquals(c, b);
			assertEquals(0, a.compareTo(b));
			assertEquals(0, b.compareTo(a));
			assertTrue(a.compareTo(c) < 0);
			assertTrue(c.compareTo(a) > 0);
		}
		{
			Tuple2<Integer, NonComparableObj> a = new Tuple2<>(1, new NonComparableObj(1));
			Tuple2<Integer, NonComparableObj> b = new Tuple2<>(1, new NonComparableObj(1));
			assertThrows(ClassCastException.class, () -> a.compareTo(b));
		}
		{
			Tuple2<Integer, ComparableObj> a = new Tuple2<>(1, new ComparableObj(2));

			assertEquals(0, a.compareTo(new Tuple2<>(1, new ComparableObj(2))));
			assertTrue(a.compareTo(new Tuple2<>(1, new ComparableObj(0))) > 0);
		}
	}
}
