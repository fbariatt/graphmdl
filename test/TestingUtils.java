import graphMDL.CodeTable;
import graphMDL.CodeTableRow;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.sparql.algebra.Algebra;

import java.util.*;
import java.util.stream.BaseStream;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public final class TestingUtils
{
	/**
	 * Assert that two code tables are equal.
	 *
	 * @param expectedCT     The expected code table
	 * @param actualCT       The code table to test
	 * @param testEmbeddings Whether the loaded code table's rows contain embeddings
	 */
	public static void assertCodeTablesAreEqual(CodeTable expectedCT, CodeTable actualCT, boolean testEmbeddings)
	{
		// Standard tables should be loaded correctly
		assertEquals(expectedCT.getStandardTable(), actualCT.getStandardTable());

		// Description lengths should be loaded correctly
		assertEquals(expectedCT.getRewrittenGraphPortCount(), actualCT.getRewrittenGraphPortCount());
		assertEquals(expectedCT.getModelDL(), actualCT.getModelDL(), 0.01);
		assertEquals(expectedCT.getEncodedGraphLength(), actualCT.getEncodedGraphLength(), 0.01);

		// Singleton usages and lengths should be loaded correctly
		assertEquals(expectedCT.getSingletonVertexUsages(), actualCT.getSingletonVertexUsages());
		assertEquals(expectedCT.getSingletonEdgeUsages(), actualCT.getSingletonEdgeUsages());
		assertEquals(expectedCT.getSingletonVertexLengths(), actualCT.getSingletonVertexLengths());
		assertEquals(expectedCT.getSingletonEdgeLengths(), actualCT.getSingletonEdgeLengths());

		// Tests for non-singleton patterns
		assertEquals(expectedCT.getRows().size(), actualCT.getRows().size());
		for (int i = 0; i < expectedCT.getRows().size(); ++i)
		{
			final CodeTableRow originalRow = expectedCT.getRows().get(i);
			final CodeTableRow loadedRow = actualCT.getRows().get(i);

			// Patterns should have the same structure
			assertEquals(
					originalRow.getPatternInfo().getAutomorphismInfo().getCanonicalName(),
					loadedRow.getPatternInfo().getAutomorphismInfo().getCanonicalName()
			);
			assertEquals(
					originalRow.getPatternInfo().getAutomorphismInfo().getAutomorphisms(),
					loadedRow.getPatternInfo().getAutomorphismInfo().getAutomorphisms()
			);
			assertEquals(originalRow.getPatternLabelCount(), loadedRow.getPatternLabelCount());
			assertEquals(originalRow.getPatternSTEncodingLength(), loadedRow.getPatternSTEncodingLength(), 0.01);
			assertEquals(originalRow.getImageBasedSupport(), loadedRow.getImageBasedSupport());

			// Embeddings should be correctly loaded
			if (testEmbeddings)
				assertEquals(originalRow.getEmbeddings(), loadedRow.getEmbeddings());
			else
				assertNull(loadedRow.getEmbeddings());

			// Cover-dependant attributes should be loaded correctly
			assertEquals(originalRow.getUsage(), loadedRow.getUsage());
			assertEquals(originalRow.getCodeLength(), loadedRow.getCodeLength(), 0.01);
			assertEquals(originalRow.getPortUsages(), loadedRow.getPortUsages());
			assertEquals(originalRow.getPortCodeLengths(), loadedRow.getPortCodeLengths());
			assertEquals(originalRow.getPortUsageSum(), loadedRow.getPortUsageSum());
		}
	}

	/**
	 * This assertion only fails if all the given runnables throw an assertion error, i.e.
	 * it passes if <em>any</em> of them passes without an error.
	 */
	public static void assertAny(Iterable<Runnable> assertions)
	{
		List<AssertionError> errors = new ArrayList<>(); // We collect all errors to show them on fail
		for (Runnable assertion : assertions)
		{
			try
			{
				assertion.run();
				return; // If any assertion passes without an error, we exit the function
			} catch (AssertionError e)
			{
				errors.add(e);
			}
		}
		// If we arrive here it means that all assertions failed
		throw new AssertionError("AssertAny: all assertions failed\n" + errors.stream().map(e -> String.format("- %s\n", e.getMessage())).collect(Collectors.joining()));
	}

	/**
	 * See {@link #assertAny(Iterable)}.
	 */
	public static void assertAny(Runnable... assertions)
	{
		assertAny(Arrays.asList(assertions));
	}

	/**
	 * Assert that the two given streams contain equal elements.
	 * From https://stackoverflow.com/a/34818800
	 */
	static void assertStreamEquals(BaseStream<?, ?> s1, BaseStream<?, ?> s2)
	{
		Iterator<?> iter1 = s1.iterator();
		Iterator<?> iter2 = s2.iterator();
		while (iter1.hasNext() && iter2.hasNext())
			assertEquals(iter1.next(), iter2.next());
		assert !iter1.hasNext() && !iter2.hasNext();
	}

	/**
	 * Assert that the two given strings are equal when all newlines and spaces are removed.
	 */
	public static void assertStringEqualsIgnoreSpacesAndNewlines(String expected, String actual)
	{
		assertEquals(
				expected.replace("\n", "").replace(" ", ""),
				actual.replace("\n", "").replace(" ", "")
		);
	}

	/**
	 * Simple method to assert that the two SPARQL queries are equal.
	 * This was written because simply converting the queries to string does not suffice, as the string will be different if
	 * the order of the triples is different, which semantically does not matter.
	 *
	 * @implNote This method works by compiling the queries to the algebra, which is a much simpler language, and then comparing the different lines in the result.
	 * It is not perfect, but does enough of a good job.
	 */
	public static void assertSparqlQuerySemanticallyEqual(Query expected, Query actual)
	{
		assertEquals(expected.queryType(), actual.queryType());
		assertStreamEquals(
				Algebra.compile(expected).toString().lines().sorted(),
				Algebra.compile(actual).toString().lines().sorted()
		);
	}

	/**
	 * See {@link #assertSparqlQuerySemanticallyEqual(Query, Query)}.
	 */
	public static void assertSparqlQuerySemanticallyEqual(String expected, Query actual)
	{
		assertSparqlQuerySemanticallyEqual(QueryFactory.create(expected), actual);
	}

	/**
	 * Assert that two lists contain the same elements, even if they may not be in the same order.
	 * Note that if an element appears multiple times in the expected value, it should also appear multiple times in the actual value.
	 */
	public static <T> void assertListEqualsIgnoreOrder(List<T> expected, List<T> actual)
	{
		Map<T, Integer> expectedCount = new HashMap<>();
		expected.forEach(v -> expectedCount.merge(v, 1, Integer::sum));
		Map<T, Integer> actualCount = new HashMap<>();
		actual.forEach(v -> actualCount.merge(v, 1, Integer::sum));
		assertEquals(expectedCount, actualCount);
	}
}
