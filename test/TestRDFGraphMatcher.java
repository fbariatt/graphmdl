import graph.Graph;
import org.junit.Before;
import org.junit.Test;
import patternMatching.RDFGraphMatcher;
import utils.GeneralUtils;

import java.util.function.BiConsumer;

import static org.junit.Assert.assertEquals;

public class TestRDFGraphMatcher
{
	@Before
	public void setUp()
	{
		GeneralUtils.configureLogging();
	}

	@Test
	public void patternEmbeddings()
	{
		// Undirected simple graphs
		{
			final RDFGraphMatcher matcher = new RDFGraphMatcher(ExampleGraphFactory.createSimpleUndirected());
			BiConsumer<Graph, Integer> assertEmbeddingsSize = (pattern, expectedSize) ->
					assertEquals(expectedSize.intValue(), matcher.getEmbeddings(pattern).size());

			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleUndirectedBlankAY(), 3);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleUndirectedBlankBZ(), 3);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleUndirectedXABlank(), 5);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleUndirectedXWaXaBlank(), 1);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleUndirectedXYZ(), 9);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleUndirectedYBZ(), 3);
			// Completely unrelated pattern that does not have any embeddings in this data
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPersonBornDiedInCity(), 0);
		}
		// directed simple graphs
		{
			final RDFGraphMatcher matcher = new RDFGraphMatcher(ExampleGraphFactory.createSimpleDirected());
			BiConsumer<Graph, Integer> assertEmbeddingsSize = (pattern, expectedSize) ->
					assertEquals(expectedSize.intValue(), matcher.getEmbeddings(pattern).size());


			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleDirectedBlankBZ(), 3);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleDirectedXAAX(), 2);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleDirectedXABlank(), 5);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleDirectedXYZ(), 9);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createSimpleDirectedYBZ(), 3);
			// Completely unrelated pattern that does not have any embeddings in this data
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPersonBornDiedInCity(), 0);
		}
		// Directed multigraphs
		{
			final RDFGraphMatcher matcher = new RDFGraphMatcher(ExampleGraphFactory.createMultiDirected());
			BiConsumer<Graph, Integer> assertEmbeddingsSize = (pattern, expectedSize) ->
					assertEquals(expectedSize.intValue(), matcher.getEmbeddings(pattern).size());

			assertEmbeddingsSize.accept(ExamplePatternsFactory.createMultiDirectedBlankWithStringNameBornDiedInCity(), 2);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPerson(), 4);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPersonBornDiedInCity(), 4);
			assertEmbeddingsSize.accept(ExamplePatternsFactory.createMultiDirectedNearMonumentsOfHeight123LocatedInBlank(), 2);
		}
	}

	@Test
	public void imageBasedSupport()
	{
		// Undirected simple graphs
		{
			final RDFGraphMatcher matcher = new RDFGraphMatcher(ExampleGraphFactory.createSimpleUndirected());

			assertEquals(1, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleUndirectedBlankAY()));
			assertEquals(1, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleUndirectedBlankBZ()));
			assertEquals(3, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleUndirectedXABlank()));
			assertEquals(1, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleUndirectedXWaXaBlank()));
			assertEquals(1, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleUndirectedXYZ()));
			assertEquals(1, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleUndirectedYBZ()));
			// Completely unrelated pattern that does not have any embeddings in this data
			assertEquals(0, matcher.imageBasedSupport(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPersonBornDiedInCity()));
		}
		// directed simple graphs
		{
			final RDFGraphMatcher matcher = new RDFGraphMatcher(ExampleGraphFactory.createSimpleDirected());

			assertEquals(1, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleDirectedBlankBZ()));
			assertEquals(2, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleDirectedXAAX()));
			assertEquals(3, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleDirectedXABlank()));
			assertEquals(1, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleDirectedXYZ()));
			assertEquals(1, matcher.imageBasedSupport(ExamplePatternsFactory.createSimpleDirectedYBZ()));
			// Completely unrelated pattern that does not have any embeddings in this data
			assertEquals(0, matcher.imageBasedSupport(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPersonBornDiedInCity()));
		}
		// Directed multigraphs
		{
			final RDFGraphMatcher matcher = new RDFGraphMatcher(ExampleGraphFactory.createMultiDirected());

			assertEquals(2, matcher.imageBasedSupport(ExamplePatternsFactory.createMultiDirectedBlankWithStringNameBornDiedInCity()));
			assertEquals(2, matcher.imageBasedSupport(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPerson()));
			assertEquals(2, matcher.imageBasedSupport(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPersonBornDiedInCity()));
			assertEquals(1, matcher.imageBasedSupport(ExamplePatternsFactory.createMultiDirectedNearMonumentsOfHeight123LocatedInBlank()));
		}
	}
}
