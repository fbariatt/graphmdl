import graph.Graph;
import graph.GraphFactory;
import graph.printers.RDFGraphPrefixes;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedIndependentVertexAndEdgeST;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class TestSimpleUndirectedStandardTables
{
	private static void assertSTAreEqual(Map<String, Double> expectedST, Map<String, Double> actualST)
	{
		assertEquals(expectedST.size(), actualST.size());
		expectedST.forEach((k, v) -> {
			assertTrue(actualST.containsKey(k));
			assertEquals(v, actualST.get(k), 0.01);
		});
	}

	@Test
	public void testExampleGraphSTs()
	{
		final Graph data = ExampleGraphFactory.createSimpleUndirected();
		final SimpleUndirectedIndependentVertexAndEdgeST st = new SimpleUndirectedIndependentVertexAndEdgeST(data);

		Map<String, Double> expectedVertexST = new HashMap<>();
		expectedVertexST.put("x", 1.17);
		expectedVertexST.put("y", 3.17);
		expectedVertexST.put("z", 1.58);
		expectedVertexST.put("w", 3.17);
		assertSTAreEqual(expectedVertexST, st.getVertexLabelsCodeLengths());

		Map<String, Double> expectedEdgeST = new HashMap<>();
		expectedEdgeST.put("a", 0.81);
		expectedEdgeST.put("b", 1.22);
		assertSTAreEqual(expectedEdgeST, st.getEdgeLabelsCodeLengths());
	}

	@Test
	public void testExamplePatternsSTLength()
	{
		final Graph data = ExampleGraphFactory.createSimpleUndirected();
		final SimpleUndirectedIndependentVertexAndEdgeST st = new SimpleUndirectedIndependentVertexAndEdgeST(data);

		assertEquals(14.80, st.encode(ExamplePatternsFactory.createSimpleUndirectedBlankBZ()), 0.01);

		assertEquals(15.98, st.encode(ExamplePatternsFactory.createSimpleUndirectedBlankAY()), 0.01);

		assertEquals(32.87, st.encode(ExamplePatternsFactory.createSimpleUndirectedXYZ()), 0.1);
	}

	@Test
	public void testThrowsExceptionIfGraphLabelNotInST()
	{
		final Graph data = ExampleGraphFactory.createSimpleUndirected();
		final SimpleUndirectedIndependentVertexAndEdgeST st = new SimpleUndirectedIndependentVertexAndEdgeST(data);

		final Graph graphWithInvalidLabels = GraphFactory.createDefaultGraph();
		graphWithInvalidLabels.addVertex("thisLabelDoesNotExist");

		assertThrows(RuntimeException.class, () -> st.encode(graphWithInvalidLabels));

		final Graph otherGraphWithInvalidLabels = GraphFactory.createDefaultGraph();
		otherGraphWithInvalidLabels.addVertex();
		otherGraphWithInvalidLabels.addVertex();
		otherGraphWithInvalidLabels.addEdge(0, 1, "thisLabelAlsoDoesNotExist");

		assertThrows(RuntimeException.class, () -> st.encode(otherGraphWithInvalidLabels));
	}
}
