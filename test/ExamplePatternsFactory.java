import graph.Edge;
import graph.Graph;
import graph.GraphFactory;
import graph.printers.RDFGraphPrefixes;
import graphMDL.Utils;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedUtils;
import org.junit.Test;

import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Create instances of our classic example patterns for testing purposes
 */
public class ExamplePatternsFactory
{
	/* ==== SIMPLE UNDIRECTED ==== */
	public static Graph createSimpleUndirectedXYZ()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex("x");
		g.addVertex("y");
		g.addVertex("z");
		g.addEdge(0, 1, "a");
		g.addEdge(1, 0, "a");
		g.addEdge(1, 2, "b");
		g.addEdge(2, 1, "b");
		return g;
	}

	@Test
	public void testSimpleUndirectedXYZ()
	{
		Graph g = createSimpleUndirectedXYZ();
		assertEquals(3, g.getVertexCount());
		assertEquals(4, g.getEdgeCount());
		assertEquals(3, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(2, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createSimpleUndirectedXABlank()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex("x");
		g.addVertex();
		g.addEdge(0, 1, "a");
		g.addEdge(1, 0, "a");
		return g;
	}

	@Test
	public void testSimpleUndirectedXABlank()
	{
		Graph g = createSimpleUndirectedXABlank();
		assertEquals(2, g.getVertexCount());
		assertEquals(2, g.getEdgeCount());
		assertEquals(1, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(1, g.getVerticesWithLabel("x").count());
		assertEquals(1, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createSimpleUndirectedBlankBZ()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex();
		g.addVertex("z");
		g.addEdge(0, 1, "b");
		g.addEdge(1, 0, "b");
		return g;
	}

	@Test
	public void testSimpleUndirectedBlankBZ()
	{
		Graph g = createSimpleUndirectedBlankBZ();
		assertEquals(2, g.getVertexCount());
		assertEquals(2, g.getEdgeCount());
		assertEquals(1, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(1, g.getVerticesWithLabel("z").count());
		assertEquals(1, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createSimpleUndirectedBlankAY()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex();
		g.addVertex("y");
		g.addEdge(0, 1, "a");
		g.addEdge(1, 0, "a");
		return g;
	}

	@Test
	public void testSimpleUndirectedBlankAY()
	{
		Graph g = createSimpleUndirectedBlankAY();
		assertEquals(2, g.getVertexCount());
		assertEquals(2, g.getEdgeCount());
		assertEquals(1, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(1, g.getVerticesWithLabel("y").count());
		assertEquals(1, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createSimpleUndirectedYBZ()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex("y");
		g.addVertex("z");
		g.addEdge(0, 1, "b");
		g.addEdge(1, 0, "b");
		return g;
	}

	@Test
	public void testSimpleUndirectedYBZ()
	{
		Graph g = createSimpleUndirectedYBZ();
		assertEquals(2, g.getVertexCount());
		assertEquals(2, g.getEdgeCount());
		assertEquals(3, SimpleUndirectedUtils.graphLabelCount(g));
	}

	public static Graph createSimpleUndirectedXWaXaBlank()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex()
				.addLabel("x")
				.addLabel("w");
		g.addVertex("x");
		g.addVertex();
		g.addEdge(0, 1, "a");
		g.addEdge(1, 0, "a");
		g.addEdge(1, 2, "a");
		g.addEdge(2, 1, "a");
		return g;
	}

	@Test
	public void testSimpleUndirectedXWaXaBlank()
	{
		Graph g = createSimpleUndirectedXWaXaBlank();
		assertEquals(3, g.getVertexCount());
		assertEquals(4, g.getEdgeCount());
		assertEquals(5, SimpleUndirectedUtils.graphLabelCount(g));
	}

	/* ==== SIMPLE DIRECTED ==== */

	public static Graph createSimpleDirectedXYZ()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex("x");
		g.addVertex("y");
		g.addVertex("z");
		g.addEdge(0, 1, "a");
		g.addEdge(1, 2, "b");
		return g;
	}

	@Test
	public void testSimpleDirectedXYZ()
	{
		Graph g = createSimpleDirectedXYZ();
		assertEquals(3, g.getVertexCount());
		assertEquals(2, g.getEdgeCount());
		assertEquals(3, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(2, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createSimpleDirectedXAAX()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex("x");
		g.addVertex("x");
		g.addEdge(0, 1, "a");
		g.addEdge(1, 0, "a");
		return g;
	}

	@Test
	public void testSimpleDirectedXAAX()
	{
		Graph g = createSimpleDirectedXAAX();
		assertEquals(2, g.getVertexCount());
		assertEquals(2, g.getEdgeCount());
		assertEquals(2, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(1, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createSimpleDirectedXABlank()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex("x");
		g.addVertex();
		g.addEdge(0, 1, "a");
		return g;
	}

	@Test
	public void testSimpleDirectedXABlank()
	{
		Graph g = createSimpleDirectedXABlank();
		assertEquals(2, g.getVertexCount());
		assertEquals(1, g.getEdgeCount());
		assertEquals(2, Utils.graphLabelCount(g));
	}

	public static Graph createSimpleDirectedBlankBZ()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex();
		g.addVertex("z");
		g.addEdge(0, 1, "b");
		return g;
	}

	@Test
	public void testSimpleDirectedBlankBZ()
	{
		Graph g = createSimpleDirectedBlankBZ();
		assertEquals(2, g.getVertexCount());
		assertEquals(1, g.getEdgeCount());
		assertEquals(1, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(1, g.getVerticesWithLabel("z").count());
		assertEquals(1, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createSimpleDirectedYBZ()
	{
		Graph g = GraphFactory.createSimpleGraph();
		g.addVertex("y");
		g.addVertex("z");
		g.addEdge(0, 1, "b");
		return g;
	}

	@Test
	public void testSimpleDirectedYBZ()
	{
		Graph g = createSimpleDirectedYBZ();
		assertEquals(2, g.getVertexCount());
		assertEquals(1, g.getEdgeCount());
		assertEquals(3, Utils.graphLabelCount(g));
	}

	/* ==== MULTI DIRECTED ==== */

	public static Graph createMultiDirectedBookHasAuthorPersonBornDiedInCity()
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex("Book");
		g.addVertex("Person");
		g.addVertex("City");
		g.addEdge(0, 1, "author");
		g.addEdge(1, 2, "born_in");
		g.addEdge(1, 2, "died_in");
		return g;
	}

	@Test
	public void testMultiDirectedBookHasAuthorPersonBornDiedInCity()
	{
		Graph g = createMultiDirectedBookHasAuthorPersonBornDiedInCity();
		assertEquals(3, g.getVertexCount());
		assertEquals(3, g.getEdgeCount());
		assertEquals(3, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(3, g.getVertices().flatMap(v -> v.getLabels().keySet().stream()).collect(Collectors.toSet()).size());
		assertEquals(3, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createMultiDirectedBookHasAuthorPerson()
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex("Book");
		g.addVertex("Person");
		g.addEdge(0, 1, "author");
		return g;
	}

	@Test
	public void testMultiDirectedBookHasAuthorPerson()
	{
		Graph g = createMultiDirectedBookHasAuthorPerson();
		assertEquals(2, g.getVertexCount());
		assertEquals(1, g.getEdgeCount());
		assertEquals(2, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(2, g.getVertices().flatMap(v -> v.getLabels().keySet().stream()).collect(Collectors.toSet()).size());
		assertEquals(1, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createMultiDirectedBlankWithStringNameBornDiedInCity()
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex();
		g.addVertex("City");
		g.addVertex("Type:String");
		g.addEdge(0, 1, "born_in");
		g.addEdge(0, 1, "died_in");
		g.addEdge(0, 2, "name");
		return g;
	}

	@Test
	public void testMultiDirectedBlankWithStringNameBornDiedInCity()
	{
		Graph g = createMultiDirectedBlankWithStringNameBornDiedInCity();
		assertEquals(3, g.getVertexCount());
		assertEquals(3, g.getEdgeCount());
		assertEquals(2, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(2, g.getVertices().flatMap(v -> v.getLabels().keySet().stream()).collect(Collectors.toSet()).size());
		assertEquals(3, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createMultiNearMonumentsInBlank()
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex("Monument");
		g.addVertex("Monument");
		g.addVertex();
		g.addEdge(0, 1, "near");
		g.addEdge(1, 0, "near");
		g.addEdge(0, 2, "is_located");
		g.addEdge(1, 2, "is_located");
		return g;
	}

	@Test
	public void testMultiNearMonumentsInBlank()
	{
		Graph g = createMultiNearMonumentsInBlank();
		assertEquals(3, g.getVertexCount());
		assertEquals(4, g.getEdgeCount());
		assertEquals(2, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(1, g.getVertices().flatMap(v -> v.getLabels().keySet().stream()).collect(Collectors.toSet()).size());
		assertEquals(2, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createMultiMonumentIsLocatedInCity()
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex("Monument");
		g.addVertex("City");
		g.addEdge(0, 1, "is_located");
		return g;
	}

	@Test
	public void testMultiMonumentIsLocatedInCity()
	{
		Graph g = createMultiMonumentIsLocatedInCity();
		assertEquals(2, g.getVertexCount());
		assertEquals(1, g.getEdgeCount());
		assertEquals(2, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(2, g.getVertices().flatMap(v -> v.getLabels().keySet().stream()).collect(Collectors.toSet()).size());
		assertEquals(1, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createMultiMonumentHeight123()
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex("Monument");
		g.addVertex("Type:Integer")
				.addLabel("Value:123");
		g.addEdge(0, 1, "height");
		return g;
	}

	@Test
	public void testMultiMonumentHeight123()
	{
		Graph g = createMultiMonumentHeight123();
		assertEquals(2, g.getVertexCount());
		assertEquals(1, g.getEdgeCount());
		assertEquals(3, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(3, g.getVertices().flatMap(v -> v.getLabels().keySet().stream()).collect(Collectors.toSet()).size());
		assertEquals(1, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createMultiDirectedNearMonumentsOfHeight123LocatedInBlank()
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex();
		g.addVertex("Monument");
		g.addVertex("Monument");
		g.addVertex()
				.addLabel("Value:123")
				.addLabel("Type:Integer");
		g.addVertex()
				.addLabel("Value:123")
				.addLabel("Type:Integer");
		g.addEdge(1, 0, "is_located");
		g.addEdge(2, 0, "is_located");
		g.addEdge(1, 2, "near");
		g.addEdge(2, 1, "near");
		g.addEdge(1, 3, "height");
		g.addEdge(2, 4, "height");
		return g;
	}

	@Test
	public void testMultiDirectedNearMonumentsOfHeight123LocatedInBlank()
	{
		Graph g = createMultiDirectedNearMonumentsOfHeight123LocatedInBlank();
		assertEquals(5, g.getVertexCount());
		assertEquals(6, g.getEdgeCount());
		assertEquals(6, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(3, g.getVertices().flatMap(v -> v.getLabels().keySet().stream()).collect(Collectors.toSet()).size());
		assertEquals(3, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}

	public static Graph createMultiPersonNameNoValue()
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex("Person");
		g.addVertex("Type:String");
		g.addEdge(0, 1, "name");
		return g;
	}

	@Test
	public void testMultiPersonNameNoValue()
	{
		Graph g = createMultiPersonNameNoValue();
		assertEquals(2, g.getVertexCount());
		assertEquals(1, g.getEdgeCount());
		assertEquals(2, g.getVertices().mapToLong(v -> v.getLabels().values().size()).sum());
		assertEquals(2, g.getVertices().flatMap(v -> v.getLabels().keySet().stream()).collect(Collectors.toSet()).size());
		assertEquals(1, g.getEdges().map(Edge::getLabel).collect(Collectors.toSet()).size());
	}
}

