import org.junit.Test;
import utils.BinQuickSort;
import utils.GeneralUtils;
import utils.MapUtils;
import utils.SortedList;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * Tests for the utility classes and methods in the project.
 */
public class TestUtils
{
	@Test
	public void testNumericallyCompareStrings()
	{
		String[] values = {"1", "10", "2", "9", "orange", "123", "101"};
		String[] sortedValues = {"1", "2", "9", "10", "101", "123", "orange"};

		Arrays.sort(values, GeneralUtils::numericallyCompareStrings);
		assertArrayEquals(sortedValues, values);
	}

	@Test
	public void testSetIntersection()
	{
		{
			Set<Integer> setA = new HashSet<>();
			setA.add(1);
			setA.add(2);
			setA.add(3);

			Set<Integer> setB = new HashSet<>();
			setB.add(4);
			setB.add(5);
			setB.add(6);

			Set<Integer> expectedResult = new HashSet<>();

			assertTrue(GeneralUtils.isIntersectionEmpty(setA, setB));
			assertTrue(GeneralUtils.isIntersectionEmpty(setB, setA));
			assertEquals(expectedResult, GeneralUtils.intersection(setA, setB));
			assertEquals(expectedResult, GeneralUtils.intersection(setB, setA));
		}
		{
			Set<Integer> setA = new HashSet<>();
			setA.add(1);
			setA.add(2);
			setA.add(3);

			Set<Integer> setB = new HashSet<>();
			setB.add(2);
			setB.add(3);
			setB.add(4);

			Set<Integer> expectedResult = new HashSet<>();
			expectedResult.add(2);
			expectedResult.add(3);

			assertFalse(GeneralUtils.isIntersectionEmpty(setA, setB));
			assertFalse(GeneralUtils.isIntersectionEmpty(setB, setA));
			assertEquals(expectedResult, GeneralUtils.intersection(setA, setB));
			assertEquals(expectedResult, GeneralUtils.intersection(setB, setA));
		}
		{
			Set<Integer> setA = new HashSet<>();
			setA.add(1);
			setA.add(2);
			setA.add(3);
			setA.add(4);
			setA.add(5);
			setA.add(6);

			Set<Integer> setB = new HashSet<>();
			setB.add(1);
			setB.add(2);
			setB.add(3);

			assertFalse(GeneralUtils.isIntersectionEmpty(setA, setB));
			assertFalse(GeneralUtils.isIntersectionEmpty(setB, setA));
			assertEquals(setB, GeneralUtils.intersection(setA, setB));
			assertEquals(setB, GeneralUtils.intersection(setB, setA));
		}
	}

	@SuppressWarnings("deprecation") // Even if the method is deprecated, as long as it is present it should be tested
	@Test
	public void incrementOrCreateMapValue()
	{
		Map<String, Integer> map = new HashMap<>(Map.of(
				"a", 1,
				"b", 5
		));
		MapUtils.incrementOrCreateMapValue(map, "a", 1);
		MapUtils.incrementOrCreateMapValue(map, "a", 2);
		MapUtils.incrementOrCreateMapValue(map, "b", 3);
		MapUtils.incrementOrCreateMapValue(map, "c", 4);
		assertEquals(Map.of("a", 4, "b", 8, "c", 4), map);
	}

	@Test
	public void testBinQuickSort()
	{
		List<List<Integer>> sortedBins = BinQuickSort.sort(Arrays.asList(2, 3, 5, 5, 1, 2, 1, 1, 99, 12));
		assertEquals(6, sortedBins.size());
		TestingUtils.assertStreamEquals(Stream.of(1, 2, 3, 5, 12, 99), sortedBins.stream().map(bin -> bin.get(0)));
		TestingUtils.assertStreamEquals(Stream.of(3, 2, 1, 2, 1, 1), sortedBins.stream().map(List::size));

		// Reverse comparison
		sortedBins = BinQuickSort.sort(Arrays.asList(2, 3, 5, 5, 1, 2, 1, 1, 99, 12), Comparator.reverseOrder());
		assertEquals(6, sortedBins.size());
		TestingUtils.assertStreamEquals(Stream.of(99, 12, 5, 3, 2, 1), sortedBins.stream().map(bin -> bin.get(0)));
		TestingUtils.assertStreamEquals(Stream.of(1, 1, 2, 1, 2, 3), sortedBins.stream().map(List::size));

		// Testing a different comparison
		sortedBins = BinQuickSort.sort(Arrays.asList(2, 3, 5, 5, 1, 2, 1, 1, 99, 12), Comparator.comparingInt(a -> a % 2));
		assertEquals(2, sortedBins.size());
		sortedBins.get(0).forEach(value -> assertEquals(0, value % 2));
		sortedBins.get(1).forEach(value -> assertEquals(1, value % 2));
		TestingUtils.assertStreamEquals(Stream.of(3, 7), sortedBins.stream().map(List::size));
	}

	@Test
	public void testMaxKTimesEachValue()
	{
		String[] values = {"a", "a", "b", "b", "b", "a", "a", "c", "c", "c"};
		assertFalse(GeneralUtils.hasMaxKTimesEachValue(Arrays.stream(values).iterator(), 3));
		assertTrue(GeneralUtils.hasMaxKTimesEachValue(Arrays.stream(values).iterator(), 4));
	}

	@Test
	public void testSortedList()
	{
		SortedList<Integer> l = new SortedList<>(Comparator.<Integer>naturalOrder());
		Stream.of(1, 5, 12, 2, 4, 6).forEach(l::add);

		List<Integer> sortedValues = new ArrayList<>(List.of(1, 2, 4, 5, 6, 12));
		assertEquals(sortedValues, l);
		assertEquals(0, GeneralUtils.lexicographicCompare(l.iterator(), sortedValues.iterator())); // Testing iterators as well

		// What happens if we change comparator? The list should be automatically re-sorted
		l.setComparator(Comparator.<Integer>naturalOrder().reversed());
		assertNotEquals(l, sortedValues);
		Collections.reverse(sortedValues);
		assertEquals(0, GeneralUtils.lexicographicCompare(l.iterator(), sortedValues.iterator())); // Testing iterators as well
	}
}
