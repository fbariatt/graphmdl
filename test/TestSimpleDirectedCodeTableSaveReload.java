import graph.Graph;
import graphMDL.*;
import graphMDL.jsonSerializer.simpleGraphs.directed.SimpleDirectedGraphMDLDeserializer;
import graphMDL.jsonSerializer.simpleGraphs.directed.SimpleDirectedGraphMDLSerializer;
import graphMDL.simpleGraphs.directed.SimpleDirectedCodeTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedIndependentVertexAndEdgeST;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import patternMatching.RDFGraphMatcher;
import utils.GeneralUtils;

import java.util.Comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

/**
 * Can a code table be saved to JSON and then reloaded giving the same table?
 */
public class TestSimpleDirectedCodeTableSaveReload
{
	private final Graph data;
	private final SimpleDirectedCodeTable originalCT;
	private final JSONObject serialization;
	private final Comparator<CodeTableRow> ctHeuristic;

	public TestSimpleDirectedCodeTableSaveReload()
	{
		this.data = ExampleGraphFactory.createSimpleUndirected();
		final SimpleDirectedIndependentVertexAndEdgeST st = new SimpleDirectedIndependentVertexAndEdgeST(data);

		this.ctHeuristic = CodeTableHeuristics.PartialOrder.noOrder;
		originalCT = new SimpleDirectedCodeTable(st, ctHeuristic);

		Graph pattern = ExamplePatternsFactory.createSimpleDirectedXYZ();
		originalCT.addRow(new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(pattern),
				st.encode(pattern),
				new RDFGraphMatcher(data).imageBasedSupport(pattern),
				Utils.graphLabelCount(pattern)
		));

		Graph unusedPattern = ExamplePatternsFactory.createSimpleDirectedBlankBZ();
		originalCT.addRow(new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(unusedPattern),
				st.encode(unusedPattern),
				new RDFGraphMatcher(data).imageBasedSupport(unusedPattern),
				Utils.graphLabelCount(unusedPattern)
		));

		originalCT.apply(data, false);
		serialization = new SimpleDirectedGraphMDLSerializer(originalCT, data).getJSONObject();
	}

	@BeforeClass
	public static void beforeClass()
	{
		GeneralUtils.configureLogging();
	}

	/**
	 * Tests that {@link graphMDL.jsonSerializer.GraphMDLDeserializer#createCodeTable} correctly loads a code table which is an exact
	 * copy of the serialized one.
	 */
	@Test
	public void loadCodeTable()
	{
		// When we tell to load the code table, but not embeddings and image-based support
		TestingUtils.assertCodeTablesAreEqual(
				this.originalCT,
				new SimpleDirectedGraphMDLDeserializer(serialization).createCodeTableWithoutEmbeddings(ctHeuristic),
				false
		);
		// When we tell to load everything
		TestingUtils.assertCodeTablesAreEqual(
				this.originalCT,
				new SimpleDirectedGraphMDLDeserializer(serialization).createCodeTableWithEmbeddings(ctHeuristic, data),
				true
		);
	}

	@Test
	public void withCoverAttributesThrowsExceptionIfDataIsNeededAndNotSet()
	{
		assertThrows(IllegalArgumentException.class, () ->
				new SimpleDirectedGraphMDLDeserializer(serialization).createCodeTable(ctHeuristic, true, null)
		);
	}

	@Test
	public void reapplyLoadedAndReSerialize()
	{
		// We load a code table using the serialization
		CodeTable loadedCT = new SimpleDirectedGraphMDLDeserializer(serialization).createCodeTableWithEmbeddings(ctHeuristic, data);
		// We apply this new code table to the data using its apply method
		loadedCT.apply(data, false);
		// We serialize the result
		JSONObject newSerialization = new SimpleDirectedGraphMDLSerializer((SimpleDirectedCodeTable) loadedCT, data).getJSONObject();

		// This serialization is equivalent to the one we had at the beginning
		assertEquals(serialization.toString(), newSerialization.toString());
	}
}
