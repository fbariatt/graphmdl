import graph.Graph;
import graphMDL.CodeTable;
import graphMDL.CodeTableHeuristics;
import graphMDL.CodeTableRow;
import graphMDL.PatternInfo;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedCodeTable;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedIndependentVertexAndEdgeST;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.GeneralUtils;
import utils.SingletonStopwatchCollection;
import utils.Stopwatch;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class TestSimpleUndirectedCodeTable
{
	@BeforeClass
	public static void beforeClass()
	{
		GeneralUtils.configureLogging();
	}

	@Test
	public void TestOnPaper1()
	{
		final Graph data = ExampleGraphFactory.createSimpleUndirected();
		final SimpleUndirectedIndependentVertexAndEdgeST st = new SimpleUndirectedIndependentVertexAndEdgeST(data);
		CodeTable ct = new SimpleUndirectedCodeTable(st, CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDesc));

		final Graph P1 = ExamplePatternsFactory.createSimpleUndirectedBlankBZ();
		ct.addRow(new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(P1),
				st.encode(P1),
				0,
				SimpleUndirectedUtils.graphLabelCount(P1)
		));

		final Graph P2 = ExamplePatternsFactory.createSimpleUndirectedBlankAY();
		ct.addRow(new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(P2),
				st.encode(P2),
				0,
				SimpleUndirectedUtils.graphLabelCount(P2)
		));

		ct.apply(data, false);

		assertEquals(3, ct.getRows().get(0).getUsage());
		assertEquals(3, ct.getRows().get(1).getUsage());
		assertEquals(2, ct.getSingletonVertexUsages().size());
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("w"));
		assertEquals(4, (int) ct.getSingletonVertexUsages().get("x"));
		assertEquals(1, ct.getSingletonEdgeUsages().size());
		assertEquals(1, (int) ct.getSingletonEdgeUsages().get("a"));

		assertEquals(5, ct.getRewrittenGraphPortCount());

		// Delta is quite high since I round numbers when I do computation on paper, which results in compound errors
		assertEquals(85.42, ct.getModelDL(), 0.03);
		assertEquals(89.97, ct.getEncodedGraphLength(), 0.05);
		assertEquals(175.39, ct.getModelDL() + ct.getEncodedGraphLength(), 0.08);
	}

	@Test
	public void TestOnPaperSingletonsOnly()
	{
		final Graph data = ExampleGraphFactory.createSimpleUndirected();
		final SimpleUndirectedIndependentVertexAndEdgeST st = new SimpleUndirectedIndependentVertexAndEdgeST(data);
		CodeTable ct = new SimpleUndirectedCodeTable(st, CodeTableHeuristics.PartialOrder.noOrder);

		ct.apply(data, false);

		assertEquals(0, ct.getRows().size());
		assertEquals(4, ct.getSingletonVertexUsages().size());
		assertEquals(4, (int) ct.getSingletonVertexUsages().get("x"));
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("y"));
		assertEquals(3, (int) ct.getSingletonVertexUsages().get("z"));
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("w"));
		assertEquals(2, ct.getSingletonEdgeUsages().size());
		assertEquals(4, (int) ct.getSingletonEdgeUsages().get("a"));
		assertEquals(3, (int) ct.getSingletonEdgeUsages().get("b"));

		assertEquals(8, ct.getRewrittenGraphPortCount());

		// Delta is quite high since I round numbers when I do computation on paper, which results in compound errors
		assertEquals(89.12, ct.getModelDL(), 0.01);
		assertEquals(149.59, ct.getEncodedGraphLength(), 0.01);
		assertEquals(238.71, ct.getModelDL() + ct.getEncodedGraphLength(), 0.01);
	}

	/**
	 * If one of the labels in the data graph is not covered by a pattern, it means that it should be covered by a singleton.
	 * Therefore, that label should exist in the Standard Tables associated to the Code Table.
	 * Otherwise, an error should be raised.
	 */
	@Test
	public void nonDescribedDataLabelsShouldBeDefinedInST()
	{
		Graph data = ExampleGraphFactory.createSimpleUndirected();
		final SimpleUndirectedIndependentVertexAndEdgeST st = new SimpleUndirectedIndependentVertexAndEdgeST(data);
		CodeTable ct = new SimpleUndirectedCodeTable(st, CodeTableHeuristics.PartialOrder.noOrder);

		// Let's add a vertex label that does not exist in the ST
		data.getVertex(0).addLabel("notExists");
		assertThrows(RuntimeException.class, () -> ct.apply(data, false));
		// We remove the label and now it does not throw an exception
		data.getVertex(0).delLabel("notExists");
		ct.apply(data, false);

		// Let's add an edge label that does not exist in the ST
		data.addEdge(1, 2, "notExists");
		assertThrows(RuntimeException.class, () -> ct.apply(data, false));
		// Because of the exception of the previous line, some stopwatches are left running and will cause problems with the next test
		SingletonStopwatchCollection.currentThreadStopWatches().values().stream()
				.filter(Stopwatch::isRunning)
				.forEach(Stopwatch::stop);
		// We remove the label and now it does not throw an exception
		data.delEdge(1, 2, "notExists");
		ct.apply(data, false);
	}

	@Test
	public void forbidSameLabelOnVerticesAndEdges()
	{
		Map<String, Double> vertexST = new HashMap<>();
		vertexST.put("aVertexLabel", 1.0);
		vertexST.put("aVertexLabelThatIsAlsoAnEdgeLabel", 2.0);

		Map<String, Double> edgeST = new HashMap<>();
		edgeST.put("anEdgeLabel", 3.0);
		edgeST.put("aVertexLabelThatIsAlsoAnEdgeLabel", 4.0);

		assertThrows(IllegalArgumentException.class, () -> new SimpleUndirectedCodeTable(new SimpleUndirectedIndependentVertexAndEdgeST(vertexST, edgeST), CodeTableHeuristics.PartialOrder.noOrder));
	}
}
