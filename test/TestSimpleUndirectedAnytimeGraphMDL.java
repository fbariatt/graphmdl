import graphMDL.*;
import graphMDL.anytime.AnytimeGraphMDL;
import graphMDL.anytime.SimpleUndirectedAnytimeGraphMDL;
import graphMDL.rewrittenGraphs.PatternCTInfo;
import graphMDL.rewrittenGraphs.RewrittenGraph;
import org.junit.Before;
import org.junit.Test;
import utils.GeneralUtils;

import static org.junit.Assert.assertEquals;

public class TestSimpleUndirectedAnytimeGraphMDL
{
	private static AnytimeGraphMDL anytimeGraphMDL;

	@Before
	public void setUp()
	{
		GeneralUtils.configureLogging();
		anytimeGraphMDL = new SimpleUndirectedAnytimeGraphMDL(
				ExampleGraphFactory.createSimpleUndirected(),
				CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDescImageBasedSupportDesc),
				PatternConstraints.noConstraints(),
				new FilterEmbeddingsCoverStrategy()
		);
	}

	/**
	 * Running GraphMDL+ without pruning. Verified on paper.
	 */
	@Test
	public void noPruningTestedOnPaper()
	{
		CodeTable ct = anytimeGraphMDL.computeBestCodeTableWithoutPruning(0, -1, true, null);
		assertEquals(16, anytimeGraphMDL.getTestedCodeTablesCount()); // 14 actual + the first and last run

		// CT
		assertEquals(3, ct.getRows().size());
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleUndirectedXWaXaBlank()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(0).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleUndirectedYBZ()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(1).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleUndirectedXABlank()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(2).getAutomorphisms().getCanonicalName()
		);

		// Description lengths
		assertEquals(77.93, ct.getModelDL(), 0.01);
		assertEquals(18.76, ct.getEncodedGraphLength(), 0.01);
		assertEquals(96.68, ct.getModelDL() + ct.getEncodedGraphLength(), 0.01);

		// Rewritten graph
		RewrittenGraph rewrittenGraph = anytimeGraphMDL.getRewrittenGraph();
		assertEquals(1, rewrittenGraph.getPortVertices().count());
		assertEquals(6, rewrittenGraph.getEmbeddingVerticesCount());
		assertEquals(6, rewrittenGraph.getPortEdgesCount());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(1, null)).count());
		assertEquals(3, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(2, null)).count());
		assertEquals(2, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(3, null)).count());
	}

	/**
	 * RunningGraphMDL+ with pruning enabled.
	 * **Not** verified on paper.
	 * - Expected values were initially based on commit d0af976.
	 * - Description lengths changed when, on commit just after 61d876e, we decided that using the "universal encoding" of prefix codes was not a good idea.
	 * Nothing changes from the version without pruning (pruning does not seem to make an impact on the example data that we use).
	 */
	@Test
	public void withPruning()
	{
		CodeTable ct = anytimeGraphMDL.computeBestCodeTableWithPruning(0, -1, true, null);
		assertEquals(24, anytimeGraphMDL.getTestedCodeTablesCount());

		// CT
		assertEquals(3, ct.getRows().size());
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleUndirectedXWaXaBlank()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(0).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleUndirectedYBZ()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(1).getAutomorphisms().getCanonicalName()
		);
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleUndirectedXABlank()).getAutomorphismInfo().getCanonicalName(),
				ct.getRows().get(2).getAutomorphisms().getCanonicalName()
		);

		// Description lengths
		assertEquals(77.929, ct.getModelDL(), 0.001);
		assertEquals(18.755, ct.getEncodedGraphLength(), 0.001);
		assertEquals(96.684, ct.getModelDL() + ct.getEncodedGraphLength(), 0.001);

		// Rewritten graph
		RewrittenGraph rewrittenGraph = anytimeGraphMDL.getRewrittenGraph();
		assertEquals(1, rewrittenGraph.getPortVertices().count());
		assertEquals(6, rewrittenGraph.getEmbeddingVerticesCount());
		assertEquals(6, rewrittenGraph.getPortEdgesCount());
		assertEquals(1, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(1, null)).count());
		assertEquals(3, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(2, null)).count());
		assertEquals(2, rewrittenGraph.getEmbeddingsOf(PatternCTInfo.nonSingleton(3, null)).count());
	}
}
