import graph.Graph;
import graph.GraphFactory;
import graph.printers.RDFGraphPrefixes;
import graphMDL.StandardTable;
import graphMDL.Utils;
import graphMDL.multigraphs.MultiDirectedStandardTable;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class TestMultiDirectedStandardTable
{
	@Test
	public void testExampleGraphST()
	{
		Graph g = ExampleGraphFactory.createMultiDirected();
		MultiDirectedStandardTable st = new MultiDirectedStandardTable(g);

		assertEquals(16, st.getLabelCount());
		assertEquals(9, st.getVertexLabelsCodeLengths().size());
		assertEquals(7, st.getEdgeLabelsCodeLengths().size());

		assertEquals(3.46, st.getVertexLabelsCodeLengths().get("Book"), 0.01);
		assertEquals(4.04, st.getVertexLabelsCodeLengths().get("Person"), 0.01);
		assertEquals(4.04, st.getVertexLabelsCodeLengths().get("City"), 0.01);
		assertEquals(4.04, st.getVertexLabelsCodeLengths().get("Monument"), 0.01);
		assertEquals(4.04, st.getVertexLabelsCodeLengths().get("Type:String"), 0.01);
		assertEquals(4.04, st.getVertexLabelsCodeLengths().get("Type:Integer"), 0.01);
		assertEquals(5.04, st.getVertexLabelsCodeLengths().get("Value:Alice"), 0.01);
		assertEquals(5.04, st.getVertexLabelsCodeLengths().get("Value:Bob"), 0.01);
		assertEquals(4.04, st.getVertexLabelsCodeLengths().get("Value:123"), 0.01);

		assertEquals(3.04, st.getEdgeLabelsCodeLengths().get("author"), 0.01);
		assertEquals(4.04, st.getEdgeLabelsCodeLengths().get("name"), 0.01);
		assertEquals(4.04, st.getEdgeLabelsCodeLengths().get("born_in"), 0.01);
		assertEquals(4.04, st.getEdgeLabelsCodeLengths().get("died_in"), 0.01);
		assertEquals(4.04, st.getEdgeLabelsCodeLengths().get("is_located"), 0.01);
		assertEquals(4.04, st.getEdgeLabelsCodeLengths().get("near"), 0.01);
		assertEquals(4.04, st.getEdgeLabelsCodeLengths().get("height"), 0.01);
	}

	@Test
	public void testExamplePatternsSTLength()
	{
		final Graph data = ExampleGraphFactory.createMultiDirected();
		final MultiDirectedStandardTable st = new MultiDirectedStandardTable(data);

		assertEquals(69.92, st.encode(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPersonBornDiedInCity()), 0.03);
		assertEquals(45.04, st.encode(ExamplePatternsFactory.createMultiNearMonumentsInBlank()), 0.02);
		assertEquals(45.16, st.encode(ExamplePatternsFactory.createMultiMonumentHeight123()), 0.02);
		assertEquals(36.12, st.encode(ExamplePatternsFactory.createMultiPersonNameNoValue()), 0.02);
		assertEquals(91.89, st.encode(ExamplePatternsFactory.createMultiDirectedNearMonumentsOfHeight123LocatedInBlank()), 0.03);
		assertEquals(61.89, st.encode(ExamplePatternsFactory.createMultiDirectedBlankWithStringNameBornDiedInCity()), 0.02);
		assertEquals(34.54, st.encode(ExamplePatternsFactory.createMultiDirectedBookHasAuthorPerson()), 0.01);
	}

	@Test
	public void testExampleSingletonsSTLength()
	{
		final Graph data = ExampleGraphFactory.createMultiDirected();
		final MultiDirectedStandardTable st = new MultiDirectedStandardTable(data);

		// Testing some vertex singletons
		String label;
		label = "Book";
		assertEquals(15.46, st.encodeSingletonVertex(label), 0.01);
		assertEquals(st.encodeSingletonVertex(label), st.encode(Utils.createVertexSingleton(label)), 0.01);
		label = "Person";
		assertEquals(16.04, st.encodeSingletonVertex(label), 0.01);
		assertEquals(st.encodeSingletonVertex(label), st.encode(Utils.createVertexSingleton(label)), 0.01);
		label = "City";
		assertEquals(16.04, st.encodeSingletonVertex(label), 0.01);
		assertEquals(st.encodeSingletonVertex(label), st.encode(Utils.createVertexSingleton(label)), 0.01);
		label = "Value:Alice";
		assertEquals(17.04, st.encodeSingletonVertex(label), 0.01);
		assertEquals(st.encodeSingletonVertex(label), st.encode(Utils.createVertexSingleton(label)), 0.01);

		// Testing some edge singletons
		label = "author";
		assertEquals(17.04, st.encodeSingletonEdge(label), 0.01);
		assertEquals(st.encodeSingletonEdge(label), st.encode(Utils.createEdgeSingleton(label)), 0.01);
		label = "name";
		assertEquals(18.04, st.encodeSingletonEdge(label), 0.01);
		assertEquals(st.encodeSingletonEdge(label), st.encode(Utils.createEdgeSingleton(label)), 0.01);
	}

	@Test
	public void testThrowsExceptionIfGraphLabelNotInST()
	{
		final Graph data = ExampleGraphFactory.createMultiDirected();
		final StandardTable st = new MultiDirectedStandardTable(data);

		final Graph graphWithInvalidLabels = GraphFactory.createDefaultGraph();
		graphWithInvalidLabels.addVertex("thisLabelDoesNotExist");

		assertThrows(RuntimeException.class, () -> st.encode(graphWithInvalidLabels));

		final Graph otherGraphWithInvalidLabels = GraphFactory.createDefaultGraph();
		otherGraphWithInvalidLabels.addVertex();
		otherGraphWithInvalidLabels.addVertex();
		otherGraphWithInvalidLabels.addEdge(0, 1, "thisLabelAlsoDoesNotExist");

		assertThrows(RuntimeException.class, () -> st.encode(otherGraphWithInvalidLabels));
	}
}
