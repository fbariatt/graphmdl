import graph.Graph;
import graph.GraphFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class TestGraph
{
	private Graph g;

	@Before
	public void setUp() throws Exception
	{
		this.g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 10; ++i)
			g.addVertex();
		for (int i = 1; i < 10; ++i)
			g.addEdge(i - 1, i, "e");
	}

	@Test
	public void testClearEdges()
	{
		assertEquals(9, g.getEdgeCount());
		g.clearEdges(g.getVertex(0));
		assertEquals(8, g.getEdgeCount());
		g.clearEdges();
		assertEquals(0, g.getEdgeCount());
		assertEquals(0, g.getEdges().count());
	}

	@Test
	public void testDelVertex()
	{
		assertEquals(10, g.getVertexCount());
		assertEquals(9, g.getEdgeCount());

		g.delVertex(5);
		assertEquals(9, g.getVertexCount());
		assertEquals(7, g.getEdgeCount());
	}

	@Test
	public void testVertexGetEdgeCounts()
	{
		for (int i = 1; i < 9; ++i)
		{
			assertEquals(1, g.getOutEdgeCount(i));
			assertEquals(1, g.getInEdgeCount(i));
		}

		assertEquals(1, g.getOutEdgeCount(0));
		assertEquals(0, g.getInEdgeCount(0));
		assertEquals(0, g.getOutEdgeCount(9));
		assertEquals(1, g.getInEdgeCount(9));
	}

	@Test
	public void testInducedSubgraph()
	{
		Set<Integer> vertices = new HashSet<>();
		vertices.add(4);
		vertices.add(5);
		Graph subgraph = g.inducedSubGraphByIndices(vertices, GraphFactory::createDefaultGraph).subgraph;

		assertEquals(2, subgraph.getVertexCount());
		assertEquals(1, subgraph.getEdgeCount());
	}

	@Test
	public void testConcat()
	{
		Graph otherGraph = GraphFactory.createDefaultGraph();
		otherGraph.addVertex();
		otherGraph.addVertex();
		otherGraph.addEdge(0, 1, "edge");

		g.concat(otherGraph);
		assertEquals(12, g.getVertexCount());
		assertEquals(10, g.getEdgeCount());
	}
}
