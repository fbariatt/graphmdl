import graph.Graph;
import graph.GraphFactory;
import graph.printers.RDFGraphPrefixes;
import graphMDL.StandardTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedIndependentVertexAndEdgeST;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class TestSimpleDirectedStandardTables
{
	private static void assertSTAreEqual(Map<String, Double> expectedST, Map<String, Double> actualST)
	{
		assertEquals(expectedST.size(), actualST.size());
		expectedST.forEach((k, v) -> {
			assertTrue(actualST.containsKey(k));
			assertEquals(v, actualST.get(k), 0.01);
		});
	}

	@Test
	public void testExampleGraphSTs()
	{
		final Graph graph = ExampleGraphFactory.createSimpleDirected();
		final StandardTable st = new SimpleDirectedIndependentVertexAndEdgeST(graph);

		Map<String, Double> expectedVertexST = new HashMap<>();
		expectedVertexST.put("x", 1.17);
		expectedVertexST.put("y", 3.17);
		expectedVertexST.put("z", 1.58);
		expectedVertexST.put("w", 3.17);
		assertSTAreEqual(expectedVertexST, st.getVertexLabelsCodeLengths());

		Map<String, Double> expectedEdgeST = new HashMap<>();
		expectedEdgeST.put("a", 0.68);
		expectedEdgeST.put("b", 1.42);
		assertSTAreEqual(expectedEdgeST, st.getEdgeLabelsCodeLengths());
	}

	@Test
	public void testExamplePatternsSTLength()
	{
		final Graph data = ExampleGraphFactory.createSimpleDirected();
		final StandardTable st = new SimpleDirectedIndependentVertexAndEdgeST(data);

		assertEquals(9.17, st.encodeSingletonVertex("x"), 0.01);
		assertEquals(11.17, st.encodeSingletonVertex("y"), 0.01);
		assertEquals(9.58, st.encodeSingletonVertex("z"), 0.01);
		assertEquals(11.17, st.encodeSingletonVertex("w"), 0.01);

		assertEquals(9.68, st.encodeSingletonEdge("a"), 0.01);
		assertEquals(10.42, st.encodeSingletonEdge("b"), 0.01);

		assertEquals(32.94, st.encode(ExamplePatternsFactory.createSimpleDirectedXYZ()), 0.01);

		assertEquals(19.7, st.encode(ExamplePatternsFactory.createSimpleDirectedXAAX()), 0.01);
	}

	@Test
	public void testThrowsExceptionIfGraphLabelNotInST()
	{
		final Graph data = ExampleGraphFactory.createSimpleDirected();
		final StandardTable st = new SimpleDirectedIndependentVertexAndEdgeST(data);

		final Graph graphWithInvalidLabels = GraphFactory.createDefaultGraph();
		graphWithInvalidLabels.addVertex("thisLabelDoesNotExist");

		assertThrows(RuntimeException.class, () -> st.encode(graphWithInvalidLabels));

		final Graph otherGraphWithInvalidLabels = GraphFactory.createDefaultGraph();
		otherGraphWithInvalidLabels.addVertex();
		otherGraphWithInvalidLabels.addVertex();
		otherGraphWithInvalidLabels.addEdge(0, 1, "thisLabelAlsoDoesNotExist");

		assertThrows(RuntimeException.class, () -> st.encode(otherGraphWithInvalidLabels));
	}
}
