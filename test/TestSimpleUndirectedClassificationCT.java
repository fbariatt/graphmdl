import graph.Graph;
import graphMDL.*;
import graphMDL.classification.SimpleClassificationCT;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedCodeTable;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedIndependentVertexAndEdgeST;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.GeneralUtils;

import static org.junit.Assert.assertEquals;

public class TestSimpleUndirectedClassificationCT
{

	@BeforeClass
	public static void beforeClass()
	{
		GeneralUtils.configureLogging();
	}

	@Test
	public void TestOnPaper1()
	{
		final Graph data = ExampleGraphFactory.createSimpleUndirected();
		final SimpleUndirectedIndependentVertexAndEdgeST st = new SimpleUndirectedIndependentVertexAndEdgeST(data);
		SimpleUndirectedCodeTable ct = new SimpleUndirectedCodeTable(st, CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDesc));

		final Graph P1 = ExamplePatternsFactory.createSimpleUndirectedBlankBZ();
		ct.addRow(new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(P1),
				st.encode(P1),
				0,
				SimpleUndirectedUtils.graphLabelCount(P1)
		));

		final Graph P2 = ExamplePatternsFactory.createSimpleUndirectedBlankAY();
		ct.addRow(new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(P2),
				st.encode(P2),
				0,
				SimpleUndirectedUtils.graphLabelCount(P2)
		));

		ct.apply(data, false);

		SimpleClassificationCT classificationCT = new SimpleClassificationCT(ct, st.getVertexLabelsCodeLengths().keySet(), st.getEdgeLabelsCodeLengths().keySet());

		// In this test we decided to test again on the same data, to see the description length
		final double graphDescriptionLength = classificationCT.computeGraphDescriptionLength(data);

		assertEquals(95.76, graphDescriptionLength, 0.05);
	}
}
