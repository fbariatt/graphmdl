import graph.Graph;
import graph.GraphFactory;
import graph.automorphisms.Automorphism;
import graph.automorphisms.AutomorphismInfo;
import graph.automorphisms.AutomorphismSearch;
import graph.automorphisms.certificates.GraphCertificate;
import graphMDL.PatternInfo;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestAutomorphisms
{
	@Test
	public void testEdgeSingleton()
	{
		Graph g = GraphFactory.createDefaultGraph();
		g.addVertex();
		g.addVertex();
		g.addEdge(0, 1, "a");

		AutomorphismInfo automorphismInfo = PatternInfo.createComputingSaturatedAutomorphisms(g).getAutomorphismInfo();
		assertEquals(1, automorphismInfo.getAutomorphisms().size());

		Graph gFlipped = GraphFactory.createDefaultGraph();
		gFlipped.addVertex();
		gFlipped.addVertex();
		gFlipped.addEdge(1, 0, "a");

		AutomorphismInfo flippedAutomorphismInfo = PatternInfo.createComputingSaturatedAutomorphisms(gFlipped).getAutomorphismInfo();
		assertEquals(1, flippedAutomorphismInfo.getAutomorphisms().size());

		assertEquals(automorphismInfo.getCanonicalName(), flippedAutomorphismInfo.getCanonicalName());
	}

	@Test
	public void testUndirectedTriangle()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 3; ++i)
			g.addVertex();
		g.addEdge(0, 1, "a");
		g.addEdge(1, 0, "a");
		g.addEdge(1, 2, "a");
		g.addEdge(2, 1, "a");
		g.addEdge(2, 0, "a");
		g.addEdge(0, 2, "a");
		AutomorphismInfo automorphismInfo = AutomorphismSearch.findAndSaturateAutomorphisms(g);

		assertEquals(6, automorphismInfo.getAutomorphisms().size());
		assertAllAutomorphismHaveSameCanonicalName(g, automorphismInfo);
	}

	@Test
	public void testDirectedSquare()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 4; ++i)
			g.addVertex();
		g.addEdge(0, 1, "a");
		g.addEdge(1, 2, "a");
		g.addEdge(2, 3, "a");
		g.addEdge(3, 0, "a");
		AutomorphismInfo automorphismInfo = AutomorphismSearch.findAndSaturateAutomorphisms(g);

		assertEquals(4, automorphismInfo.getAutomorphisms().size());
		assertAllAutomorphismHaveSameCanonicalName(g, automorphismInfo);
	}

	@Test
	public void testUndirectedSquare()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 4; ++i)
			g.addVertex();
		g.addEdge(0, 1, "a");
		g.addEdge(1, 2, "a");
		g.addEdge(2, 3, "a");
		g.addEdge(3, 0, "a");
		g.addEdge(3, 2, "a");
		g.addEdge(2, 1, "a");
		g.addEdge(1, 0, "a");
		g.addEdge(0, 3, "a");
		AutomorphismInfo automorphismInfo = AutomorphismSearch.findAndSaturateAutomorphisms(g);

		assertEquals(8, automorphismInfo.getAutomorphisms().size());
		assertAllAutomorphismHaveSameCanonicalName(g, automorphismInfo);
	}

	@Test
	public void testDirectedCross()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 5; ++i)
			g.addVertex();
		g.addEdge(1, 0, "a");
		g.addEdge(2, 0, "a");
		g.addEdge(0, 3, "a");
		g.addEdge(0, 4, "a");
		AutomorphismInfo automorphismInfo = AutomorphismSearch.findAndSaturateAutomorphisms(g);

		assertEquals(4, automorphismInfo.getAutomorphisms().size());
		assertAllAutomorphismHaveSameCanonicalName(g, automorphismInfo);
	}

	@Test
	public void testDirectedMultiLevelBranches()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 9; ++i)
			g.addVertex("v");
		g.addEdge(1, 0, "e");
		g.addEdge(2, 0, "e");
		g.addEdge(0, 3, "e");
		g.addEdge(0, 4, "e");
		g.addEdge(3, 5, "e");
		g.addEdge(3, 6, "e");
		g.addEdge(4, 7, "e");
		g.addEdge(4, 8, "e");

		AutomorphismInfo automorphismInfo = AutomorphismSearch.findAndSaturateAutomorphisms(g);
		assertEquals(16, automorphismInfo.getAutomorphisms().size());
		assertAllAutomorphismHaveSameCanonicalName(g, automorphismInfo);
	}

	@Test
	public void testDirectedHexagon()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 6; ++i)
			g.addVertex();
		g.addEdge(0, 1, "a");
		g.addEdge(1, 2, "a");
		g.addEdge(2, 3, "a");
		g.addEdge(3, 4, "a");
		g.addEdge(4, 5, "a");
		g.addEdge(5, 0, "a");
		AutomorphismInfo automorphismInfo = AutomorphismSearch.findAndSaturateAutomorphisms(g);

		assertEquals(6, automorphismInfo.getAutomorphisms().size());
		assertAllAutomorphismHaveSameCanonicalName(g, automorphismInfo);
	}

	@Test
	public void testHexagonWithOnlyOneMirrorAutomorphism()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 6; ++i)
			g.addVertex();
		g.addEdge(0, 1, "a");
		g.addEdge(0, 2, "a");
		g.addEdge(1, 3, "a");
		g.addEdge(2, 4, "a");
		g.addEdge(3, 5, "a");
		g.addEdge(4, 5, "a");
		AutomorphismInfo automorphismInfo = AutomorphismSearch.findAndSaturateAutomorphisms(g);

		assertEquals(2, automorphismInfo.getAutomorphisms().size());
		assertAllAutomorphismHaveSameCanonicalName(g, automorphismInfo);
	}

	@Test
	public void testBenzeneCycle()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 6; ++i)
			g.addVertex("C");
		g.addEdge(0, 1, "s");
		g.addEdge(1, 0, "s");
		g.addEdge(1, 2, "d");
		g.addEdge(2, 1, "d");
		g.addEdge(2, 3, "s");
		g.addEdge(3, 2, "s");
		g.addEdge(3, 4, "d");
		g.addEdge(4, 3, "d");
		g.addEdge(4, 5, "s");
		g.addEdge(5, 4, "s");
		g.addEdge(5, 0, "d");
		g.addEdge(0, 5, "d");
		AutomorphismInfo automorphismInfo = AutomorphismSearch.findAndSaturateAutomorphisms(g);

		assertEquals(6, automorphismInfo.getAutomorphisms().size());
		assertAllAutomorphismHaveSameCanonicalName(g, automorphismInfo);
	}

	/**
	 * Ensures that the same canonical name is computed from all the different automorphic forms of the graph.
	 * This should be the case because it is what defines a "canonical name".
	 *
	 * @param baseGraph        Initial graph, used to compute automorphic forms
	 * @param automorphismInfo The automorphisms of the graph
	 */
	private static void assertAllAutomorphismHaveSameCanonicalName(Graph baseGraph, AutomorphismInfo automorphismInfo)
	{
		final GraphCertificate canonicalName = automorphismInfo.getCanonicalName();
		for (Automorphism automorphism : automorphismInfo.getAutomorphisms())
		{
			// Creating the graph object obtained when applied the automorphism to the base graph
			Graph automorphicGraph = GraphFactory.fromGraphWithAutomorphism(baseGraph, automorphism);
			// Computing the canonical name of this new graph object, as we would if it was a totally unrelated graph
			GraphCertificate automorphicCanonicalName = new AutomorphismSearch(automorphicGraph).findAutomorphisms().getCanonicalName();
			// The same canonical name should be found
			assertEquals(canonicalName, automorphicCanonicalName);
		}
	}
}
