import graph.Edge;
import graph.Graph;
import graph.GraphFactory;
import graph.loaders.RDFToGraph;
import graph.loaders.TextToGraph;
import graph.printers.GraphToRDF;
import graph.printers.GraphToText;
import graph.printers.RDFGraphPrefixes;
import graphMDL.PatternInfo;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.GeneralUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class TestGraphSerializations
{

	private static final String EXAMPLE_SIMPLE_DIRECTED_AS_TEXT = "t\n" +
			"# 8 vertices\n" +
			"# 8 edges\n" +
			"v 0 y\n" +
			"v 1 x\n" +
			"v 2 z\n" +
			"v 3 x\n" +
			"v 4 z\n" +
			"v 5 x\n" +
			"v 6 z\n" +
			"v 7 w x\n" +
			"e 0 2 b\n" +
			"e 0 4 b\n" +
			"e 0 6 b\n" +
			"e 1 0 a\n" +
			"e 3 0 a\n" +
			"e 5 0 a\n" +
			"e 5 7 a\n" +
			"e 7 5 a\n";

	@BeforeClass
	public static void beforeClass()
	{
		GeneralUtils.configureLogging();
	}

	@Test
	public void writeGraphAsText()
	{
		Graph g = ExampleGraphFactory.createSimpleDirected();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		GraphToText.print(g, new PrintWriter(os), false);

		assertEquals(EXAMPLE_SIMPLE_DIRECTED_AS_TEXT, os.toString());
	}

	@Test
	public void loadGraphFromText() throws IOException
	{
		Graph graph = TextToGraph.read(new BufferedReader(new StringReader(EXAMPLE_SIMPLE_DIRECTED_AS_TEXT)));

		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(ExampleGraphFactory.createSimpleDirected()).getAutomorphismInfo().getCanonicalName(),
				PatternInfo.createComputingSaturatedAutomorphisms(graph).getAutomorphismInfo().getCanonicalName()
		);
	}

	@Test
	public void textToGraphToText() throws IOException
	{
		Graph graph = TextToGraph.read(new BufferedReader(new StringReader(EXAMPLE_SIMPLE_DIRECTED_AS_TEXT)));
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		GraphToText.print(graph, new PrintWriter(output), true);
		assertEquals(EXAMPLE_SIMPLE_DIRECTED_AS_TEXT, output.toString());

		output.reset();
		GraphToText.print(graph, new PrintWriter(output), false);
		assertEquals(EXAMPLE_SIMPLE_DIRECTED_AS_TEXT, output.toString());
	}

	@Test
	public void graphToTextToGraph() throws IOException
	{
		Graph g = ExampleGraphFactory.createSimpleDirected();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		GraphToText.print(g, new PrintWriter(output), false);
		Graph reloaded = TextToGraph.read(new BufferedReader(new StringReader(output.toString())));
		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(g).getAutomorphismInfo().getCanonicalName(),
				PatternInfo.createComputingSaturatedAutomorphisms(reloaded).getAutomorphismInfo().getCanonicalName()
		);
	}

	@Test
	public void loadGraphFromRDF()
	{
		// RDF representation of the example directed multigraph
		String ttlGraph = "@base <http://example.org/>\n" +
				"@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\n" +
				"\n" +
				"<v1>      a          <Book>;\n" +
				"     <author>     <v5>.\n" +
				"<v2>      a          <Book>;\n" +
				"     <author>     <v5>.\n" +
				"<v3>      a          <Book>;\n" +
				"     <author>     <v5>;\n" +
				"     <author>     <v6>.\n" +
				"<v5>     a          <Person>;\n" +
				"     <name>          'Alice'^^xsd:string;\n" +
				"     <born_in>     <v8>;\n" +
				"     <died_in>     <v8>.\n" +
				"<v6>     a          <Person>;\n" +
				"     <name>          'Bob'^^xsd:string;\n" +
				"     <born_in>     <v9>;\n" +
				"     <died_in>     <v9>.\n" +
				"<v8>     a          <City>.\n" +
				"<v9>     a          <City>.\n" +
				"<v10>     a          <Monument>;\n" +
				"     <is_located>     <v8>;\n" +
				"     <near>          <v11>;\n" +
				"     <height>     '123'^^xsd:integer.\n" +
				"<v11>     a          <Monument>;\n" +
				"     <is_located>     <v8>;\n" +
				"     <near>          <v10>;\n" +
				"     <height>     '123'^^xsd:integer.";

		Graph loadedGraph = RDFToGraph.read(new ByteArrayInputStream(ttlGraph.getBytes()), "ttl");

		assertEquals(13, loadedGraph.getVertexCount());
		assertEquals(16, loadedGraph.getEdgeCount());
		assertEquals(9, loadedGraph.getVertices().flatMap(v -> v.getLabels().keySet().stream()).distinct().count());
		assertEquals(7, loadedGraph.getEdges().map(Edge::getLabel).distinct().count());
	}

	@Test
	public void loadGraphWithLiteralsFromRDF()
	{
		ByteArrayOutputStream rdfSerialization = new ByteArrayOutputStream();
		ExampleGraphFactory.createModelWithLiteralsAndLists().write(rdfSerialization, "ttl");
		Graph graph = RDFToGraph.read(new ByteArrayInputStream(rdfSerialization.toByteArray()), "ttl");

		assertEquals(11, graph.getVertexCount());
		assertEquals(8, graph.getEdgeCount());
		assertEquals(5, graph.getVertices().filter(v -> v.getName() != null).count());
		assertEquals(2, graph.getVerticesWithLabel(RDFGraphPrefixes.LITERAL_VALUE_URI + "30").count());
		assertEquals(2, graph.getVerticesWithLabel(XSD.xint.getURI()).count());
		assertEquals(2, graph.getVerticesWithLabel(RDF.nil.getURI()).count());
		assertEquals(0, graph.getEdges().filter(e -> e.getLabel().equals(RDF.type.getURI())).count());
	}

	@Test
	public void rdfToGraphToRDF()
	{
		Model input = ExampleGraphFactory.createModelWithLiteralsAndLists();
		Graph loaded;
		{
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			input.write(outputStream, "ntriple");
			loaded = RDFToGraph.read(new ByteArrayInputStream(outputStream.toByteArray()), "ntriple");
		}
		Model output = new GraphToRDF.GraphToModel(loaded, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel).getModel();
		// The serialization does not use blank nodes, giving instead a custom URI to nodes that do not have one
		// however jena "isomorphism" check can not match uri nodes to blank nodes.
		// Therefore we modify the reloaded model to convert the nodes with the custom URI to blank nodes, so that the isomorphism check can pass.
		Set<Resource> semanticallyBlankNodes = Stream.concat(GeneralUtils.streamFromIterator(output.listSubjects()), GeneralUtils.streamFromIterator(output.listObjects()))
				.filter(RDFNode::isURIResource)
				.map(RDFNode::asResource)
				.filter(resource -> resource.getURI().startsWith(RDFGraphPrefixes.VERTEX_URI))
				.collect(Collectors.toSet());
		List<Statement> toRemove = new ArrayList<>(), toAdd = new ArrayList<>();
		for (Resource oldResource : semanticallyBlankNodes)
		{
			Resource newResource = output.createResource();
			output.listStatements(oldResource, null, (RDFNode) null).forEachRemaining(statement -> {
				toRemove.add(statement);
				toAdd.add(output.createStatement(newResource, statement.getPredicate(), statement.getObject()));
			});
			output.listStatements(null, null, oldResource).forEachRemaining(statement -> {
				toRemove.add(statement);
				toAdd.add(output.createStatement(statement.getSubject(), statement.getPredicate(), newResource));
			});
		}
		toRemove.forEach(output::remove);
		toAdd.forEach(output::add);
		assertTrue(input.isIsomorphicWith(output));
	}

	@Test
	public void graphToRDFToGraph()
	{
		Graph g = ExampleGraphFactory.createMultiDirected();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		new GraphToRDF.GraphToModel(g, s -> false, _v -> false).getModel().write(output, "TTL");
		Graph reloaded = RDFToGraph.read(new ByteArrayInputStream(output.toByteArray()), "TTL");
		// Serialization transformed labels to URIs, we undo that
		reloaded.getVertices().forEach(vertex -> {
			List<String> newLabels = vertex.getLabels().keySet().stream()
					.map(label -> label.replace(RDFGraphPrefixes.VERTEX_LABEL_URI, ""))
					.collect(Collectors.toList());
			vertex.clearLabels();
			newLabels.forEach(vertex::addLabel);
		});
		List<Edge> oldEdges = reloaded.getEdges().collect(Collectors.toList());
		reloaded.clearEdges();
		oldEdges.forEach(edge -> reloaded.addEdge(edge.getSource(), edge.getTarget(), edge.getLabel().replace(RDFGraphPrefixes.EDGE_LABEL_URI, "")));

		assertEquals(
				PatternInfo.createComputingSaturatedAutomorphisms(g).getAutomorphismInfo().getCanonicalName(),
				PatternInfo.createComputingSaturatedAutomorphisms(reloaded).getAutomorphismInfo().getCanonicalName()
		);
	}

	@Test
	public void patternToRDFQuery()
	{
		// Very simple pattern with two vertices without labels
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex();
			g.addVertex();
			g.addEdge(0, 1, "price");
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel);

			TestingUtils.assertSparqlQuerySemanticallyEqual(
					"SELECT ?0 ?1 WHERE { ?0 <urn:GraphMDL:EdgeLabel:price> ?1 }",
					patternQuery.getSelectQuery(false)
			);
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					"SELECT ?0 ?1 WHERE { ?0 <urn:GraphMDL:EdgeLabel:price> ?1 FILTER(?0 != ?1) }",
					patternQuery.getSelectQuery(true)
			);
		}
		// Three vertices, which complicates the filter expression
		{
			Graph g = GraphFactory.createDefaultGraph();
			IntStream.range(0, 3).forEach(_i -> g.addVertex());
			g.addEdge(0, 1, "authors");
			g.addEdge(1, 2, RDF.first.toString());
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel);

			TestingUtils.assertSparqlQuerySemanticallyEqual(
					"SELECT ?0 ?1 ?2 WHERE { ?0 <urn:GraphMDL:EdgeLabel:authors> ?1 . ?1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?2 FILTER( (?0 NOT IN (?1, ?2)) && (?1 != ?2) ) }",
					patternQuery.getSelectQuery(true)
			);
		}
		// A pattern with a non-typed literal
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex();
			g.addVertex(RDFGraphPrefixes.LITERAL_VALUE_URI + "30");
			g.addEdge(0, 1, "price");
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel);

			final String sparql = "SELECT ?0 WHERE { ?0 <urn:GraphMDL:EdgeLabel:price> \"30\" }"; // The query does not change even with vertex filter activated
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(false)
			);
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(true)
			);
		}
		// A pattern with a typed literal
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex();
			g.addVertex(List.of(RDFGraphPrefixes.LITERAL_VALUE_URI + "30", XSD.xint.getURI()));
			g.addEdge(0, 1, "price");
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel);

			final String sparql = "SELECT ?0 WHERE { ?0 <urn:GraphMDL:EdgeLabel:price> \"30\"^^<http://www.w3.org/2001/XMLSchema#int> }"; // The query does not change even with vertex filter activated
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(false)
			);
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(true)
			);
		}
		// A pattern with a typed literal without a value
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex();
			g.addVertex(XSD.xint.getURI());
			g.addEdge(0, 1, "price");
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(
					g,
					GraphToRDF::isURIIfHasColon,
					GraphToRDF.makeIsLiteralIfHasLabelIn(Set.of(XSD.xint.getURI()))
			);

			final String sparql = "SELECT ?0 ?1 WHERE { ?0 <urn:GraphMDL:EdgeLabel:price> ?1 FILTER(datatype(?1) = <http://www.w3.org/2001/XMLSchema#int>) }"; // The query does not change even with vertex filter activated
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(false)
			);
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(true)
			);
		}
		// A pattern with a literal with language
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex();
			g.addVertex(List.of(RDF.langString.getURI(), RDFGraphPrefixes.LITERAL_LANG_URI + "en", RDFGraphPrefixes.LITERAL_VALUE_URI + "Title"));
			g.addEdge(0, 1, "title");
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel);

			final String sparql = "SELECT ?0 WHERE { ?0 <urn:GraphMDL:EdgeLabel:title> \"Title\"@en }"; // The query does not change even with vertex filter activated
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(false)
			);
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(true)
			);
		}
		// A pattern with a literal with language without a value
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex();
			g.addVertex(List.of(RDF.langString.getURI(), RDFGraphPrefixes.LITERAL_LANG_URI + "en"));
			g.addEdge(0, 1, "title");
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(
					g,
					GraphToRDF::isURIIfHasColon,
					GraphToRDF.makeIsLiteralIfHasLabelIn(Set.of(RDF.langString.getURI()))
			);

			final String sparql = "SELECT ?0 ?1 WHERE { ?0 <urn:GraphMDL:EdgeLabel:title> ?1 FILTER(lang(?1) = \"en\") }"; // The query does not change even with vertex filter activated
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(false)
			);
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					sparql,
					patternQuery.getSelectQuery(true)
			);
		}
		// A pattern with more vertices and some vertex labels
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex();
			g.addVertex();
			g.addVertex("Person");
			g.addVertex();
			g.addEdge(0, 1, "authors");
			g.addEdge(1, 2, RDF.first.getURI());
			g.addEdge(1, 3, RDF.rest.getURI());
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel);

			final String queryMain = "SELECT ?0 ?1 ?2 ?3 WHERE { ?2 a <urn:GraphMDL:VertexLabel:Person>. ?0 <urn:GraphMDL:EdgeLabel:authors> ?1 . ?1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?2 ; <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> ?3 ";
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					queryMain + "}",
					patternQuery.getSelectQuery(false)
			);
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					queryMain + "FILTER ( ( (?0 NOT IN(?1, ?2, ?3)) && (?1 NOT IN(?2, ?3)) ) && (?2 != ?3) )}",
					patternQuery.getSelectQuery(true)
			);
		}
		// A pattern with a rdf:nil
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex();
			g.addVertex();
			g.addVertex("Person");
			g.addVertex(RDF.nil.getURI());
			g.addEdge(0, 1, "authors");
			g.addEdge(1, 2, RDF.first.getURI());
			g.addEdge(1, 3, RDF.rest.getURI());
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel);

			final String queryMain = "SELECT ?0 ?1 ?2 WHERE { ?2 a <urn:GraphMDL:VertexLabel:Person>. ?0 <urn:GraphMDL:EdgeLabel:authors> ?1 . ?1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?2 ; <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> <http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>  ";
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					queryMain + "}",
					patternQuery.getSelectQuery(false)
			);
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					queryMain + "FILTER ( (?0 NOT IN(?1, ?2)) && (?1 != ?2) )}",
					patternQuery.getSelectQuery(true)
			);
		}
		// A pattern with a single vertex that can be converted to a literal (with both value and type)
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex(List.of(RDFGraphPrefixes.LITERAL_VALUE_URI + "30", XSD.xint.getURI()));
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(g, GraphToRDF::isURIIfHasColon, GraphToRDF::isLiteralIfHasValueLabel);

			assertTrue(patternQuery.isEmpty());
			final String query = "SELECT WHERE {}";
			TestingUtils.assertStringEqualsIgnoreSpacesAndNewlines(
					query,
					patternQuery.getSelectQuery(false).toString()
			);
			TestingUtils.assertStringEqualsIgnoreSpacesAndNewlines(
					query,
					patternQuery.getSelectQuery(true).toString()
			);
		}
		// A pattern with a single vertex that can be converted to a literal (without a value)
		{
			Graph g = GraphFactory.createDefaultGraph();
			g.addVertex(List.of(XSD.xint.getURI()));
			GraphToRDF.PatternToQuery patternQuery = new GraphToRDF.PatternToQuery(g, GraphToRDF::isURIIfHasColon, GraphToRDF.makeIsLiteralIfHasLabelIn(Set.of(XSD.xint.getURI())));

			assertFalse(patternQuery.isEmpty());
			final String query = "SELECT ?0 WHERE { FILTER (datatype(?0) = <http://www.w3.org/2001/XMLSchema#int>)}";
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					query,
					patternQuery.getSelectQuery(false)
			);
			TestingUtils.assertSparqlQuerySemanticallyEqual(
					query,
					patternQuery.getSelectQuery(true)
			);
		}
	}

	/**
	 * Verify if it is possible to specify vertices and edges of the pattern that should not appear in the resulting query.
	 */
	@Test
	public void patternToRDFQueryWithIgnoredElements()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 4; ++i)
			g.addVertex();
		g.addEdge(0, 1, "author");
		g.addEdge(0, 3, "author");
		g.addEdge(1, 2, "born_in");
		g.addEdge(1, 2, "died_in");

		GraphToRDF.PatternToQuery patternToQuery = new GraphToRDF.PatternToQuery(
				g,
				GraphToRDF::isURIIfHasColon,
				GraphToRDF::isLiteralIfHasValueLabel,
				Set.of(g.getVertex(3)),
				Set.of(g.getEdge(1, 2, "died_in"))
		);
		TestingUtils.assertSparqlQuerySemanticallyEqual(
				"SELECT ?0 ?1 ?2 WHERE { ?0 <urn:GraphMDL:EdgeLabel:author> ?1. ?1 <urn:GraphMDL:EdgeLabel:born_in> ?2. }",
				patternToQuery.getSelectQuery(false)
		);
	}
}
