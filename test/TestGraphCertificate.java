import graph.automorphisms.certificates.CertificateElement;
import graph.automorphisms.certificates.EdgeCertificateElement;
import graph.automorphisms.certificates.GraphCertificate;
import graph.automorphisms.certificates.VertexLabelCertificateElement;
import graph.printers.RDFGraphPrefixes;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestGraphCertificate
{

	@Test
	public void testExampleCertificate()
	{
		GraphCertificate certificate = new GraphCertificate(ExamplePatternsFactory.createSimpleDirectedBlankBZ());
		List<CertificateElement> certificateElements = certificate.getCertificateElements();
		String expectedCertificate = "2vertices z1 b01";
		assertEquals(expectedCertificate, certificate.toString());
		assertEquals(2, certificateElements.size());
		assertEquals(1, certificateElements.stream().filter(element -> element instanceof VertexLabelCertificateElement).count());
		assertEquals(1, certificateElements.stream().filter(element -> element instanceof EdgeCertificateElement).count());
	}
}
