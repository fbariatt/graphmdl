import graphMDL.PrequentialCode;
import graphMDL.Utils;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;


public class TestPrequentialCodes
{
	// Expected values for epsilon = 0.5
	private static final Map<List<Integer>, Double> epsilonHalfExpectedValues = Map.of(
			List.of(0, 0), 0.0,
			List.of(1, 0), 1.0,
			List.of(1, 1, 1, 1), 10.907,
			List.of(2, 2, 4), 15.154,
			// Example from "Interpretable multiclass classification by MDL-based rule lists", but with epsilon = 0.5
			List.of(10, 8, 0, 0, 0, 0, 0), 29.295,
			// Trying large values for which factorial overflows
			List.of(100, 500, 4, 10, 0), 515.895
	);

	// Expected values for epsilon = 1
	private static final Map<List<Integer>, Double> epsilonOneExpectedValues = Map.of(
			List.of(0, 0), 0.0,
			List.of(1, 0), 1.0,
			List.of(1, 1, 1, 1), 9.714,
			List.of(2, 2, 4), 14.206,
			// Example from "Interpretable multiclass classification by MDL-based rule lists"
			List.of(10, 8, 0, 0, 0, 0, 0), 32.456,
			// Trying large values for which factorial overflows
			List.of(100, 500, 4, 10, 0), 521.061
	);

	@Test
	public void testGammaInt()
	{
		assertThrows(IllegalArgumentException.class, () -> PrequentialCode.logGammaInt(-1));
		assertThrows(IllegalArgumentException.class, () -> PrequentialCode.logGammaInt(0)); // We do not allow G(0)
		assertEquals(0.0, PrequentialCode.logGammaInt(1), 1E-6); // log(G(1)) = log(0!) = log(1) = 0
		assertEquals(0.0, PrequentialCode.logGammaInt(2), 1E-6); // log(G(2)) = log(1!) = log(1) = 0
		assertEquals(1.0, PrequentialCode.logGammaInt(3), 1E-6);
		assertEquals(2.585, PrequentialCode.logGammaInt(4), 0.001);
		assertEquals(4.585, PrequentialCode.logGammaInt(5), 0.001);
		assertEquals(518.121, PrequentialCode.logGammaInt(100), 0.001); // Value verified with WolframAlpha
	}

	@Test
	public void testGamma05()
	{
		assertThrows(IllegalArgumentException.class, () -> PrequentialCode.logGamma05(-1));
		assertEquals(Utils.log2(Math.sqrt(Math.PI)), PrequentialCode.logGamma05(0), 1E-6); // log2(gamma(0.5))
		// Values verified with WolframAlpha
		assertEquals(0.825748, PrequentialCode.logGamma05(0), 1E-6); // log2(gamma(0.5))
		assertEquals(-0.174252, PrequentialCode.logGamma05(1), 1E-6); // log2(gamma(1.5))
		assertEquals(0.410711, PrequentialCode.logGamma05(2), 1E-6); // log2(gamma(2.5))
		assertEquals(1.732639, PrequentialCode.logGamma05(3), 1E-6); // log2(gamma(3.5))
		assertEquals(521.441262, PrequentialCode.logGamma05(100), 1E-6); // log2(gamma(100.5))
	}

	@Test
	public void testEpsilon05()
	{
		// Gamma version
		epsilonHalfExpectedValues.forEach(
				(usages, expected) -> assertEquals(expected, PrequentialCode.prequentialCodeDLGamma(usages.iterator(), 0.5), 0.01)
		);
		// Iterative version
		epsilonHalfExpectedValues.forEach(
				(usages, expected) -> assertEquals(expected, PrequentialCode.prequentialCodeDLIterative(usages, 0.5), 0.001)
		);
	}

	@Test
	public void testEpsilon1()
	{
		// Gamma version
		epsilonOneExpectedValues.forEach(
				(usages, expected) -> assertEquals(expected, PrequentialCode.prequentialCodeDLGamma(usages.iterator(), 1.0), 0.001)
		);
		// Iterative version
		epsilonOneExpectedValues.forEach(
				(usages, expected) -> assertEquals(expected, PrequentialCode.prequentialCodeDLIterative(usages, 1.0), 0.001)
		);
	}
}
