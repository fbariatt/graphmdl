import graph.Graph;
import graph.GraphFactory;
import graph.WeaklyConnectedComponentSearch;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class TestWCC
{
	@Test
	public void simpleTest()
	{
		Graph g = GraphFactory.createDefaultGraph();
		for (int i = 0; i < 4; ++i)
			g.addVertex();
		g.addEdge(0, 2, "e");
		g.addEdge(1, 2, "e");
		g.addEdge(2, 3, "e");

		assertEquals(
				List.of(Stream.of(0, 1, 2, 3).map(g::getVertex).collect(Collectors.toSet())),
				WeaklyConnectedComponentSearch.computeWCCs(g)
		);

		// We consider as if one edge is not in the graph
		TestingUtils.assertListEqualsIgnoreOrder(
				List.of(
						Stream.of(0, 2, 3).map(g::getVertex).collect(Collectors.toSet()),
						Set.of(g.getVertex(1))
				),
				WeaklyConnectedComponentSearch.computeWCCs(g, Collections.emptySet(), Set.of(g.getEdge(1, 2, "e")))
		);

		// We consider as if one vertex is not in the graph
		TestingUtils.assertListEqualsIgnoreOrder(
				IntStream.range(0, 4).filter(i -> i != 2).mapToObj(i -> Set.of(g.getVertex(i))).collect(Collectors.toList()),
				WeaklyConnectedComponentSearch.computeWCCs(g, Set.of(g.getVertex(2)), Collections.emptySet())
		);
	}
}
