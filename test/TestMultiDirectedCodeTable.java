import graph.Graph;
import graphMDL.*;
import graphMDL.multigraphs.MultiDirectedCodeTable;
import graphMDL.multigraphs.MultiDirectedStandardTable;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.GeneralUtils;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestMultiDirectedCodeTable
{
	@BeforeClass
	public static void beforeClass()
	{
		GeneralUtils.configureLogging();
	}

	@Test
	public void TestOnPaperSingletonsOnly()
	{
		final Graph data = ExampleGraphFactory.createMultiDirected();
		final MultiDirectedStandardTable st = new MultiDirectedStandardTable(data);

		CodeTable ct = new MultiDirectedCodeTable(st, CodeTableHeuristics.PartialOrder.noOrder);
		ct.apply(data, false);

		assertEquals(0, ct.getRows().size());
		assertEquals(9, ct.getSingletonVertexUsages().size());
		assertEquals(7, ct.getSingletonEdgeUsages().size());

		assertEquals(13, ct.getRewrittenGraphPortCount());
		assertEquals(291.22, ct.getModelDL(), 0.01);
		assertEquals(446.34, ct.getEncodedGraphLength(), 0.01);
		assertEquals(737.56, ct.getModelDL() + ct.getEncodedGraphLength(), 0.01);
	}

	@Test
	public void testRewrittenGraphIsAlwaysCreated()
	{
		final Graph data = ExampleGraphFactory.createMultiDirected();
		final MultiDirectedStandardTable st = new MultiDirectedStandardTable(data);
		CodeTable ct = new MultiDirectedCodeTable(st, CodeTableHeuristics.PartialOrder.noOrder);

		// Even if we do not ask to create a rewritten graph, one is created.
		// This is because a rewritten graph is needed to compute the description length,
		// so we accept this behaviour and this tests documents it.
		ct.apply(data, false);
		assertTrue(ct.getRewrittenGraph().isPresent());

	}

	@Test
	public void TestOnPaper1()
	{
		final Graph data = ExampleGraphFactory.createMultiDirected();
		final MultiDirectedStandardTable st = new MultiDirectedStandardTable(data);

		CodeTable ct = new MultiDirectedCodeTable(st, CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDesc));
		Graph[] patterns = {
				ExamplePatternsFactory.createMultiDirectedNearMonumentsOfHeight123LocatedInBlank(),
				ExamplePatternsFactory.createMultiDirectedBlankWithStringNameBornDiedInCity(),
				ExamplePatternsFactory.createMultiDirectedBookHasAuthorPerson(),
		};
		Arrays.stream(patterns)
				.map(pattern -> new CodeTableRow(
						PatternInfo.createComputingSaturatedAutomorphisms(pattern),
						st.encode(pattern),
						0,
						Utils.graphLabelCount(pattern)
				))
				.forEach(ct::addRow);
		ct.apply(data, false);


		assertEquals(1, ct.getRows().get(0).getUsage());
		assertEquals(2, ct.getRows().get(1).getUsage());
		assertEquals(4, ct.getRows().get(2).getUsage());
		assertEquals(0, ct.getSingletonEdgeUsages().size());
		assertEquals(2, ct.getSingletonVertexUsages().size());
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("Value:Alice"));
		assertEquals(1, (int) ct.getSingletonVertexUsages().get("Value:Bob"));

		assertEquals(6, ct.getRewrittenGraphPortCount());
		assertEquals(232.93, ct.getModelDL(), 0.02);
		assertEquals(108.92, ct.getEncodedGraphLength(), 0.01);
		assertEquals(341.87, ct.getModelDL() + ct.getEncodedGraphLength(), 0.01);
	}
}
