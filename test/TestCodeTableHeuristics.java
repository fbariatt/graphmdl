import graph.Graph;
import graph.automorphisms.certificates.GraphCertificate;
import graphMDL.*;
import graphMDL.simpleGraphs.directed.SimpleDirectedCodeTable;
import graphMDL.simpleGraphs.directed.SimpleDirectedIndependentVertexAndEdgeST;
import org.junit.Before;
import org.junit.Test;
import patternMatching.RDFGraphMatcher;
import utils.GeneralUtils;

import java.util.Comparator;
import java.util.stream.Stream;

public class TestCodeTableHeuristics
{

	private Graph data;
	private SimpleDirectedIndependentVertexAndEdgeST st;

	@Before
	public void setUp()
	{
		GeneralUtils.configureLogging();
		data = ExampleGraphFactory.createSimpleDirected();
		st = new SimpleDirectedIndependentVertexAndEdgeST(data);
	}

	private CodeTable createCT(Comparator<CodeTableRow> heuristic)
	{
		CodeTable ct = new SimpleDirectedCodeTable(st, heuristic);
		ct.addRow(createCTRow(ExamplePatternsFactory.createSimpleDirectedBlankBZ()));
		ct.addRow(createCTRow(ExamplePatternsFactory.createSimpleDirectedXYZ()));
		ct.addRow(createCTRow(ExamplePatternsFactory.createSimpleDirectedXABlank()));
		return ct;
	}

	private CodeTableRow createCTRow(Graph pattern)
	{
		return new CodeTableRow(
				PatternInfo.createComputingSaturatedAutomorphisms(pattern),
				st.encode(pattern),
				new RDFGraphMatcher(data).imageBasedSupport(pattern),
				Utils.graphLabelCount(pattern)
		);
	}

	private GraphCertificate getCanonicalName(Graph g)
	{
		return PatternInfo.createComputingSaturatedAutomorphisms(g).getAutomorphismInfo().getCanonicalName();
	}

	@Test
	public void testLexicographicAsc()
	{
		CodeTable ct = createCT(CodeTableHeuristics.TotalOrder.lexicographicAsc);
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);

		// If we remove a row and re-add it, the order stays the same
		CodeTableRow row1 = ct.getRows().get(1);
		ct.removeRow(row1);
		ct.addRow(row1);
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);
	}

	@Test
	public void testVertexCountDesc()
	{
		CodeTable ct = createCT(CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.vertexCountDesc));
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);

		// If we remove a row and re-add it, the order stays the same
		CodeTableRow row1 = ct.getRows().get(1);
		ct.removeRow(row1);
		ct.addRow(row1);
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);
	}

	@Test
	public void testImageBasedSupportDesc()
	{
		CodeTable ct = createCT(CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.imageBasedSupportDesc));
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);

		// If we remove a row and re-add it, the order stays the same
		CodeTableRow row1 = ct.getRows().get(1);
		ct.removeRow(row1);
		ct.addRow(row1);
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);
	}

	@Test
	public void testLabelCountDesc()
	{
		CodeTable ct = createCT(CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDesc));
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);

		// If we remove a row and re-add it, the order stays the same
		CodeTableRow row1 = ct.getRows().get(1);
		ct.removeRow(row1);
		ct.addRow(row1);
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);
	}

	@Test
	public void testNoOrder()
	{
		CodeTable ct = createCT(CodeTableHeuristics.PartialOrder.noOrder);
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);

		// If we remove a row and re-add it, the order is *not* preserved, and it is instead added at the end
		CodeTableRow row0 = ct.getRows().get(0);
		ct.removeRow(row0);
		ct.addRow(row0);
		TestingUtils.assertStreamEquals(
				Stream.of(getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXYZ()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedXABlank()), getCanonicalName(ExamplePatternsFactory.createSimpleDirectedBlankBZ())),
				ct.getRows().stream().map(row -> row.getAutomorphisms().getCanonicalName())
		);
	}
}
