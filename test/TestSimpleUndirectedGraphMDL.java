import graph.Graph;
import graphMDL.*;
import graphMDL.simpleGraphs.undirected.SimpleUndirectedKrimp;
import org.junit.Test;
import utils.GeneralUtils;

import java.util.Arrays;
import java.util.function.Supplier;

import static org.junit.Assert.assertEquals;

public class TestSimpleUndirectedGraphMDL
{
	/**
	 * A simple test based on the results of the program at a given time. Added so that we know if something is changed by a refactor or modification.
	 * - Initially based on the results of the program at commit 96dde66, before adding the possibility of directed GraphMDL.
	 * - Description lengths changed when we switched from Elias Gamma encoding to Elias Delta encoding as universal integer encoding,
	 * on the commit just after 2f7c3a1.
	 * - Description lengths changed again when, on commit just after e1c486a, we decided that prefix codes in the CT should be encoded with a kind of "universal encoding", meaning that the DL
	 * of a code c is now 2*c+1.
	 * - Added test a second case where pruning is disabled (done while implementing pruning for GraphMDL+ which extracted pruning code from the Krimp/GraphMDL class). Verified
	 * on commit f558a4e.
	 * - Description lengths changed when, on commit just after 61d876e, we decided that using the "universal encoding" of prefix codes was not a good idea.
	 */
	@Test
	public void simpleTest()
	{
		GeneralUtils.configureLogging();

		Graph data = ExampleGraphFactory.createSimpleUndirected();
		Graph[] candidates = {ExamplePatternsFactory.createSimpleUndirectedXYZ(), ExamplePatternsFactory.createSimpleUndirectedXABlank(), ExamplePatternsFactory.createSimpleUndirectedBlankBZ()};

		Supplier<Krimp> krimpSupplier = () -> new SimpleUndirectedKrimp(
				data,
				Arrays.stream(candidates),
				CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.imageBasedSupportDescLabelCountDesc),
				CodeTableHeuristics.TotalOrder.arbitrarilyBreakEqualities(CodeTableHeuristics.PartialOrder.labelCountDescImageBasedSupportDesc),
				true
		);


		// With pruning:
		{
			Krimp krimp = krimpSupplier.get();
			CodeTable codeTable = krimp.computeBestCodeTableWithPruning(true);

			assertEquals(1, codeTable.getRows().size());
			assertEquals(
					PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleUndirectedXYZ()).getAutomorphismInfo().getCanonicalName(),
					codeTable.getRows().get(0).getAutomorphisms().getCanonicalName()
			);
			assertEquals(3, codeTable.getRows().get(0).getPatternGraph().getVertexCount());
			assertEquals(83.366, codeTable.getModelDL(), 0.001);
			assertEquals(42.02, codeTable.getEncodedGraphLength(), 0.001);
		}
		// Without pruning:
		{

			Krimp krimp = krimpSupplier.get();
			CodeTable codeTable = krimp.computeBestCodeTableWithoutPruning(true);

			assertEquals(2, codeTable.getRows().size());
			assertEquals(
					PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleUndirectedXYZ()).getAutomorphismInfo().getCanonicalName(),
					codeTable.getRows().get(0).getAutomorphisms().getCanonicalName()
			);
			assertEquals(
					PatternInfo.createComputingSaturatedAutomorphisms(ExamplePatternsFactory.createSimpleUndirectedXABlank()).getAutomorphismInfo().getCanonicalName(),
					codeTable.getRows().get(1).getAutomorphisms().getCanonicalName()
			);
			assertEquals(87.536, codeTable.getModelDL(), 0.001);
			assertEquals(42.02, codeTable.getEncodedGraphLength(), 0.001);
		}

	}
}
