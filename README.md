# GraphMDL, GraphMDL+ and KG-MDL
This repository contains the source code of the GraphMDL, GraphMDL+, and KG-MDL tools for graph pattern mining based on minimum description length (MDL).
See bibliography section at the bottom of this file for publications explaining the approaches.

The code in this project is licensed under the Gnu General Public License version 3. Copyright (C) 2019-2022 Francesco Bariatti, Peggy Cellier, Sébastien Ferré.
You can find a copy of the license as `LICENSE.txt`.

## Requirements
The project now uses the [gradle build tool](https://gradle.org/) to manage dependencies and builds.

- Only java 11 or higher is required in order to compiler and/or execute the project.

### Dependencies
The project depends on the following libraries, which are managed by gradle (thus they are not provided in this repository):

- [Apache Jena](https://jena.apache.org/index.html): used for computing graph embeddings and for handling RDF data.
- [Argparse4j](https://argparse4j.github.io/index.html): used for parsing command-line arguments.
- [org.json](https://github.com/stleary/JSON-java): used for JSON serialization.
- [Apache log4j](https://logging.apache.org/): used for managing the program output.
- [Junit4](https://junit.org/junit4/): used for unit tests.

## Building
In the main directory of the project, execute `./gradlew build` (Linux) or `gradlew.bat build` (Windows).  
This will download all dependencies, compile and test the project, and create an executable jar `build/libs/GraphMDL-full.jar`

A pre-compiled executable jar `GraphMDL.jar` is also available in this repository.

## Usage
Run the project's jar file, such as `java -jar GraphMDL.jar`.

Adding a `-h` argument to the program makes it print a help message listing the possible commands and needed parameters.  
Help can also be shown for each available sub-command.

The main command is `graphMDL+`, which runs either the GraphMDL+ or the KG-MDL algorithm (depending on options) to 
generate and select a set of graph patterns that minimize description length on a data graph.  
The `classify` command is also available, which uses GraphMDL+ results to perform a classification task, typically used
when evaluating the approach.

GraphMDL, GraphMDL+ and KG-MDL produce as output a JSON file that can be used in conjunction with the [GraphMDL Visualizer](https://gitlab.inria.fr/fbariatt/graphmdl-visualizer) tool for a better visualization and interpretation of the results.

### Input file format
When the program needs to receive some graphs as input, it expects a text file containing the graph structure(s).
Graph files can contain one or multiple graphs, but some commands only expect one graph (when a method can accept multiple graphs, it is stated in the help text).

The graph file format is the following (similar to gSpan format):

- Lines starting with `#` are comments and are ignored
- Every graph must start with a line containing `t`
- A vertex is declared `v id labels...`
	- Id can be anything as long as each id is unique in the graph, and it does not contain spaces
	- Labels is any number of space-separated strings
- An edge is declared `e id id label`
	- Ids must be of already declared vertices
	- An edge can only have one label
	- When undirected graphs are expected, edges must be doubled: they must be defined once in each direction.
	- For algorithms accepting multigraphs, multiple edges can be declared between the same pair of vertices (using multiple `e` lines).

**Knowledge Graphs** can be converted from usual formats (RDF/XML, Turtle, NTriple) into the textual format specified above with help of the `convert` command.

## Changelog
- Current version, January 2023:
  - Complete set of automorphisms of each pattern no longer computed: avoid memory explosion for complex patterns with many combinations. 
  - General improvements and added features.
- Commit 0b586d2, August 2022:
  - Made conversion between RDF and text formats reversible (in particular, patterns can now be easily converted to SPARQL queries).
  - Using new description length computations for the KG-MDL algorithm (in particular, using prequential codes).
  - Updated dependencies to newer versions.
  - General refactoring, improvements, and bug fixes.
- Commit e56e51e, March 2022:
	- Added Multigraph and Knowledge Graph support with the KG-MDL algorithm used in [Thesis]. Note however that this algorithm may change in the future, as it is still in development.
	- The project now uses  the [gradle build tool](https://gradle.org/) to manage dependencies and builds.
	- Using Elias delta encoding instead of gamma encoding for computing the description length of integers. Note that this changes the description lengths, which will not be exactly the same as the ones published in previous papers.
	- Switched from java 8 to java 11. Note that this change could potentially introduce slight differences in results due to minor differences in non-deterministic operations between the two java versions.
	- Implemented GraphMDL+ pruning
	- Added an alternative method for computing only the embeddings used for covering the data graph instead of computing all embeddings of all patterns (which was a problem for patterns with a very large number of embeddings).
		- This method also allows to set a maximum time allowed for each pattern, to avoid problematic patterns slowing down the whole computation.
	- Added some more utility commands to perform graph-related manipulations
	- A lot of reworking and refactoring of the code, making it cleaner and more modular (or maybe introducing new bugs, who knows?).
- Dec 2020:
	- Added source code of GraphMDL+ tool as used for the experimental evaluation of [SAC2021] (commit 626430c).
	- Added results of experimental evaluation for [SAC2021].
	- Added some unit testing.
	- Added new version of source code, introducing some small new features and some refactoring.
- Jun 2020:
	- Added JSON serialization
	- Added automorphism detection based on T. Junttila and P. Kaski, “Engineering an Efficient Canonical Labeling Tool for Large and Sparse Graphs,” in 2007 Proceedings of the Ninth Workshop on Algorithm Engineering and Experiments (ALENEX), 2007.
- Commit 3c536e1, Nov 2019: version used for experimental evaluation of [EGC2020] and [IDA2020].


## Experimental data
In the `data/` folder you can find the datasets that we used in the experiments of published papers (see bibliography below).

### SAC 2021 paper experimental evaluation
The datasets used for the experimental evaluation of the [SAC2021] paper can be found in the `data/source_data-IDA2020-SAC2021` folder.

In the `data/SAC2021-experimental_results.zip` archive you can find the GraphMDL+ result files generated with a maximum runtime of 4h (only the AIDS-CM file reached this limit, the other experiments terminated before).
They can be visualised with the [GraphMDL Visualizer](https://gitlab.inria.fr/fbariatt/graphmdl-visualizer) tool.

### IDA 2020 paper experimental evaluation
The datasets used for the experimental evaluation of the [IDA2020] paper can be found in the `data/source_data-IDA2020-SAC2021` folder.

In the `data/IDA2020-rewritten_graphs.tar.gz` archive you can find a visualisation of the patterns selected by GraphMDL and the rewritten graphs that they yield on the AIDS-CA, AIDS-CM and UD-PUD-En datasets. The visualisations are encoded as [graphviz](https://graphviz.gitlab.io/) files. You will need to install graphviz and run `fdp -T svg -O -Goverlap=false filename` or a similar command.


## Bibliography
The approaches in this repository have been used to produce the following papers:

- [Thesis] Francesco Bariatti. "Mining Tractable Sets of Graph Patterns with the Minimum Description Length Principle", PhD thesis, Université de Rennes 1, 2021. [https://hal.inria.fr/tel-03523742](https://hal.inria.fr/tel-03523742)
- [SAC2021] Francesco Bariatti, Peggy Cellier, and Sébastien Ferré. "GraphMDL+: Interleaving the Generation and MDL-based Selection of Graph Patterns." 36th ACM/SIGAPP Symposium on Applied Computing (SAC ’21). 2021. [DOI: 10.1145/3412841.3441917](https://doi.org/10.1145/3412841.3441917)
- [IDA2020] Francesco Bariatti, Peggy Cellier, and Sébastien Ferré. "GraphMDL: Graph Pattern Selection based on Minimum Description Length." International Symposium on Intelligent Data Analysis. Springer, Cham, 2020. [DOI: 10.1007/978-3-030-44584-3_5](https://doi.org/10.1007/978-3-030-44584-3_5)
- [EGC2020] Francesco Bariatti, Peggy Cellier, and Sébastien Ferré. "GraphMDL: sélection de motifs de graphes avec le principe MDL." EGC 2020. 2020.

